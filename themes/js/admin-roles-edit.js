jQuery(document).ready(function ($) {
    var rolePermissions = {
		"3": "module-cms",
        "7": "module-administrator",
		"15": "module-banner",
        "19": "module-services",
		"24": "module-post",
		"28": "module-gallery",
		"32": "module-rates",
		"36": "module-teacher",
		"41": "module-schedule",
		"45": "module-class",
		"49": "email-settings",
    };

    $.each(rolePermissions, function (key, value) {
		$("#" + value).click(function(){
			$("." + value).prop("checked",$("#" + value).prop("checked"))
		})
    });
});