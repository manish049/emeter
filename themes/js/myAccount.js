$(function() {

//change password form validation
$("#btnChangePass").click(function(){

   $("#changePass-form").validate({
        
		errorElement: 'span',
		
	 	rules: {
        	current_pass:{
                     required: true,
					 rangelength:[6,8]
                     },
		    new_pass:{
                    required: true,
					rangelength:[6,8]
					    },
		    re_pass: {
                    required: true,
					equalTo:"#new_pass"
				   }
			    },
				
		messages:{
             current_pass: {
	                  required: en_lang.text_required,
					  rangelength: en_lang.text_range_invalid
	                        },
			 new_pass: {
	                  required: en_lang.text_required,
					  rangelength: en_lang.text_range_invalid
	                        },		
			 re_pass: {
	                  required: en_lang.text_required,
					  equalTo: en_lang.text_equalTo
	                   }	
			     },
		    submitHandler: function(form) 
			  {
            //form.submit();
			   jQuery.ajax({
						  type: "POST",
						  url: url_changePass,
						  datatype: 'html',
						  data: $('form#changePass-form').serialize(),
						  success: function (data)
						           {
						        
								 	  if(data != 'success')
									  {
									  $("#erorMsgpass").show();
					                  $("#pop-mesg-pass").html("<p class='text-danger'><strong>"+ warning +" </strong> "+ change_password_error +"</p>");
									  return false;
									  }
									  else{
									  
									  $('#erorMsgpass').replaceWith("<div class='bg-success' style='display:block;'><p class='text-success'><strong>"+ success +" </strong> "+ password_change_success +"</p></div>");
								
								  window.location.href = url_login;
										  }
								
									  }
                                  });
                                  return false; // required to block normal submit since ajax is used
			
              }
    });
   }); //change password form validation ends here
   
//change telephone pin form validation
$("#btnChangePhone").click(function(){
	$("#changePhone-form").validate({

    errorElement: 'span',

    rules: {
        /*old_phone:{
			 required: true,
			 phone:true
			 },*/
        new_phone: {
            required: true,
            phone: true
        },
        /*re_phone: {
			required: true,
			equalTo:"#new_phone"
		   }*/
    },
    messages: {
        /*old_phone: {
			  required: en_lang.text_required,
			  phone: en_lang.text_phone_invalid
					},*/
        new_phone: {
            required: en_lang.text_required,
            phone: en_lang.text_phone_invalid
        },
        /*re_phone: {
		  required: en_lang.text_required,
		  equalTo: en_lang.text_equalToPhone
		   }*/
    },
    submitHandler: function(form) {
        //form.submit();
        jQuery.ajax({
            type: "POST",
            url: url_changeTelephone,
            datatype: 'html',
            data: $('form#changePhone-form').serialize(),
            success: function(data) {
                if (data != 'success') {
                    $("#erorMsgphone").show();
                    $("#pop-mesg-phone").html("<p class='text-danger'><strong>" + warning + " </strong> " + change_telephone_error + "</p>");
                    return false;
                } else {
                    $('#erorMsgphone').replaceWith("<div class='bg-success' style='display:block;'><p class='text-success'><strong>" + success + " </strong> " + telephone_change_success + "</p></div>");
					
					console.log($('#new_phone').val());
					$('#userTelephone').html($('#new_phone').val());
					
					$('#changePhone-form').trigger('reset'); //reset form
					
                    setTimeout(function() {
                        $('#modal-change-telephone').modal('hide');
						$("#erorMsgphone").html('');
                    }, 3000);

                    // window.location.href = redirect_url;
                }
			}
        });
        return false; // required to block normal submit since ajax is used
	}
	});
	});   //change telephone form validation ends here
   
 
 //email change
 $("#btnChangeEmail").click(function(){

   $("#changeEmail-form").validate({
        
		errorElement: 'span',
		
	 	rules: {
        	email:{
                     required: true,
					 email:true
                     },
		    re_email:{
                    required: true,
					equalTo:"#email"
					    },
		       },
		messages:{
             email: {
	                  required: en_lang.text_required,
					  email: en_lang.text_invalid_email
	                        },
			 re_email: {
	                  required: en_lang.text_required,
					  equalTo: en_lang.text_equalToEmail
	                   }	
			     },
		    submitHandler: function(form) 
			  {
            //form.submit();
			   jQuery.ajax({
						  type: "POST",
						  url: url_changeEmail,
						  datatype: 'html',
						  data: $('form#changeEmail-form').serialize(),
						  success: function (data)

						           {
						        if(data != 'success')
									  {
									  $("#erorMsgEmail").show();
					                  $("#pop-mesg-email").html("<p class='text-danger'><strong>"+ warning +" </strong> "+ change_email_error +"</p>");
									  return false;
									  }
									  else{
									  
									  $('#erorMsgEmail').replaceWith("<div class='bg-success' style='display:block;'><p class='text-success'><strong>"+ success +" </strong> "+ email_change_success +"</p></div>");
									  
								   //document.changeShippingAddress-form.reset(); //reset form
                                   setTimeout(function () {
                                    $('#modal-change-email').modal('hide');
                                                     }, 4000);
									  
									  //window.location.href=redirect_url;
										  }
								
									  }
                                  });
                                  return false; // required to block normal submit since ajax is used
			
              }
    });
   });  //email change ends here
   
   
 
//Address Changing function
 $("#btnChangeAddress").click(function(){
 
   $("#changeAddress-form").validate({
        
		errorElement: 'span',
		
	 	rules: {
        	first_name:{
                     required: true,
					 rangelength:[4,20]
                     },
		    last_name:{
                    required: true,
					rangelength:[4,20]
					    },
		    address1:{
                    required: true
					  },
		    address2:{
                    required: true,
				     },
		    state:{
                    required: true,
				     },
		    city:{
                    required: true,
				     },
		    country:{
                    required: true,
				     }	   
		      },
			  
		messages: {
        	first_name:{
                     required: en_lang.text_required,
					 rangelength: en_lang.text_nameRange
                     },
		    last_name:{
                    required: en_lang.text_required,
					rangelength: en_lang.text_nameRange
					    },
		    address1:{
                    required: en_lang.text_required
					  },
		    address2:{
                    required: en_lang.text_required,
				     },
		    state:{
                    required: en_lang.text_required,
				     },
		    city:{
                    required: en_lang.text_required,
				     },
		    country:{
                    required: en_lang.text_required,
				     }	   
		      },
		    submitHandler: function(form) {
              jQuery.ajax({
				  type: "POST",
				  url: url_changeAddress,
				  datatype: 'html',
				  data: $('form#changeAddress-form').serialize(),
				  success: function (data){
					 
					if(data != 'success'){
					  	$("#erorMsgChangeAdd").show();
					  	$("#pop-mesg-changeadd").html("<p class='text-danger'><strong>"+ warning +" </strong> "+ change_address_error +"</p>");
					  	return false;
					}
					else{  
					  $('#erorMsgChangeAdd').replaceWith("<div class='bg-success' style='display:block;'><p class='text-success'><strong>"+ success +" </strong> "+ address_change_success +"</p></div>");
					 
					 window.location.href = redirect_url;
					}
				}
			});
            return false; // required to block normal submit since ajax is used
		}
    });
   }); //address change function ends here
 
    
   });
   
 $.validator.addMethod("phone", function (value, element) {
	    return this.optional(element) || /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/i.test(value);
	}, "Invalid phone number");   