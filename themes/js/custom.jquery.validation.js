$(document).ready(function() {
	
	$("#contactForm").validate({
		submitHandler : function (form) {
			form.submit();
				console.log('asdfasdjfl');
		},
		errorClass : 'error',
		errorElement : 'span',
		rules : {
			full_name : {
				required : true
			},
			email : {
				required : true,
				email : true
			},
			message : {
				required : true
			}
		},
		messages : {
			full_name : {
				required : "First Name field is required."
			},
			email : {
				required : "Email field is required",
				email : "Email should be valid."
			},
			message : {
				required : "Message field is required"
			}
		},
		errorPlacement: function (error, element) {
            error.insertAfter(element);
        }
	});
	
	
	}); // end of ready event