<html>
	<head>
		<title>Emeter Api- Pay Reward</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" type="text/javascript"></script>
	</head>
	<body>
		<div class="container">
			<h3>Reward List</h3>		
			<table class="table table-striped table-bordered table-condensed">
				<thead>
					<tr>
						<th>S.N.</th>
						<th>User Id</th>						
						<th>User Name</th>
						<!-- <th>Event</th> -->
						<th>Total Reward</th>
						<th>Paid Reward</th>
						<th>Remaining Reward</th>
						<th>Operation</th>
					</tr>
				</thead>
				<tbody>
				<?php if($rewards): ?>
					<?php $iterator = 0; ?>
					<?php foreach ($rewards as $reward): ?>					
					
						<tr>
							<td><?php  echo ++$iterator; ?></td>
							<td><?php  echo $reward['participant_id']; ?></td>
							<td><?php  echo $reward['user_email']; ?></td>
							<!-- <td><?php  //echo $reward['eventname']; ?></td> -->
							<td><?php  echo $reward['total_balance']; ?></td>
							<td><?php  echo $reward['paid_balance']; ?></td>
							<td><?php  echo $reward['remaining_balance']; ?></td>
							<td>
								<?php if($reward['remaining_balance'] > 50): ?>
									<a href="<?php echo base_url(). 'reward/pay_reward/'.$reward['ub_id']; ?>" >Pay</a>
								<?php else: ?>
									<a href="#" >Pay</a>
								<?php endif; ?>
								|
								<form action="<?php echo base_url(). 'reward/reward_detail/'; ?>" method="post" style="display:inline-block;">
									<input type="hidden" name="participant_id" value="<?php echo $reward['participant_id']; ?>">
									<input type="hidden" name="user_email" value="<?php echo $reward['user_email']; ?>">
									<input type="submit" value="Details" style="background:none; border:none; color:337AB7;">
								</form>
								<!-- <a href="<?php //echo base_url(). 'reward/reward_detail/'.$reward['participant_id'].'/'. urlencode($reward['user_email']); ?>" >Details</a> -->
							</td>
						</tr>
					<?php endforeach; ?>
				<?php else: ?>
					No Reward Record found
				<?php endif; ?>
				</tbody>
			</table>
		</div>
	</body>
</html>