<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $this->page_title;?></title>
<link type="text/css" rel="stylesheet" href="<?php echo base_url().CSS_DIR; ?>bootstrap.min.css">
<link type="text/css" rel="stylesheet" href="<?php echo base_url().CSS_DIR; ?>style.css">

<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
</head>
<body>
<div class="wrapper">
	<header>
 		<div class="container">
  			<div class="center">
  
  				<div class="row  logo">
    				<a href="<?php echo base_url();?>"><img src="<?php echo base_url().IMG_DIR; ?>logo.png" /></a>
      			</div>
				<div id="product-sec">
					<h1><?php if(isset($maintainance_msg->site_status))echo strtoupper(strtolower($maintainance_msg->site_status));?></h1>
					<div class="probg-area">
						<div class="abt-secbg">
							<br />
							<?php if(isset($maintainance_msg->heading))echo $maintainance_msg->heading;?>
							<?php if(isset($maintainance_msg->content))echo $maintainance_msg->content;?>
						</div>
					</div>
				</div>
	    	</div>
	 	</div>   
	<div class="clearfix"></div>
	</header> 
</div>
</body>
</html>
