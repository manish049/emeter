<html>
	<head>
		<title>Emeter Api- Pay Reward</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" type="text/javascript"></script>
	</head>
	<body>
		<div class="container">
			<h3>Reward Detail : <?php echo $participant_id.'/'. $user_email; ?></h3>
			<table class="table table-striped table-bordered table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>User Id</th>						
						<th>Name</th>
						<th>Email</th>
						<th>Event</th>
						<th>Reward</th>
						<th>Rewarded on</th>
						<th>Reward Info</th>
						
					</tr>
				</thead>
				<tbody>
				<?php if($reward_details): ?>
					<?php $iterator = 0; ?>
					<?php foreach ($reward_details as $reward_detail): ?>					
					
						<tr>
							<td><?php  echo ++$iterator; ?></td>
							<td><?php  echo $reward_detail['participantid']; ?></td>
							<td><?php  echo $reward_detail['name']; ?></td>							
							<td><?php  echo $reward_detail['emailid']; ?></td>
							<td><?php  echo $reward_detail['eventname']; ?></td>
							<td><?php  echo $reward_detail['amount']. ' '. $reward_detail['reward_unit']; ?></td>
							<td><?php  echo $reward_detail['rewarded_date']; ?></td>
							<td><?php  echo $reward_detail['reward_type']; ?></td>
						</tr>
					<?php endforeach; ?>
				<?php else: ?>
					No Reward Record found
				<?php endif; ?>
				</tbody>
			</table>
		</div>
	</body>
</html>