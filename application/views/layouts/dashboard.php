<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php /*?><link rel="shortcut icon" href="<?php echo base_url().IMG_DIR; ?>fav.png"><?php */?>
<title><?php echo $template['title']; ?></title>
<link href="<?php echo base_url().CSS_DIR; ?>reset.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url().CSS_DIR; ?>admin.css" rel="stylesheet" type="text/css">

<script src="<?php echo base_url().JS_DIR; ?>jquery.min.js" type="text/javascript"></script>

<script src="<?php echo base_url().JS_DIR; ?>DD_roundies_0.0.2a.js" type="text/javascript"></script>
<script src="<?php echo base_url().JS_DIR; ?>jquery.validate.min.js" type="text/javascript"></script>

<!--script and style for tabber-->
<script src="<?php echo base_url().JS_DIR; ?>tabcontent.js" type="text/javascript"></script>
<link href="<?php echo base_url().CSS_DIR; ?>tabcontent.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
var siteurl='<?php echo base_url();?>';
var baseUrl='<?=base_url()?>';
DD_roundies.addRule('#box', '6px', true);
DD_roundies.addRule('#box h1', '6px 6px 0 0', true);
DD_roundies.addRule('.btn', '4px', true);
</script>
<script src="<?php echo base_url().JS_DIR; ?>timer.js" type="text/javascript"></script>

<script type="text/javascript" charset="utf-8">
function ConfirmDelete(itemname) {
    var reconfirm = confirm("Are you sure you want to Delete this " + itemname);
    if (reconfirm) {
        return true;
    } else {
        return false;
    }
}
</script>
<script src="<?php echo base_url().JS_DIR;?>jquery.tablesorter.js" type="text/javascript" charset="utf-8"></script>
</head>

<body>
<header id="mainhead">
  <div class="hd_sec">
    <h1>
        <a href="<?php echo base_url().ADMIN_DASHBOARD_PATH; ?>">
        	<img src="<?php echo  base_url().IMG_DIR.'/logo.png'; ?>" alt="<?php echo WEBSITE_NAME; ?>"><span>admin panel</span>
       	</a>
            
		<?php echo $this->general->date_formate($this->general->get_local_time('time'));?>
      
      <!-- <div id="clock" class="clock"></div> -->
    </h1>
  </div>
  <style>
  
  </style>
  <nav id="main_nav">
    <ul class="navi">
      <li class="home" <?php if($this->uri->segment(2) == 'dashboard'){ echo 'current'; }?>><a href="<?php echo site_url(ADMIN_DASHBOARD_PATH.'/dashboard/index')?>"><span class="hm">Dashboard</span></a></li>

        <!-- <li <?php if($this->uri->segment(2) == 'site-settings' OR $this->uri->segment(2) == 'seo' OR $this->uri->segment(2) == 'site-logs'){ echo 'class="current"'; }?>>
        	<a href="<?php echo site_url(ADMIN_DASHBOARD_PATH.'/site-settings/index')?>"><span>System Settings</span></a>
        	<ul>
            	<li><a href="<?php echo site_url(ADMIN_DASHBOARD_PATH.'/site-settings/index')?>">Site Settings</a></li>
            	<?php if(LOG_ADMIN_ACTIVITY=='Y' && LOG_ADMIN_INVALID_LOGIN=='Y'){ 	?>
                <li><a href="<?php echo site_url(ADMIN_DASHBOARD_PATH.'/site-logs/index')?>">Site Logs</a></li>
            	<?php } ?>
        	</ul>	
        </li>
 -->
        <li <?php if($this->uri->segment(2) == 'reward'){ echo 'class="current"'; }?>>
            <a href="<?php echo site_url(ADMIN_DASHBOARD_PATH.'/reward/index')?>"><span>Reward Management</span></a>
        </li>
    </ul>
    <ul class="navi profile">
      <li>Hello, <b>
        <?php echo $this->session->userdata(ADMIN_USER_NAME); ?>
        </b></li>
      <li><a href="#"><span></span></a>
        <ul>
          <li><a href="<?php echo site_url(ADMIN_DASHBOARD_PATH.'/change-password/index')?>">Change Password</a></li>
          <!-- <li><a href="<?php echo site_url(ADMIN_DASHBOARD_PATH.'/administrator/index')?>">Manage Administrator</a></li> -->
          <li><a href="<?php echo site_url(ADMIN_DASHBOARD_PATH.'/logout');?>">logout</a></li>
        </ul>
      </li>
    </ul>
    <div class="clearfix"></div>
  </nav>

</header>
<div id="container"> <?php echo $template['body']; ?> </div>
<footer id="ftr_sec">
  <p> &copy; <?php echo date('Y'); ?> <?php echo WEBSITE_NAME; ?>. All rights reserved. </p>
</footer>
</body>
</html>