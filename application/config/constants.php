<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

define('GOOGLE_API_KEY', 'AIzaSyDH9Y4UrWCiu_X8-FuQLG1R3MgobA8Xdis');


/* End of file constants.php */
/* Location: ./application/config/constants.php */


//admin login session
define('ADMIN_LOGIN_ID',				'admin_user_id');
define('ADMIN_USER_NAME',				'admin_user_name');
define('ADMIN_USER_TYPE',				'admin_user_type');

//admin & dashboard path
define('ADMIN_LOGIN_PATH',			'admin');
define('ADMIN_DASHBOARD_PATH',		'dashboard');
define('SESSION','EMETERTNEB');

define('THEMES_DIR', 'themes/');
define('CSS_DIR',THEMES_DIR.'css/');
define('JS_DIR',THEMES_DIR.'js/');

define('IMG_DIR',THEMES_DIR.'images/');

define('SMS_USERNAME', 'vidabesto');
define('SMS_PASSWORD', '12345678');
define('SENDER_ID', 'ALERTS');
