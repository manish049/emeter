<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_site_settings extends CI_Model 
{

	public function __construct() 
	{
		parent::__construct();
	}
	
	// field validation
	public $validate_site_settings =  array(

			array('field' => 'site_name', 'label' => 'Website Name', 'rules' => 'required'),
			array('field' => 'site_status', 'label' => 'Site Status', 'rules' => 'required'),
			
			// array('field' => 'contact_name', 'label' => 'Contact Name', 'rules' => 'required'),
			// array('field' => 'contact_email', 'label' => 'Contact Email', 'rules' => 'required|valid_email'),
			// array('field' => 'contact_number', 'label' => 'Contact Number', 'rules' => 'trim|required'),
			// array('field' => 'fax_number', 'label' => 'Fax Number', 'rules' => 'trim|required'),
			
			// array('field' => 'address', 'label' => 'Address', 'rules' => 'trim|required'),
			// array('field' => 'state', 'label' => 'State', 'rules' => 'trim|required'),
			// array('field' => 'city', 'label' => 'city', 'rules' => 'trim|required'),
			// array('field' => 'zip_code', 'label' => 'Zip', 'rules' => 'trim|required'),
			
			// array('field' => 'facebook_url', 'label' => 'Facebook Link', 'rules' => 'required|trim'),
			// array('field' => 'twitter_url', 'label' => 'Twitter Link', 'rules' => 'required|trim'),
			// array('field' => 'google_url', 'label' => 'Google Plus Link', 'rules' => 'required|trim'),
			
			// array('field' => 'google_map_url', 'label' => 'Google Plus Link', 'rules' => 'required|trim'),
			// array('field' => 'instagram_url', 'label' => 'Instagram Link', 'rules' => 'required|trim'),
			// array('field' => 'slogan', 'label' => 'Slogan', 'rules' => 'required|trim'),
			// array('field' => 'google_play_url', 'label' => 'Google Play Link', 'rules' => 'required|trim'),
			// array('field' => 'app_store_url', 'label' => 'IOs App Store Link', 'rules' => 'required|trim'),
		
			array('field' => 'sms_username', 'label' => 'SMS Api Password', 'rules' => 'trim|required'),
			array('field' => 'sms_password', 'label' => 'SMS Api Password', 'rules' => 'trim|required'),

		);
		
		
	public function file_settings_do_upload()
	{
		$config['upload_path'] = './'.WEBSITE_LOGO_PATH;//define in constants
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		$config['remove_spaces'] = TRUE;		
		//$config['max_size'] = '2000';
		$config['max_width'] = '300';
		$config['max_height'] = '100';

		// load upload library and set config				
		if(isset($_FILES['site_logo']['tmp_name']))
		{
			$this->upload->initialize($config);
			$this->upload->do_upload('site_logo');
		}		
	}
			
	public function get_site_setting()
	{		
		$query = $this->db->get('site_settings');

		if ($query->num_rows() > 0)
		{
		   return $query->row_array();
		} 

		return false;
	}
	
	
	public function update_site_settings($img_path)
	{
		$data = array(
			'site_name' => $this->input->post('site_name', TRUE),
			'site_status' => $this->input->post('site_status', TRUE),
			
			// 'contact_name' => $this->input->post('contact_name', TRUE),
			'contact_email' => $this->input->post('contact_email', TRUE),
			// 'contact_number' => $this->input->post('contact_number', TRUE),
			// 'fax_number' => $this->input->post('fax_number', TRUE),
			
			// 'address' => $this->input->post('address', TRUE),
			// 'state' => $this->input->post('state', TRUE),
			// 'city' => $this->input->post('city', TRUE),
			// 'zip_code' => $this->input->post('zip_code', TRUE),
			
			'log_admin_activity' => $this->input->post('log_admin_activity', TRUE),
			'log_admin_invalid_login' => $this->input->post('log_admin_invalid_login', TRUE),
			   
			'facebook_url' => $this->input->post('facebook_url', TRUE),
			// 'twitter_url' => $this->input->post('twitter_url', TRUE),
			// 'google_url' => $this->input->post('google_url', TRUE),
			'google_map_link' => $this->input->post('google_map_url', TRUE),
			'instagram_url' => $this->input->post('instagram_url', TRUE),

			// 'google_analytics_code' => $this->input->post('google_analytics_code'),
			'copy_right_text' => $this->input->post('copy_right_text',TRUE), 
			'slogan' => $this->input->post('slogan',TRUE), 
			'app_store_url' => $this->input->post('app_store_url', TRUE),
			'google_play_url' => $this->input->post('google_play_url', TRUE),
			
			'smtp_username' => $this->input->post('smtp_username', TRUE),

			'smtp_password' => $this->input->post('smtp_password', TRUE),

			// for sms api
			// 'sms_username' => $this->input->post('sms_username', TRUE);
			// 'sms_password' => $this->input->post('sms_password', TRUE);
			

       );
			
		
		/*if(isset($img_path) && $img_path !="")
		{
			@unlink('./'.$this->input->post('img_old'));
			$data['site_logo'] = $img_path;
		}*/

		$this->db->update('site_settings', $data); 
		//echo $this->db->last_query(); exit;
		
		return true;
	}
}
