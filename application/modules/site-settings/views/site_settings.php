<section class="title">
  <div class="wrap">
    <h2><a href="<?=site_url(ADMIN_DASHBOARD_PATH)?>">ADMIN</a> &raquo; Sitesetting  Management </h2>
  </div>
</section>
<article id="bodysec" class="sep">
  <div class="wrap">
    <section class="smfull">
      <div class="confrmmsg">
        <?php 
            if($this->session->flashdata('message')){
            echo "<p>".$this->session->flashdata('message')."</p>";
            }
        ?>
      </div>
      <div class="box_block">
        <form name="sitesetting" method="post" action="" enctype="multipart/form-data" accept-charset="utf-8">
          <fieldset>
            <div class="title_h3">App Settings</div>
            <ul class="frm">
              <li>
                <div>
                  <label>App Name<span>*</span> :</label>
                  <input type="text" name="site_name" class="inputtext" size=45 value="<?php echo set_value('site_name',$site_set['site_name']);?>">
                  <?=form_error('site_name')?>
                </div>
              </li>
              
             <!--  <li>
                <div>
                  <label>App Status</label>
                  <input name="site_status" type="radio" value="1" <?php echo set_radio('site_status','1',TRUE);?> />Online
                  <input name="site_status" type="radio" value="2" <?php if($site_set['site_status'] == '2'){ echo 'checked="checked"';} echo set_radio('site_status','2'); ?> />Offline
                  <input name="site_status" type="radio" value="3" <?php if($site_set['site_status'] == '3'){ echo 'checked="checked"';} echo set_radio('site_status','3'); ?>/>Maintainance
                </div>
              </li>  -->
            </ul>
          </fieldset>
        
          <fieldset>
            <div class="title_h3">Contact Settings</div>
            <ul class="frm">
            	<!-- <li><div>
                  <label>Contact Name<span>*</span> :</label>
                  <input type="text" name="contact_name" id="contact_name" value="<?php echo set_value('contact_name',$site_set['contact_name']);?>"  />
                  <?=form_error('contact_name')?>
                </div></li> -->
              
			  <li>
                <div>
                  <label>Contact Email<span>*</span> :</label>
                  <input name="contact_email" type="text" class="inputtext" value="<?php echo set_value('contact_email',$site_set['contact_email']);?>" size=45>
                  <?=form_error('contact_email')?>
                </div>
              </li>       
            </ul>
          </fieldset>
          
          <!-- 
          <fieldset>
            <div class="title_h3">Address Settings</div>
            <ul class="frm">
            	<li><div>
                  <label>Address<span>*</span> :</label>
                  <input type="text" name="address" id="address" value="<?php //echo set_value('address',$site_set['address']);?>"  />
                  <?=form_error('address')?>
                </div></li>
              
			  <li>
                <div>
                  <label>State<span>*</span> :</label>
                  <input name="state" type="text" class="inputtext" value="<?php //echo set_value('state',$site_set['state']);?>" size=45>
                  <?=form_error('state')?>
                </div>
              </li>
           
		       <li>
                <div>
                  <label>City<span>*</span> :</label>
                  <input name="city" type="text" class="inputtext" value="<?php //echo set_value('city',$site_set['city']);?>" size=45>
                  <?=form_error('city')?>
                </div>
              </li>
              
              <li>
                <div>
                  <label>Postal Code(Zip)<span>*</span> :</label>
                  <input name="zip_code" type="text" class="inputtext" value="<?php //echo set_value('zip_code',$site_set['zip_code']);?>" size=45>
                  <?=form_error('zip_code')?>
                </div>
              </li>
            </ul>
          </fieldset>
            -->
          
          <fieldset>
            <div class="title_h3">Site Logs</div>
            <ul class="frm">
            	<li>
                <div>
                  <label>Log Admin's Activity</label>
                  <input name="log_admin_activity" type="radio" value="N" checked="checked" />No
                  <input name="log_admin_activity" type="radio" value="Y" <?php if($site_set['log_admin_activity'] == 'Y'){ echo 'checked="checked"';}?> />Yes
                 </div>
              </li>  
              
              <li>
                <div>
                  <label>Log Admin's Invalid Login</label>
                  <input name="log_admin_invalid_login" type="radio" value="N" checked="checked" />No
                  <input name="log_admin_invalid_login" type="radio" value="Y" <?php if($site_set['log_admin_invalid_login'] == 'Y'){ echo 'checked="checked"';}?> />Yes
                 </div>
              </li>
		    </ul>
          </fieldset>

          <!-- <fieldset>
            <div class="title_h3">SMS Api Credentials</div>
            <ul class="frm">
              <li>
                <div>
                  <label>Username</label>
                  <input name="sms_username" type="text" value="<?php //echo set_value('sms_username',$site_set['sms_username']);?>" />
                 </div>
              </li>  
              
              <li>
                <div>
                  <label>Password</label>
                  <input name="sms_password" type="text" value="<?php //echo set_value('sms_password',$site_set['sms_password']);?>" />

                 </div>
              </li>
            </ul>
          </fieldset> -->
          
          
          <!-- <fieldset>
            <div class="title_h3">Find us</div>
            <ul class="frm"> -->
			        <!-- <li>
                <div>
                  <label>Facebook Link :</label>
                  <input name="facebook_url" type="text" class="inputtext" value="<?php echo set_value('facebook_url',$site_set['facebook_url']);?>" size=45>
                </div>
              </li> -->
              
			        <!-- <li>
                <div>
                  <label>Twitter Link :</label>
                  <input type="text" name="twitter_url" class="inputtext" size="45" value="<?php echo set_value('twitter_url',$site_set['twitter_url']);?>" />
                </div>
              </li> -->
              <!-- <li>
                <div>
                  <label>Google Plus Link :</label>
                  <input type="text" name="google_url" class="inputtext" size="45" value="<?php echo set_value('google_url',$site_set['google_url']);?>" />
                </div>
              </li> -->
              <!-- <li>
                <div>
                  <label>Instagram Link :</label>
                  <input type="text" name="instagram_url" class="inputtext" size="45" value="<?php echo set_value('instagram_url',$site_set['instagram_url']);?>" />
                </div>
              </li>
              <li>
                <div>
                  <label>Google Map Link :</label>
                  <input type="text" name="google_map_url" class="inputtext" size="45" value="<?php echo set_value('google_map_url',$site_set['google_map_link']);?>" />
                </div>
              </li>
              <li>
                <div>
                  <label>IOs APP Strore Link :</label>
                  <input name="app_store_url" type="text" class="inputtext" value="<?php echo set_value('app_store_url',$site_set['app_store_url']);?>" size=45>
                </div>
              </li>
               <li>
                <div>
                  <label>Google Play Link :</label>
                  <input name="google_play_url" type="text" class="inputtext" value="<?php echo set_value('google_play_url',$site_set['google_play_url']);?>" size=45>
                </div>
              </li> -->
          <!--   </ul>
          </fieldset>
           -->
          <!-- <fieldset>
            <div class="title_h3">SMTP Settings</div>
            <ul class="frm">              
              <li>
                <div>
                  <label>SMTP username<span>*</span> :</label>
                  <input name="smtp_username" type="text" class="inputtext" value="<?php echo set_value('smtp_username',$site_set['smtp_username']);?>" size=45>
                  <?=form_error('smtp_username')?>
                </div>
              </li>
              <li>
                <div>
                  <label>SMTP Password<span>*</span> :</label>
                  <input name="smtp_password" type="text" class="inputtext" value="<?php echo set_value('smtp_password',$site_set['smtp_password']);?>" size=45>
                  <?=form_error('smtp_password')?>
                </div>
              </li>        
            </ul>
          </fieldset> -->
          <fieldset>
            <!-- <div class="title_h3">Google analytics Code & Copy Right Text</div> -->
            <div class="title_h3">Slogan &amp; Copy Right Text</div>
            <ul class="frm">
<!--               <li>
                <div>
                  <label>Google Analytics Code :</label>
                  <textarea name="google_analytics_code" cols="34" rows="3" id="google_analytics_code"><?php //echo set_value('google_analytics_code',$site_set['google_analytics_code']);?></textarea>
                </div>
              </li> -->
              <!-- <li>
                <div>
                  <label>Slogan<span>*</span> :</label>
                  <textarea name="slogan" cols="34" rows="3" id="slogan"><?php echo set_value('slogan',$site_set['slogan']);?></textarea>
            <?=form_error('slogan')?>
                </div>
              </li> -->
              <li>
                <div>
                  <label>Copyright Text<span>*</span> :</label>
                  <textarea name="copy_right_text" cols="34" rows="3" id="copy_right_text"><?php echo set_value('copy_right_text',$site_set['copy_right_text']);?></textarea>
  					<?=form_error('copy_right_text')?>
                </div>
              </li>
            </ul>
          </fieldset>
          
          <fieldset class="btn">
            <input class="butn" type="submit" name="Submit" value="Update" />
          </fieldset>
        </form>
      </div>
    </section>
    <div class="clearfix"></div>
  </div>
</article>
<div> </div>
