<section class="title">
  <div class="wrap">
    <h2>Dashboard</h2>
  </div>
</section>

<article id="bodysec">
  <div class="wrap">
    <section class="dashboard">
       <div class="box_block">
        <h3>Site Statistics</h3>
       		<ul class="list">
          		<li>Total Channel Owners  <span><?php // echo $total_channel_owners;?></span></li>
                <li>Total Subscribers <span><?php // echo $total_subscribers; ?></span></li>
                <?php /*<li>Total Concurrent Viewers <span><?php echo $total_concurrent_viewers; ?></span></li> */?>
                <li>Total Video Upload <span><?php // echo $total_video_uploads; ?></span></li>
                <li>Total Revenue From Channel Owners <span><?php //echo $total_revenue_from_channel_owners; ?></span></li>
                <li>Total Revenue From Subscribers <span><?php // echo $total_revenue_from_subscribers;?></span></li>
        	</ul>
      </div>
      
      <div class="box_block">
        <h3>Channel Owners</h3>
		<?php /*if($channel_owners){?>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl_list">
                <thead>
                    <tr>
                        <th align="left">Username</th>
                        <th align="left">Email</th>
                        <th align="left">Registered Date</th>
                        <th align="left">Status</th>
                    </tr>
                  </thead>
                <tbody>
                <?php foreach($channel_owners as $channel_owner){ ?>
                    <tr>
                        <td align="left" >
                            <a href="<?php echo site_url(ADMIN_DASHBOARD_PATH.'/member/channel_owner/edit_channel_owner/'.$channel_owner->id); ?>"><?php echo $channel_owner->user_name;?></a>
                        </td>
                        <td align="left" ><?php echo $channel_owner->email;?></td>
                        <td align="left" ><?php echo date('D, M d, Y H:i A',strtotime($channel_owner->reg_date));?></td>
                        <td align="left" ><?php if($channel_owner->status=='1'){echo "Active";} else {echo "Inactive";}?></td>
                    </tr>
                    <?php } ?>
              </tbody>
            </table>
        <?php } else {?>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl_list">
                <tfoot>
                    <tr>
                        <td colspan="3" align="left">No any inactive channel owners found</td>
                    </tr>
                </tfoot>
            </table>
        <?php } */?>
      </div>
    </section>

    <section class="dashboard">
        <div class="box_block">
            <h3>Subscribers</h3>
            <?php /*if($subscribers){?>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl_list">
                    <thead>
                    <tr>
                        <th align="left">Username</th>
                        <th align="left">Email</th>
                        <th align="left">Registered Date</th>
                        <th align="left">Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($subscribers as $subscriber){ ?>
                        <tr>
                            <td align="left" >
                                <a href="<?php echo site_url(ADMIN_DASHBOARD_PATH.'/member/subscriber/edit_subscriber/'.$subscriber->id); ?>"><?php echo $subscriber->user_name;?></a>
                            </td>
                            <td align="left" ><?php echo $subscriber->email;?></td>
                            <td align="left" ><?php echo date('D, M d, Y H:i A',strtotime($subscriber->reg_date));?></td>
                            <td align="left" ><?php if($subscriber->status=='1'){echo "Active";} else {echo "Inactive";}?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            <?php } else {?>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl_list">
                    <tfoot>
                    <tr>
                        <td colspan="3" align="left">No any inactive subscribers found</td>
                    </tr>
                    </tfoot>
                </table>
            <?php }*/ ?>
        </div>
        <div class="box_block">
            <h3>Uploaded Videos</h3>
            <?php /*if($uploaded_videos){?>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl_list">
                    <thead>
                    <tr>
                        <th align="left">Uploaded By (Username)</th>
                        <th align="left">Type</th>
                        <th align="left">Size (KB)</th>
                        <th align="left">Upload Date</th>
                        <th align="left">Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($uploaded_videos as $video){ ?>
                        <tr>
                            <td align="left" ><?php echo $video->user_name;?></td>
                            <td align="left" ><?php if($video->video_type=='1'){echo 'Primary';} elseif($video->video_type=='2'){echo 'Commercial';} else{echo 'Demo';}?></td>
                            <td align="left" ><?php echo $video->video_size;?></td>
                            <td align="left" ><?php echo $video->upload_date;?></td>
                            <td align="left" ><?php if($video->status=='1'){echo "Pending";} else {echo "Approved";}?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            <?php } else {?>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl_list">
                    <tfoot>
                    <tr>
                        <td colspan="3" align="left">No any inactive videos found</td>
                    </tr>
                    </tfoot>
                </table>
            <?php }*/ ?>
        </div>
    	<?php /*?><div class="box_block">
        <h3>Revenue & Product Statistics</h3>
       		<ul class="list">
          		<li>Total Money Deposited From PayPayl: <span><?php if($payment_from_paypal->amount!=''){echo $payment_from_paypal->amount; } else {echo "0"; }?> <?php echo DEFAULT_CURRENCY_SIGN; ?></span></li>
                <li>Total Revenue collected from item Listing Price: <span><?php echo $total_revenue_from_item_listing;?> <?php echo DEFAULT_CURRENCY_SIGN; ?></span></li>
                <li></li>
                <li>Total Products Listed till date: <span><?php echo $total_total_listed_products;?></span></li>
                <li>Total Products Sold: <span><?php echo $total_sold_products;?></span></li>
                <li>Total Products Sold Cost: <span><?php if($total_product_sold_cost->totalcost!=''){echo $total_product_sold_cost->totalcost;} else {echo "0";}?> <?php echo DEFAULT_CURRENCY_SIGN; ?></span></li>  
        	</ul>
      </div><?php */?>
      
     	<?php /*?><div class="box_block">
        <h3>Recent Products</h3>
       	<?php if($recent_products){?>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl_list">
          <thead>
            <tr>
              <th align="left">Product Name</th>
              <th align="left">Seller ID</th>
              <th align="left">Seller Name</th>
              <th align="left">Added Date</th>
              <th align="left">Listing Start Date</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach($recent_products as $item){ ?>
            <tr>
              	<td align="left" >
              		<a href="<?php echo site_url(ADMIN_DASHBOARD_PATH.'/product/edit_product/'.$item->template_id.'/'.$item->product_id);?>"><?php echo $item->listing_title;?></a>
            	</td>
              	<td align="left" ><?php echo $item->seller_id;?></td>
              	<td align="left" ><?php echo $item->user_name;?></td>
              	<td align="left" ><?php echo $item->added_date;?></td>
              	<td align="left" ><?php echo $item->listing_start_date;?></td>
            </tr>
            <?php } ?>
          </tbody>
          </table>
          <?php } else {?>
          <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl_list">
          <tfoot>
            <tr>
              <td colspan="3" align="left">No any recent product/auction found</td>
            </tr>
          </tfoot>
          </table>
          <?php } ?>  
      </div><?php */?>       
    </section>
    <div class="clearfix"></div>
  </div>
</article>
<div> </div>
