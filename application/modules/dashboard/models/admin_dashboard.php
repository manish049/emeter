<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_dashboard extends CI_Model 
{

	public function __construct() 
	{
		parent::__construct();
		
	}
	
	
	function count_total_members($user_type)
	{
		$this->db->where('user_type',$user_type);
		$query = $this->db->get('members');
		//echo $this->db->last_query(); exit;
		if($query->num_rows()>0)
		{
			return $query->num_rows();
		}
		return 0;
	}
	
	
	function get_total_revenue_from_members($user_type)
	{
		$this->db->select('SUM( T.amount ) as total_amount');
		$this->db->from('transaction T');
        $this->db->join('members M','M.id=T.user_id','left');
		$this->db->where('transaction_status','Completed');
		$this->db->where('M.user_type', $user_type);
		
		$query = $this->db->get();
		//echo $this->db->last_query(); 
		
		if($query->num_rows() > 0)
		{
			$data = $query->row();
			return $data->total_amount;
		}
		return false;
	}


    public function count_total_concurrent_viewers()
    {
        //TODO count concurrent viewers
        return 'left to count';
    }

    public function count_total_video_uploads()
    {
         return $this->db->count_all('channel_owner_video');
    }

    public function get_recent_members($user_type)
    {
        $this->db->select('id, user_name, email, reg_date, status');
        $this->db->where('user_type',$user_type);

        $this->db->where("(status='1' || status='2')");

        //$this->db->order_by("status", "DESC");
        //$this->db->order_by("reg_date", "DESC");
        $this->db->order_by("status DESC, reg_date DESC");
        $this->db->limit(5);
        $query = $this->db->get('members');

        //echo $this->db->last_query();

        if($query->num_rows()>=1)
        {
            return $query->result();
        }
        return false;
    }

    public function get_recent_uploaded_videos()
    {
        $this->db->select('V.id, V.video_type, V.video_size, V.upload_date, V.status, M.user_name');
        $this->db->from('channel_owner_video V');
        $this->db->join('members M','M.id=V.user_id');
        $this->db->where("(V.status='1' || V.status='2')");

        //$this->db->order_by("status", "DESC");
        //$this->db->order_by("reg_date", "DESC");
        $this->db->order_by("V.status DESC, V.upload_date DESC");
        $this->db->limit(5);
        $query = $this->db->get();

        //echo $this->db->last_query();

        if($query->num_rows()>=1)
        {
            return $query->result();
        }
        return false;
    }

    public function get_recent_active_products()
	{
		$this->db->select('P.id as product_id, P.product_code, P.added_date, P.listing_start_date, P.duration, P.seller_id, L.id as template_id, L.listing_title, M.user_name');
		$this->db->from('products P');
		$this->db->join('listing_template L','P.template_id=L.id');
		$this->db->join('members M', 'L.seller_id=M.id');
		$this->db->where('P.status','2');
		$this->db->where('P.listing_payment_status','1');
		$this->db->order_by("P.last_update", "DESC");
		$this->db->limit(5);
		$query = $this->db->get();
		//echo $this->db->last_query();
		
		if($query->num_rows()>=1)
		{
			return $query->result();
		}
		return false;
	}

	
	function total_payment_by_payment_gateway($payment_method)
	{
		$this->db->select_sum('amount');
		$this->db->where('payment_method',$payment_method);
		$this->db->where('transaction_status','Completed');
		$query = $this->db->get('transaction');
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
		return false;
	}

	function total_cost_products_sold()
	{
		$this->db->select('SUM(product_cost) as totalcost');
		$this->db->from('product_order');
		$this->db->where('status !=',3);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			$data = $query->row();
			return $data;
		}
		return false;
	}
	
	//count total products listed till today
	function count_total_listed_products()
	{
		$this->db->select('id');
		$this->db->from('products');
		$this->db->where('status !=',1);
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			$data = $query->num_rows();
			return $data;
		}
		return 0;
	}
}
