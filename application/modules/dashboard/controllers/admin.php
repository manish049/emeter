<?php if ( !defined('BASEPATH') ) exit('No direct script access allowed');

    class Admin extends CI_Controller {

        function __construct()
        {
            parent::__construct();

            if ( !$this->general->admin_logged_in() ) {
                redirect(ADMIN_LOGIN_PATH, 'refresh');
                exit;
            }
            $this->load->model('admin_dashboard');
        }


        public function index()
        {
			 $this->data = '';
           /* $this->data['total_channel_owners']     = $this->admin_dashboard->count_total_members('3');
            $this->data['total_subscribers']        = $this->admin_dashboard->count_total_members('4');
            $this->data['total_concurrent_viewers'] = $this->admin_dashboard->count_total_concurrent_viewers();
            $this->data['total_video_uploads']      = $this->admin_dashboard->count_total_video_uploads();

            $total_revenue_from_channel_owners = $this->admin_dashboard->get_total_revenue_from_members('3');
            $total_revenue_from_subscribers    = $this->admin_dashboard->get_total_revenue_from_members('4');

            if ( $total_revenue_from_channel_owners != false ) {
                $this->data['total_revenue_from_channel_owners'] = $total_revenue_from_channel_owners;
            } else {
                $this->data['total_revenue_from_channel_owners'] = 0;
            }
            if ( $total_revenue_from_subscribers != false ) {
                $this->data['total_revenue_from_subscribers'] = $total_revenue_from_subscribers;
            } else {
                $this->data['total_revenue_from_subscribers'] = 0;

            }

            $this->data['channel_owners'] = $this->admin_dashboard->get_recent_members('3');
            $this->data['subscribers'] = $this->admin_dashboard->get_recent_members('4');
            $this->data['uploaded_videos'] = $this->admin_dashboard->get_recent_uploaded_videos();*/

            /*if($this->admin_dashboard->get_total_revenue('pay_for_listing_package')!=false){
                $this->data['total_revenue_from_item_listing'] = $this->admin_dashboard->get_total_revenue('pay_for_listing_package');
            }else{
                $this->data['total_revenue_from_item_listing'] = 0;
            }
            $this->data['total_sold_products'] = $this->admin_dashboard->count_total_sold_products();
            $this->data['total_product_sold_cost']=$this->admin_dashboard->total_cost_products_sold();

            $this->data['total_total_listed_products']=$this->admin_dashboard->count_total_listed_products();

            $this->data['count_total_members'] = $this->admin_dashboard->count_total_members();
            $this->data['recent_products'] = $this->admin_dashboard->get_recent_active_products();
            $this->data['recent_members'] = $this->admin_dashboard->get_recent_members();*/

            $this->template
                ->set_layout('dashboard')
                ->enable_parser(false)
                ->title(WEBSITE_NAME . '- Dashboard')
                ->build('a_dashboard', $this->data);

        }

    }

    /* End of file welcome.php */
    /* Location: ./application/controllers/welcome.php */