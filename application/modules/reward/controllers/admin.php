<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
*/

class Admin extends CI_Controller
{
	function __construct()
    {
    	parent::__construct();
        if ( !$this->general->admin_logged_in() ) {
            redirect(ADMIN_LOGIN_PATH, 'refresh');
            exit;
        }
    	$this->load->model('reward_model');
    }

    

    public function index()
    {
    	
		$this->data['rewards'] = $this->reward_model->get_reward_list();

        $this->template
                ->set_layout('dashboard')
                ->enable_parser(FALSE)
                ->title(WEBSITE_NAME.'- Reward')
                ->build('reward_list', $this->data);
	
    }

    public function reward_detail($participant_id= 0, $user_email = '')
    {
    	$participant_id = $this->input->post('participant_id');

    	// $data['user_email'] = urldecode($user_email);
    	
    	$this->data['reward_details'] = $this->reward_model->get_reward_detail($participant_id);
    	$this->data['participant_id'] = $participant_id;
        $this->template
                ->set_layout('dashboard')
                ->enable_parser(FALSE)
                ->title(WEBSITE_NAME.'- Reward Details')
                ->build('reward_detail_list', $this->data);
    }

    public function pay_reward($user_id)
    {
    	
    	$reward_pay_status = $this->reward_model->pay_reward($user_id);
    	
    	redirect(site_url('dashboard/reward/index'), 'refresh');
    }
}