

<section class="title">
  <div class="wrap">
    <h2><a href="<?=site_url(ADMIN_DASHBOARD_PATH)?>">ADMIN</a> &raquo; Reward Management</h2>
  </div>
</section>

<article id="bodysec" class="sep">
	<div class="wrap">
		<aside class="lftsec"><?php $this->load->view('menu'); ?></aside>
		<section class="smfull">
			<?php
				 if($this->session->flashdata('message')) 
				 {
					 ?>
						<div id="displayErrorMessage" class="confrmmsg">
  							<p><?php echo $this->session->flashdata('message'); ?></p>
						</div>
					<?php
                 }
			?>
			<h3>Reward Detail : <?php echo $participant_id; ?></h3>
			<div class="box_block">
  				<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl_list tbl_full">
    				<thead>
                        <tr>
                        	<th width="5%">S.N.</th>
                            <!-- <th width="15%">Name</th> -->
                            <th width="20%">Referer Name</th>
                            <th width="15%">Email</th>                            
                            <th width="15%">Reward</th>
                            <th width="18%">Rewarded On</th>
                            <th width="5%">Reward Info</th>
                            <!-- <th width="12%" class="optn"> Operations </th> -->
                        </tr>
                    </thead>
    				<tbody>
					<?php 
                    $sn_count=0;
                    if($reward_details)
                    {
                        foreach($reward_details as $reward_detail)
                        { ?>
                          <tr>
                            <td><?php echo ++$sn_count; ?></td>
                            <td><?php echo $reward_detail['name']; ?></td>
                            <td><?php echo $reward_detail['emailid']; ?></td>
                            <td><?php  echo $reward_detail['amount']. ' '. $reward_detail['reward_unit']; ?></td>
                            <td><?php  echo $reward_detail['rewarded_date']; ?></td>
							<td><?php  echo $reward_detail['reward_type']; ?></td>
                            
                            
                            
                          </tr>
                        <?php 
                        }
                    }
                    ?>
                </tbody>
  				</table>
  			</div>
		</section>
  		<div class="clearfix"></div>
	</div>
</article>
<div> </div>
