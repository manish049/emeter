<section class="title">
  <div class="wrap">
    <h2><a href="<?=site_url(ADMIN_DASHBOARD_PATH)?>">ADMIN</a> &raquo; Reward Management</h2>
  </div>
</section>

<article id="bodysec" class="sep">
	<div class="wrap">
		<aside class="lftsec"><?php $this->load->view('menu'); ?></aside>
		<section class="smfull">
			<?php
				 if($this->session->flashdata('message')) 
				 {
					 ?>
						<div id="displayErrorMessage" class="confrmmsg">
  							<p><?php echo $this->session->flashdata('message'); ?></p>
						</div>
					<?php
                 }
			?>
			<h3>Reward List</h3>
			<div class="box_block">
  				<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl_list tbl_full">
    				<thead>
                        <tr>
                        	<th width="5%">S.N.</th>
                            <th width="15%">User Id</th>
                            <th width="20%">Referer Name</th>
                            <th width="15%">Referer Email</th>                            
                            <th width="15%">Total Reward</th>
                            <th width="18%">Remaining Reward</th>
                            <th width="5%">Paid Reward</th>
                            <th width="12%" class="optn"> Operations </th>
                        </tr>
                    </thead>
    				<tbody>
					<?php 
                    $sn_count=0;
                    if($rewards)
                    {
                        foreach($rewards as $reward)
                        { ?>
                          <tr>
                            <td><?php echo ++$sn_count; ?></td>
                            <td><?php echo $reward['participantid']; ?></td>
                            <td><?php echo $reward['name']; ?></td>
                            <td><?php echo $reward['emailid']; ?></td>
                            <td><?php echo $reward['total_balance']; ?></td>
                            <td><?php echo $reward['remaining_balance']; ?></td>
                            <td><?php echo $reward['paid_balance']; ?></td>
                            
                            
                             <td class="optn">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                    	<?php if($reward['remaining_balance'] >= 50): ?>
                                    		<td width="10">
									<a href="<?php echo site_url('dashboard/reward/pay_reward/'.$reward['id']); ?>" >Pay</a>
									</td>
								<?php else: ?>
									<td width="10">
										<a href="#" >Pay</a>
									</td>
									
								<?php endif; ?>
                                    <td width="33">  
	                                    <form action="<?php echo site_url('dashboard/reward/reward_detail/'); ?>" method="post" style="display:inline-block;">
											<input type="hidden" name="participant_id" value="<?php echo $reward['participantid']; ?>">
											<input type="submit" value="Details" style="background:none; border:none; color:337AB7;">
										</form>       
                                    </td> 
                                    </tr>
                                </table>
                            </td>
                          </tr>
                        <?php 
                        }
                    }
                    ?>
                </tbody>
  				</table>
  			</div>
		</section>
  		<div class="clearfix"></div>
	</div>
</article>
<div> </div>
