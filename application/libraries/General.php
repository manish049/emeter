<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class General {
	/**
	 * CodeIgniter global
	 *
	 * @var string
	 **/
	protected $ci;

	/**
	 * account status ('not_activated', etc ...)
	 *
	 * @var string
	 **/
	protected $status;

	/**
	 * error message (uses lang file)
	 *
	 * @var string
	 **/
	protected $errors = array();


	public function __construct() {
		
		$this->ci =& get_instance();
				//date_default_timezone_set('Asia/Katmandu');

				//define site settings info
			$site_info = $this->get_site_settings_info();
			if($site_info)
			{
				
				define('SITE_NAME',$site_info['site_name']);
				define('WEBSITE_NAME',$site_info['site_name']);
				define('CONTACT_EMAIL',$site_info['contact_email']);
				define('DEFAULT_PAGE_TITLE',$site_info['default_page_title']);
				define('DEFAULT_META_KEYS',$site_info['default_meta_keys']);
				define('DEFAULT_META_DESC',$site_info['default_meta_desc']);
				define('SITE_STATUS',$site_info['site_status']);
				define('SITE_OFFLINE_MSG',$site_info['site_offline_msg']);
				define('COPY_RIGHT_TEXT',$site_info['copy_right_text']);
				define('LOG_ADMIN_ACTIVITY',$site_info['log_admin_activity']);
				define('LOG_ADMIN_INVALID_LOGIN',$site_info['log_admin_invalid_login']);

				// define('SMS_USERNAME',$site_info['sms_username']);
				// define('SMS_PASSWORD',$site_info['sms_password']);

			}
			else
			{
				define('SITE_NAME','Moving social network');
				define('WEBSITE_NAME', 'Emeter App');

				define('CONTACT_EMAIL','silwalprabin@gmail.com');
				define('DEFAULT_PAGE_TITLE','');
				define('DEFAULT_META_KEYS','');
				define('DEFAULT_META_DESC','');
				define('SITE_STATUS','');
				define('SITE_OFFLINE_MSG','');
				define('COPY_RIGHT_TEXT','@ 2016 Emeter');
				define('LOG_ADMIN_ACTIVITY','Y');
				define('LOG_ADMIN_INVALID_LOGIN','Y');

			}
	/*				
			//update member login info
			if($this->ci->session->userdata(SESSION.'user_id'))
				$this->updateOnlineMembers();			
	*/
	}
	
	public function get_site_settings_info()
	{
		$query = $this->ci->db->get("site_settings");
		if ($query->num_rows() > 0) 
		{
			$data=$query->row_array();				
		}	
		else
		{
			$data = false;
		}
		$query->free_result();
		return $data;
	}
	
	public function random_number() 
	{
			return mt_rand(100, 999) . mt_rand(100,999) . mt_rand(11, 99);
	} 
	
	public function salt() 
	{
		return substr(md5(uniqid(rand(), true)), 0, '10');
	}
	
	public function hash_password($password, $salt) 
	{
	   
		return  sha1($salt.sha1($salt.sha1($password)));
		
	}
		
	public function get_real_ipaddr()
	{
		if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
			$ip=$_SERVER['HTTP_CLIENT_IP'];
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
	    	$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
		else
			$ip=$_SERVER['REMOTE_ADDR'];

		return $ip;
	}
	public function reg_confirmation_email($activation_code,$user_id)
    {
		//load email library
    	$this->ci->load->library('email');				
		//$config['protocol'] = 'sendmail';
		$config['mailtype'] = 'html';
		$config['charset']  = 'utf-8';
		$config['newline']  = "\n";
		$config['wordwrap'] = TRUE;
		
		$this->ci->email->initialize($config);
		$this->ci->load->model('email_model');		
		
		//get subjet & body
		$template=$this->ci->email_model->get_email_template("register_notification");
		
        $subject=$template['subject'];
        $emailbody=$template['email_body'];
		
		//check blank valude before send message
		if(isset($subject) && isset($emailbody))
		{
			//parse email
			
			$confirm="<a href='".$this->lang_uri('register/activation/'.$activation_code.'/'.$user_id)."'>".$this->lang_uri('register/activation/'.$activation_code.'/'.$user_id)."</a>";
	 
					$parseElement=array("USERNAME"=>$this->ci->input->post('username'),
										"CONFIRM"=>$confirm,
										"SITENAME"=>SITE_NAME,
										"EMAIL"=>$this->ci->input->post('email'),									
										"FIRSTNAME"=>$this->ci->input->post('first_name'),
										"PASSWORD"=>$this->ci->input->post('password'));
					$subject=$this->ci->email_model->parse_email($parseElement,$subject);
					$emailbody=$this->ci->email_model->parse_email($parseElement,$emailbody);
								
			$this->ci->email->to($this->ci->input->post('email', TRUE)); 

			$this->ci->email->from(CONTACT_EMAIL);
			
			$this->ci->email->subject($subject);
			 $this->ci->email->message($emailbody); 
			
			$this->ci->email->send();
			
			//echo $this->email->print_debugger();exit;
		}
    }
	function lang_uri($path)
	{			
						
			//if($_SERVER['HTTP_HOST'] == "localhost")
			//echo $path; exit;
			return base_url().$path;
		//	return site_url($this->ci->config->item('lang').$path);
			//else
			//return site_url($path);
	}
	//Change & Get Time Zone based on settings
	function get_local_time($time="none")
	{
		//$gmt_info=$this->get_gmt_info();
		//$gmt_info=$this->get_gmt_info_fi();
		//$gmt_time=explode(':',$gmt_info);		
		$hour_delay=	5;//$gmt_time[0];
		$minute_delay=	45;//$gmt_time[1];		
		
		if($time!='none')
		return date("Y-m-d H:i:s",mktime (gmdate("H")+$hour_delay,gmdate("i")+$minute_delay,gmdate("s"),
					gmdate("m"),gmdate("d"),gmdate("Y")));
		else
		return date("Y-m-d",mktime(gmdate("H")-$hour_delay,gmdate("i")-$minute_delay,gmdate("s"),gmdate("m"),gmdate("d"),gmdate("Y")));				
				
	}

	/**
	 * admin_logged_in check if user is logged in or not
	 * @return string session data
	 */
	public function admin_logged_in()
	{		
		return $this->ci->session->userdata(ADMIN_LOGIN_ID);
	}

	//function to admin logout
	public function admin_logout()
	{		
		$this->ci->db->where('id',$this->ci->session->userdata(ADMIN_LOGIN_ID));
		$this->ci->db->update('members',array('is_login' => '0'));
		$this->ci->session->unset_userdata(ADMIN_LOGIN_ID);
		return true;
	}



	public function generate_username() 
	{
		return substr(md5(uniqid(rand(), true)), 0, '10');
	}
	
	
	
	function create_password($length=8,$use_upper=1,$use_lower=1,$use_number=1,$use_custom="")
	{
		$upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$lower = "abcdefghijklmnopqrstuvwxyz";
		$number = "0123456789";
		$seed_length = '';
		$seed = '';
		$password = '';
		
		if($use_upper)
		{
			$seed_length += 26;
			$seed .= $upper;
		}
		if($use_lower)
		{
			$seed_length += 26;
			$seed .= $lower;
		}
		if($use_number)
		{
			$seed_length += 10;
			$seed .= $number;
		}
		if($use_custom)
		{
			$seed_length +=strlen($use_custom);
			$seed .= $use_custom;
		}
		
		for($x=1;$x<=$length;$x++)
		{
			$password .= $seed{rand(0,$seed_length-1)};
		}
	
		return $password;
	}

	//date format only
	//for date in format: 12th march 2014
	function date_formate($date)
	{
		$str_date=strtotime($date);
		$dt_frmt=date("D, dS M Y",$str_date);
		return $dt_frmt;
	}


	public function generate_permission_array($array_perms){
        $formated = array();
        if($array_perms && count($array_perms)>0){
            foreach ($array_perms as $item){
                $formated[$item->code] = $item->name; 
            }
        }
        return $formated;
    }
	
	
	public function get_admin_role_permission($user_type)
	{
        $this->ci->db->select($this->ci->db->dbprefix('admin_permissions').'.code, '.$this->ci->db->dbprefix('admin_permissions').'.name ');
       
	    $this->ci->db->from('admin_permissions');
		
        $this->ci->db->where($this->ci->db->dbprefix('admin_permissions').'.permission_id = '.$this->ci->db->dbprefix('admin_roles_permission').'.permission_id');
		
        $query = $this->ci->db->get_where('admin_roles_permission',array('user_type'=>$user_type));
		
        //echo $this->ci->db->last_query(); exit;
		
        if ($query->num_rows() > 0){
            return $this->generate_permission_array($query->result());
        }else{
            return array();
        }
    }
	
	//function to log admins activity
	function log_admin_activity($data){
        $this->ci->load->library('user_agent');
        //Extra Info
        $extra_info = '';
        if($this->ci->agent->mobile())
            $extra_info .= 'mobile:'.$this->ci->agent->mobile();
        
		if($data['extra_info'])
		{
            $extra_info .= $data['extra_info'];
        }
		
        $data_log = array('log_user_id' => $data['user_id'], 'log_user_type' => $data['user_type'], 'module_name' => $data['module'], 'module_desc' => $data['module_desc'], 'log_action' => $data['action'], 'log_ip' => $this->ci->input->ip_address(), 'log_platform' => $this->ci->agent->platform(), 'log_browser' => $this->ci->agent->browser().' | '.$this->ci->agent->version(), 'log_agent' => $this->ci->input->user_agent(), 'log_referrer' => $this->ci->agent->referrer(), 'log_extra_info' => $extra_info);
        
		$this->ci->db->insert("log_admin_activity",$data_log);
    }
	
	
	//log admin's login error
	function log_invalid_logins($data){           
        $this->ci->load->library('user_agent');
        $encrypted_pwd = $this->ci->encrypt->encode($data['password'],'kks');
        //Extra Info
        $extra_info = '';
        if($this->ci->agent->mobile())
            $extra_info .= 'mobile:'.$this->ci->agent->mobile();
        
        $data_log = array('log_module' => $data['module'], 'log_username' => $data['username'], 'log_password' => $encrypted_pwd, 'log_ip' => $this->ci->input->ip_address(), 'log_platform' => $this->ci->agent->platform(), 'log_browser' => $this->ci->agent->browser().' | '.$this->ci->agent->version(), 'log_agent' => $this->ci->input->user_agent(), 'log_referrer' => $this->ci->agent->referrer(), 'log_desc' => $data['desc'], 'log_extra_info' => $extra_info);
        $this->ci->db->insert('log_invalid_logins', $data_log);
		
		//echo $this->ci->db->last_query(); exit;  
    }
	
}
