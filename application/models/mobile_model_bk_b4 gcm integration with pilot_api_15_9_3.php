<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/



class Mobile_model extends CI_Model
{
	function __construct()
    {
        // Construct our parent class
        parent::__construct(); 
        $this->load->helper('inflector');

    }
	public function get_region_list($last_update)
	{
		
		$this->db->select_max('last_update');
		
		$new_last_update_query = $this->db->get('emt_region_list');
		if ($new_last_update_query->num_rows() > 0) 
		{
			$res_last_update = $new_last_update_query->row_array();		
			$new_last_update = $res_last_update['last_update'];		
		}
		else
		{
			$new_last_update = 'no';
		}
		
		if($new_last_update != $last_update )
		{	
			if($last_update)
			{
				$query = $this->db->get_where("emt_region_list",array('last_update >'=>$last_update));
			}
			else
			{
				
				$query = $this->db->get("emt_region_list");
				
			}
				
			if ($query->num_rows() > 0) 
			{
				$data_all=$query->result_array();	
				$data= array('status'=>'success','last_update'=>$new_last_update,'data'=>$data_all	);	
			}
			else
			{
				$data = false;
			}
			
			$query->free_result();	
		}
		else
		{
			$data =false;
		}
			
		return $data;

	}

	public function get_section_list($last_update)
	{
		
		$this->db->select_max('last_update');
		$new_last_update_query = $this->db->get('emt_section_list');

		if ($new_last_update_query->num_rows() > 0) 
		{
			$res_last_update = $new_last_update_query->row_array();		
			$new_last_update = $res_last_update['last_update'];		
		}
		else
		{
			$new_last_update = 'no';
		}
		
		if($new_last_update != $last_update )
		{	
			if($last_update)
			{
				$query = $this->db->get_where("emt_section_list",array('last_update >'=>$last_update));
			}
			else
			{
				$query = $this->db->get("emt_section_list");
				
			}
				
			if ($query->num_rows() > 0) 
			{
				$data_all=$query->result_array();	
				$data= array('status'=>'success','last_update'=>$new_last_update,'data'=>$data_all	);	
			}
			else
			{
				$data = false;
			}
			
			$query->free_result();	
		}
		else
		{
			$data =false;
		}
			
		return $data;

	
	}
	
	public function get_section_list_by_region_id($last_update,$region_id)
	{
		if(!$region_id)
		{
			$this->get_section_list($last_update);
			//exit;
		}
		
		$this->db->select_max('last_update');
		$new_last_update_query = $this->db->get('emt_section_list');

		if ($new_last_update_query->num_rows() > 0) 
		{
			$res_last_update = $new_last_update_query->row_array();		
			$new_last_update = $res_last_update['last_update'];		
		}
		else
		{
			$new_last_update = 'no';
		}
		
		if($new_last_update != $last_update )
		{	
			if($last_update)
			{
				$query = $this->db->get_where("emt_section_list",array('last_update >'=>$last_update,'region_id'=>$region_id));
			}
			else
			{
				$query = $this->db->get_where("emt_section_list",array('region_id'=>$region_id));
				
			}
				
			if ($query->num_rows() > 0) 
			{
				$data_all=$query->result_array();	
				$data= array('status'=>'success','last_update'=>$new_last_update,'data'=>$data_all	);	
			}
			else
			{
				$data = false;
			}
			
			$query->free_result();	
		}
		else
		{
			$data =false;
		}
			
		return $data;

	
	}
	
	public function get_distribution_list($last_update)
	{
		
		$this->db->select_max('last_update');
		$new_last_update_query = $this->db->get('emt_distribution_list');

		if ($new_last_update_query->num_rows() > 0) 
		{
			$res_last_update = $new_last_update_query->row_array();		
			$new_last_update = $res_last_update['last_update'];		
		}
		else
		{
			$new_last_update = 'no';
		}
		
		if($new_last_update != $last_update )
		{	
			if($last_update)
			{
				$query = $this->db->get_where("emt_distribution_list",array('last_update >'=>$last_update));
			}
			else
			{
				$query = $this->db->get("emt_distribution_list");
				
			}
				
			if ($query->num_rows() > 0) 
			{
				$data_all=$query->result_array();	
				$data= array('status'=>'success','last_update'=>$new_last_update,'data'=>$data_all	);	
			}
			else
			{
				$data = false;
			}
			
			$query->free_result();	
		}
		else
		{
			$data =false;
		}
			
		return $data;

	
	}
	
	public function get_distribution_list_by_section_id($last_update,$section_id)
	{
		if(!$section_id)
		{
			$this->get_section_list($last_update);
			//exit;
		}
		
		$this->db->select_max('last_update');
		$new_last_update_query = $this->db->get('emt_distribution_list');
		if ($new_last_update_query->num_rows() > 0) 
		{
			$res_last_update = $new_last_update_query->row_array();		
			$new_last_update = $res_last_update['last_update'];		
		}
		else
		{
			$new_last_update = 'no';
		}
		
		if($new_last_update != $last_update )
		{	
			$this->db->distinct();
		    $this->db->select();
   		 	$this->db->from("emt_distribution_list");        
	
			if($last_update)
			{
				$this->db->where('last_update >',$last_update);
				$this->db->where('section_id',$section_id);
				$query = $this->db->get();
			}
			else
			{
				$this->db->where('section_id',$section_id);
				$query = $this->db->get();
				
			}
				
			if ($query->num_rows() > 0) 
			{
				$data_all=$query->result_array();	
				$data= array('status'=>'success','last_update'=>$new_last_update,'data'=>$data_all	);	
			}
			else
			{
				$data = false;
			}
			
			$query->free_result();	
		}
		else
		{
			$data =false;
		}
			
		return $data;

	
	}
	
	public function get_distribution_list_by_reg_and_section_id($last_update,$region_id,$section_id)
	{
		if(!$region_id)
		{
			$this->get_distribution_list_by_section_id($last_update,$section_id);
			exit;
		}
		if(!$section_id)
		{
			$this->get_section_list($last_update);
			//exit;
		}
		
		$this->db->select_max('last_update');
		$new_last_update_query = $this->db->get('emt_distribution_list');
		if ($new_last_update_query->num_rows() > 0) 
		{
			$res_last_update = $new_last_update_query->row_array();		
			$new_last_update = $res_last_update['last_update'];		
		}
		else
		{
			$new_last_update = 'no';
		}
		
		if($new_last_update != $last_update )
		{	
			$this->db->distinct();
		    $this->db->select();
   		 	$this->db->from("emt_distribution_list");        
	
			if($last_update)
			{
				$this->db->where('last_update >',$last_update);
				$this->db->where('section_id',$section_id);
				$this->db->where('region_id',$region_id);
				$query = $this->db->get();
			}
			else
			{
				$this->db->where('section_id',$section_id);
				$this->db->where('region_id',$region_id);
				$query = $this->db->get();
				
			}
				
			if ($query->num_rows() > 0) 
			{
				$data_all=$query->result_array();	
				$data= array('status'=>'success','last_update'=>$new_last_update,'data'=>$data_all	);	
			}
			else
			{
				$data = false;
			}
			
			$query->free_result();	
		}
		else
		{
			$data =false;
		}
			
		return $data;

	
	}
	
	public function get_appliance_list($last_update)
	{
		
		$this->db->select_max('last_update');
		$new_last_update_query = $this->db->get('appliance_list');

		if ($new_last_update_query->num_rows() > 0) 
		{
			$res_last_update = $new_last_update_query->row_array();		
			$new_last_update = $res_last_update['last_update'];		
		}
		else
		{
			$new_last_update = 'no';
		}
		
		if($new_last_update != $last_update )
		{	
			if($last_update)
			{
				$query = $this->db->get_where("appliance_list",array('last_update >'=>$last_update));
			}
			else
			{
				$query = $this->db->get("appliance_list");
				
			}
				
			if ($query->num_rows() > 0) 
			{
				$data_all=$query->result_array();	
				$data= array('status'=>'success','last_update'=>$new_last_update,'image_path'=>base_url().'assets/images/appliance/','data'=>$data_all);	
			}
			else
			{
				$data = false;
			}
			
			$query->free_result();	
		}
		else
		{
			$data =false;
		}
			
		return $data;

	
	}
	
	public function get_appliance_by_id($last_update,$id)
	{
		if(!$id)
		{
			$this->get_section_list($last_update);
			//exit;
		}
		
		$this->db->select_max('last_update');
		$new_last_update_query = $this->db->get('appliance_list');

		if ($new_last_update_query->num_rows() > 0) 
		{
			$res_last_update = $new_last_update_query->row_array();		
			$new_last_update = $res_last_update['last_update'];		
		}
		else
		{
			$new_last_update = 'no';
		}
		
		if($new_last_update != $last_update )
		{	
			if($last_update)
			{
				$query = $this->db->get_where("appliance_list",array('last_update >'=>$last_update,'id'=>$id));
			}
			else
			{
				$query = $this->db->get_where("appliance_list",array('id'=>$id));
				
			}
				
			if ($query->num_rows() > 0) 
			{
				$data_all=$query->result_array();	
				$data= array('status'=>'success','last_update'=>$new_last_update,'data'=>$data_all	);	
			}
			else
			{
				$data = false;
			}
			
			$query->free_result();	
		}
		else
		{
			$data =false;
		}
			
		return $data;

	
	}
	
	public function get_section_id_by_name($name)
	{
		if(!$name)
		{
			return false;
			//exit;
		}
		$this->db->select('emt_region_list.code region_id,emt_section_list.code section_id,emt_section_list.name');
		$this->db->join('emt_region_list','emt_region_list.id = emt_section_list.region_id');
		$query = $this->db->get_where("emt_section_list",array('emt_section_list.name'=>$name));
		if ($query->num_rows() > 0) 
		{
			return $query->row_array();	
		}
		$query->free_result();	
		return false;

	
	}
	public function is_connection_id_exist($connection_id)
	{
			$query = $this->db->get_where("server_connection_list",array('connection_id'=>$connection_id));
			if ($query->num_rows() > 0) 
			{
				$data=$query->row_array();	
			}
			else
			{
				$data = false;
			}
			return $data;
	}

	public function is_connection_id_exist_karnataka($connection_id)
	{
			$query = $this->db->get_where("server_connection_list_karnataka",array('connection_id'=>$connection_id));
			if ($query->num_rows() > 0) 
			{
				$data=$query->row_array();	
			}
			else
			{
				$data = false;
			}
			return $data;
	}
	
	public function do_insertion_userdata($service_no_ip)
	{
		
		$all_data_fetched = $this->get_fetched_data_server($service_no_ip);
		
		//print_r(json_encode($all_data_fetched));exit;
		//print_r($all_data_fetched);
		if(count($all_data_fetched['usage']) >0)
		{
			 $ud_assessment_date = $all_data_fetched['usage'][0]['assessment_date'];
	  		 $ud_reading=$all_data_fetched['usage'][0]['reading'];
	  		 $ud_units = $all_data_fetched['usage'][0]['units'];
	  		 $ud_amount = $all_data_fetched['usage'][0]['amount'];
	  		 $ud_payment_date = $all_data_fetched['usage'][0]['payment_date'];
	   		 $ud_status = $all_data_fetched['usage'][0]['status'];
		}
		else
		{
			 $ud_assessment_date = "Assessment Entry Date";
	  		 $ud_reading= "Reading";
	  		 $ud_units = "Consumed Unit";
	  		 $ud_amount = "Total BillAmount(Rs.)";
	  		 $ud_payment_date = "Payment Date";
	   		 $ud_status = "AssessmentStatus";
		}
		
		$state = 'chennai';
		$provider = 'tneb';
		
		$data = array(
			'connection_id' => $service_no_ip,
		   	'serviceno' => $all_data_fetched['ServiceNo'] ,
		   	'name' => $all_data_fetched['Name'] ,
		   	'region' => $all_data_fetched['Region'],
	   		'phase' => $all_data_fetched['Phase'],
		   	'circle' => $all_data_fetched['Circle'],
		   	'section' => $all_data_fetched['Section'],
		   	'load' => $all_data_fetched['Load'],
		   	'distribution' => $all_data_fetched['Distribution'],
		   	'meterno' => $all_data_fetched['MeterNo'],
		   	'connectionnumber' => $all_data_fetched['ConnectionNumber'],
		   	'address' => $all_data_fetched['Address'],
		   	'servicestatus' => $all_data_fetched['ServiceStatus'],
		   
		   	'ud_assessment_date' => $ud_assessment_date,
		   	'ud_reading' => $ud_reading,
		   	'ud_units' => $ud_units,
		   	'ud_amount' => $ud_amount,
		   	'ud_payment_date' => $ud_payment_date,
		   	'ud_status' => $ud_status,
		   	'state' => $state,
		   	'provider' => $provider,
	   
		);
		//print_r(json_encode($data));
		//exit;

		$status = $this->db->insert('server_connection_list', $data); 
		// $current_insert_id = 1;
		// var_dump($status);
		// exit;
		if($status)
		{
			$current_insert_id = $this->db->insert_id();
			$this->insert_user_data($all_data_fetched,$current_insert_id,true);
		}
		
		return $all_data_fetched;
	}
	
	public function insert_user_data($all_data_fetched,$current_insert_id,$is_first_time = false)
	{
	
		if((count($all_data_fetched['usage']) > 1 && $is_first_time) ||(count($all_data_fetched['usage']) > 0 && !$is_first_time) )
		{
			$all_data_usage_batch_insert = array();
			$count_loop = 0;
			foreach($all_data_fetched['usage'] as $single_data_usage)
			{
				if($count_loop == 0 && count($all_data_fetched['usage']) > 1)
				{
					$count_loop=$count_loop+1;
					continue;
				}

				$cur_data = array(
								  'fk_connection_id' => $current_insert_id ,
								  'assessment_date' => $single_data_usage['assessment_date'],
								  'reading' => $single_data_usage['reading'],
								  'units' => $single_data_usage['units'],
								  'amount' => $single_data_usage['amount'],
								  'payment_date' =>   $single_data_usage['payment_date'],
								  'status' => $single_data_usage['status']
								);
				array_push($all_data_usage_batch_insert,$cur_data);
			}
			$this->db->insert_batch('server_usage_data', $all_data_usage_batch_insert); 
		}
			
	}
	
	public function do_update_userdata($service_no_ip,$connection_detail,$last_update)
	{
		//NOTE: $last_update is max assessment_date of client;

		$today_date = date("Y-m-d");
		
		if($today_date == $last_update)
		{
			$all_data_fetched = $this->get_fetched_data_server($service_no_ip,$connection_detail);
			return $all_data_fetched;
			exit;
		}
		
		$this->db->select_max('assessment_date');
		$this->db->where('fk_connection_id',$connection_detail['id']);
		$this->db->order_by("assessment_date", "desc");
		$query = $this->db->get("server_usage_data");

		if ($query->num_rows() > 0) 
		{
			$max_date = $query->row_array();	
		}
		else
		{
			$max_date ="";
		}
		
		if($max_date == "")
		{
			$all_data_fetched = $this->get_fetched_data_server($service_no_ip);
		}
		else
		{
			$all_data_fetched = $this->get_fetched_data_server($service_no_ip,$last_update);
		}

		//$all_data_fetched = $this->get_fetched_data_server($service_no_ip,$last_update);
		//print_r(json_encode($all_data_fetched)); exit;
		
		if (!$last_update) 
		{
			$this->delete_user_data($connection_detail['id'],FALSE);
		}
		else
		{
			$this->delete_user_data($connection_detail['id'],$last_update);

		}
		
		//exit;
		print_r(json_encode($all_data_fetched));
		$this->insert_user_data($all_data_fetched,$connection_detail['id'],false);
		exit;
		return $all_data_fetched;

	}

	public function delete_user_data($connection_id = FALSE, $assessment_date = FALSE)
	{
		if ($connection_id ==FALSE) 
		{
			return;
		}
		
		$this->db->where('fk_connection_id',$connection_id);
		//return $assessment_date;exit;
		if ($assessment_date != FALSE)
		{
			$this->db->where('assessment_date >',$assessment_date);
			//return $this->db->get('server_usage_data')->result();
		}


		$this->db->delete('server_usage_data');

	}
	
	public function get_fetched_data_server($service_no_ip,$last_update= false)
	{
		
		error_reporting(0);
		//phpinfo(); exitl
		//	$last_update ="2015-1-1";
		if($last_update)
		{
			$temp_d_format =date_create_from_format("Y-m-d",$last_update);

			if($temp_d_format)
			{
				$last_update = date_format($temp_d_format,"Y-m-d");
			}
			else
			{

				$last_update = false;
				$error_result['status'] = "error";
				$error_result['data'] = "Sorry,last update data is invalid.";
				print_r(json_encode($error_result)); exit;	
			}
		}
		//echo $last_update; exit;
	
		$regno = substr($service_no_ip, 0, 2); // first 2 digits

		if($regno !=  ('01'|| '02'|| '03'|| '04'|| '05'|| '06'|| '07'|| '08'|| '09'))
		{
			$error_result['status'] = "error";
			$error_result['data'] = "Sorry, Your service no. is not valid. Please try with proper input..";
			print_r(json_encode($error_result)); exit;	
		}


		$code = $regno; // same as the reg no
        $sec = substr($service_no_ip,2, 3); // next 3 digits
        $dist = substr($service_no_ip,5, 3); // next 3 digits
        $serno = substr($service_no_ip,8, 99); // remaining digits
        $temp_sn = $serno;
        $disp_service_no = $regno . '-' . $sec. '-' . $dist . '-'. $serno;

		$loop_cur_count =-1;
		$html = "";
		$extra_ordinary = FALSE;
		if($regno == 2 || $regno ==4)
		{
			
			if(strlen($serno) != 4 )
			{
				switch (strlen($serno))
				{
					case 1:
					$serno = "000".$serno;
					break;
					case 2:
					$serno = "00".$serno;
					break;
					case 3:
					$serno = "0".$serno;
					break;
					default:
					break;
					
				}
				$service_no_ip = $regno.$sec.$dist.$serno;
			}
			if (($regno == 4 || $regno == 2)  && strlen($temp_sn)==3) {
				//echo "three<br />";
				$serno = $temp_sn;
				$service_no_ip = $regno.$sec.$dist.$serno;
			}
			//echo $service_no_ip."<br />";
			$url = 'https://wss.tangedco.gov.in/wss/AccSummaryNew.htm?scnumber='.urlencode($service_no_ip);

			$this->load->library('dom_parser');
			//$html = $this->dom_parser->file_get_html("http://tneb.tnebnet.org/newlt/consumerwise_gmc_report.php?encserno=VEFOR0VEQ098fDEyMXx8MDA5fHw4&rsno=MQ%3D%3D");
			$html = $this->dom_parser->file_get_html($url);
			
			if (strpos($html,'Invalid Input') != FALSE) 
			{
				//echo "hello";
				$extra_ordinary = TRUE;
				//echo $serno;

    			$plainserno = 'TANGEDCO||'. $sec .'||'. $dist. '||' . $serno; // introduced as TANGEDCO changed the format to base64 encoded string
    			//$plainserno = 'TANGEDCO||'. $sec .'||'. $dist. '||987'; // introduced as TANGEDCO changed the format to base64 encoded string
            	
            	$encserno = base64_encode($plainserno);
            	//echo $encserno;
            	// exit;
            	//echo urlencode($encserno);
			 	$plainrsno = $regno;
			 
				while(substr($plainrsno,0, 1) == '0')
				{
					$plainrsno = substr($plainrsno,1, strlen($plainrsno));
				}// remove leading zeroes
								 
				$rsno = base64_encode($plainrsno);

				//echo "<br />".urlencode($rsno);
				//exit;
				//echo "http://tneb.tnebnet.org/newlt/consumerwise_gmc_report.php?encserno=VEFOR0VEQ098fDAyOHx8MDA2fHwwNDQx&rsno=NA%3D%3D";
				$url = 'http://tneb.tnebnet.org/newlt/consumerwise_gmc_report.php?encserno='.urlencode($encserno).'&rsno='.urlencode($rsno);
				
				$this->load->library('dom_parser');
			
				//$html = $this->dom_parser->file_get_html("http://tneb.tnebnet.org/newlt/consumerwise_gmc_report.php?encserno=VEFOR0VEQ098fDAyOHx8MDA2fHw0NDE%3D&rsno=NA%3D%3D");
				$html = $this->dom_parser->file_get_html($url);		
				//echo $html;exit;		
			}
		}
		else
		{
			// For breaking security while call url
			$plainserno = 'TANGEDCO||'. $sec .'||'. $dist. '||' . $serno; // introduced as TANGEDCO changed the format to base64 encoded string
            $encserno = base64_encode($plainserno);
			 $plainrsno = $regno;
			 
			 while(substr($plainrsno,0, 1) == '0')
			 {
				 $plainrsno = substr($plainrsno,1, strlen($plainrsno));
			 }// remove leading zeroes
								 
			$rsno = base64_encode($plainrsno);
			
			$url = 'http://tneb.tnebnet.org/newlt/consumerwise_gmc_report.php?encserno='.urlencode($encserno).'&rsno='.urlencode($rsno);
			
			$this->load->library('dom_parser');
		
			//$html = $this->dom_parser->file_get_html("http://tneb.tnebnet.org/newlt/consumerwise_gmc_report.php?encserno=VEFOR0VEQ098fDEyMXx8MDA5fHw4&rsno=MQ%3D%3D");
			$html = $this->dom_parser->file_get_html($url);
					
		}
		//echo $html;exit;

		$tag = $html->find('table',7);
		
		
		// $error_result['status'] = "error";
		// $error_result['data'] = $tag;
		// print_r(json_encode($error_result)); exit;	

		//echo urlencode(strip_tags($tag->find("tr",0)->innertext)); exit;
	
		if($tag && urlencode($tag->innertext) == "++%3Ctr+align%3D%22center%22%3E%3Ctd+align%3D%22center%22%3E++%3C%2Ftd%3E%3C%2Ftr%3E" || urlencode($tag->innertext) == "+%3Ctr+align%3D%22center%22%3E%3Ctd+align%3D%22center%22%3E+%3C%2Ftd%3E%3C%2Ftr%3E")
		{
			//for 021000048 && 011210078
			//If Dues To Be Paid  Exist
			$tag = $html->find('table',9);

			
		}
		else if($tag && urlencode(strip_tags($tag->find("tr",0)->innertext)) == "++%09%09%09%09Electricity+Consumption++%09%09%09")
		{
			$tag = $html->find('table',8);
			
		}
		else if($tag && urlencode(strip_tags($tag->find("tr",0)->innertext)) == "++%09%09%09%09Disconnection%2FReconnection+Details++%09%09%09")
		{
			$tag = $html->find('table',9);
			
		}
		
		$count = 0;
		$my_all_result =array();
		$my_all_history = array();
		$my_single_history = array();
		
		//echo $tag;
		if($tag == "" || $tag == null)
		{
			$error_result['status'] = "error";
			$error_result['data'] = "Sorry, Your service no. is not valid. Please try with proper input.";
			print_r(json_encode($error_result)); 
			exit;	
		}
		
		
			
		foreach($tag->find('tr') as $single_row)
		{
			
			
			//For region 2 and 4 api, there is title at top so skip it
			if(($regno == 2 || $regno ==4) && $loop_cur_count == -1 && $extra_ordinary == FALSE)
			{
				//echo "********";
				$loop_cur_count = $loop_cur_count+1;
				continue; //Skip top title
			}
			
			$loop_cur_count = $loop_cur_count+1;
			//echo "<br />-- Start --<br />";
			//echo "<pre>loop count {$loop_cur_count}</pre>";
			//echo "<pre>".$loop_cur_count."</pre>";

			if($last_update)
			{
				if($loop_cur_count == 0)
				{
					continue;
				}
				$my_single_history_temp_ass = str_replace('&nbsp;', '',trim(strip_tags($single_row->find('td',1)->innertext)));
				$my_convert_try = date_create_from_format("d/m/Y",$my_single_history_temp_ass);
				if($my_convert_try)
				{
					$my_single_history['assessment_date']= date_format($my_convert_try,"Y-m-d");	
				}
				else
				{
					if($my_single_history_temp_ass == "-" && $loop_cur_count > 1)
					{	
						$my_single_history['assessment_date'] = $my_single_history['assessment_date'];
					}
					else
					{
						$my_single_history['assessment_date'] = $my_single_history_temp_ass;
					}
				}

				if($my_single_history['assessment_date'] <= $last_update )
				{
					continue;
				}
			}
			else
			{
										
				$my_single_history_temp_ass = str_replace('&nbsp;', '',trim(strip_tags($single_row->find('td',1)->innertext)));
				if($loop_cur_count == 0)
				{
					$my_single_history['assessment_date']=$my_single_history_temp_ass;
				}
				else
				{
					$my_convert_try_else = date_create_from_format("d/m/Y",$my_single_history_temp_ass);
					
					if($my_convert_try_else)
					{
						$my_single_history['assessment_date']= date_format($my_convert_try_else,"Y-m-d");
					}
					else
					{					
						if($my_single_history_temp_ass == "-")
						{	
							$my_single_history['assessment_date'] = $my_single_history['assessment_date'];
						}
						else
						{
							$my_single_history['assessment_date']=$my_single_history_temp_ass;
						}
					}
				}
			}
			
			
			$my_single_history['reading'] =str_replace('&nbsp;', '',trim(strip_tags($single_row->find('td',2)->innertext)));
			$my_single_history['units']=str_replace('&nbsp;', '',trim(strip_tags($single_row->find('td',3)->innertext)));
			
			if($my_single_history_temp_ass == "-")
			{
				 $my_single_history['amount']=$single_row->find('td',11)->innertext;
			}
			else
			{
				$my_single_history['amount']=str_replace('&nbsp;', '',trim(strip_tags($single_row->find('td',9)->innertext)));
			}

			$my_single_history_temp= str_replace('&nbsp;', '',trim(strip_tags($single_row->find('td',13)->innertext)));
			
			//echo "<pre>".count($single_row->find('td', 13))."</pre>";
			//echo "<pre>".str_replace('&nbsp;', '',trim(strip_tags($single_row->find('td',13)->innertext)))."</pre>";
			//print_r($my_single_history_temp);
			

			if($loop_cur_count == 0)
			{

				$my_single_history['payment_date']=$my_single_history_temp;

			}
			else
			{

				$my_convert_try_sec = date_create_from_format("d/m/Y",$my_single_history_temp);
				
				if($my_convert_try_sec)
				{

					$my_single_history['payment_date']= date_format($my_convert_try_sec,"Y-m-d");
					
				}
				else
				{
					//$my_single_history['payment_date'] = '0000-00-00';

					$my_single_history['payment_date'] = $my_single_history['assessment_date'];

				}

			}

			$orginal= $single_row->find('td',18)->innertext;
			//echo "<pre>".$orginal."</pre>";
			$my_single_history_status = str_replace('&nbsp;', '', strip_tags($single_row->find('td',15)->innertext));
			
			if($my_single_history['payment_date'] == "" && ($my_single_history_status == "NORMAL" || $my_single_history_status == "NOT IN USE"))
			{
				if($my_convert_try_sec && $my_single_history_status == "NORMAL")
				{
					$my_single_history['status']="Paid";
				}
				else if($my_single_history_status == "NORMAL")
				{
					$my_single_history['status']="Unpaid";
				}
				else
				{
				$my_single_history['status']=$my_single_history_status;
				}

				$my_single_history['payment_date'] = "0000-00-00";
				
			}
			else if($my_single_history_status == "NORMAL")
			{
				if($my_convert_try_sec)
				{
					$my_single_history['status']="Paid";
				}
				else
				{
					$my_single_history['status']="Unpaid";
				}
				
			}
			else
			{
				if($my_convert_try_sec)
				{
					if($my_single_history_status == null)
					{
						$my_single_history['status']= "Paid";
					}
					else
					{
						$my_single_history['status'] = $my_single_history_status;
					}
				}
				else
				{
					//$my_single_history['status']=$my_single_history_status;
					$my_single_history['status']="Paid";
				}
			}
			
			array_push($my_all_history,$my_single_history);
			//echo "<br />-- End --<br />";
		}
		//var_dump($extra_ordinary);
		if(($regno == 2 || $regno ==4))
		{
			$tag_sub_head = $html->find('table',2);
			
			$first_row = $tag_sub_head->find('tr',0);
			$second_row = $tag_sub_head->find('tr',1);
			$third_row = $tag_sub_head->find('tr',2);
			$fourth_row = $tag_sub_head->find('tr',3);
			$fifth_row = $tag_sub_head->find('tr',4);
			$sixth_row = $tag_sub_head->find('tr',5);
			$seventh_row = $tag_sub_head->find('tr',6);
			$eighth_row = $tag_sub_head->find('tr',7);
			$consumer_name= str_replace('&nbsp;', '',trim(strip_tags($first_row->find('td',1)->innertext)));
			$region_name= str_replace('&nbsp;', '',trim(strip_tags($second_row->find('td',1)->innertext)));
			$circle_name= str_replace('&nbsp;', '',trim(strip_tags($third_row->find('td',1)->innertext)));
			$phase_name= str_replace('&nbsp;', '',trim(strip_tags($third_row->find('td',3)->innertext)));
			$section_name= str_replace('&nbsp;', '',trim(strip_tags($fourth_row->find('td',1)->innertext)));
			$load_name= str_replace('&nbsp;', '',trim(strip_tags($fourth_row->find('td',3)->innertext)));
			$distribution_name= str_replace('&nbsp;', '',trim(strip_tags($fifth_row->find('td',1)->innertext)));
			$service_number = str_replace('&nbsp;', '',trim(strip_tags($sixth_row->find('td',1)->innertext)));
			$meter_number = str_replace('&nbsp;', '',trim(strip_tags($sixth_row->find('td',3)->innertext)));
			$address_name = str_replace('&nbsp;', '',trim(strip_tags($seventh_row->find('td',1)->innertext)));
			$service_status = str_replace('&nbsp;', '',trim(strip_tags($eighth_row->find('td',1)->innertext)));
				
		}
		else
		{
			// print_r($my_all_history);exit;
			$tag_head = $html->find('table',2);
			
			$consumer_head= trim(strip_tags($tag_head->find('tr',0)->find('td',0)->outertext));
			$consumer_name_array = explode("CONSUMER NAME:",$consumer_head);
			
			$tag_sub_head = $html->find('table',3);

			$first_row = $tag_sub_head->find('tr',0);
			$second_row = $tag_sub_head->find('tr',1);
			$region_name= str_replace('&nbsp;', '',trim(strip_tags($first_row->find('td',0)->innertext)));
			$phase_name= str_replace('&nbsp;', '',trim(strip_tags($first_row->find('td',2)->innertext)));
			$circle_name= str_replace('&nbsp;', '',trim(strip_tags($second_row->find('td',0)->innertext)));
			$load_name= str_replace('&nbsp;', '',trim(strip_tags($second_row->find('td',2)->innertext)));
			$load_name_integer = str_replace('&nbsp;', '',trim(str_replace('KW', '',$load_name)));
			//echo  $url; exit;

			 $slabe_rate = $html->find('table',4);
    		 // returns all the <tr> tag inside $table
    		 $all_slabe_rate_trs_count = $slabe_rate->find('tr');
    		 $all_slabe_count = count($all_slabe_rate_trs_count);
    
	 	//	echo $all_slabe_count; exit;

		
			
			if($load_name_integer != "" && $load_name_integer >0)
			{
						
				//	print_r($tag_sub_head->find('tr',9)->innertext); exit;
				$third_row = $tag_sub_head->find('tr',2+$all_slabe_count);
				$fourth_row = $tag_sub_head->find('tr',3+$all_slabe_count);
				$fifth_row =$tag_sub_head->find('tr',4+$all_slabe_count);
				$sixth_row =$tag_sub_head->find('tr',5+$all_slabe_count);
				$seventh_row =$tag_sub_head->find('tr',6+$all_slabe_count);
				//	$eighth_row =$tag_sub_head->find('tr',7+$all_slabe_count);
				
				//print_r($eighth_row->innertext); exit;
				$meter_number= str_replace('&nbsp;', '',trim(strip_tags($fourth_row->find('td',2)->innertext)));
				$service_number=  str_replace('&nbsp;', '',trim(strip_tags($fifth_row->find('td',0)->innertext)));
				$address_name = trim(trim(strip_tags($sixth_row->find('td',0)->innertext)),'&nbsp;');
				$service_status = str_replace('&nbsp;', '',trim(strip_tags($seventh_row->find('td',0)->innertext)));
				$section_name= str_replace('&nbsp;', '',trim(strip_tags($third_row->find('td',0)->innertext)));
				$distribution_name= str_replace('&nbsp;', '',trim(strip_tags($fourth_row->find('td',0)->innertext)));
				
			}
			else 
			{
				$error_result['status'] = "error";
				$error_result['data'] = "Sorry, Your service no. is not valid. Please try with proper input.";
				//print_r(json_encode($error_result)); exit;
				return 	$error_result;
			}
			
		 	   $consumer_name = $consumer_name_array[1];
		}

		$my_all_result['ServiceNo'] =  $disp_service_no;
		$my_all_result['Name'] = $consumer_name;
		$my_all_result['Region'] = $region_name;
		$my_all_result['Phase'] = $phase_name;
		$my_all_result['Circle'] = $circle_name;
		$my_all_result['Section'] = $section_name;
		$my_all_result['Load'] = $load_name;
		$my_all_result['Distribution'] = $distribution_name;
		$my_all_result['MeterNo'] = $meter_number;
		$my_all_result['ConnectionNumber'] = $service_number;
		$my_all_result['Address'] = $address_name;
		$my_all_result['ServiceStatus'] = $service_status;
		$my_all_result['usage'] = $my_all_history;
				
		return $my_all_result;
	}
	
	
	public function get_karnataka($username = FALSE, $password = FALSE, $last_update = FALSE)
	{
		error_reporting(0);

		// check if last update date is valid or not
		if($last_update)
		{
			$temp_d_format =date_create_from_format("Y-m-d",$last_update);
			if($temp_d_format)
			{
				$last_update = date_format($temp_d_format,"Y-m-d");
			}
			else
			{

				$last_update = FALSE;
				$error_result['status'] = "error";
				$error_result['message'] = "Sorry, last update data is invalid.";
				$error_result['data'] = array();
				print_r(json_encode($error_result)); exit;	
			}
		}

		// If username or password is not posted
		if ($username===FALSE OR $password === FALSE) 
		{

			$error_result['status'] = "error";
			$error_result['message'] = "Please Type Username and Password";
			$error_result['data'] = array();
			print_r(json_encode($error_result)); 

			exit;	

		} 
		
		
		

		$client_detail_array = array();		
		$client_detail = array();
		$status = array();
		
		$status['status'] = TRUE;

		$url = "https://www.bescom.co.in/SCP/Myhome.aspx";

		$afterLoginUrl = "https://www.bescom.co.in/SCP/MyAccount/AccountSummary.aspx?Name=IAccountSummaryView";
		
		$this->ckfile = dirname(__FILE__) . '\cookies1.txt';

		if (file_exists($this->ckfile)) {
		    @file_put_contents($this->ckfile, "");
		} 
		
		//$useragent = $_SERVER['HTTP_USER_AGENT'];
		
		// $username = "tprakass";
		// $password = "Muruga78vam$";

		//$username = "srini.bhagyodaya@gmail.com";		
		//$password = "s@1ram";

		//$f = fopen('log.txt', 'w'); // file to write request header for debug purpose

		$regexViewstate = '/__VIEWSTATE\" value=\"(.*)\"/i';
		$regexEventVal  = '/__EVENTVALIDATION\" value=\"(.*)\"/i';
		$regexUip = '/__UipId\" value=\"(.*)\"/i';

		// Initialize CURL
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120); // timeout on connect
		curl_setopt($ch, CURLOPT_TIMEOUT, 120);

		//curl_setOpt($ch, CURLOPT_USERAGENT, $useragent);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $this->ckfile);

		curl_setopt($ch, CURLOPT_COOKIEFILE, $this->ckfile);

		$data=curl_exec($ch);


		//echo $data;exit;
		
		$regs  = array();

		/*
		    Get __VIEWSTATE & __EVENTVALIDATION & __UipId
		 */

		$viewstate = $this->regexExtract_get($data,$regexViewstate,$regs,1);
		//echo $viewstate;exit;
		$eventval = $this->regexExtract_get($data, $regexEventVal,$regs,1);
		//echo $eventval;exit;
		$user_input_id = $this->regexExtract_get($data, $regexUip,$regs,1);
		//print_r($user_input_id);exit;
		$userIP = explode('"', $user_input_id);
		$user_input_id =  $userIP[0];

	    /* Post Data */

		//$postfields['masterPageId'] = "ctl00_ctl00_MasterPageContentPlaceHolder_MasterPageContentPlaceHolder";
		//$postfields['ErrorLabelId'] = 'ctl00$ctl00$MasterPageContentPlaceHolder$lblErrorMessage';
		//$postfields['__LASTFOCUS'] = "";

		$postfields['__EVENTTARGET'] = "";
		$postfields['__EVENTARGUMENT'] = "";
		$postfields['__VIEWSTATE'] = $viewstate;
		//$postfields['__DirtyFlag'] = "N";
		//$postfields['__UipId'] = $user_input_id;

		$postfields['__EVENTVALIDATION'] = $eventval;

		//$postfields['ctl00$ctl00$siteSearch$txtsearch'] = "Search";

		$postfields['ctl00$ctl00$MasterPageContentPlaceHolder$ucLogin$txtUserName'] = $username;
		$postfields['ctl00$ctl00$MasterPageContentPlaceHolder$ucLogin$txtPassword'] = $password;
		$postfields['ctl00$ctl00$MasterPageContentPlaceHolder$ucLogin$btnLogin'] = "Sign In";


		// foreach ( $postfields as $key => $value) {
		//     $post_items[] = rawurlencode($key) . '=' . rawurlencode($value);
		// }

		// $post_string = implode ('&', $post_items);
		// // Start Login Process

		//print_r($post_items);exit;

		curl_setOpt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
		//curl_setopt($ch,CURLOPT_HTTPHEADER,array('Origin: https://www.bescom.co.in', 'Host: www.bescom.co.in', 'Content-Type: application/x-www-form-urlencoded'));

		curl_setopt($ch, CURLOPT_URL, $url);   

		curl_setOpt($ch, CURLOPT_REFERER, 'https://www.bescom.co.in/SCP/Myhome.aspx');		

		$data = curl_exec($ch);

		
		curl_setOpt($ch, CURLOPT_HTTPGET, TRUE);
		curl_setOpt($ch, CURLOPT_POST, FALSE);
		curl_setopt($ch, CURLOPT_URL, $afterLoginUrl);   
		//curl_setopt($ch, CURLOPT_COOKIEFILE, $ckfile);   
		//curl_setopt($ch, CURLOPT_TIMEOUT, 50);   

		$data = curl_exec($ch);


		//curl_close($ch);
		//echo $data;	exit;

		$this->load->library('dom_parser');	
		$html = $this->dom_parser->str_get_html($data);

		$tag = $html->find('table',13);
		
		
		//echo $tag;exit;
		if($tag == "" || $tag == null)
		{
			$error_result['status'] = "error";
			$error_result['message'] = "Sorry, The user credentials are invalid";
			$error_result['data'] = array();

			print_r(json_encode($error_result)); 
			exit;	
		}
		
		$loop_count = -1;
		foreach($tag->find('tr') as $single_row)
		{
			if ($loop_count == -1)
			{
				$loop_count++;
				continue;
			}

			$client_detail['connection'] = trim(strip_tags($single_row->find('td',0)->innertext));

			$connection_no = $client_detail['connection'];
			if ($connection_no == "")
			{
				$status['status'] = "error";
				$status['message'] = "Invalid Username OR Password";
				$error_result['data'] = array();

				print_r(json_encode($error_result)); 
				exit;	
			}
			$client_detail['name'] = trim(strip_tags($single_row->find('td',1)->innertext));
			//$client_detail['address'] = trim(strip_tags($single_row->find('td',2)->innertext));
			//$client_detail['current_balance'] = trim(strip_tags($single_row->find('td',3)->innertext));
			//$client_detail['account_status']  = trim(strip_tags($single_row->find('td',6)->innertext));
			$client_detail['status']  = trim(strip_tags($single_row->find('td',6)->innertext));
			//print_r(json_encode($client_detail)); 

			$details_url='https://www.bescom.co.in/SCP/MyAccount/AccountDetails.aspx?AccountId='.urlencode($connection_no).'&RowIndex%20='.urlencode($loop_count);
			//$details_url2='https://www.bescom.co.in/SCP/MyAccount/AccountDetails.aspx';
			//$detail_html_data = $this->dom_parser->file_get_html($details_url);
		   // echo $detail_html_data;

			
			// Initialize CURL
			//$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $details_url);
			

			$content=curl_exec($ch);

			//echo $content;exit;
			
			
			//$viewstate = $this->regexExtract_get($content,$regexViewstate,$regs,1);
			
			//$file=file_get_contents($details_url);

			//$eventval = $this->regexExtract_get($content, $regexEventVal,$regs,1);

            
            //$data['__VIEWSTATE']=$viewstate;
            //$data['__EVENTVALIDATION']=$eventval;
            /*$options = array(
				//CURLOPT_RETURNTRANSFER => TRUE, // return web page
				//CURLOPT_HEADER => FALSE, // don't return headers
				//CURLOPT_FOLLOWLOCATION => true, // follow redirects
				CURLOPT_ENCODING => "", // handle all encodings
				//CURLOPT_USERAGENT => "spider", // who am i
				//CURLOPT_AUTOREFERER => true, // set referer on redirect
				CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
				CURLOPT_TIMEOUT => 120, // timeout on response
				CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
				//CURLOPT_POST => true,
				//CURLOPT_POSTFIELDS => $data,
			);
			*/
            //$ch = curl_init( $url );
			//curl_setopt_array( $ch, $options );
			//$content = curl_exec ($ch);
			
            //curl_close($ch);
            $html = $this->dom_parser->str_get_html($content);

			$tag = $html->find('table',0);

			//echo $tag;exit;
			
			$account_summary = array();
			foreach($tag->find('tr') as $single_detail_row)
			{
				$key = underscore(strtolower(trim(strip_tags($single_detail_row->find('td',0)->innertext))));
				$value = trim(strip_tags($single_detail_row->find('td',1)->innertext));
				$account_summary[$key] = $value;

			}
			$client_detail['summary']  =  $account_summary;

			/* Code for Usage History */
			

			$usage_history_url = 'https://www.bescom.co.in/SCP/UsageHistory/UsageHistory.aspx';

			$usage_history_result = $this->get_usage_history($ch, $usage_history_url,$connection_no, $last_update);
			
			//echo $usage_history_result;
			if (count($usage_history_result) == 0) 
			{
				$error_result['status'] = "error";
				$error_result['message'] = "Username or Password Invalid or No Usage History";
				$error_result['data'] = array();

				print_r(json_encode($error_result)); 
				exit;	
			}

			$client_detail['usage_history']  =  $usage_history_result;
			
			/* code for both billing and payment history */

			$billing_payment_history_url = 'https://www.bescom.co.in/SCP/MyAccount/BillingHistory.aspx';

			$billing_payment_result  = $this->get_billing_payment_history($ch,$billing_payment_history_url, $connection_no, $last_update);

			$client_detail['billing_history'] = $billing_payment_result['billing_history'];

			$client_detail['payment_history'] = $billing_payment_result['payment_history'];

			//$client_detail_array[$loop_count] = $client_detail;

			array_push($client_detail_array,$client_detail);

			
			$loop_count++;
			

		}
		//echo "<br />".count($client_detail_array);exit;
		
		curl_close($ch);
		if (count($client_detail_array) == 0)
		{

			$status['status'] = "error";
			$status['message'] = "Invalid Username-Password";
			$status['data'] = array();

			//$client_detail_array = $status;
			return $status;
		}
		else 
		{
			$status['status'] = "success";
			$status['message'] = "Login success";
			$status['data'] = $client_detail_array;
			return $status;
		}


		
	}

	function do_post_request($url, $data, $optional_headers = null)
	{
		$params = array('http' => array(
	      	'method' => 'POST',
	      	'content' => $data
	    ));
	  	if ($optional_headers !== null) {
	    	$params['http']['header'] = $optional_headers;
	  	}
	  	$ctx = stream_context_create($params);
	  	$fp = @fopen($url, 'rb', false, $ctx);
	  	if (!$fp) {
	    	throw new Exception("Problem with $url, $php_errormsg");
	  	}
	  	$response = @stream_get_contents($fp);
	  	if ($response === false) {
	    	throw new Exception("Problem reading data from $url, $php_errormsg");
	  	}
	  	return $response;
	}

	function get_results()
	{
		echo "<pre>\n";
			
		// THIS STRING (CASE SENSITIVE) HAS THE LOGIN CREDENTIALS FOR THE SITE
		$post = "txtUserName=tprakass&txtPassword=Muruga78vam%24";

		//$post = "txtUserName=tprakass&txtPassword=Muruga78vam%24";

		// READ THE SITE PAGE WITH THE LOGIN FORM
		$baseurl = 'https://www.bescom.co.in/SCP/Myhome.aspx';

		// FOR FRONIUS USE AN EXPLICIT URL TO PROCESS THE LOGIN
		$posturl = 'https://www.bescom.co.in/SCP/Myhome.aspx';

		// WHERE TO GO AFTER THE LOGIN
		$nexturl = 'https://www.bescom.co.in/SCP/MyAccount/AccountSummary.aspx?Name=IAccountSummaryView';

		// SET UP OUR CURL ENVIRONMENT
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $baseurl);

		curl_setopt($ch, CURLOPT_FAILONERROR, TRUE);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);

		// CALL THE WEB PAGE
		$htm = curl_exec($ch);
		$err = curl_errno($ch);
		$inf = curl_getinfo($ch);
		if ($htm === FALSE)
		{
		    echo "\nCURL GET FAIL: $baseurl CURL_ERRNO=$err ";
		    var_dump($inf);
		    die();
		}

		// NOW POST THE DATA WE HAVE FILLED IN
		curl_setopt($ch, CURLOPT_URL, $posturl);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

		// WAIT A RESPECTABLE PERIOD OF TIME
		sleep(3);

		// CALL THE WEB PAGE TO COMPLETE THE LOGIN
		$xyz = curl_exec($ch);
		$err = curl_errno($ch);
		$inf = curl_getinfo($ch);
		if ($xyz === FALSE)
		{
		    echo "\nCURL POST FAIL: $posturl CURL_ERRNO=$err ";
		    var_dump($inf);
		}

		// NOW ON TO THE NEXT PAGE
		curl_setopt($ch, CURLOPT_URL, $nexturl);
		curl_setopt($ch, CURLOPT_POST, FALSE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, '');

		$xyz = curl_exec($ch);
		$err = curl_errno($ch);
		$inf = curl_getinfo($ch);
		if ($xyz === FALSE)
		{
		    echo "\nCURL 2ND GET FAIL: $posturl CURL_ERRNO=$err ";
		    var_dump($inf);
		}

		print_r($xyz); exit;
		// ACIVATE THIS TO SHOW OFF THE DATA WE RETRIEVED AFTER THE LOGIN
		// echo htmlentities($xyz);

		// REFINE THE DATA SOME
		$xyz = strip_tags($xyz, '<div><td>');

		$arr = explode('<div id="ctl00_MainContent_UpdatePanel1">', $xyz);
		// KEEP THE SECOND HALF
		$xyz = $arr[1];
		// ADD SOME INSURANCE ABOUT SPACING
		$xyz = str_replace('>', '> ', $xyz);
		$xyz = strip_tags($xyz);
		echo htmlentities($xyz);
		exit;
	}
	
	public function get_power_cut()
	{
		error_reporting(0);
		//phpinfo(); exitl
        $url = 'http://tneb.tnebnet.org/cpro/today.html';
		$this->load->library('dom_parser');
		
		$my_final_result_array = array();
		//$html = $this->dom_parser->file_get_html("http://tneb.tnebnet.org/newlt/consumerwise_gmc_report.php?encserno=VEFOR0VEQ098fDEyMXx8MDA5fHw4&rsno=MQ%3D%3D");
		$html = $this->dom_parser->file_get_html($url);
		$tag = $html->find('table',0);
		//print_r(strip_tags($tag->innertext));
		
		$time_a = explode("between",strip_tags($tag->innertext));
		//Get Starting time
		$time_1 = explode("to",$time_a[1]);
		$time_1_ampm =substr(trim($time_1[0]),-4);
		$time_1_num = explode($time_1_ampm,trim($time_1[0]));
		//Get Ending time
		$time_2 = explode("for",$time_1[1]);
		$time_2_ampm = substr(trim($time_2[0]),-4);
		$time_2_num = explode($time_2_ampm,trim($time_2[0]));
		
		$time_array = array(
							'start'=>array('time'=>$time_1_num[0],'ampm'=>$time_1_ampm),
							'end'=>array('time'=>$time_2_num[0],'ampm'=>$time_2_ampm)
							);
		//print_r($time_array);
		
		

		$tag2 = $html->find('table',1);
		if (!$tag2)
		{
			return '';
		}
		//var_dump($tag2);
		//echo $tag2;
		$table_row_data = $tag2->find('tr'); 

		//echo count($table_row_data);
		//echo $tag2;
	
		// $my_day_array = array();
		$my_data_temp = array();
		$power_cut_rows_count = -1;
		foreach ($table_row_data as $pc_rows ) 
		{
				
			if ($power_cut_rows_count == -1)
			{
				$power_cut_rows_count++;
				continue;
			}
			$date = trim(strip_tags($pc_rows->find('td',0)->innertext));

			$my_data_temp['date'] = $date;

			$my_data_temp['time'] = $time_array;
			$area_data = trim(strip_tags($pc_rows->find('td',1)->innertext));
			$area_data = trim(str_replace(array('--1','--2--'),'',$area_data));

			//$area_data = trim(str_replace(array('(',')'),',',(strip_tags($pc_rows->find('td',1)->innertext))));
			$area_data = str_replace(array("SOWCARPET WEST AREA : "), '', $area_data);
			//print_r($area_data);exit;
			//echo "<pre>";print_r($area_data);echo "<pre />";

			$temp_areas = explode(":",trim($area_data));
			//echo "<br />Data after exploding : <br />";
			//echo "<pre>";print_r($temp_areas);echo "<pre />";
			// continue;
			// get current name
			//$current_area_name = $temp_areas[0];
			//echo "<br />".$current_area_name."<br />";
			$areas_temp = array();

			for ($i=0; $i < count($temp_areas)-1 ; $i++) 
			{ 
				// echo "</ br >Exploded data</br >";
				// echo "<pre>";print_r($temp_areas[$i]);echo "</pre>";

				//echo "</br>/////</br>";
				$area_trimmed = explode(" AREA", $temp_areas[$i]);
				//echo "</br>Data after exploding AREA</br>";
				//echo "<pre>";print_r($area_trimmed);echo "</pre>";

				$area_temp_data = array();
				if ($i == 0) 
				{
					go_first_label:
					$current_area_name = trim($area_trimmed[0]);
					//echo "</br>".$current_area_name."</br>";
					//$i++;					
					if($current_area_name)
					{
						$cur_section_data = $this->get_section_id_by_name($current_area_name);
						//region_id,code section_id,name
						//print_r($cur_section_data);exit;
						// if($cur_section_data)
						// {							
							$area_temp_data['name'] = $current_area_name;
							$area_temp_data['section_id'] = ($cur_section_data['section_id'] !=NULL) ? $cur_section_data['section_id'] : '';
							$area_temp_data['region_id'] = ($cur_section_data['region_id'] != NULL) ? $cur_section_data['region_id'] : '';
						// }
						// else 
						// {
						// 	$area_temp_data['name'] = $current_area_name;
						// }
					
					} 
					
					
					//$temp_areas_explode_dot = explode(".", $temp_areas[$i]);
					//echo "<pre>";print_r($temp_areas_explode_dot);echo "</pre>";
					//echo $next_area_name =  strrchr($temp_areas[$i] , ".");

					$next_area_name =  strrpos($temp_areas[$i+1] , ".");
					if ($next_area_name == FALSE) 
					{
						$next_area_name =  strrpos($temp_areas[$i+1] , ",");
					}
					if ($next_area_name == FALSE)
						continue;
					// echo "<pre>".$next_area_name."< /pre>";
					// continue;
					// sub areas
					//echo "//</br>Sub Areas</br>";

					$subareas = substr($temp_areas[$i+1], 0,$next_area_name);

					//echo "< /br>".$subareas."< /br>";
					//continue;
					$sub_areas_exploded =  explode(",",$subareas);
					$sub_areas_by_area = array();
					foreach (array_filter($sub_areas_exploded) as $sub_area)
					{
						array_push($sub_areas_by_area, trim($sub_area));
					}
					$area_temp_data['sub_areas'] = $sub_areas_by_area;
					array_push($areas_temp,$area_temp_data);
					
					continue;
				} 

				//echo "<br>".$temp_areas[$i]."<br>";
				$area_temp_data = array();

				$last_dot_index =  strrpos($temp_areas[$i] , ".");

				$last_dot_index2 =  strrpos($temp_areas[$i] , ",");

				if ($last_dot_index != FALSE && $last_dot_index2 != FASLE) 
				{
					$last_dot_index = ($last_dot_index > $last_dot_index2) ? $last_dot_index : $last_dot_index2;
				} 
				else if ($last_dot_index == FASLE) 
				{
					$last_dot_index = $last_dot_index2;
				}
				else if ($last_dot_index2 == FALSE)

					goto go_first_label;

				else 
				{
					$last_dot_index = $last_dot_index2;
				}
				//echo "<br>".$last_dot_index."<br>";
				//var_dump($last_dot_index);
					// $area_name_temp = $temp_areas[$i];
				$area_name_temp = substr($temp_areas[$i], $last_dot_index+1);

				//echo "<br>".$area_name_temp."<br>";
				// echo $i;
				// echo $area_name_temp;

				//echo "</br>";

				$area_current = trim(str_replace(" AREA", "", $area_name_temp));
				//echo "<br>".$area_current."<br>";
				//echo $area_current;exit;
				//$area_temp_data['name'] = $area_current;

				if($area_current)
				{
					$cur_section_data = $this->get_section_id_by_name($area_current);
					//region_id,code section_id,name
					// if($cur_section_data)
					// {							
						$area_temp_data['name'] = $area_current;
						$area_temp_data['section_id'] = ($cur_section_data['section_id'] !=NULL) ? $cur_section_data['section_id'] : '';
						$area_temp_data['region_id'] = ($cur_section_data['region_id'] != NULL) ? $cur_section_data['region_id'] : '';
					// }
					// else 
					// {
					// 	$area_temp_data['name'] = $area_current;
					// 	$area_temp_data['section_id'] = $cur_section_data['section_id'];
					// 	$area_temp_data['region_id'] = $cur_section_data['region_id'];
					// }
				
				}
				

				$last_dot_index =  strrpos($temp_areas[$i+1] , ".");

				$last_dot_index2 =  strrpos($temp_areas[$i+1] , ",");

				if ($last_dot_index != FALSE && $last_dot_index2 != FASLE) 
				{
					$last_dot_index = ($last_dot_index > $last_dot_index2) ? $last_dot_index : $last_dot_index2;
				} 
				else if ($last_dot_index == FASLE) 
				{
					$last_dot_index = $last_dot_index2;
				}
				else if ($last_dot_index2 == FALSE)
					continue;

				else 
				{
					$last_dot_index = $last_dot_index2;
				}

				
				// echo "<pre>".$last_dot_index."< /pre>";
				// continue;
				// sub areas
				//echo "//</br>Sub Areas</br>";

				$subareas = substr($temp_areas[$i+1], 0,$last_dot_index);
				//echo "</br>";
				//echo "//</br>Sub Areas List</br>";
				// echo "< /br>".$subareas."< /br>";
				// continue;

				$sub_areas_exploded =  explode(",",$subareas);
				$sub_areas_by_area = array();
				foreach (array_filter($sub_areas_exploded) as $sub_area)
				{
					array_push($sub_areas_by_area, trim($sub_area));
				}
				$area_temp_data['sub_areas'] = $sub_areas_by_area;
				// print_r($area_temp_data);
				// exit;
				array_push($areas_temp,$area_temp_data);

				//echo "</br>";

				//echo "</br>//</br>";
				//echo "</br>/////</br>";
				//print_r(json_encode($areas_temp));
			}

			$my_data_temp['areas'] = $areas_temp;

			//print_r(json_encode($areas_temp));
			//exit;
			// foreach ($temp_areas as $temp_area) {
			//$my_datas_temp['day'] = $my_data_temp;
			//array_push($my_day_array,$my_data_temp);
			array_push($my_final_result_array,$my_data_temp);
			// }
			// echo "<br />";
			// echo trim($temp_area_data[0]);
			// echo "<br />";
			$power_cut_rows_count++;
		}
		//exit;
		//$temp_array = array('days'=>$my_day_array);
		//array_push($my_final_result_array,$temp_array);
		//print_r(json_encode($my_final_result_array));
		return($my_final_result_array);
		exit;

		// if($cur_area_name)
		// {
		// 	$cur_section_data = $this->get_section_id_by_name($cur_area_name);

		// 	//region_id,code section_id,name

		// 	if($cur_section_data)
		// 	{
		// 		$all_area_name[$j] = array("name"=>$cur_area_name,"section_id"=>$cur_section_data['section_id'],"region_id"=>$cur_section_data['region_id']);
		// 		$j++;
		// 	}
		
		// }

	}


	public function regexExtract_get($text, $regex, $regs, $nthValue)
	{
		if (preg_match($regex, $text, $regs)) 
		{
			$result = $regs[$nthValue];
		}
		else 
		{
			$result = "";
		}
		return $result;
	} 

	// retrieve usage history
	public function get_usage_history($ch, $url, $connection, $last_update = FALSE)
	{
		//$ch = curl_init();

		$data['ctl00$ctl00$MasterPageContentPlaceHolder$MasterPageContentPlaceHolder$ddlcustomeraccounts'] = $connection;

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

		// curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		// curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		// //curl_setopt($ch, CURLOPT_COOKIEJAR, $this->ckfile);
		
		// curl_setopt($ch, CURLOPT_COOKIEFILE, $this->ckfile);

		//$content=curl_exec($ch);

		// echo $content;exit;

		// $viewstate = $this->regexExtract_get($content,$regexViewstate,$regs,1);
		// 	echo $viewstate;exit;
		// //$file=file_get_contents($details_url);

		// $eventval = $this->regexExtract_get($content, $regexEventVal,$regs,1);

        
        //$data['__VIEWSTATE']=$viewstate;
       // $data['__EVENTVALIDATION']=$eventval;

		$content = curl_exec ($ch);

        //curl_close($ch);
		//echo $content;exit;

        
        $html = $this->dom_parser->str_get_html($content);

		$tag = $html->find('table',19);
		$usage_history_array = array();

		//echo $tag;exit;
		if($tag != "" && $tag != null)
		{
			$usage_history_count = -1;

			foreach($tag->find('tr') as $usage_history_row)
			{
				$usage_history = array();
				if ($usage_history_count == -1)
				{
					$usage_history_count++;
					continue;
				}


				$usage_history['usage'] = trim(strip_tags($usage_history_row->find('td',0)->innertext));
				$billing_date = trim(strip_tags($usage_history_row->find('td',1)->innertext));

				$billing_date = date_create_from_format('d-M-Y', $billing_date);
				
				if ($billing_date)
					$usage_history['billing_date'] = date_format($billing_date, 'Y-m-d');
				else
					$usage_history['billing_date'] = trim(strip_tags($usage_history_row->find('td',1)->innertext));
				if ($last_update)
				{
					if($usage_history['billing_date'] <= $last_update)
						continue;
				}
				$usage_history['avg_daily_usage'] = trim(strip_tags($usage_history_row->find('td',2)->innertext));
				$usage_history['present_reading'] = trim(strip_tags($usage_history_row->find('td',3)->innertext));
				$usage_history['no_days'] = trim(strip_tags($usage_history_row->find('td',4)->innertext));
				$usage_history['charge'] = trim(strip_tags($usage_history_row->find('td',5)->innertext));

				//$usage_history_array[$usage_history_count] = $usage_history;

				array_push($usage_history_array, $usage_history);

				$usage_history_count++;

			}
		}
		//echo $tag;
		
		//print_r(json_encode($usage_history_array));

		return $usage_history_array;
	}

	public function get_billing_history($billing_url)
	{
		// $ch = curl_init();
		// curl_setopt($ch, CURLOPT_URL, $billing_url);

		// curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		// curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		
		// curl_setopt($ch, CURLOPT_COOKIEFILE, $this->ckfile);

		// $content=curl_exec($ch);
		
  		// url_close($ch);
		//       //echo $content;
  		//       $html = $this->dom_parser->str_get_html($content);

		// $tag = $html->find('table',14);

		// //echo $tag;

		// $billing_history_array = array();
		// $billing_history_count = -1;

		// foreach($tag->find('tr') as $billing_history_row)
		// {
		// 	$billing_history = array();
		// 	if ($billing_history_count == -1)
		// 	{
		// 		$billing_history_count++;
		// 		continue;
		// 	}
		// 	$billing_history['bill_no'] = trim(strip_tags($billing_history_row->find('td',0)->innertext));
		// 	$billing_history['bill_amt'] = trim(strip_tags($billing_history_row->find('td',1)->innertext));
		// 	$billing_history['bill_date'] = trim(strip_tags($billing_history_row->find('td',2)->innertext));
			
		// 	$billing_history_array[$billing_history_count] = $billing_history;

		// 	$billing_history_count++;
		// }
		// //echo json_encode($billing_history_array);
		
		// return $billing_history_array;
        
	}

	// retrieve payment history
	public function get_payment_history($payment_url)
	{
		// $ch = curl_init();
		// curl_setopt($ch, CURLOPT_URL, $payment_url);

		// curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		// curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		
		// curl_setopt($ch, CURLOPT_COOKIEFILE, $this->ckfile);

		// $content=curl_exec($ch);
		
  //       curl_close($ch);
        
  //       $html = $this->dom_parser->str_get_html($content);

		// $tag = $html->find('table',16);

		// //echo $tag;
		
		// $payment_history_array = array();
		// $payment_history_count = -1;

		// foreach($tag->find('tr') as $payment_history_row)
		// {
		// 	$payment_history = array();
		// 	if ($payment_history_count == -1)
		// 	{
		// 		$payment_history_count++;
		// 		continue;
		// 	}
		// 	$payment_history['receipt_id'] = trim(strip_tags($payment_history_row->find('td',0)->innertext));
		// 	$payment_history['payment_amt'] = trim(strip_tags($payment_history_row->find('td',1)->innertext));
		// 	$payment_history['payment_date'] = trim(strip_tags($payment_history_row->find('td',2)->innertext));
		// 	$payment_history['payment_desc'] = trim(strip_tags($payment_history_row->find('td',3)->innertext));
			
		// 	$payment_history_array[$payment_history_count] = $payment_history;

		// 	$payment_history_count++;
		// }
		// //echo json_encode($payment_history_array);
		// //exit;
		// return $payment_history_array;
	}

	// retrieve both billing and payment history
	public function get_billing_payment_history($ch, $billing_payment_url, $connection, $last_update = FALSE)
	{
		//$ch = curl_init();

		$data['ctl00$ctl00$MasterPageContentPlaceHolder$MasterPageContentPlaceHolder$BillingHistoryView$ddlCustomerAccountNumber'] = $connection;

		curl_setopt($ch, CURLOPT_URL, $billing_payment_url);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

		// curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		// curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		
		// curl_setopt($ch, CURLOPT_COOKIEFILE, $this->ckfile);

		//$content=curl_exec($ch);
		

        /*$options = array(
			// CURLOPT_RETURNTRANSFER => TRUE, // return web page
			// CURLOPT_HEADER => FASLE, // don't return headers
			// CURLOPT_FOLLOWLOCATION => true, // follow redirects
			// CURLOPT_ENCODING => "", // handle all encodings
			// //CURLOPT_USERAGENT => "spider", // who am i
			// CURLOPT_AUTOREFERER => true, // set referer on redirect

			//CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
			//CURLOPT_TIMEOUT => 120, // timeout on response

			//CURLOPT_MAXREDIRS => 10, // stop after 10 redirects

			CURLOPT_POST => TRUE,
			CURLOPT_POSTFIELDS => $data,
		);
		*/
        //$ch = curl_init( $url );
		//curl_setopt_array($ch, $options);

		$content = curl_exec ($ch);


       // curl_close($ch);

        $html = $this->dom_parser->str_get_html($content);

        $billing_payment_history = array();
		$billing_history_array = array();
        // Code for billing history

        $tag = $html->find('table',14);

        if($tag != "" && $tag != null)
		{
			$billing_history_count = -1;

			foreach($tag->find('tr') as $billing_history_row)
			{
				$billing_history = array();
				if ($billing_history_count == -1)
				{
					$billing_history_count++;
					continue;
				}
				$billing_history['bill_no'] = trim(strip_tags($billing_history_row->find('td',0)->innertext));
				$billing_history['bill_amt'] = trim(strip_tags($billing_history_row->find('td',1)->innertext));
				
				$bill_date = trim(strip_tags($billing_history_row->find('td',2)->innertext));
				$bill_date = date_create_from_format('d-M-Y', $bill_date);

				if ($bill_date)
					$billing_history['bill_date'] = date_format($bill_date, 'Y-m-d');
				else
					$billing_history['bill_date'] = trim(strip_tags($billing_history_row->find('td',2)->innertext));
				
				if ($last_update)
				{
					if($billing_history['bill_date'] <= $last_update)
						continue;
				}
				//$billing_history_array[$billing_history_count] = $billing_history;
				array_push($billing_history_array, $billing_history);

				$billing_history_count++;
			}

			$billing_payment_history['billing_history'] = $billing_history_array;
		}

		//echo $tag;
        
		

		$tag = $html->find('table',16);

		$payment_history_array = array();

		if($tag != "" && $tag != null)
		{
			$payment_history_count = -1;

			foreach($tag->find('tr') as $payment_history_row)
			{
				$payment_history = array();
				if ($payment_history_count == -1)
				{
					$payment_history_count++;
					continue;
				}
				$payment_history['receipt_id'] = trim(strip_tags($payment_history_row->find('td',0)->innertext));
				$payment_history['payment_amt'] = trim(strip_tags($payment_history_row->find('td',1)->innertext));
				
				$payment_date = trim(strip_tags($payment_history_row->find('td',2)->innertext));
				$payment_date = date_create_from_format('d-M-Y', $payment_date);
				if ($payment_date)
					$payment_history['payment_date'] = date_format($payment_date, 'Y-m-d');
				else
					$payment_history['payment_date'] = trim(strip_tags($payment_history_row->find('td',2)->innertext));
				
				if($last_update)
					if ($payment_history['payment_date'] <= $last_update)
						continue;
				$payment_history['payment_desc'] = trim(strip_tags($payment_history_row->find('td',3)->innertext));
				
				//$payment_history_array[$payment_history_count] = $payment_history;

				array_push($payment_history_array,$payment_history);

				$payment_history_count++;
			}

			$billing_payment_history['payment_history'] = $payment_history_array;
		}

		//print_r($billing_payment_history);exit;

		return $billing_payment_history;
	}

	public function insert_usage_history_karnataka($usage_history_data,$current_insert_id)
	{
		if (count($usage_history_data) > 0)
		{

			$all_usage_history_array = array();

			foreach ($usage_history_data as $single_usage_history) 
			{
				
				$temp_array = array(
						'fk_connection_id'=>$current_insert_id,
						'usage'=>$single_usage_history['usage'],
						'billing_date'=>$single_usage_history['billing_date'],
						'avg_daily_usage'=>$single_usage_history['avg_daily_usage'],
						'present_reading'=>$single_usage_history['present_reading'],
						'no_days'=>$single_usage_history['no_days'],
						'charge'=>$single_usage_history['charge'],
					);

				array_push($all_usage_history_array, $temp_array);
				
			}

			$this->db->insert_batch('server_usage_history_karnataka', $all_usage_history_array);

		}
	}

	public function insert_billing_history_karnataka($billing_history_data, $current_insert_id)
	{
		
		if (count($billing_history_data) >0)
		{
			$all_billing_history_aray = array();

			foreach ($billing_history_data as $single_billing_history) 
			{
				$temp_array = array(
					'fk_connection_id'=>$current_insert_id,
					'bill_no'=>$single_billing_history['bill_no'],
					'bill_amt'=>$single_billing_history['bill_amt'],
					'bill_date'=>$single_billing_history['bill_date'],
				);

				array_push($all_billing_history_aray, $temp_array);
			}

			$this->db->insert_batch('server_billing_history_karnataka', $all_billing_history_aray);
		}
	}

	public function insert_payment_history_karnataka($payment_history_data, $current_insert_id)
	{
		if (count($payment_history_data) > 0)
		{

			$all_payment_history_array = array();

			foreach ($payment_history_data as $single_payment_history) 
			{
				$temp_array = array(
						'fk_connection_id'=>$current_insert_id,
						'receipt_id'=>$single_payment_history['receipt_id'],
						'payment_amt'=>$single_payment_history['payment_amt'],
						'payment_date'=>$single_payment_history['payment_date'],
						'payment_desc'=>$single_payment_history['payment_desc'],
					);

				array_push($all_payment_history_array, $temp_array);
			}

			$this->db->insert_batch('server_payment_history_karnataka', $all_payment_history_array);
		}
	}

	public function add_connection_data_karnataka($all_fetched_data)
	{
		
		if ($all_fetched_data)
		{
			$temp_data = array(
					'connection_id'        => $all_fetched_data['connection'],
					'name'                 => $all_fetched_data['name'],
					'address'              => $all_fetched_data['summary']['correspondence_address'],
					'account_type'         => $all_fetched_data['summary']['account_type'],
					'bill_cycle'           => $all_fetched_data['summary']['bill_cycle'],
					'sub_division'         => $all_fetched_data['summary']['sub_division'],
					'existing_load'        => $all_fetched_data['summary']['existing_load'],
					'existing_tarrif_code' => $all_fetched_data['summary']['existing_tariff_code'],
					'existing_tarrif_name' => $all_fetched_data['summary']['existing_tariff_name'],
					'account_status'       => $all_fetched_data['status'],
					'state'                => 'karnataka',
					'provider'             => 'bescom',
					
				);
			$status = $this->db->insert('server_connection_list_karnataka', $temp_data);

			if ($status)
			{
				$current_insert_id = $this->db->insert_id();
				$this->insert_usage_history_karnataka($all_fetched_data['usage_history'], $current_insert_id);
				$this->insert_billing_history_karnataka($all_fetched_data['billing_history'], $current_insert_id);
				$this->insert_payment_history_karnataka($all_fetched_data['payment_history'], $current_insert_id);
			}
		}
	}

	public function delete_usage_history($connection_no, $last_update)
	{

		$this->db->where('fk_connection_id', $connection_no);

		if ($last_update)
			$this->db->where('billing_date >', $last_update);
		
		$this->db->delete('server_usage_history_karnataka');


	}

	public function delete_billing_history($connection_no, $last_update)
	{
		$this->db->where('fk_connection_id', $connection_no);

		if ($last_update)
			$this->db->where('bill_date >', $last_update);

		$this->db->delete('server_billing_history_karnataka');
	}

	public function delete_payment_history($connection_no, $last_update)
	{
		$this->db->where('fk_connection_id', $connection_no);

		if ($last_update)
			$this->db->where('payment_date >', $last_update);

		$this->db->delete('server_payment_history_karnataka');
	}

	public function update_connection_data_karnataka($all_fetched_data, $connection_number, $last_update)
	{
		if ($all_fetched_data)
		{
			$this->delete_usage_history($connection_number, $last_update);
			$this->delete_billing_history($connection_number, $last_update);
			$this->delete_payment_history($connection_number, $last_update);

			$this->insert_usage_history_karnataka($all_fetched_data['usage_history'], $connection_number);
			$this->insert_billing_history_karnataka($all_fetched_data['billing_history'], $connection_number);
			$this->insert_payment_history_karnataka($all_fetched_data['payment_history'], $connection_number);

		}
	}
	// To store karnataka data
	public function save_karnataka_data($username, $password, $last_update)
	{
		$all_fetched_data = $this->get_karnataka($username, $password, $last_update);
		//return $all_fetched_data; exit;
		if (count($all_fetched_data['data']) > 0)
		{

			foreach ($all_fetched_data['data'] as $single_connection_data) 
			{

				$connection_number = $single_connection_data['connection'];

				$connection_detail = $this->is_connection_id_exist_karnataka($connection_number);
				
				if ($connection_detail)
				{

					$this->update_connection_data_karnataka($single_connection_data, $connection_detail['id'], $last_update);
				
				}
				else
				{	

					$this->add_connection_data_karnataka($single_connection_data);

				}

			}

		}

		return $all_fetched_data;
	}

	public function get_pilot()
	{

		$request = $this->input->post('request');
		
		//$request = '{"Data":[{"Minute":"3","Hour":"2","Time":"AM","Date":"31-08-2015","Area":"09-210"},{"Minute":"3","Hour":"3","Time":"AM","Date":"31-08-2015","Area":"09-270"},{"Minute":"6","Hour":"6","Time":"AM","Date":"31-08-2015","Area":"09-288"}]}';
		if(!$request  OR ($request == '' OR $request == NULL))
		{
			$status['status'] = 'success';
			$status['message'] = 'invalid request';
			$status['data'] = 'no request parameter';
			print_r(json_encode($status));
			exit;
		}

		$request = json_decode($request);
		
		//print_r($request);

		//echo count($request->Data);

		
		if ($request)
		{
			$data_temp = array();

			foreach ($request->Data as $data_key =>$data) {

				$area_date = array();

				// echo "<pre>".$data->Hour."</pre>";
				// echo "<pre>".$data->Minute."</pre>";
				// echo "<pre>".$data->Time."</pre>";
				// echo "<pre>".$data->Date."</pre>";

				//echo "<pre>".$data->Area."</pre>";

				//echo "<pre>".$data->Date." ".$data->Hour.":".$data->Minute.":00 ".$data->Time."</pre>";
				

				$area = $data->Area;

				//$post_date = $data->Date." ".$data->Hour.":".$data->Minute.":00 ".$data->Time;

				$area_date['post_date'] = $data->Date;
				$area_date['hour'] = $data->Hour;
				$area_date['minute'] = $data->Minute;
				$area_date['time'] = $data->Time;
				$area_date['area'] = $data->Area;

				//$area_date['date'] = $post_date;

				// str_replace(PHP_EOL, '', $data->Date);

				array_push($data_temp, $area_date);

				//print_r(json_encode($area_date));

			}

			if ($data_temp)

				$this->mobile_model->add_user_request($data_temp);

			return 
				$data_temp;

			
		}
		else
			echo "Invalid Post Data";

		exit;

		
		
		// $data = $this->mobile_model->get_pilot($request);

		// if ($data)
		// {
		// 	$status = 'success';
		// 	$message = 'data retrieved';
		// }
		// else
		// {
		// 	$status = 'error';
		// 	$message = 'invalid request';
		// }		

		// print_r(json_encode(array('status'=>$status, 'message'=>$message, 'data'=>$data)));
		
		// exit;		
		
	}


	public function add_user_request($user_request_data)
	{
		$this->db->insert_batch('user_request', $user_request_data);
	}


	public function store_user($gcm_regid, $connection_id) {
        // insert user into database

        $data = array(
        		'gcm_regid' => $gcm_regid,
        		'fk_connection_id' => $connection_id,
        		'created_at' => date('Y-m-d'),
    		);


        $query = $this->db->limit(1)->get_where('reg_users', array('gcm_regid'=>$gcm_regid));

        if ($query->num_rows() == 1)
        {
        	$id = $query->row()->id;

        	return $this->db->update('reg_users', $data, array('id'=>$id));
        }
        else
        {
        	return $this->db->insert('reg_users',$data);
        }

        // if ($result) {

        //     // get user details
        //     $id =  mysqli_insert_id($this->con); // last inserted id
        //     $result = mysqli_query($this->con,"SELECT * FROM reg_users WHERE id = $id") or die(mysqli_error($this->con));
        //     // return user details
        //     if (mysqli_num_rows($result) > 0) {
        //         return mysqli_fetch_array($result,MYSQLI_ASSOC);
        //     } else {
        //         return false;
        //     }

        // } else {

        //     return false;

        // }
    }

    public function get_all_registration_ids()
    {
    	$query = $this->db->distinct('gcm_regid')->get('reg_users');

    	if ($query->num_rows() > 0)
    	{
    		return $query->result();
    	}
    	else
    		return FALSE;
    }

    public function send_message($message)
    {

    	$data = $this->get_all_registration_ids();
    	if ($data)
    	{

    		$gcm_regids = array(); 

    		// foreach ($data as $gcm_data) {

    		// 	array_push($gcm_regids, $gcm_data->gcm_regid);

    		// }
    		array_push($gcm_regids, 'APA91bFHCdhjc156ibRt9mclYTmVSeP55HYemRyb_6nCaFiIgR_iNnnU6EQIHu2GaWswGRzGs8YuQ0DaDdFPAj0VqFB9BE0nBI0V6FBo8q1ZMHVnYE-Red6szBTHszM32L5U1oOdFiYL');
    		//array_push($gcm_regids, 'APA91bHNQB7kpH3nG0m4a4jetr-pG1tG7oUhuGPpHYkV0moewc5lJB1UsPuwKxK_0UQwvdj6kwI6f-fXKneOPb3sqgg6IBtGi9Z2mk3saCZ9xs5Fm99rF_LuHiDnRDvah82Z_DtzP-SR');
    		$this->send_push_message($gcm_regids, $message);
    	}

    }


    public function send_push_message($registatoin_ids, $message)
    {
    	$url = 'https://android.googleapis.com/gcm/send';
 
        $fields = array(
            'registration_ids' => $registatoin_ids,
            'data' => $message,
        );
 
        $headers = array(
            'Authorization: key=' . GOOGLE_API_KEY,
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();
 
        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
 
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 
        // disable SSL certificate support
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
 
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
 
        // execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
 
        // Close connection
        curl_close($ch);
        echo $result;
    }


    public function get_all_connection_chennai()
    {
    	
    	$query  = $this->db->get('server_connection_list');

    	if ($query->num_rows() > 0)
    	{

    		$connection_datas = $query->result();

    		$areas_array = array();
    		foreach ($connection_datas as $connection_data) {

    			$temp_area_con = $connection_data->serviceno;
    			$area = substr($temp_area_con, 0, 6);
    			array_push($areas_array, $area);
    		}

    		return $areas_array;

    	}
    	else
    	{
    		return FALSE;
    	}

    }

}