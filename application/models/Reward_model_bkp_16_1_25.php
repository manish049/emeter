<?php  defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */

class Reward_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function get_reward_list()
	{

		$this->db->select('ub.ub_id, ub.user_email, r.eventname, ub.participant_id, ub.total_balance, ub.paid_balance, ub.remaining_balance');
		$this->db->from('user_conversion_balance ub');
		$this->db->join('reward_conversion r', 'r.emailid = ub.user_email AND r.participantid = ub.participant_id');
		$this->db->group_by('ub.user_email');
		
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			$result = $query->result_array();

			$query->free_result();

			return $result;
		}

		return FALSE;

	}

	public function pay_reward($user_balance_id)
	{

		$this->db->set('paid_balance', 'paid_balance + '. 50, FALSE);
		$this->db->set('remaining_balance', 'remaining_balance - '. 50, FALSE);
		$this->db->set('last_paid_date', $this->general->get_local_time('now'));

		$this->db->where('ub_id', $user_balance_id);

		$this->db->update('user_conversion_balance');

		return $this->db->affected_rows();

	}

	/**
	 * this method retrieves reward details by participant id
	 * 
	 * @param  integer $participantid participant id
	 * @return mixed                 
	 */
	public function get_reward_detail($participantid = 0, $emailid)
	{
		$this->db->select('eventname, participantid, emailid, name, rewarddtlid, rewarded_date , amount, reward_unit, reward_type');
		$this->db->from('reward_conversion');
		$this->db->where('participantid', $participantid);
		$this->db->where('emailid', $emailid);

		$query = $this->db->get();
		// echo $this->db->last_query();
		if($query->num_rows() > 0)
		{

			$result = $query->result_array();
			$query->free_result();

			return $result;

		}
		return FALSE;
	}

}