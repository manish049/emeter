<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/



class Mobile_model extends CI_Model
{
	function __construct()
    {
        // Construct our parent class
        parent::__construct(); 
        $this->load->helper('inflector');
        $this->load->library('dom_parser');

    }
	public function get_region_list($last_update)
	{
		
		$this->db->select_max('last_update');
		
		$new_last_update_query = $this->db->get('emt_region_list');
		if ($new_last_update_query->num_rows() > 0) 
		{
			$res_last_update = $new_last_update_query->row_array();		
			$new_last_update = $res_last_update['last_update'];		
		}
		else
		{
			$new_last_update = 'no';
		}
		
		if($new_last_update != $last_update )
		{	
			if($last_update)
			{
				$query = $this->db->get_where("emt_region_list",array('last_update >'=>$last_update));
			}
			else
			{
				
				$query = $this->db->get("emt_region_list");
				
			}
				
			if ($query->num_rows() > 0) 
			{
				$data_all=$query->result_array();	
				$data= array('status'=>'success','last_update'=>$new_last_update,'data'=>$data_all	);	
			}
			else
			{
				$data = false;
			}
			
			$query->free_result();	
		}
		else
		{
			$data =false;
		}
			
		return $data;

	}

	public function get_section_list($last_update)
	{
		
		$this->db->select_max('last_update');
		$new_last_update_query = $this->db->get('emt_section_list');

		if ($new_last_update_query->num_rows() > 0) 
		{
			$res_last_update = $new_last_update_query->row_array();		
			$new_last_update = $res_last_update['last_update'];		
		}
		else
		{
			$new_last_update = 'no';
		}
		
		if($new_last_update != $last_update )
		{	
			if($last_update)
			{
				$query = $this->db->get_where("emt_section_list",array('last_update >'=>$last_update));
			}
			else
			{
				$query = $this->db->get("emt_section_list");
				
			}
				
			if ($query->num_rows() > 0) 
			{
				$data_all=$query->result_array();	
				$data= array('status'=>'success','last_update'=>$new_last_update,'data'=>$data_all	);	
			}
			else
			{
				$data = false;
			}
			
			$query->free_result();	
		}
		else
		{
			$data =false;
		}
			
		return $data;

	
	}
	
	public function get_section_list_by_region_id($last_update,$region_id)
	{
		if(!$region_id)
		{
			$this->get_section_list($last_update);
			//exit;
		}
		
		$this->db->select_max('last_update');
		$new_last_update_query = $this->db->get('emt_section_list');

		if ($new_last_update_query->num_rows() > 0) 
		{
			$res_last_update = $new_last_update_query->row_array();		
			$new_last_update = $res_last_update['last_update'];		
		}
		else
		{
			$new_last_update = 'no';
		}
		
		if($new_last_update != $last_update )
		{	
			if($last_update)
			{
				$query = $this->db->get_where("emt_section_list",array('last_update >'=>$last_update,'region_id'=>$region_id));
			}
			else
			{
				$query = $this->db->get_where("emt_section_list",array('region_id'=>$region_id));
				
			}
				
			if ($query->num_rows() > 0) 
			{
				$data_all=$query->result_array();	
				$data= array('status'=>'success','last_update'=>$new_last_update,'data'=>$data_all	);	
			}
			else
			{
				$data = false;
			}
			
			$query->free_result();	
		}
		else
		{
			$data =false;
		}
			
		return $data;

	
	}
	
	public function get_distribution_list($last_update)
	{
		
		$this->db->select_max('last_update');
		$new_last_update_query = $this->db->get('emt_distribution_list');

		if ($new_last_update_query->num_rows() > 0) 
		{
			$res_last_update = $new_last_update_query->row_array();		
			$new_last_update = $res_last_update['last_update'];		
		}
		else
		{
			$new_last_update = 'no';
		}
		
		if($new_last_update != $last_update )
		{	
			if($last_update)
			{
				$query = $this->db->get_where("emt_distribution_list",array('last_update >'=>$last_update));
			}
			else
			{
				$query = $this->db->get("emt_distribution_list");
				
			}
				
			if ($query->num_rows() > 0) 
			{
				$data_all=$query->result_array();	
				$data= array('status'=>'success','last_update'=>$new_last_update,'data'=>$data_all	);	
			}
			else
			{
				$data = false;
			}
			
			$query->free_result();	
		}
		else
		{
			$data =false;
		}
			
		return $data;

	
	}
	
	public function get_distribution_list_by_section_id($last_update,$section_id)
	{
		if(!$section_id)
		{
			$this->get_section_list($last_update);
			//exit;
		}
		
		$this->db->select_max('last_update');
		$new_last_update_query = $this->db->get('emt_distribution_list');
		if ($new_last_update_query->num_rows() > 0) 
		{
			$res_last_update = $new_last_update_query->row_array();		
			$new_last_update = $res_last_update['last_update'];		
		}
		else
		{
			$new_last_update = 'no';
		}
		
		if($new_last_update != $last_update )
		{	
			$this->db->distinct();
		    $this->db->select();
   		 	$this->db->from("emt_distribution_list");        
	
			if($last_update)
			{
				$this->db->where('last_update >',$last_update);
				$this->db->where('section_id',$section_id);
				$query = $this->db->get();
			}
			else
			{
				$this->db->where('section_id',$section_id);
				$query = $this->db->get();
				
			}
				
			if ($query->num_rows() > 0) 
			{
				$data_all=$query->result_array();	
				$data= array('status'=>'success','last_update'=>$new_last_update,'data'=>$data_all	);	
			}
			else
			{
				$data = false;
			}
			
			$query->free_result();	
		}
		else
		{
			$data =false;
		}
			
		return $data;

	
	}
	
	public function get_distribution_list_by_reg_and_section_id($last_update,$region_id,$section_id)
	{
		if(!$region_id)
		{
			$this->get_distribution_list_by_section_id($last_update,$section_id);
			exit;
		}
		if(!$section_id)
		{
			$this->get_section_list($last_update);
			//exit;
		}
		
		$this->db->select_max('last_update');
		$new_last_update_query = $this->db->get('emt_distribution_list');
		if ($new_last_update_query->num_rows() > 0) 
		{
			$res_last_update = $new_last_update_query->row_array();		
			$new_last_update = $res_last_update['last_update'];		
		}
		else
		{
			$new_last_update = 'no';
		}
		
		if($new_last_update != $last_update )
		{	
			$this->db->distinct();
		    $this->db->select();
   		 	$this->db->from("emt_distribution_list");        
	
			if($last_update)
			{
				$this->db->where('last_update >',$last_update);
				$this->db->where('section_id',$section_id);
				$this->db->where('region_id',$region_id);
				$query = $this->db->get();
			}
			else
			{
				$this->db->where('section_id',$section_id);
				$this->db->where('region_id',$region_id);
				$query = $this->db->get();
				
			}
				
			if ($query->num_rows() > 0) 
			{
				$data_all=$query->result_array();	
				$data= array('status'=>'success','last_update'=>$new_last_update,'data'=>$data_all	);	
			}
			else
			{
				$data = false;
			}
			
			$query->free_result();	
		}
		else
		{
			$data =false;
		}
			
		return $data;

	
	}
	
	public function get_appliance_list($last_update)
	{
		
		$this->db->select_max('last_update');
		$new_last_update_query = $this->db->get('appliance_list');

		if ($new_last_update_query->num_rows() > 0) 
		{
			$res_last_update = $new_last_update_query->row_array();		
			$new_last_update = $res_last_update['last_update'];		
		}
		else
		{
			$new_last_update = 'no';
		}
		
		if($new_last_update != $last_update )
		{	
			if($last_update)
			{
				$query = $this->db->get_where("appliance_list",array('last_update >'=>$last_update));
			}
			else
			{
				$query = $this->db->get("appliance_list");
				
			}
				
			if ($query->num_rows() > 0) 
			{
				$data_all=$query->result_array();	
				$data= array('status'=>'success','last_update'=>$new_last_update,'image_path'=>base_url().'assets/images/appliance/','data'=>$data_all);	
			}
			else
			{
				$data = false;
			}
			
			$query->free_result();	
		}
		else
		{
			$data =false;
		}
			
		return $data;

	
	}
	
	public function get_appliance_by_id($last_update,$id)
	{
		if(!$id)
		{
			$this->get_section_list($last_update);
			//exit;
		}
		
		$this->db->select_max('last_update');
		$new_last_update_query = $this->db->get('appliance_list');

		if ($new_last_update_query->num_rows() > 0) 
		{
			$res_last_update = $new_last_update_query->row_array();		
			$new_last_update = $res_last_update['last_update'];		
		}
		else
		{
			$new_last_update = 'no';
		}
		
		if($new_last_update != $last_update )
		{	
			if($last_update)
			{
				$query = $this->db->get_where("appliance_list",array('last_update >'=>$last_update,'id'=>$id));
			}
			else
			{
				$query = $this->db->get_where("appliance_list",array('id'=>$id));
				
			}
				
			if ($query->num_rows() > 0) 
			{
				$data_all=$query->result_array();	
				$data= array('status'=>'success','last_update'=>$new_last_update,'data'=>$data_all	);	
			}
			else
			{
				$data = false;
			}
			
			$query->free_result();	
		}
		else
		{
			$data =false;
		}
			
		return $data;

	
	}
	
	public function get_section_id_by_name($name)
	{
		if(!$name)
		{
			return false;
			//exit;
		}
		$this->db->select('emt_region_list.code region_id,emt_section_list.code section_id,emt_section_list.name');
		$this->db->join('emt_region_list','emt_region_list.id = emt_section_list.region_id');
		$query = $this->db->get_where("emt_section_list",array('emt_section_list.name'=>$name));
		if ($query->num_rows() > 0) 
		{
			return $query->row_array();	
		}
		$query->free_result();	
		return false;

	
	}
	public function is_connection_id_exist($connection_id)
	{
			$query = $this->db->get_where("server_connection_list",array('connection_id'=>$connection_id));
			if ($query->num_rows() > 0) 
			{
				$data=$query->row_array();	
			}
			else
			{
				$data = false;
			}
			return $data;
	}

	public function is_connection_id_exist_karnataka($connection_id)
	{
			$query = $this->db->get_where("server_connection_list_karnataka",array('connection_id'=>$connection_id));
			if ($query->num_rows() > 0) 
			{
				$data=$query->row_array();	
			}
			else
			{
				$data = false;
			}
			return $data;
	}
	
	public function do_insertion_userdata($service_no_ip)
	{
		
		$all_data_fetched = $this->get_fetched_data_server($service_no_ip);
		
		// print_r(json_encode($all_data_fetched));exit;
		// print_r($all_data_fetched);
		if(count($all_data_fetched['usage']) >0)
		{
			 $ud_assessment_date = $all_data_fetched['usage'][0]['assessment_date'];
	  		 $ud_reading=$all_data_fetched['usage'][0]['reading'];
	  		 $ud_units = $all_data_fetched['usage'][0]['units'];
	  		 $ud_amount = $all_data_fetched['usage'][0]['amount'];
	  		 $ud_payment_date = $all_data_fetched['usage'][0]['payment_date'];
	   		 $ud_status = $all_data_fetched['usage'][0]['status'];
		}
		else
		{
			 $ud_assessment_date = "Assessment Entry Date";
	  		 $ud_reading= "Reading";
	  		 $ud_units = "Consumed Unit";
	  		 $ud_amount = "Total BillAmount(Rs.)";
	  		 $ud_payment_date = "Payment Date";
	   		 $ud_status = "AssessmentStatus";
		}
		
		$state = 'chennai';
		$provider = 'tneb';
		
		$data = array(

			'connection_id' => $service_no_ip,
		   	'serviceno' => $all_data_fetched['ServiceNo'] ,
		   	'name' => $all_data_fetched['Name'] ,
		   	'region' => $all_data_fetched['Region'],
	   		'phase' => $all_data_fetched['Phase'],
		   	'circle' => $all_data_fetched['Circle'],
		   	'section' => $all_data_fetched['Section'],
		   	'load' => $all_data_fetched['Load'],
		   	'distribution' => $all_data_fetched['Distribution'],
		   	'meterno' => $all_data_fetched['MeterNo'],
		   	'connectionnumber' => $all_data_fetched['ConnectionNumber'],
		   	'address' => $all_data_fetched['Address'],
		   	'servicestatus' => $all_data_fetched['ServiceStatus'],		   
		   	'ud_assessment_date' => $ud_assessment_date,
		   	'ud_reading' => $ud_reading,
		   	'ud_units' => $ud_units,
		   	'ud_amount' => $ud_amount,
		   	'ud_payment_date' => $ud_payment_date,
		   	'ud_status' => $ud_status,
		   	'state' => $state,
		   	'provider' => $provider,
	   
		);
		//print_r(json_encode($data));
		//exit;

		$status = $this->db->insert('server_connection_list', $data); 
		// $current_insert_id = 1;
		// var_dump($status);
		// exit;
		if($status)
		{
			$current_insert_id = $this->db->insert_id();
			$this->insert_user_data($all_data_fetched,$current_insert_id,true);
		}
		
		return $all_data_fetched;
	}
	
	public function insert_user_data($all_data_fetched,$current_insert_id,$is_first_time = false)
	{
	
		if((count($all_data_fetched['usage']) > 1 && $is_first_time) ||(count($all_data_fetched['usage']) > 0 && !$is_first_time) )
		{
			$all_data_usage_batch_insert = array();
			$count_loop = 0;
			foreach($all_data_fetched['usage'] as $single_data_usage)
			{
				if($count_loop == 0 && count($all_data_fetched['usage']) > 1)
				{
					$count_loop=$count_loop+1;
					continue;
				}

				$cur_data = array(
								  'fk_connection_id' => $current_insert_id ,
								  'assessment_date' => $single_data_usage['assessment_date'],
								  'reading' => $single_data_usage['reading'],
								  'units' => $single_data_usage['units'],
								  'amount' => $single_data_usage['amount'],
								  'payment_date' =>   $single_data_usage['payment_date'],
								  'status' => $single_data_usage['status']
								);
				array_push($all_data_usage_batch_insert,$cur_data);
			}
			$this->db->insert_batch('server_usage_data', $all_data_usage_batch_insert); 
		}
			
	}
	
	public function do_update_userdata($service_no_ip,$connection_detail,$last_update)
	{
		//NOTE: $last_update is max assessment_date of client;

		$today_date = date("Y-m-d");
		
		if($today_date == $last_update)
		{
			$all_data_fetched = $this->get_fetched_data_server($service_no_ip,$connection_detail);
			return $all_data_fetched;
			exit;
		}
		
		$this->db->select_max('assessment_date');
		$this->db->where('fk_connection_id',$connection_detail['id']);
		$this->db->order_by("assessment_date", "desc");
		$query = $this->db->get("server_usage_data");

		if ($query->num_rows() > 0) 
		{
			$max_date = $query->row_array();	
		}
		else
		{
			$max_date ="";
		}
		
		if($max_date == "")
		{
			$all_data_fetched = $this->get_fetched_data_server($service_no_ip);
		}
		else
		{
			$all_data_fetched = $this->get_fetched_data_server($service_no_ip,$last_update);
		}

		//$all_data_fetched = $this->get_fetched_data_server($service_no_ip,$last_update);
		//print_r(json_encode($all_data_fetched)); exit;
		
		if (!$last_update) 
		{
			$this->delete_user_data($connection_detail['id'],FALSE);
		}
		else
		{
			$this->delete_user_data($connection_detail['id'],$last_update);
		}
		
		//exit;
		// print_r(json_encode($all_data_fetched));
		$this->insert_user_data($all_data_fetched,$connection_detail['id'],false);
		// exit;
		return $all_data_fetched;

	}

	public function delete_user_data($connection_id = FALSE, $assessment_date = FALSE)
	{
		if ($connection_id ==FALSE) 
		{
			return;
		}
		
		$this->db->where('fk_connection_id',$connection_id);
		//return $assessment_date;exit;
		if ($assessment_date != FALSE)
		{
			$this->db->where('assessment_date >',$assessment_date);
			//return $this->db->get('server_usage_data')->result();
		}

		$this->db->delete('server_usage_data');

	}
	
	public function get_fetched_data_server($service_no_ip,$last_update = false)
	{		
		error_reporting(0);

		//	phpinfo(); exitl
		//	$last_update ="2015-1-1";
		if($last_update)
		{
			$temp_d_format = date_create_from_format("Y-m-d",$last_update);

			if($temp_d_format)
			{
				$last_update = date_format($temp_d_format,"Y-m-d");
			}
			else
			{

				$last_update = false;
				$error_result['status'] = "error";
				$error_result['data'] = "Sorry,last update date is invalid.";
				print_r(json_encode($error_result)); exit;	

			}
		}
		//echo $last_update; exit;
	
		$regno = substr($service_no_ip, 0, 2); // first 2 digits

		if($regno !=  ('01' || '02' || '03' || '04'|| '05' || '06' || '07' || '08' || '09'))
		{
			$error_result['status'] = "error";
			$error_result['data'] = "Sorry, Your service no. is not valid. Please try with proper input..";
			print_r(json_encode($error_result)); exit;	
		}


		$code = $regno; // same as the reg no
        $sec = substr($service_no_ip,2, 3); // next 3 digits
        $dist = substr($service_no_ip,5, 3); // next 3 digits
        $serno = substr($service_no_ip,8, 99); // remaining digits
        $temp_sn = $serno;
        $disp_service_no = $regno . '-' . $sec. '-' . $dist . '-'. $serno;

		$loop_cur_count = -1;
		$html = "";
		$extra_ordinary = FALSE;
		$iframe = FALSE;
		if($regno == 2 || $regno ==4)
		{
			
			if(strlen($serno) != 4 )
			{

				switch (strlen($serno))
				{
					case 1:
					$serno = "000".$serno;
					break;
					case 2:
					$serno = "00".$serno;
					break;
					case 3:
					$serno = "0".$serno;
					break;
					default:
					break;
					
				}

				$service_no_ip = $regno.$sec.$dist.$serno;
			}

			if (($regno == 4 || $regno == 2)  && strlen($temp_sn)==3) 
			{
				// echo "serno_length: three<br />";
				$serno = $temp_sn;
				$service_no_ip = $regno.$sec.$dist.$serno;
			}
			elseif($regno == 4 && strlen($temp_sn) == 2)
			{
				// echo "serno_length: two<br />";
				$serno = $temp_sn;
				$service_no_ip = $regno.$sec.$dist.$serno;

			}

			//echo $service_no_ip."<br />";
			$url = 'https://wss.tangedco.gov.in/wss/AccSummaryNew.htm?scnumber='.urlencode($service_no_ip);


			$html = $this->dom_parser->file_get_html($url);
			// print_r($html)

			if (strpos($html,'Invalid Input') != FALSE) 
			{
				
				$extra_ordinary = TRUE;		

				// added 2016-1-22
				// for connection no: service number 0256700551 (connection starting with 2 and having 2 digit) 
				if ($regno == 2 && strlen($temp_sn)==2) 
				{
					
					$serno = $temp_sn;
					
				}	

    			$plainserno = 'TANGEDCO||'. $sec .'||'. $dist. '||' . $serno; // introduced as TANGEDCO changed the format to base64 encoded string
            	
            	$encserno = base64_encode($plainserno);
            	
			 	$plainrsno = $regno;
			 
				while(substr($plainrsno,0, 1) == '0')
				{
					$plainrsno = substr($plainrsno, 1, strlen($plainrsno));
				}// remove leading zeroes
								 
				$rsno = base64_encode($plainrsno);
				
				$url = 'http://tneb.tnebnet.org/newlt/consumerwise_gmc_report.php?encserno='.urlencode($encserno).'&rsno='.urlencode($rsno);
				
				$html = $this->dom_parser->file_get_html($url);		
				
				// echo $html;exit;		
			}
		}
		else
		{
			// For breaking security while call url
			$plainserno = 'TANGEDCO||'. $sec .'||'. $dist. '||' . $serno; // introduced as TANGEDCO changed the format to base64 encoded string
            $encserno = base64_encode($plainserno);
			$plainrsno = $regno;
			 
			while(substr($plainrsno,0, 1) == '0')
			{
				$plainrsno = substr($plainrsno,1, strlen($plainrsno));
			}// remove leading zeroes
								 
			$rsno = base64_encode($plainrsno);
			
			$url = 'http://tneb.tnebnet.org/newlt/consumerwise_gmc_report.php?encserno='.urlencode($encserno).'&rsno='.urlencode($rsno);
						
			$html = $this->dom_parser->file_get_html($url);
			
			/**
			*	2015-11-17
			*	New modification for iframe issues		
			**/
			
			/**
			*	2015-11-18
			*	New modification for zeros issues start
			*
			**/
			$tag = $html->find('table',7);

			if($tag == "" || $tag == null)
			{
				$iframe = TRUE;
				if(strlen($serno) != 4 )
				{
					switch (strlen($serno))
					{
						case 1:
						$serno = "000".$serno;
						break;
						case 2:
						$serno = "00".$serno;
						break;
						case 3:
						$serno = "0".$serno;
						break;
						default:
						break;
						
					}
					$service_no_ip = $regno.$sec.$dist.$serno;
				}	

				/* 2015-11-18 New modification for zeros issues	End */
				$url = 'https://wss.tangedco.gov.in/wss/AccSummaryNew.htm?scnumber='.urlencode($service_no_ip);
				$html = $this->dom_parser->file_get_html($url);	
				
			}


		}

		//echo $html;exit;

		$tag = $html->find('table',7);	

		if (strpos($tag,'Meter Change') != FALSE) 
		{
			$tag = $html->find('table',10);
			
		}	
		else if($tag && urlencode($tag->innertext) == "++%3Ctr+align%3D%22center%22%3E%3Ctd+align%3D%22center%22%3E++%3C%2Ftd%3E%3C%2Ftr%3E" || urlencode($tag->innertext) == "+%3Ctr+align%3D%22center%22%3E%3Ctd+align%3D%22center%22%3E+%3C%2Ftd%3E%3C%2Ftr%3E")
		{
			//for 021000048 && 011210078
			//If Dues To Be Paid  Exist
			$tag = $html->find('table',9);
		}
		else if($tag && urlencode(strip_tags($tag->find("tr",0)->innertext)) == "++%09%09%09%09Electricity+Consumption++%09%09%09")
		{
			$tag = $html->find('table',8);			
		}
		else if($tag && urlencode(strip_tags($tag->find("tr",0)->innertext)) == "++%09%09%09%09Disconnection%2FReconnection+Details++%09%09%09")
		{
			$tag = $html->find('table',9);			
		}

		$count = 0;
		$my_all_result =array();
		$my_all_history = array();
		$my_single_history = array();
		
		// echo $tag;

		if($tag == "" || $tag == null)
		{
			$error_result['status'] = "error";
			$error_result['data'] = "Sorry, Your service no. is not valid. Please try with proper input.";
			print_r(json_encode($error_result)); 
			exit;	
		}
		
		$loop_count_up = 0;			
		foreach($tag->find('tr') as $single_row)
		{		
			$sub_url = substr($url,0, 11);

			// echo $sub_url.'<br>';
			// For region 2 and 4 api, there is title at top so skip it
			// if(($regno == 2 || $regno == 4) && $loop_cur_count == -1 && $extra_ordinary == FALSE)
			// {

			// 	// echo "********";
			// 	$loop_cur_count = $loop_cur_count+1;
			// 	continue; //Skip top title

			// }
			// elseif($loop_cur_count == -1 && $iframe == TRUE)
			// {

			// 	// echo "********";				
			// 	$loop_cur_count = $loop_cur_count+1;
			// 	continue; //Skip top title

			// }
			// if()
			// else
			// {
			// 	$loop_cur_count = $loop_cur_count+1;
			// }
			
			// $loop_cur_count = $loop_cur_count+1;

			if($sub_url == 'https://wss' && $loop_cur_count == -1 )
			{
				$loop_cur_count = $loop_cur_count+1;
				continue;		
			}
			elseif(($sub_url == 'http://tneb' && $iframe == TRUE) && $loop_cur_count == -1)	
			{
				$loop_cur_count = $loop_cur_count+1;
				continue;
			}
			elseif(($sub_url == 'http://tneb' && $iframe == FALSE) && $loop_cur_count <0)
			{
				$loop_cur_count = $loop_cur_count+1;
				continue;
			}
			$loop_cur_count++;
			if($last_update)
			{
				if ($loop_count_up == 0)
				{
					$loop_count_up++;
					continue;
				}				
				$my_single_history_temp_ass = str_replace('&nbsp;', '',trim(strip_tags($single_row->find('td',1)->innertext)));
				$my_convert_try = date_create_from_format("d/m/Y",$my_single_history_temp_ass);
				$my_convert_try2 = date_create_from_format("d-m-Y",$my_single_history_temp_ass);
				
				if($my_convert_try)
				{
					$my_single_history['assessment_date']= date_format($my_convert_try,"Y-m-d");	
				}
				elseif($my_convert_try2)
				{
					$my_single_history['assessment_date']= date_format($my_convert_try2,"Y-m-d");
				}
				else
				{
					if($my_single_history_temp_ass == "-" && $loop_count_up >= 1)
					{	
						$my_single_history['assessment_date'] = $my_single_history['assessment_date'];
					}
					else
					{
						$my_single_history['assessment_date'] = $my_single_history_temp_ass;
					}
				}
				// echo $my_single_history['assessment_date'].'<br>';

				if( $my_single_history['assessment_date'] <= $last_update )
				{
					continue;
				}
			}
			else
			{
										
				$my_single_history_temp_ass = str_replace('&nbsp;', '',trim(strip_tags($single_row->find('td',1)->innertext)));
				if($loop_count_up == 0)
				{
					$my_single_history['assessment_date']=$my_single_history_temp_ass;					
				}
				else
				{
					$my_convert_try_else = date_create_from_format("d/m/Y",$my_single_history_temp_ass);
					$my_convert_try_else2 = date_create_from_format("d-m-Y",$my_single_history_temp_ass);
					
					if($my_convert_try_else)
					{
						$my_single_history['assessment_date']= date_format($my_convert_try_else,"Y-m-d");
					}
					elseif($my_convert_try_else2)
					{
						$my_single_history['assessment_date']= date_format($my_convert_try_else2,"Y-m-d");
					}
					else
					{					
						if($my_single_history_temp_ass == "-")
						{	
							$my_single_history['assessment_date'] = $my_single_history['assessment_date'];
						}
						else
						{
							$my_single_history['assessment_date']=$my_single_history_temp_ass;
						}
					}
				}
			}			
			
			$my_single_history['reading'] = str_replace('&nbsp;', '',trim(strip_tags($single_row->find('td',2)->innertext)));
			$my_single_history['units'] = str_replace('&nbsp;', '',trim(strip_tags($single_row->find('td',3)->innertext)));
			
			// echo $my_single_history['units'];exit;
			if($sub_url == 'http://tneb' && $iframe == FALSE)
			{
				if($my_single_history_temp_ass == "-")
				{
					 $my_single_history['amount']=$single_row->find('td',16)->innertext;				 
				}
				else
				{	
					$amount = str_replace('&nbsp;', '',trim(strip_tags($single_row->find('td',14)->innertext)));
					if(empty($amount))
					{
						$amount = trim(strip_tags($single_row->find('td',11)->innertext));
					}
					$my_single_history['amount']= $amount;
				}
			}
			else
			{
				if($my_single_history_temp_ass == "-")
				{
					 $my_single_history['amount']=$single_row->find('td',11)->innertext;				 
				}
				else
				{	
					$amount = str_replace('&nbsp;', '',trim(strip_tags($single_row->find('td',9)->innertext)));
					// $my_single_history['amount']= empty($amount)? '0': $amount;
					$my_single_history['amount']= $amount;
				}
			}

			// echo "{$loop_cur_count} : {$my_single_history_temp_ass}<pre>".$my_single_history['amount']."</pre>";
			// continue;
			$my_single_history_temp = '';
			if($sub_url == 'http://tneb' && $iframe == FALSE)
			{
				$my_single_history_temp = str_replace('&nbsp;', '',trim(strip_tags($single_row->find('td',18)->innertext)));
			}
			else
			{
				$my_single_history_temp = str_replace('&nbsp;', '',trim(strip_tags($single_row->find('td',13)->innertext)));
			}

			$my_convert_try_sec = '';
			$my_convert_try_sec2 = '';
			if($loop_count_up == 0)
			{

				$my_single_history['payment_date']=$my_single_history_temp;

			}
			else
			{

				$my_convert_try_sec = date_create_from_format("d/m/Y",$my_single_history_temp);
				
				$my_convert_try_sec2 = date_create_from_format("d-m-Y",$my_single_history_temp);
				
				if($my_convert_try_sec)
				{

					$my_single_history['payment_date']= date_format($my_convert_try_sec,"Y-m-d");
					
				}
				elseif($my_convert_try_sec2)
				{

					$my_single_history['payment_date']= date_format($my_convert_try_sec2,"Y-m-d");
				
				}
				elseif ($my_single_history_temp == '-') 
				{
					$my_single_history['payment_date'] = '0000-00-00';
				}
				else
				{
					// $my_single_history['payment_date'] = '0000-00-00';

					$my_single_history['payment_date'] = $my_single_history['assessment_date'];
					// $my_single_history['payment_date'] = $my_single_history_temp;

				}

			}

			$my_single_history_status = '';
			if($sub_url == 'http://tneb' && $iframe == FALSE)
			{
				$my_single_history_status = str_replace('&nbsp;', '', strip_tags($single_row->find('td',20)->innertext));
				
				if($my_single_history_status == '')
					$my_single_history_status = str_replace('&nbsp;', '', strip_tags($single_row->find('td',19)->innertext));
			}
			else
			{
				$my_single_history_status = str_replace('&nbsp;', '', strip_tags($single_row->find('td',15)->innertext));
				
				if($my_single_history_status == '')
					$my_single_history_status = str_replace('&nbsp;', '', strip_tags($single_row->find('td',14)->innertext));
			}
			// echo $my_single_history['payment_date']. '---'. $my_single_history_status. '<br>';
			
			if(($my_single_history['payment_date'] == "" || $my_single_history['payment_date'] == "-" || $my_single_history['payment_date'] == '0000-00-00') && ($my_single_history_status == "NORMAL" || $my_single_history_status == "NOT IN USE" || $my_single_history_status == "NL" || $my_single_history_status == ""))
			{
				if(($my_convert_try_sec || $my_convert_try_sec2) && $my_single_history_status == "NORMAL")
				{
					$my_single_history['status']="Paid";
				}				
				else if($my_single_history_status == "NORMAL")
				{
					$my_single_history['status']="Unpaid";
				}
				else if($my_single_history_status == "NL")
				{
					$my_single_history['status']="Unpaid";
				}
				else if($my_single_history_status == "")
				{
					$my_single_history['status']="Unpaid";
				}
				else
				{
					$my_single_history['status']=$my_single_history_status;
				}

				$my_single_history['payment_date'] = "0000-00-00";
				
			}
			elseif($my_single_history_status == "NORMAL")
			{
				if($my_convert_try_sec)
				{
					$my_single_history['status']="Paid";
				}
				else
				{
					$my_single_history['status']="Unpaid";
				}				
				
			}
			else
			{
				if($my_convert_try_sec || $my_convert_try_sec2)
				{
					if($my_single_history_status == null || $my_single_history_status == 'NL')
					{
						$my_single_history['status']= "Paid";
					}
					else
					{
						$my_single_history['status'] = $my_single_history_status;
					}
				}
				else
				{
					// $my_single_history['status']=$my_single_history_status;
					$my_single_history['status']="Paid";
				}
			}

			$loop_count_up++;
			array_push($my_all_history, $my_single_history);

			//echo "<br />-- End --<br />";
		}
		
		// print_r(json_encode($my_all_history));

		if(($regno == 2 || $regno == 4) && $extra_ordinary == FALSE || $iframe == TRUE)
		{
			$tag_sub_head = $html->find('table',2);			
			
			$first_row = $tag_sub_head->find('tr',0);
			$second_row = $tag_sub_head->find('tr',1);
			$third_row = $tag_sub_head->find('tr',2);
			$fourth_row = $tag_sub_head->find('tr',3);
			$fifth_row = $tag_sub_head->find('tr',4);
			$sixth_row = $tag_sub_head->find('tr',5);
			$seventh_row = $tag_sub_head->find('tr',6);
			$eighth_row = $tag_sub_head->find('tr',7);

			$consumer_name= str_replace('&nbsp;', '',trim(strip_tags($first_row->find('td',1)->innertext)));
			$region_name= str_replace('&nbsp;', '',trim(strip_tags($second_row->find('td',1)->innertext)));
			$circle_name= str_replace('&nbsp;', '',trim(strip_tags($third_row->find('td',1)->innertext)));
			$phase_name= str_replace('&nbsp;', '',trim(strip_tags($third_row->find('td',3)->innertext)));
			$section_name= str_replace('&nbsp;', '',trim(strip_tags($fourth_row->find('td',1)->innertext)));
			$load_name= str_replace('&nbsp;', '',trim(strip_tags($fourth_row->find('td',3)->innertext)));
			$distribution_name= str_replace('&nbsp;', '',trim(strip_tags($fifth_row->find('td',1)->innertext)));
			$service_number = str_replace('&nbsp;', '',trim(strip_tags($sixth_row->find('td',1)->innertext)));
			$meter_number = str_replace('&nbsp;', '',trim(strip_tags($sixth_row->find('td',3)->innertext)));
			$address_name = str_replace('&nbsp;', '',trim(strip_tags($seventh_row->find('td',1)->innertext)));
			$service_status = str_replace('&nbsp;', '',trim(strip_tags($eighth_row->find('td',1)->innertext)));
			
		}
		else
		{
			// print_r($my_all_history);exit;
			$tag_head = $html->find('table',2);
			
			$consumer_head= trim(strip_tags($tag_head->find('tr',0)->find('td',0)->outertext));
			$consumer_name_array = explode("CONSUMER NAME:",$consumer_head);
			
			$tag_sub_head = $html->find('table',3);

			$first_row = $tag_sub_head->find('tr',0);
			$second_row = $tag_sub_head->find('tr',1);
			$region_name= str_replace('&nbsp;', '',trim(strip_tags($first_row->find('td',0)->innertext)));
			$phase_name= str_replace('&nbsp;', '',trim(strip_tags($first_row->find('td',2)->innertext)));
			$circle_name= str_replace('&nbsp;', '',trim(strip_tags($second_row->find('td',0)->innertext)));
			$load_name= str_replace('&nbsp;', '',trim(strip_tags($second_row->find('td',2)->innertext)));
			$load_name_integer = str_replace('&nbsp;', '',trim(str_replace('KW', '',$load_name)));
			//echo  $url; exit;

			$slabe_rate = $html->find('table',4);
    		 // returns all the <tr> tag inside $table
    		$all_slabe_rate_trs_count = $slabe_rate->find('tr');
    		$all_slabe_count = count($all_slabe_rate_trs_count);
    
	 		//	echo $all_slabe_count; exit;
			if($load_name_integer != "" && $load_name_integer >0)
			{
						
				//	print_r($tag_sub_head->find('tr',9)->innertext); exit;
				$third_row = $tag_sub_head->find('tr',2+$all_slabe_count);
				$fourth_row = $tag_sub_head->find('tr',3+$all_slabe_count);
				$fifth_row =$tag_sub_head->find('tr',4+$all_slabe_count);
				$sixth_row =$tag_sub_head->find('tr',5+$all_slabe_count);
				$seventh_row =$tag_sub_head->find('tr',6+$all_slabe_count);
				//	$eighth_row =$tag_sub_head->find('tr',7+$all_slabe_count);
				
				//print_r($eighth_row->innertext); exit;
				$meter_number= str_replace('&nbsp;', '',trim(strip_tags($fourth_row->find('td',2)->innertext)));
				$service_number=  str_replace('&nbsp;', '',trim(strip_tags($fifth_row->find('td',0)->innertext)));
				$address_name = trim(trim(strip_tags($sixth_row->find('td',0)->innertext)),'&nbsp;');
				$service_status = str_replace('&nbsp;', '',trim(strip_tags($seventh_row->find('td',0)->innertext)));
				$section_name= str_replace('&nbsp;', '',trim(strip_tags($third_row->find('td',0)->innertext)));
				$distribution_name= str_replace('&nbsp;', '',trim(strip_tags($fourth_row->find('td',0)->innertext)));
				
			}
			else 
			{
				$error_result['status'] = "error";
				$error_result['data'] = "Sorry, Your service no. is not valid. Please try with proper input.";
				print_r(json_encode($error_result)); exit;				
			}
			
	 	    $consumer_name = $consumer_name_array[1];
		}

		$my_all_result['ServiceNo'] =  $disp_service_no;
		$my_all_result['Name'] = $consumer_name;
		$my_all_result['Region'] = $region_name;
		$my_all_result['Phase'] = $phase_name;
		$my_all_result['Circle'] = $circle_name;
		$my_all_result['Section'] = $section_name;
		$my_all_result['Load'] = $load_name;
		$my_all_result['Distribution'] = $distribution_name;
		$my_all_result['MeterNo'] = $meter_number;
		$my_all_result['ConnectionNumber'] = $service_number;
		$my_all_result['Address'] = $address_name;
		$my_all_result['ServiceStatus'] = $service_status;
		$my_all_result['usage'] = $my_all_history;		

		return $my_all_result;
	}
	
	
	public function get_karnataka($username = FALSE, $password = FALSE, $last_update = FALSE)
	{
		error_reporting(0);

		// check if last update date is valid or not
		if($last_update)
		{
			$temp_d_format =date_create_from_format("Y-m-d",$last_update);
			if($temp_d_format)
			{
				$last_update = date_format($temp_d_format,"Y-m-d");
			}
			else
			{

				$last_update = FALSE;
				$error_result['status'] = "error";
				$error_result['message'] = "Sorry, last update date is invalid.";
				$error_result['data'] = array();
				print_r(json_encode($error_result)); exit;	

			}
		}

		// If username or password is not posted
		if ($username===FALSE OR $password === FALSE) 
		{

			$error_result['status'] = "error";
			$error_result['message'] = "Please Type Username and Password";
			$error_result['data'] = array();
			print_r(json_encode($error_result)); 

			exit;	

		} 
		
		
		

		$client_detail_array = array();		
		$client_detail = array();
		$status = array();
		
		$status['status'] = TRUE;

		$url = "https://www.bescom.co.in/SCP/Myhome.aspx";

		$afterLoginUrl = "https://www.bescom.co.in/SCP/MyAccount/AccountSummary.aspx?Name=IAccountSummaryView";
		
		$this->ckfile = dirname(__FILE__) . '\cookies1.txt';

		if (file_exists($this->ckfile)) {
		    @file_put_contents($this->ckfile, "");
		} 
		
		//$useragent = $_SERVER['HTTP_USER_AGENT'];
		
		// $username = "tprakass";
		// $password = "Muruga78vam$";

		//$username = "srini.bhagyodaya@gmail.com";		
		//$password = "s@1ram";

		//$f = fopen('log.txt', 'w'); // file to write request header for debug purpose

		$regexViewstate = '/__VIEWSTATE\" value=\"(.*)\"/i';
		$regexEventVal  = '/__EVENTVALIDATION\" value=\"(.*)\"/i';
		$regexUip = '/__UipId\" value=\"(.*)\"/i';

		// Initialize CURL
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120); // timeout on connect
		curl_setopt($ch, CURLOPT_TIMEOUT, 120);

		//curl_setOpt($ch, CURLOPT_USERAGENT, $useragent);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $this->ckfile);

		curl_setopt($ch, CURLOPT_COOKIEFILE, $this->ckfile);

		$data=curl_exec($ch);
		
		$regs  = array();

		/*
		    Get __VIEWSTATE & __EVENTVALIDATION & __UipId
		 */

		$viewstate = $this->regexExtract_get($data,$regexViewstate,$regs,1);

		$eventval = $this->regexExtract_get($data, $regexEventVal,$regs,1);

		$user_input_id = $this->regexExtract_get($data, $regexUip,$regs,1);

		$userIP = explode('"', $user_input_id);
		$user_input_id =  $userIP[0];

	    /* Post Data */
		$postfields['__EVENTTARGET'] = "";
		$postfields['__EVENTARGUMENT'] = "";
		$postfields['__VIEWSTATE'] = $viewstate;
		

		$postfields['__EVENTVALIDATION'] = $eventval;

		$postfields['ctl00$ctl00$MasterPageContentPlaceHolder$ucLogin$txtUserName'] = $username;
		$postfields['ctl00$ctl00$MasterPageContentPlaceHolder$ucLogin$txtPassword'] = $password;
		$postfields['ctl00$ctl00$MasterPageContentPlaceHolder$ucLogin$btnLogin'] = "Sign In";

		curl_setOpt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);

		curl_setopt($ch, CURLOPT_URL, $url);   

		curl_setOpt($ch, CURLOPT_REFERER, 'https://www.bescom.co.in/SCP/Myhome.aspx');		

		$data = curl_exec($ch);

		
		curl_setOpt($ch, CURLOPT_HTTPGET, TRUE);
		curl_setOpt($ch, CURLOPT_POST, FALSE);
		curl_setopt($ch, CURLOPT_URL, $afterLoginUrl);   

		$data = curl_exec($ch);



		$this->load->library('dom_parser');	
		$html = $this->dom_parser->str_get_html($data);

		$tag = $html->find('table',13);
		
		
		// echo $tag;exit;
		if($tag == "" || $tag == null)
		{
			$error_result['status'] = "error";
			$error_result['message'] = "Sorry, The user credentials are invalid";
			$error_result['data'] = array();

			print_r(json_encode($error_result)); 
			exit;	
		}
		
		$loop_count = -1;
		foreach($tag->find('tr') as $single_row)
		{
			if ($loop_count == -1)
			{
				$loop_count++;
				continue;
			}

			$client_detail['connection'] = trim(strip_tags($single_row->find('td',0)->innertext));

			$connection_no = $client_detail['connection'];
			if ($connection_no == "")
			{
				$status['status'] = "error";
				$status['message'] = "Invalid Username OR Password";
				$status['data'] = array();

				print_r(json_encode($status)); 
				exit;	
			}
			$client_detail['name'] = trim(strip_tags($single_row->find('td',1)->innertext));
			//$client_detail['address'] = trim(strip_tags($single_row->find('td',2)->innertext));
			//$client_detail['current_balance'] = trim(strip_tags($single_row->find('td',3)->innertext));
			//$client_detail['account_status']  = trim(strip_tags($single_row->find('td',6)->innertext));
			$client_detail['status']  = trim(strip_tags($single_row->find('td',6)->innertext));
			//print_r(json_encode($client_detail)); 

			$details_url='https://www.bescom.co.in/SCP/MyAccount/AccountDetails.aspx?AccountId='.urlencode($connection_no).'&RowIndex%20='.urlencode($loop_count);
			//$details_url2='https://www.bescom.co.in/SCP/MyAccount/AccountDetails.aspx';
			//$detail_html_data = $this->dom_parser->file_get_html($details_url);
		   // echo $detail_html_data;

			
			// Initialize CURL
			//$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $details_url);
			

			$content=curl_exec($ch);

			//echo $content;exit;
			
			
			//$viewstate = $this->regexExtract_get($content,$regexViewstate,$regs,1);
			
			//$file=file_get_contents($details_url);

			//$eventval = $this->regexExtract_get($content, $regexEventVal,$regs,1);

            
            //$data['__VIEWSTATE']=$viewstate;
            //$data['__EVENTVALIDATION']=$eventval;
            /*$options = array(
				//CURLOPT_RETURNTRANSFER => TRUE, // return web page
				//CURLOPT_HEADER => FALSE, // don't return headers
				//CURLOPT_FOLLOWLOCATION => true, // follow redirects
				CURLOPT_ENCODING => "", // handle all encodings
				//CURLOPT_USERAGENT => "spider", // who am i
				//CURLOPT_AUTOREFERER => true, // set referer on redirect
				CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
				CURLOPT_TIMEOUT => 120, // timeout on response
				CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
				//CURLOPT_POST => true,
				//CURLOPT_POSTFIELDS => $data,
			);
			*/
            //$ch = curl_init( $url );
			//curl_setopt_array( $ch, $options );
			//$content = curl_exec ($ch);
			
            //curl_close($ch);
            $html = $this->dom_parser->str_get_html($content);

			$tag = $html->find('table',0);

			//echo $tag;exit;
			
			$account_summary = array();
			foreach($tag->find('tr') as $single_detail_row)
			{
				$key = underscore(strtolower(trim(strip_tags($single_detail_row->find('td',0)->innertext))));
				$value = trim(strip_tags($single_detail_row->find('td',1)->innertext));
				$account_summary[$key] = $value;

			}
			$client_detail['summary']  =  $account_summary;

			/* Code for Usage History */
			

			$usage_history_url = 'https://www.bescom.co.in/SCP/UsageHistory/UsageHistory.aspx';

			$usage_history_result = $this->get_usage_history($ch, $usage_history_url,$connection_no, $last_update);
			
			// print_r($usage_history_result);
			if (count($usage_history_result) == 0) 
			{
				$error_result['status'] = "error";
				$error_result['message'] = "Username or Password Invalid or No Usage History";
				$error_result['data'] = array();

				print_r(json_encode($error_result)); 
				exit;	
			}

			$client_detail['usage_history']  =  $usage_history_result;
			
			/* code for both billing and payment history */

			$billing_payment_history_url = 'https://www.bescom.co.in/SCP/MyAccount/BillingHistory.aspx';

			$billing_payment_result  = $this->get_billing_payment_history($ch,$billing_payment_history_url, $connection_no, $last_update);

			$client_detail['billing_history'] = $billing_payment_result['billing_history'];

			$client_detail['payment_history'] = $billing_payment_result['payment_history'];

			//$client_detail_array[$loop_count] = $client_detail;

			array_push($client_detail_array,$client_detail);

			
			$loop_count++;
			

		}
		//echo "<br />".count($client_detail_array);exit;
		
		curl_close($ch);
		if (count($client_detail_array) == 0)
		{

			$status['status'] = "error";
			$status['message'] = "Invalid Username-Password";
			$status['data'] = array();

			//$client_detail_array = $status;
			return $status;
		}
		else 
		{
			$status['status'] = "success";
			$status['message'] = "Login success";
			$status['data'] = $client_detail_array;
			return $status;
		}


		
	}

	function do_post_request($url, $data, $optional_headers = null)
	{
		$params = array('http' => array(
	      	'method' => 'POST',
	      	'content' => $data
	    ));
	  	if ($optional_headers !== null) {
	    	$params['http']['header'] = $optional_headers;
	  	}
	  	$ctx = stream_context_create($params);
	  	$fp = @fopen($url, 'rb', false, $ctx);
	  	if (!$fp) {
	    	throw new Exception("Problem with $url, $php_errormsg");
	  	}
	  	$response = @stream_get_contents($fp);
	  	if ($response === false) {
	    	throw new Exception("Problem reading data from $url, $php_errormsg");
	  	}
	  	return $response;
	}

	function get_results()
	{
		echo "<pre>\n";
			
		// THIS STRING (CASE SENSITIVE) HAS THE LOGIN CREDENTIALS FOR THE SITE
		$post = "txtUserName=tprakass&txtPassword=Muruga78vam%24";

		//$post = "txtUserName=tprakass&txtPassword=Muruga78vam%24";

		// READ THE SITE PAGE WITH THE LOGIN FORM
		$baseurl = 'https://www.bescom.co.in/SCP/Myhome.aspx';

		// FOR FRONIUS USE AN EXPLICIT URL TO PROCESS THE LOGIN
		$posturl = 'https://www.bescom.co.in/SCP/Myhome.aspx';

		// WHERE TO GO AFTER THE LOGIN
		$nexturl = 'https://www.bescom.co.in/SCP/MyAccount/AccountSummary.aspx?Name=IAccountSummaryView';

		// SET UP OUR CURL ENVIRONMENT
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $baseurl);

		curl_setopt($ch, CURLOPT_FAILONERROR, TRUE);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);

		// CALL THE WEB PAGE
		$htm = curl_exec($ch);
		$err = curl_errno($ch);
		$inf = curl_getinfo($ch);
		if ($htm === FALSE)
		{
		    echo "\nCURL GET FAIL: $baseurl CURL_ERRNO=$err ";
		    var_dump($inf);
		    die();
		}

		// NOW POST THE DATA WE HAVE FILLED IN
		curl_setopt($ch, CURLOPT_URL, $posturl);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

		// WAIT A RESPECTABLE PERIOD OF TIME
		sleep(3);

		// CALL THE WEB PAGE TO COMPLETE THE LOGIN
		$xyz = curl_exec($ch);
		$err = curl_errno($ch);
		$inf = curl_getinfo($ch);
		if ($xyz === FALSE)
		{
		    echo "\nCURL POST FAIL: $posturl CURL_ERRNO=$err ";
		    var_dump($inf);
		}

		// NOW ON TO THE NEXT PAGE
		curl_setopt($ch, CURLOPT_URL, $nexturl);
		curl_setopt($ch, CURLOPT_POST, FALSE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, '');

		$xyz = curl_exec($ch);
		$err = curl_errno($ch);
		$inf = curl_getinfo($ch);
		if ($xyz === FALSE)
		{
		    echo "\nCURL 2ND GET FAIL: $posturl CURL_ERRNO=$err ";
		    var_dump($inf);
		}

		print_r($xyz); exit;
		// ACIVATE THIS TO SHOW OFF THE DATA WE RETRIEVED AFTER THE LOGIN
		// echo htmlentities($xyz);

		// REFINE THE DATA SOME
		$xyz = strip_tags($xyz, '<div><td>');

		$arr = explode('<div id="ctl00_MainContent_UpdatePanel1">', $xyz);
		// KEEP THE SECOND HALF
		$xyz = $arr[1];
		// ADD SOME INSURANCE ABOUT SPACING
		$xyz = str_replace('>', '> ', $xyz);
		$xyz = strip_tags($xyz);
		echo htmlentities($xyz);
		exit;
	}
	
	public function get_power_cut()
	{
		error_reporting(0);
		//phpinfo(); exitl
        $url = 'http://tneb.tnebnet.org/cpro/today.html';
		$this->load->library('dom_parser');
		
		$my_final_result_array = array();
		//$html = $this->dom_parser->file_get_html("http://tneb.tnebnet.org/newlt/consumerwise_gmc_report.php?encserno=VEFOR0VEQ098fDEyMXx8MDA5fHw4&rsno=MQ%3D%3D");
		$html = $this->dom_parser->file_get_html($url);
		$tag = $html->find('table',0);
		//print_r(strip_tags($tag->innertext));
		
		$time_a = explode("between",strip_tags($tag->innertext));
		//Get Starting time
		$time_1 = explode("to",$time_a[1]);
		$time_1_ampm =substr(trim($time_1[0]),-4);
		$time_1_num = explode($time_1_ampm,trim($time_1[0]));
		//Get Ending time
		$time_2 = explode("for",$time_1[1]);
		$time_2_ampm = substr(trim($time_2[0]),-4);
		$time_2_num = explode($time_2_ampm,trim($time_2[0]));
		
		$time_array = array(
							'start'=>array('time'=>$time_1_num[0],'ampm'=>$time_1_ampm),
							'end'=>array('time'=>$time_2_num[0],'ampm'=>$time_2_ampm)
							);
		//print_r($time_array);
		
		

		$tag2 = $html->find('table',1);
		if (!$tag2)
		{
			return '';
		}
		//var_dump($tag2);
		//echo $tag2;
		$table_row_data = $tag2->find('tr'); 

		//echo count($table_row_data);
		//echo $tag2;
	
		// $my_day_array = array();
		$my_data_temp = array();
		$power_cut_rows_count = -1;
		foreach ($table_row_data as $pc_rows ) 
		{
				
			if ($power_cut_rows_count == -1)
			{
				$power_cut_rows_count++;
				continue;
			}
			$date = trim(strip_tags($pc_rows->find('td',0)->innertext));

			$my_data_temp['date'] = $date;

			$my_data_temp['time'] = $time_array;
			$area_data = trim(strip_tags($pc_rows->find('td',1)->innertext));
			$area_data = trim(str_replace(array('--1','--2--'),'',$area_data));

			//$area_data = trim(str_replace(array('(',')'),',',(strip_tags($pc_rows->find('td',1)->innertext))));
			$area_data = str_replace(array("SOWCARPET WEST AREA : "), '', $area_data);
			//print_r($area_data);exit;
			//echo "<pre>";print_r($area_data);echo "<pre />";

			$temp_areas = explode(":",trim($area_data));
			//echo "<br />Data after exploding : <br />";
			//echo "<pre>";print_r($temp_areas);echo "<pre />";
			// continue;
			// get current name
			//$current_area_name = $temp_areas[0];
			//echo "<br />".$current_area_name."<br />";
			$areas_temp = array();

			for ($i=0; $i < count($temp_areas)-1 ; $i++) 
			{ 
				// echo "</ br >Exploded data</br >";
				// echo "<pre>";print_r($temp_areas[$i]);echo "</pre>";

				//echo "</br>/////</br>";
				$area_trimmed = explode(" AREA", $temp_areas[$i]);
				//echo "</br>Data after exploding AREA</br>";
				//echo "<pre>";print_r($area_trimmed);echo "</pre>";

				$area_temp_data = array();
				if ($i == 0) 
				{
					go_first_label:
					$current_area_name = trim($area_trimmed[0]);
					//echo "</br>".$current_area_name."</br>";
					//$i++;					
					if($current_area_name)
					{
						$cur_section_data = $this->get_section_id_by_name($current_area_name);
						//region_id,code section_id,name
						//print_r($cur_section_data);exit;
						// if($cur_section_data)
						// {							
							$area_temp_data['name'] = $current_area_name;
							$area_temp_data['section_id'] = ($cur_section_data['section_id'] !=NULL) ? $cur_section_data['section_id'] : '';
							$area_temp_data['region_id'] = ($cur_section_data['region_id'] != NULL) ? $cur_section_data['region_id'] : '';
						// }
						// else 
						// {
						// 	$area_temp_data['name'] = $current_area_name;
						// }
					
					} 
					
					
					//$temp_areas_explode_dot = explode(".", $temp_areas[$i]);
					//echo "<pre>";print_r($temp_areas_explode_dot);echo "</pre>";
					//echo $next_area_name =  strrchr($temp_areas[$i] , ".");

					$next_area_name =  strrpos($temp_areas[$i+1] , ".");
					if ($next_area_name == FALSE) 
					{
						$next_area_name =  strrpos($temp_areas[$i+1] , ",");
					}
					if ($next_area_name == FALSE)
						continue;
					// echo "<pre>".$next_area_name."< /pre>";
					// continue;
					// sub areas
					//echo "//</br>Sub Areas</br>";

					$subareas = substr($temp_areas[$i+1], 0,$next_area_name);

					//echo "< /br>".$subareas."< /br>";
					//continue;
					$sub_areas_exploded =  explode(",",$subareas);
					$sub_areas_by_area = array();
					foreach (array_filter($sub_areas_exploded) as $sub_area)
					{
						array_push($sub_areas_by_area, trim($sub_area));
					}
					$area_temp_data['sub_areas'] = $sub_areas_by_area;
					array_push($areas_temp,$area_temp_data);
					
					continue;
				} 

				//echo "<br>".$temp_areas[$i]."<br>";
				$area_temp_data = array();

				$last_dot_index =  strrpos($temp_areas[$i] , ".");

				$last_dot_index2 =  strrpos($temp_areas[$i] , ",");

				if ($last_dot_index != FALSE && $last_dot_index2 != FASLE) 
				{
					$last_dot_index = ($last_dot_index > $last_dot_index2) ? $last_dot_index : $last_dot_index2;
				} 
				else if ($last_dot_index == FASLE) 
				{
					$last_dot_index = $last_dot_index2;
				}
				else if ($last_dot_index2 == FALSE)

					goto go_first_label;

				else 
				{
					$last_dot_index = $last_dot_index2;
				}
				//echo "<br>".$last_dot_index."<br>";
				//var_dump($last_dot_index);
					// $area_name_temp = $temp_areas[$i];
				$area_name_temp = substr($temp_areas[$i], $last_dot_index+1);

				//echo "<br>".$area_name_temp."<br>";
				// echo $i;
				// echo $area_name_temp;

				//echo "</br>";

				$area_current = trim(str_replace(" AREA", "", $area_name_temp));
				//echo "<br>".$area_current."<br>";
				//echo $area_current;exit;
				//$area_temp_data['name'] = $area_current;

				if($area_current)
				{
					$cur_section_data = $this->get_section_id_by_name($area_current);
					//region_id,code section_id,name
					// if($cur_section_data)
					// {							
						$area_temp_data['name'] = $area_current;
						$area_temp_data['section_id'] = ($cur_section_data['section_id'] !=NULL) ? $cur_section_data['section_id'] : '';
						$area_temp_data['region_id'] = ($cur_section_data['region_id'] != NULL) ? $cur_section_data['region_id'] : '';
					// }
					// else 
					// {
					// 	$area_temp_data['name'] = $area_current;
					// 	$area_temp_data['section_id'] = $cur_section_data['section_id'];
					// 	$area_temp_data['region_id'] = $cur_section_data['region_id'];
					// }
				
				}
				

				$last_dot_index =  strrpos($temp_areas[$i+1] , ".");

				$last_dot_index2 =  strrpos($temp_areas[$i+1] , ",");

				if ($last_dot_index != FALSE && $last_dot_index2 != FASLE) 
				{
					$last_dot_index = ($last_dot_index > $last_dot_index2) ? $last_dot_index : $last_dot_index2;
				} 
				else if ($last_dot_index == FASLE) 
				{
					$last_dot_index = $last_dot_index2;
				}
				else if ($last_dot_index2 == FALSE)
					continue;

				else 
				{
					$last_dot_index = $last_dot_index2;
				}

				
				// echo "<pre>".$last_dot_index."< /pre>";
				// continue;
				// sub areas
				//echo "//</br>Sub Areas</br>";

				$subareas = substr($temp_areas[$i+1], 0,$last_dot_index);
				//echo "</br>";
				//echo "//</br>Sub Areas List</br>";
				// echo "< /br>".$subareas."< /br>";
				// continue;

				$sub_areas_exploded =  explode(",",$subareas);
				$sub_areas_by_area = array();
				foreach (array_filter($sub_areas_exploded) as $sub_area)
				{
					array_push($sub_areas_by_area, trim($sub_area));
				}
				$area_temp_data['sub_areas'] = $sub_areas_by_area;
				// print_r($area_temp_data);
				// exit;
				array_push($areas_temp,$area_temp_data);

				//echo "</br>";

				//echo "</br>//</br>";
				//echo "</br>/////</br>";
				//print_r(json_encode($areas_temp));
			}

			$my_data_temp['areas'] = $areas_temp;

			//print_r(json_encode($areas_temp));
			//exit;
			// foreach ($temp_areas as $temp_area) {
			//$my_datas_temp['day'] = $my_data_temp;
			//array_push($my_day_array,$my_data_temp);
			array_push($my_final_result_array,$my_data_temp);
			// }
			// echo "<br />";
			// echo trim($temp_area_data[0]);
			// echo "<br />";
			$power_cut_rows_count++;
		}
		//exit;
		//$temp_array = array('days'=>$my_day_array);
		//array_push($my_final_result_array,$temp_array);
		//print_r(json_encode($my_final_result_array));
		return($my_final_result_array);
		exit;

		// if($cur_area_name)
		// {
		// 	$cur_section_data = $this->get_section_id_by_name($cur_area_name);

		// 	//region_id,code section_id,name

		// 	if($cur_section_data)
		// 	{
		// 		$all_area_name[$j] = array("name"=>$cur_area_name,"section_id"=>$cur_section_data['section_id'],"region_id"=>$cur_section_data['region_id']);
		// 		$j++;
		// 	}
		
		// }

	}


	public function regexExtract_get($text, $regex, $regs, $nthValue)
	{
		if (preg_match($regex, $text, $regs)) 
		{
			$result = $regs[$nthValue];
		}
		else 
		{
			$result = "";
		}
		return $result;
	} 

	// retrieve usage history
	public function get_usage_history($ch, $url, $connection, $last_update = FALSE)
	{
		//$ch = curl_init();

		$data['ctl00$ctl00$MasterPageContentPlaceHolder$MasterPageContentPlaceHolder$ddlcustomeraccounts'] = $connection;

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

		// curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		// curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		// //curl_setopt($ch, CURLOPT_COOKIEJAR, $this->ckfile);
		
		// curl_setopt($ch, CURLOPT_COOKIEFILE, $this->ckfile);

		//$content=curl_exec($ch);

		// echo $content;exit;

		// $viewstate = $this->regexExtract_get($content,$regexViewstate,$regs,1);
		// 	echo $viewstate;exit;
		// //$file=file_get_contents($details_url);

		// $eventval = $this->regexExtract_get($content, $regexEventVal,$regs,1);

        
        //$data['__VIEWSTATE']=$viewstate;
       // $data['__EVENTVALIDATION']=$eventval;

		$content = curl_exec ($ch);

        //curl_close($ch);
		//echo $content;exit;

        
        $html = $this->dom_parser->str_get_html($content);

		$tag = $html->find('table',19);
		$usage_history_array = array();

		//echo $tag;exit;
		if($tag != "" && $tag != null)
		{
			$usage_history_count = -1;

			foreach($tag->find('tr') as $usage_history_row)
			{
				$usage_history = array();
				if ($usage_history_count == -1)
				{
					$usage_history_count++;
					continue;
				}


				$usage_history['usage'] = trim(strip_tags($usage_history_row->find('td',0)->innertext));
				$billing_date = trim(strip_tags($usage_history_row->find('td',1)->innertext));

				$billing_date = date_create_from_format('d-M-Y', $billing_date);
				
				if ($billing_date)
					$usage_history['billing_date'] = date_format($billing_date, 'Y-m-d');
				else
					$usage_history['billing_date'] = trim(strip_tags($usage_history_row->find('td',1)->innertext));
				if ($last_update)
				{
					if($usage_history['billing_date'] <= $last_update)
						continue;
				}
				$usage_history['avg_daily_usage'] = trim(strip_tags($usage_history_row->find('td',2)->innertext));
				$usage_history['present_reading'] = trim(strip_tags($usage_history_row->find('td',3)->innertext));
				$usage_history['no_days'] = trim(strip_tags($usage_history_row->find('td',4)->innertext));
				$usage_history['charge'] = trim(strip_tags($usage_history_row->find('td',5)->innertext));

				//$usage_history_array[$usage_history_count] = $usage_history;

				array_push($usage_history_array, $usage_history);

				$usage_history_count++;

			}
		}
		//echo $tag;
		
		//print_r(json_encode($usage_history_array));

		return $usage_history_array;
	}

	public function get_billing_history($billing_url)
	{
		// $ch = curl_init();
		// curl_setopt($ch, CURLOPT_URL, $billing_url);

		// curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		// curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		
		// curl_setopt($ch, CURLOPT_COOKIEFILE, $this->ckfile);

		// $content=curl_exec($ch);
		
  		// url_close($ch);
		//       //echo $content;
  		//       $html = $this->dom_parser->str_get_html($content);

		// $tag = $html->find('table',14);

		// //echo $tag;

		// $billing_history_array = array();
		// $billing_history_count = -1;

		// foreach($tag->find('tr') as $billing_history_row)
		// {
		// 	$billing_history = array();
		// 	if ($billing_history_count == -1)
		// 	{
		// 		$billing_history_count++;
		// 		continue;
		// 	}
		// 	$billing_history['bill_no'] = trim(strip_tags($billing_history_row->find('td',0)->innertext));
		// 	$billing_history['bill_amt'] = trim(strip_tags($billing_history_row->find('td',1)->innertext));
		// 	$billing_history['bill_date'] = trim(strip_tags($billing_history_row->find('td',2)->innertext));
			
		// 	$billing_history_array[$billing_history_count] = $billing_history;

		// 	$billing_history_count++;
		// }
		// //echo json_encode($billing_history_array);
		
		// return $billing_history_array;
        
	}

	// retrieve payment history
	public function get_payment_history($payment_url)
	{
		// $ch = curl_init();
		// curl_setopt($ch, CURLOPT_URL, $payment_url);

		// curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		// curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		
		// curl_setopt($ch, CURLOPT_COOKIEFILE, $this->ckfile);

		// $content=curl_exec($ch);
		
  //       curl_close($ch);
        
  //       $html = $this->dom_parser->str_get_html($content);

		// $tag = $html->find('table',16);

		// //echo $tag;
		
		// $payment_history_array = array();
		// $payment_history_count = -1;

		// foreach($tag->find('tr') as $payment_history_row)
		// {
		// 	$payment_history = array();
		// 	if ($payment_history_count == -1)
		// 	{
		// 		$payment_history_count++;
		// 		continue;
		// 	}
		// 	$payment_history['receipt_id'] = trim(strip_tags($payment_history_row->find('td',0)->innertext));
		// 	$payment_history['payment_amt'] = trim(strip_tags($payment_history_row->find('td',1)->innertext));
		// 	$payment_history['payment_date'] = trim(strip_tags($payment_history_row->find('td',2)->innertext));
		// 	$payment_history['payment_desc'] = trim(strip_tags($payment_history_row->find('td',3)->innertext));
			
		// 	$payment_history_array[$payment_history_count] = $payment_history;

		// 	$payment_history_count++;
		// }
		// //echo json_encode($payment_history_array);
		// //exit;
		// return $payment_history_array;
	}

	// retrieve both billing and payment history
	public function get_billing_payment_history($ch, $billing_payment_url, $connection, $last_update = FALSE)
	{
		//$ch = curl_init();

		$data['ctl00$ctl00$MasterPageContentPlaceHolder$MasterPageContentPlaceHolder$BillingHistoryView$ddlCustomerAccountNumber'] = $connection;

		curl_setopt($ch, CURLOPT_URL, $billing_payment_url);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

		// curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		// curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		
		// curl_setopt($ch, CURLOPT_COOKIEFILE, $this->ckfile);

		//$content=curl_exec($ch);
		

        /*$options = array(
			// CURLOPT_RETURNTRANSFER => TRUE, // return web page
			// CURLOPT_HEADER => FASLE, // don't return headers
			// CURLOPT_FOLLOWLOCATION => true, // follow redirects
			// CURLOPT_ENCODING => "", // handle all encodings
			// //CURLOPT_USERAGENT => "spider", // who am i
			// CURLOPT_AUTOREFERER => true, // set referer on redirect

			//CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
			//CURLOPT_TIMEOUT => 120, // timeout on response

			//CURLOPT_MAXREDIRS => 10, // stop after 10 redirects

			CURLOPT_POST => TRUE,
			CURLOPT_POSTFIELDS => $data,
		);
		*/
        //$ch = curl_init( $url );
		//curl_setopt_array($ch, $options);

		$content = curl_exec ($ch);


       // curl_close($ch);

        $html = $this->dom_parser->str_get_html($content);

        $billing_payment_history = array();
		$billing_history_array = array();
        // Code for billing history

        $tag = $html->find('table',14);

        if($tag != "" && $tag != null)
		{
			$billing_history_count = -1;

			foreach($tag->find('tr') as $billing_history_row)
			{
				$billing_history = array();
				if ($billing_history_count == -1)
				{
					$billing_history_count++;
					continue;
				}
				$billing_history['bill_no'] = trim(strip_tags($billing_history_row->find('td',0)->innertext));
				$billing_history['bill_amt'] = trim(strip_tags($billing_history_row->find('td',1)->innertext));
				
				$bill_date = trim(strip_tags($billing_history_row->find('td',2)->innertext));
				$bill_date = date_create_from_format('d-M-Y', $bill_date);

				if ($bill_date)
					$billing_history['bill_date'] = date_format($bill_date, 'Y-m-d');
				else
					$billing_history['bill_date'] = trim(strip_tags($billing_history_row->find('td',2)->innertext));
				
				if ($last_update)
				{
					if($billing_history['bill_date'] <= $last_update)
						continue;
				}
				//$billing_history_array[$billing_history_count] = $billing_history;
				array_push($billing_history_array, $billing_history);

				$billing_history_count++;
			}

			$billing_payment_history['billing_history'] = $billing_history_array;
		}

		//echo $tag;
        
		

		$tag = $html->find('table',16);

		$payment_history_array = array();

		if($tag != "" && $tag != null)
		{
			$payment_history_count = -1;

			foreach($tag->find('tr') as $payment_history_row)
			{
				$payment_history = array();
				if ($payment_history_count == -1)
				{
					$payment_history_count++;
					continue;
				}
				$payment_history['receipt_id'] = trim(strip_tags($payment_history_row->find('td',0)->innertext));
				$payment_history['payment_amt'] = trim(strip_tags($payment_history_row->find('td',1)->innertext));
				
				$payment_date = trim(strip_tags($payment_history_row->find('td',2)->innertext));
				$payment_date = date_create_from_format('d-M-Y', $payment_date);
				if ($payment_date)
					$payment_history['payment_date'] = date_format($payment_date, 'Y-m-d');
				else
					$payment_history['payment_date'] = trim(strip_tags($payment_history_row->find('td',2)->innertext));
				
				if($last_update)
					if ($payment_history['payment_date'] <= $last_update)
						continue;
				$payment_history['payment_desc'] = trim(strip_tags($payment_history_row->find('td',3)->innertext));
				
				//$payment_history_array[$payment_history_count] = $payment_history;

				array_push($payment_history_array,$payment_history);

				$payment_history_count++;
			}

			$billing_payment_history['payment_history'] = $payment_history_array;
		}

		//print_r($billing_payment_history);exit;

		return $billing_payment_history;
	}

	public function insert_usage_history_karnataka($usage_history_data,$current_insert_id)
	{
		if (count($usage_history_data) > 0)
		{

			$all_usage_history_array = array();

			foreach ($usage_history_data as $single_usage_history) 
			{
				
				$temp_array = array(
						'fk_connection_id'=>$current_insert_id,
						'usage'=>$single_usage_history['usage'],
						'billing_date'=>$single_usage_history['billing_date'],
						'avg_daily_usage'=>$single_usage_history['avg_daily_usage'],
						'present_reading'=>$single_usage_history['present_reading'],
						'no_days'=>$single_usage_history['no_days'],
						'charge'=>$single_usage_history['charge'],
					);

				array_push($all_usage_history_array, $temp_array);
				
			}

			$this->db->insert_batch('server_usage_history_karnataka', $all_usage_history_array);

		}
	}

	public function insert_billing_history_karnataka($billing_history_data, $current_insert_id)
	{
		
		if (count($billing_history_data) >0)
		{
			$all_billing_history_aray = array();

			foreach ($billing_history_data as $single_billing_history) 
			{
				$temp_array = array(
					'fk_connection_id'=>$current_insert_id,
					'bill_no'=>$single_billing_history['bill_no'],
					'bill_amt'=>$single_billing_history['bill_amt'],
					'bill_date'=>$single_billing_history['bill_date'],
				);

				array_push($all_billing_history_aray, $temp_array);
			}

			$this->db->insert_batch('server_billing_history_karnataka', $all_billing_history_aray);
		}
	}

	public function insert_payment_history_karnataka($payment_history_data, $current_insert_id)
	{
		if (count($payment_history_data) > 0)
		{

			$all_payment_history_array = array();

			foreach ($payment_history_data as $single_payment_history) 
			{
				$temp_array = array(
						'fk_connection_id'=>$current_insert_id,
						'receipt_id'=>$single_payment_history['receipt_id'],
						'payment_amt'=>$single_payment_history['payment_amt'],
						'payment_date'=>$single_payment_history['payment_date'],
						'payment_desc'=>$single_payment_history['payment_desc'],
					);

				array_push($all_payment_history_array, $temp_array);
			}

			$this->db->insert_batch('server_payment_history_karnataka', $all_payment_history_array);
		}
	}

	public function add_connection_data_karnataka($all_fetched_data)
	{
		
		if ($all_fetched_data)
		{
			$temp_data = array(
					'connection_id'        => $all_fetched_data['connection'],
					'name'                 => $all_fetched_data['name'],
					'address'              => $all_fetched_data['summary']['correspondence_address'],
					'account_type'         => $all_fetched_data['summary']['account_type'],
					'bill_cycle'           => $all_fetched_data['summary']['bill_cycle'],
					'sub_division'         => $all_fetched_data['summary']['sub_division'],
					'existing_load'        => $all_fetched_data['summary']['existing_load'],
					'existing_tarrif_code' => $all_fetched_data['summary']['existing_tariff_code'],
					'existing_tarrif_name' => $all_fetched_data['summary']['existing_tariff_name'],
					'account_status'       => $all_fetched_data['status'],
					'state'                => 'karnataka',
					'provider'             => 'bescom',
					
				);
			$status = $this->db->insert('server_connection_list_karnataka', $temp_data);

			if ($status)
			{
				$current_insert_id = $this->db->insert_id();
				$this->insert_usage_history_karnataka($all_fetched_data['usage_history'], $current_insert_id);
				$this->insert_billing_history_karnataka($all_fetched_data['billing_history'], $current_insert_id);
				$this->insert_payment_history_karnataka($all_fetched_data['payment_history'], $current_insert_id);
			}
		}
	}

	public function delete_usage_history($connection_no, $last_update)
	{

		$this->db->where('fk_connection_id', $connection_no);

		if ($last_update)
			$this->db->where('billing_date >', $last_update);
		
		$this->db->delete('server_usage_history_karnataka');


	}

	public function delete_billing_history($connection_no, $last_update)
	{
		$this->db->where('fk_connection_id', $connection_no);

		if ($last_update)
			$this->db->where('bill_date >', $last_update);

		$this->db->delete('server_billing_history_karnataka');
	}

	public function delete_payment_history($connection_no, $last_update)
	{
		$this->db->where('fk_connection_id', $connection_no);

		if ($last_update)
			$this->db->where('payment_date >', $last_update);

		$this->db->delete('server_payment_history_karnataka');
	}

	public function update_connection_data_karnataka($all_fetched_data, $connection_number, $last_update)
	{
		if ($all_fetched_data)
		{
			$this->delete_usage_history($connection_number, $last_update);
			$this->delete_billing_history($connection_number, $last_update);
			$this->delete_payment_history($connection_number, $last_update);

			$this->insert_usage_history_karnataka($all_fetched_data['usage_history'], $connection_number);
			$this->insert_billing_history_karnataka($all_fetched_data['billing_history'], $connection_number);
			$this->insert_payment_history_karnataka($all_fetched_data['payment_history'], $connection_number);

		}
	}

	// To store karnataka data
	public function save_karnataka_data($username, $password, $last_update)
	{
		$all_fetched_data = $this->get_karnataka($username, $password, $last_update);
		//return $all_fetched_data; exit;
		if (count($all_fetched_data['data']) > 0)
		{

			foreach ($all_fetched_data['data'] as $single_connection_data) 
			{

				$connection_number = $single_connection_data['connection'];

				$connection_detail = $this->is_connection_id_exist_karnataka($connection_number);
				
				if ($connection_detail)
				{

					$this->update_connection_data_karnataka($single_connection_data, $connection_detail['id'], $last_update);
				
				}
				else
				{	

					$this->add_connection_data_karnataka($single_connection_data);

				}

			}

		}

		return $all_fetched_data;
	}

	public function get_pilot()
	{


		$request = $this->input->post('request');

		// $message = array('message'=>'hello');

		// $result = $this->send_message($message);
		// exit;

		//$request = '{"Data":[{"Minute":"3","Hour":"2","Time":"AM","Date":"31-08-2015","Area":"09250"},{"Minute":"3","Hour":"3","Time":"AM","Date":"31-08-2015","Area":"09270"},{"Minute":"6","Hour":"6","Time":"AM","Date":"31-08-2015","Area":"09288"}]}';


		if(!$request  OR ($request == '' OR $request == NULL))
		{

			$status['status'] = 'success';
			//$status['message'] = 'invalid request';
			//$status['data'] = 'no request parameter';
			print_r(json_encode($status));
			exit;

		}



		$request = json_decode($request);

		//print_r($request);

		//echo count($request->Data);

		if ($request)
		{

			$status['status'] = 'success';
			$status['status_message'] = 'success';

			//$status['data'] = $this->input->post('request');
			$status['data'] = $request;

			print_r(json_encode($status));

			$data_request = array();

			foreach ($request->Data as $data)
			{

				$area_date = array();



				//$post_date = $data->Date." ".$data->Hour.":".$data->Minute.":00 ".$data->Time;

				$area_date['post_date'] = $data->Date;
				$area_date['hour'] = $data->Hour;
				$area_date['minute'] = $data->Minute;
				$area_date['time'] = $data->Time;
				$area_date['area'] = $data->Area;

				// For area name
				$area_date['name'] = $this->get_area_name_by_area_code($area_date['area']);

				if (!$area_date['name'])
				{
					$area_date['name'] = '';
				}
				
				//$area_date['date'] = $post_date;

				// str_replace(PHP_EOL, '', $data->Date);

				array_push($data_request, $area_date);

				

				//print_r(json_encode($area_date));

			}
			// exit;

			//print_r($data_request);exit;

			$user_connection_data = $this->get_all_registered_users();

			//print_r($user_connection_data);exit;

			if ($user_connection_data)
			{
				$data_index = 0;
				foreach ($data_request as $data)
				{
					$valid_reg_id = array();
					foreach ($user_connection_data as $user_con_data)
					{
						if (stristr($user_con_data['connection_id'], $data['area']))
						{

							$temp_con_area = explode('-->', $user_con_data['connection_id']);

							foreach ($temp_con_area as $area_con)
							{
								//echo $area_con;

								if (substr($area_con, 0, strlen($data['area'])) == $data['area'])
								{
									
									$valid_reg_id[$user_con_data['id']] = $user_con_data['gcm_regid'];

									//array_push($valid_reg_id, $user_con_data['gcm_regid']);

									//$message = array('message'=>array('type'=>'unscheduled', 'hour'=> $data['hour'], 'minute' => $data['minute']));

									//$valid_reg_id['message'] = $message;

								}

							}

						}
						// else

						//continue;

						if ($valid_reg_id)
						{

							//$valid_reg_id = array_unique($valid_reg_id);

							//print_r($valid_reg_id); exit;


							// print_r($valid_reg_id);


							$new_valid_regs = array_chunk($valid_reg_id, 1000);
							//print_r($new_valid_regs);exit;

							//$message = array('message'=>array('type'=>'unscheduled', 'hour'=> $data['hour'], 'minute' => $data['minute']));

							$message = array('message'=>'Power resume notification','start_time'=>'', 'end_time'=>$data['hour'].':'.$data['minute'].' '.$data['time'], 'area'=>$data['name'], 'data'=>$this->input->post('request'));
							//$message = array('message'=>'tests');

							$status = $this->send_push_message($new_valid_regs, $message);

							//$status = '{"multicast_id":8123660516902882100,"success":1,"failure":0,"canonical_ids":0,"results":[{"message_id":"0:1441368182347770%0b8e96a6f9fd7ecd"}]}';
							//echo $status;

							if(!empty($status) && is_array($status))
							{
								//print_r($valid_reg_id);
						        foreach($status as $key=>$row)
						        {

						            if(array_key_exists('canonical_ids',$row) && $row['canonical_ids'] > 0)
						            {
						                $canonical_ids = $row['canonical_ids'];

						                if(array_key_exists('results',$row) && !empty($row['results']) && is_array($row['results']))
						                {
						                    foreach($row['results'] as $k=>$v)
						                    {
						                        if(array_key_exists('registration_id',$v))
						                        {

						                            $userid = array_search($new_valid_regs[$key][$k], $valid_reg_id);

						                            $newgcmid  = $v['registration_id'];

						                            $test  = $this->search_reg_id($newgcmid);

						                            if ($test)
						                            {
						                            	$this->delete_gcm_id($userid);
						                            }
						                            else
						                            {
						                            	$this->update_gcm_id($userid,$newgcmid);
						                            }

						                        }
						                    }


						                }
						            }

						            if(array_key_exists('results',$row) && !empty($row['results']) && is_array($row['results']))
					                {

					                	$unsuccessful_ids = array();

					                    foreach($row['results'] as $k=>$v)
					                    {
					                        if (array_key_exists('error',$v) && ($v['error'] == 'InvalidRegistration' || $v['error'] == 'NotRegistered'))
					                        {
					                        	$userid = array_search($new_valid_regs[$key][$k], $valid_reg_id);
					                        	$this->delete_gcm_id($userid);
					                        }
					                        elseif (array_key_exists('error', $v) && ($v['error']== 'Unavailable'))
					                        {
					                        	//$unsuccessful_ids = array();
					                        	$unsuccessful_id = array_search($new_valid_regs[$key][$k], $valid_reg_id);
					                        	array_push($unsuccessful_ids, $valid_reg_id[$unsuccessful_id]);

					                        }

					                    }

					                    //$new_valid_regs = array_chunk($unsuccessful_ids, 1000);
					                    if (!empty($unsuccessful_ids) && is_array($unsuccessful_ids))
					                    {
					                    	$status = $this->send_message($unsuccessful_ids, $message);
					                    }
					                }
						        }
						    }

						    //echo json_encode($status);
						}

					}

					//$data_index++;

				}
			}


			// if ($data_temp)

			// 	$this->mobile_model->add_user_request($data_temp);

			// return

			// 	$data_temp;


		}
		else
		{

			$stat['status'] = 'success';
			$stat['status_message'] = 'error';
			$stat['data'] = array();
			print_r(json_encode($stat));

		}

		exit;

	}


	public function add_user_request($user_request_data)
	{

		$this->db->insert_batch('user_request', $user_request_data);

	}


	public function store_user($gcm_regid, $connection_id)
	{
        // insert user into database
		$data = array(        		
		        		'connection_id' => $connection_id,        		
			);

        //$where = "gcm_regid='".$gcm_regid."' OR old_gcm_regid='".$gcm_regid."'";

        $this->db->where('gcm_regid', $gcm_regid);
        $this->db->or_where('old_gcm_regid', $gcm_regid);        
        $this->db->limit(1);
        $query = $this->db->get('reg_users');

        if ($query->num_rows() == 1)
        {
        	$id = $query->row()->id;

        	$this->db->update('reg_users', $data, array('id'=>$id));
        	return $query->row()->gcm_regid;
        	
        }
        else
        {

    		$data['gcm_regid'] = $gcm_regid;
    		$data['old_gcm_regid'] = $gcm_regid;
    		$data['created_at'] = date('Y-m-d');

        	$this->db->insert('reg_users',$data);

        	return $gcm_regid;
        }

	}
	
    public function get_all_registration_ids()
    {
    	$query = $this->db->distinct('gcm_regid')->get('reg_users');

    	if ($query->num_rows() > 0)
    	{
    		return $query->result();
    	}
    	else
    		return FALSE;
    }

    // public function send_message($message)
    // {

    // 	$data = $this->get_all_registration_ids();

    // 	if ($data)
    // 	{

    // 		$gcm_regids = array(); 

    // 		foreach ($data as $gcm_data) 
    // 		{

    // 			array_push($gcm_regids, $gcm_data->gcm_regid);

    // 		}

    // 		//array_push($gcm_regids, 'APA91bHd_8iWbfUmU5maakz-GAuSer5bqQ-SrMrjejgX_1DpsCuOQ_nt5pBM6IUhguWvSfAzrYDgigNtHlGLK6y-lgjP07ER7azdv-8sRnSrXM6dNiGgIT-oQWF8G17cRKpDmqzMLm_e');
    		
    // 		//array_push($gcm_regids, 'APA91bHNQB7kpH3nG0m4a4jetr-pG1tG7oUhuGPpHYkV0moewc5lJB1UsPuwKxK_0UQwvdj6kwI6f-fXKneOPb3sqgg6IBtGi9Z2mk3saCZ9xs5Fm99rF_LuHiDnRDvah82Z_DtzP-SR');
    		
    // 		return $this->send_push_message($gcm_regids, $message);

    // 	}


    // }

    public function send_message($registatoin_ids, $message)
    {
    	$url = 'https://android.googleapis.com/gcm/send';
		 
        $fields = array(
            'registration_ids' => $registatoin_ids,
            'time_to_live' => 60,
            'data' => $message,
        );
 
        $headers = array(
            'Authorization: key=' . GOOGLE_API_KEY,
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();
 
        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
 
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 
        // disable SSL certificate support
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
 
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
 
        // execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
 
        // Close connection
        curl_close($ch);

        $result = json_decode($result,1);

        return $result;
       
		//return $this->send_push_message($registatoin_ids, $message);
		
    }


    public function send_push_message($registatoin_ids, $message)
    {
    	$status = array();
	    $result = $registatoin_ids;
	    if($result)
	    {
	    	foreach ($result as $regids) 
	    	{

	    		$url = 'https://android.googleapis.com/gcm/send';
		 
		        $fields = array(
		            'registration_ids' => $regids,
        			'time_to_live' => 60,

		            'data' => $message,
		        );
		 
		        $headers = array(
		            'Authorization: key=' . GOOGLE_API_KEY,
		            'Content-Type: application/json'
		        );
		        // Open connection
		        $ch = curl_init();
		 
		        // Set the url, number of POST vars, POST data
		        curl_setopt($ch, CURLOPT_URL, $url);
		 
		        curl_setopt($ch, CURLOPT_POST, true);
		        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		 
		        // disable SSL certificate support
		        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		 
		        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
		 
		        // execute post
		        $result = curl_exec($ch);

		        if ($result === FALSE) {
		            die('Curl failed: ' . curl_error($ch));
		        }
		 
		        // Close connection
		        curl_close($ch);
		        $result = json_decode($result,1);
		        $status[] = $result;
	    	}
	    	
    	}
       	return $status;
    }


    public function get_all_connection_chennai()
    {
    	
    	$query  = $this->db->get('server_connection_list');

    	if ($query->num_rows() > 0)
    	{

    		$connection_datas = $query->result();

    		$areas_array = array();

    		foreach ($connection_datas as $connection_data) {

    			$temp_area_con = $connection_data->name;
    			$area = substr($temp_area_con, 0, 6);
    			array_push($areas_array, $area);

    		}

    		return $areas_array;
    	}
    	
    	else
    	{
    		return FALSE;
    	}

    }


    public function get_all_registered_users()
    {
    	$query  = $this->db->get('reg_users');

    	if ($query->num_rows() > 0)
    	{
    		return $query->result_array();
    	}
    	
    	else
    	{
    		return FALSE;
    	}
    }

    public function update_gcm_id($user_id, $gcm_regid)
    {
    	
        $this->db->where('id', $user_id);
        $data = array(
        		'gcm_regid' => $gcm_regid,
        	);

        $query = $this->db->update('reg_users',$data);
  		
    }


    public function delete_gcm_id($user_id)
    {
    	$this->db->where('id', $user_id);        

        $query = $this->db->delete('reg_users');
    }

	// for power cut gcm messaging model
    public function send_power_cut_gcm()
    {
    	
		//For Power Cut		

    	$power_cut_data_array = file_get_contents('http://nepaimpressions.com/prabin/pc.json');


		$power_cut_data_array = json_decode($power_cut_data_array, TRUE);

		$power_cut_data_array = $power_cut_data_array['data'];

    	//print_r(json_encode($power_cut_data_array));

    	//exit;

    	//$power_cut_data_array = $this->get_power_cut();	

		if (!$power_cut_data_array)
		{
			
			print_r(json_encode(array('status' => 'success','message'=>'No power cut for today')));

		}
		
			
		$registration_ids = $this->get_all_registered_users();	

		// print_r($registration_ids);
		// exit;
		if (!$registration_ids)
		{

			print_r(json_encode(array('status' => 'success','message'=>'No Device Registered')));

		}

		if ($power_cut_data_array)
		{
			foreach($power_cut_data_array as $power_cut_data) 
			{
				//$area_arr  = array();
				$date = $power_cut_data['date'];
				//echo $date;

				$new_date = date('d-m-y');
				//echo $new_date;
				

				if ($date != $new_date)
				{
					//echo "no match";
					continue;
				}



				$start = $power_cut_data['time']['start']['time'].' '.$power_cut_data['time']['start']['ampm'];
				$end = $power_cut_data['time']['end']['time'].' '.$power_cut_data['time']['end']['ampm'];
				
				foreach ($power_cut_data['areas'] as $areas) 
				{
					//echo $areas['section_id'];
					$area = $areas['region_id'].$areas['section_id'];

					//echo $area;		

					if (empty($area))
					{
						continue;
					}
					//echo "area match";
					$valid_reg_id = array();

					foreach ($registration_ids as $registration_id) 
					{
						//echo $registration_id['connection_id'];

						//var_dump(strstr($registration_id['connection_id'], $area));
						
						if (stristr($registration_id['connection_id'], $area))
						{
							//echo $registration_id['connection_id'];

							$temp_con_area = explode('-->', $registration_id['connection_id']);

							foreach ($temp_con_area as $area_con) 
							{
								//echo $area_con;
								
								if (substr($area_con, 0, 5) == $area)
								{

									$valid_reg_id[$registration_id['id']] = $registration_id['gcm_regid'];

									//array_push($valid_reg_id, $user_con_data['gcm_regid']);
									
									//$message = array('message'=>array('type'=>'unscheduled', 'hour'=> $data['hour'], 'minute' => $data['minute']));
									
									//$valid_reg_id['message'] = $message;

									//$valid_reg_id[$registration_id['id']]['area'] = $area['name'];
								}

							}

						}
					}

					if ($valid_reg_id)
					{
						$new_valid_regs = array_chunk($valid_reg_id, 1000);
						//print_r($new_valid_regs);exit;

						$message = array('message'=> 'Power cut notification ','start_time'=> $start, 'end_time' => $end);
						
						//$message = array('message'=>'tests');
						
						$status = $this->send_push_message($new_valid_regs, $message);
						
						//$status = '{"multicast_id":8123660516902882100,"success":1,"failure":0,"canonical_ids":0,"results":[{"message_id":"0:1441368182347770%0b8e96a6f9fd7ecd"}]}';
						//echo $status;

						if(!empty($status) && is_array($status))
						{
							//print_r($valid_reg_id);
					        foreach($status as $key=>$row)
					        {

					            if(array_key_exists('canonical_ids',$row) && $row['canonical_ids'] > 0)
					            {
					                $canonical_ids = $row['canonical_ids'];

					                if(array_key_exists('results',$row) && !empty($row['results']) && is_array($row['results']))
					                {

					                    foreach($row['results'] as $k=>$v)
					                    {
					                        if(array_key_exists('registration_id',$v))
					                        {

					                            $userid = array_search($new_valid_regs[$key][$k], $valid_reg_id);

					                            $newgcmid  = $v['registration_id'];

					                            $test  = $this->search_reg_id($newgcmid);

					                            if ($test)
					                            {
					                            	$this->delete_gcm_id($userid);
					                            } 
					                            else
					                            {
					                            	$this->update_gcm_id($userid,$newgcmid);
					                            }
					                        }
					                        
					                    }
					                }
					            }

					            if(array_key_exists('results',$row) && !empty($row['results']) && is_array($row['results']))
				                {

				                	$unsuccessful_ids = array();

				                    foreach($row['results'] as $k=>$v)
				                    {				                        
				                        if (array_key_exists('error',$v) && ($v['error'] == 'InvalidRegistration' || $v['error'] == 'NotRegistered'))
				                        {
				                        	$userid = array_search($new_valid_regs[$key][$k], $valid_reg_id);
				                        	$this->delete_gcm_id($userid);
				                        }
				                        elseif (array_key_exists('error', $v) && ($v['error']== 'Unavailable'))
				                        {
				                        	//$unsuccessful_ids = array();
				                        	$unsuccessful_id = array_search($new_valid_regs[$key][$k], $valid_reg_id);
				                        	array_push($unsuccessful_ids, $valid_reg_id[$unsuccessful_id]);
				                        }

				                    }

				                    //$new_valid_regs = array_chunk($unsuccessful_ids, 1000);
				                    
				                    if (!empty($unsuccessful_ids) && is_array($unsuccessful_ids))
				                    {
				                    	$status = $this->send_message($unsuccessful_ids, $message);
				                    }
				                }
					        }
					    }
						    
					    echo json_encode($status);

					}
					
				}
				
			}
		}


		//$result = $this->send_message($message);

		//echo $result;

		exit;
    }

    public function search_reg_id($gcm_reg_id)
    {

    	$query = $this->db->get_where('reg_users', array('gcm_regid'=> $gcm_reg_id));

    	if ($query->num_rows() > 0)
    	{
    		return TRUE;
    	}
    	else
    		return FALSE;
    }

    /**
    * get area name from area code posted by admin
	* @param string
	* @return mixed
    **/

    public function get_area_name_by_area_code($area)
    {
    	$area = (int) $area;
    	$query = $this->db->select('name')
    				->from('server_connection_list')
    				
    				->like('connection_id', $area, 'after')
    				->get();
    	if($query->num_rows() == 1)
    		return $query->row()->name;
    	else
    		return FALSE;
    }

    public function save_referal_webhook_response($response_data)
    {
    	if($response_data && is_array($response_data))
    	{
    		$participantid 		= $response_data['participantid'];

    		$emailid 			= $response_data['emailid'];

    		$securekey 			= $response_data['securekey'];
    		$name 				= $response_data['name'];
    		$storeuserid 		= $response_data['storeuserid'];
    		$campaignid 		= $response_data['campaignid'];
    		$campaignname 		= $response_data['campaignname'];
    		
    		$rewardid 			= $response_data['rewardid'];
    		$reward_type 		= $response_data['reward_type'];

    		$amount 			= $response_data['amount'];
    		$reward_unit 		= $response_data['reward_unit'];
    		$reward_description = $response_data['reward_description'];
    		$couponcode 		= $response_data['couponcode'];
    		$expires 			= $response_data['expires'];
    		$platform 			= $response_data['platform'];
    		$rewarddetails 		= $response_data['rewarddetails'];

    		$rewarded_date = NULL;
    		if(is_null($response_data['rewarded_date']))
    		{
    			$rewarded_date 		= NULL;
    		}
    		else
    		{
    			$reward_date 		= date_create($response_data['rewarded_date']);
				$rewarded_date 		= date_format($reward_date, 'Y-m-d');
    		}
    		

    		$response = array(

    			'response_data' => json_encode($response_data),
    			'response_date' => $this->general->get_local_time('now'),
    			'response_type' => 'reward_response',
			);

    		$reward_data = array(

    			'rewardid'           => $rewardid,
    			'securekey'          => $securekey,
    			'emailid'            => $emailid,
    			'name'               => $name,
    			'storeuserid'        => $storeuserid,
    			'campaignid'         => $campaignid,
    			'campaignname'       => $campaignname,
    			'participantid'      => $participantid,    			
    			'reward_type'        => $reward_type,
    			'rewarded_date'      => $rewarded_date,
    			'amount'             => $amount,
    			'reward_unit'        => $reward_unit,
    			'reward_description' => $reward_description,
    			'couponcode'         => $couponcode,
    			'expires'            => $expires,
    			'platform'           => $platform,

			);

    		$reward_detail_data = array();
    		if(is_array($rewarddetails) && count($rewarddetails) > 0)
    		{
				foreach ($rewarddetails as $reward_detail) 
				{

					$rewarded_detail_date = NULL;

					if(is_null($reward_detail['rewarded_date']))
		    		{
		    			$rewarded_detail_date = NULL;
		    		}
		    		else
		    		{
		    			$reward_date = date_create($reward_detail['rewarded_date']);
						$rewarded_detail_date = date_format($reward_date, 'Y-m-d');
		    		}
					
					$reward_detail_temp_arr = array(

						'rewarddtlid' 	=> $reward_detail['rewarddtlid'],
						'amount' 	  	=> $reward_detail['amount'], 
						'rewarded_date' => $rewarded_detail_date,

					);
					array_push($reward_detail_data, $reward_detail_temp_arr);
				}
			}

    		$this->db->trans_start();

    		$this->db->insert('webhook_response', $response);

			$reward_id = '';
			$referer_data = $this->check_referer_exist($participantid);
    		if($referer_data)
    		{
    			$reward_data['total_balance'] = $amount;
    			$reward_data['remaining_balance'] = $reward_data['total_balance'] - $referer_data['paid_balance'];
    			
    			$this->db->where('id', $referer_data['id']); 

    			$this->db->update('reward_referal', $reward_data);
    			
    			$reward_id = $referer_data['id'];
    		}
    		else
    		{
    			$reward_data['total_balance'] = $amount;
    			$reward_data['remaining_balance'] = $amount;
    			$reward_data['paid_balance'] = 0;

    			$this->db->insert('reward_referal', $reward_data);
				$reward_id = $this->db->insert_id();
    		}

			foreach ($reward_detail_data as $reward_detail) {

				$reward_detail['participant_id'] = $participantid;
				$reward_detail['reward_id'] = $reward_id;

				$this->db->replace('reward_detail', $reward_detail);
			}
			

			$this->db->trans_complete();

			if ($this->db->trans_status() === TRUE)
			{
			        return TRUE;
			}
			return FALSE;
    		
    	}
    }

    public function save_conversion_response($response_data)
    {
    	
    	if($response_data && is_array($response_data))
    	{

    		$campaignid 		= $response_data['campaignid'];
    		$campaignname 		= $response_data['campaignname'];
    		$eventname			= $response_data['eventname'];
    		$extrainfo			= $response_data['extraInfo'];
    		
    		$referrer			= $response_data['referrer'];

    			$participantid 		= $referrer['participantid'];
    			$emailid 			= $referrer['emailid'];
	   			$name 				= $referrer['name'];
    			$storeuserid 		= $referrer['storeuserid'];
    			$rewarddtlid 		= $referrer['rewarddtlid'];

    			$rewarded_date = NULL;

	    		if(is_null($referrer['rewarded_date']))
	    		{
	    			$rewarded_date 		= NULL;
	    		}
	    		else
	    		{
	    			$reward_date 		= date_create($referrer['rewarded_date']);
					$rewarded_date 		= date_format($reward_date, 'Y-m-d');
	    		}

    			$amount 			= $referrer['amount'];
    			$reward_unit 		= $referrer['reward_unit'];
    			$reward_type 		= $referrer['reward_type'];
    			$reward_description = $referrer['reward_description'];
    			$reviewperiod		= $referrer['reviewperiod'];
    			$platform 			= $referrer['platform'];
    			$reachedmaxlimit 	= $referrer['reachedmaxlimit'];    		

			$conversion_reward_data = array(

				'campaignid' => $campaignid,
				'campaignname' => $campaignname,
				'eventname' => $eventname,
				'extrainfo' => $extrainfo,
				'participantid' => $participantid,
				'emailid' => $emailid,
				'name' => $name,
				'storeuserid' => $storeuserid,
				'rewarddtlid' => $rewarddtlid,
				'rewarded_date' => $rewarded_date,
				'amount' => $amount,
				'reward_unit' => $reward_unit,
				'reward_type' => $reward_type,
				'reward_description' => $reward_description,
				'reviewperiod' => $reviewperiod,
				'platform' => $platform,
				'reachedmaxlimit' => $reachedmaxlimit,

			); 

    		$response = array(

    			'response_data' => json_encode($response_data),
    			'response_date' => $this->general->get_local_time('now'),
    			'response_type' => 'conversion_response',

			);

			// Transaction Start
    		$this->db->trans_start();

			$this->db->insert('webhook_response', $response);

			//check if user already exists
    		$conversion_user_exist = $this->check_conversion_user_exist($emailid, $participantid);
    		if($conversion_user_exist === TRUE)
    		{
    			// if user already exists update user balance
    			$this->update_conversion_user_balance($emailid, $participantid, $amount);
    		}
    		else
    		{
    			// if user doesnot exist insert user balance in the database
    			$this->insert_conversion_user_balance($emailid, $participantid, $amount);
    		}

    		// insert conversion reward detail in database
    		$this->db->insert('reward_conversion', $conversion_reward_data);

    		// Transaction Complete
			$this->db->trans_complete();

			// check transaction status
			if ($this->db->trans_status() === TRUE)
			{
			        return TRUE;
			}
			return FALSE;
    		
    	}
    }

    /**
     * check_conversion_user_exist
     *  This method checks if referrer already exists
     * @param  string $emailid       email id of referrer
     * @param  integer $participantid participant id of user
     * @return boolean                True if referrer exists False otherwise
     */
    public function check_conversion_user_exist($emailid, $participantid)
    {
    	$this->db->where('emailid', $emailid);
    	$this->db->where('participantid', $participantid);
    	$query = $this->db->get('reward_conversion');
    	if($query->num_rows() > 0)
    	{
    		return TRUE;
    	}
    	return FALSE;
    }

    public function update_conversion_user_balance($emailid, $participant_id, $balance)
    {
    	$this->db->set('total_balance', 'total_balance + '. $balance, FALSE);
    	$this->db->set('remaining_balance', 'remaining_balance + '. $balance, FALSE);
    	$this->db->where('user_email', $emailid);
    	$this->db->where('participant_id', $participant_id);
    	$query = $this->db->update('user_conversion_balance');
    	return $this->db->affected_rows();
    }

    public function insert_conversion_user_balance($emailid, $participant_id, $balance)
    {
    	$user_conversion_balance_data = array(

			'user_email' => $emailid,
			'participant_id' => $participant_id,
			'total_balance' => $balance,
			'remaining_balance' => $balance,

		);

    	$this->db->insert('user_conversion_balance', $user_conversion_balance_data);
    	return $this->db->affected_rows();
    }


    public function check_referer_exist($participantid)
    {
    	$this->db->select('id, participantid, paid_balance');
    	$this->db->where('participantid', $participantid);
    	$query = $this->db->get('reward_referal');

    	if($query->num_rows() > 0)
    	{
    		return $query->row_array();
    	}
    	return FALSE;
    }

    /**********Start Portion for Mobile verification *********/
    // check if user already exists
    public function check_user_already_exists($mobile_number)
    {
    	$this->db->where('mobile', $mobile_number);
    	// $this->db->where('status', 1);
    	$query = $this->db->get('members_verification_temp');
    	if($query->num_rows() > 0)
    	{
    		return $query->row_array();
    	}
    	return FALSE;
    }

    // create new user
    public function create_user($mobile_number, $sender_id, $verification_code,  $message)
    {
    	$user_exists = $this->check_user_already_exists($mobile_number);
    	if($user_exists)
    	{

    		// check if previous sms is sent 5 minutes earlier from current time or nor
    		$senttimestamp = strtotime("".$user_exists['sent_date']. '+ 5 minute');
    		$currenttimestamp = strtotime("".$this->general->get_local_time('now'));
    		// echo "Sent Time: {$user_exists['sent_date']}<br>";
    		// echo "Current Time: {$this->general->get_local_time('now')}<br>";
    		// echo $senttimestamp.'<br>';
    		// print_r($currenttimestamp);exit;
    		if($currenttimestamp <= $senttimestamp)
    		{
    			return 1;
    		}
    		else
    		{
    			$this->delete_user($mobile_number);
    		}
    		
    	}
    	
    	$user_data = array(

				'sender_id' => $sender_id,
				'mobile' => $mobile_number,
				'verification_code' => $verification_code,
				'message' => $message,
				'sent_date' => $this->general->get_local_time('now'),
				'status' => 1,

			);

		$insert_status = $this->db->insert('members_verification_temp', $user_data);

		if($insert_status)
		{
			return 2;
		}
		else
		{
			return 3;
		}
    		
    }

    // send sms
    public function send_sms($mobile_number, $sender_id, $verification_code, $message)
    {
    	
    	$url = "http://fastsms.way2mint.com/SendSMS/sendmsg.php?uname=".SMS_USERNAME."&pass=".SMS_PASSWORD."&send=".$sender_id."&dest=91".$mobile_number."&msg=".urlencode($message); //."&intl=1";

    	$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
		$output = curl_exec($ch);

		if (curl_errno($ch)) 
		{

			$error_result["status"] = "error";
        	$error_result["message"] = curl_error($ch);
        	print_r(json_encode($error_result)); 
			exit;
        	
    	}
		curl_close($ch);
		
		// $error = $this->check_error_code($putput);

		// if($error)
		// {
		// 	$error_result["status"] = "error";
  //       	$error_result["message"] = $error;
  //       	print_r(json_encode($error_result)); 
		// 	exit;
		// }
		
		$this->update_user($mobile_number, $verification_code, $output);
		// $user_create_status = $this->create_user($mobile_number, $sender_id, $verification_code, $message);
    }

    // activate user 
    public function activate_user($mobile_number, $verification_code) 
    {
        $this->db->where('mobile', $mobile_number);
        $this->db->where('verification_code', $verification_code);
        $this->db->limit(1);
        $query = $this->db->get('members_verification_temp'); 

        if($query->num_rows() > 0) 
        {
        	$result = $query->row_array();
        	$query->free_result();
        	$this->activate_user_status($result['id']);

        	if(!$this->check_actual_user($mobile_number, $verification_code))
        	{
        		$this->create_actual_user($result);
        	}

        	return 1;
        }  
        else
        {
        	return 2;
        }               
                
    }
     
    public function activate_user_status($user_id)
    {
    	$this->db->set('status', 2);
    	$this->db->where('id', $user_id);
    	$this->db->update('members_verification_temp');
        return $this->db->affected_rows();        
    }


    public function delete_user($mobile_number)
    {
    	$this->db->where('mobile', $mobile_number);
    	$this->db->delete('members_verification_temp');

    	$this->db->where('mobile', $mobile_number);
    	$this->db->delete('member_sms_verification');
    }

    // update sms id after sending sms
    public function update_user($mobile_number, $verification_code, $sms_id)
    {
    	$this->db->set('sms_id', $sms_id);
    	$this->db->where('mobile', $mobile_number);
    	$this->db->where('verification_code', $verification_code);
    	$this->db->update('members_verification_temp');
        return $this->db->affected_rows();   
    }

    // create user after verification is inserted
    public function create_actual_user($member_detail)
    {
    	$user_data = array(

    			'sms_id' => $member_detail['sms_id'],
				'sender_id' => $member_detail['sender_id'],
				'mobile' => $member_detail['mobile'],
				'verification_code' => $member_detail['verification_code'],
				// 'message' => $message,
				'sent_date' =>  $member_detail['sent_date'],
				'status' => 2,

			);

    	$this->db->insert('member_sms_verification', $user_data);
    }

    // for error sent back by sms api
    public function check_error_code($output)
    {
    	$error = substr($output,0, 5);
    	$error_code = '';
    	switch ($error) {
		    case "0x200":
		        $error_code = "Invalid Username or Password";
		        break;
		    case "0x201":
		        $error_code = "Account suspended due to one of several defined reasons";
		        break;
		    case "0x202":
		        $error_code = "Invalid Source Address/Sender ID";
		        break;
	        case "0x203":
		        $error_code = "Message length exceeded";
		        break;
		    case "0x204":
		        $error_code = "Message length exceeded";
		        break;
		    case "0x205":
		        $error_code = "DRL URL is not set";
		        break;
	       	case "0x206":
		        $error_code = "Only the subscribed service type can be accessed";
		        break;
		    case "0x207":
		        $error_code = "Invalid Source IP – kindly check if the IP is responding";
		        break;
		    case "0x208":
		        $error_code = " Account deactivated/expired";
		        break;
	        case "0x209":
		        $error_code = "Invalid message length";
		        break;
		    case "0x210":
		        $error_code = "Invalid Parameter values";
		        break;
		    case "0x211":
		        $error_code = "Invalid Message Length";
		        break;
		    case "0x212":
		        $error_code = "Invalid Message Length";
		        break;
		    case "0x213":
		        $error_code = "Invalid Destination Number";
		        break;
    	}

    	return $error_code;
	}

    public function check_actual_user($mobile_number, $verification_code)
    {
    	$this->db->where('mobile', $mobile_number);
    	$this->db->where('verification_code', $verification_code);
    	$query = $this->db->get('member_sms_verification');
        return $query->num_rows() > 0;
    }

    // for creating user not in use
    public function create_actual_user_v2($mobile_number, $sender_id, $sms_id, $verification_code)
    {
    	$user_data = array(

    			'sms_id' => $sms_id,
				'sender_id' => $sender_id,
				'mobile' => $mobile_number,
				'verification_code' => $verification_code,
				'sent_date' =>  $this->general->get_local_time('now'),
				'status' => 1,

			);

    	$this->db->insert('member_sms_verification', $user_data);
    }

    public function update_verification_code($mobile_number, $verification_code)
    {
    	$this->db->set('verification_code', $verification_code);
    	$this->db->where('mobile', $mobile_number);
    	$this->db->update('member_sms_verification');
    }

    /********** End Portion for Mobile verification *********/


    /********* Ranking Portion Code ******/

    public function calculate_state_average($state = '',  $connection, $load)
    {    	
		$state_code = substr($connection, 0,2); 

    	$this->db->select('sc.id as cid, sc.connection_id, sc.serviceno, 
    		IFNULL(AVG(`amount`),0) AS `amount_avg`, 
    		IFNULL(AVG(`units`),0) AS `unit_avg`', false);
    	$this->db->from('server_connection_list sc');
    	$this->db->join('server_usage_data sd', 'sd.fk_connection_id = sc.id', 'LEFT');
    	$this->db->like('sc.serviceno', $state_code, 'after');
    	$this->db->where('sc.load', $load);
    	$this->db->group_by('sc.id'); 
    	$this->db->order_by('unit_avg');
    	$query = $this->db->get();
    	// echo $this->db->last_query();exit;

    	if($query->num_rows() > 0)
    	{
    	// 	$result = $query->result_array();
    		
    	// 	$units_column = array_column($result, 'unit_avg');
    	// 	$amount_column = array_column($result, 'amount_avg');
    	// 	$units_avg = round(array_sum($units_column)/ count($units_column), 2);
    	// 	$amount_avg = round(array_sum($amount_column)/ count($amount_column), 2);

    	// 	$i = 1;
    	// 	foreach ($result as &$result_item) 
    	// 	{
    	// 		$result_item['rank'] = $i++;
    	// 	}
    	// 	// print_r(json_encode($result));exit;

    	// 	foreach ($result as $key => $result_itm) 
    	// 	{
    	// 		if($result_itm['connection_id'] != $connection)
    	// 		{
    	// 			unset($result[$key]);
    	// 		}
    	// 	}
    		
    	// 	// print_r(json_encode($result));exit;
    	// 	$state_data = array();
    	// 	$state_average = array('unit_avg' => $units_avg, 'amount_avg' => $amount_avg);
    	// 	foreach ($result as $itm) {
    	// 		$state_data['rank'] = $itm['rank'];
    	// 		break;
    	// 	}
    	// 	if(!$state_data['rank'])
    	// 	{
    	// 		$state_data['rank'] = 0;
    	// 	}
    	// 	$state_data['state_average'] = $state_average;
    	// 	// print_r(json_encode($state_data)); exit;
    	// 	return $state_data;
    	// }    
    	// $state_data = array();
    	// $state_data['rank']  = 0;
    	// $state_data['state_average'] = array('unit_avg' => 0, 'amount_avg' => 0);
    	// return $state_data;
    	
    		$result = $query->result_array();
    		
    		$units = array();
    		$amounts = array();
    		foreach($result as $res)
    		{
    			array_push($units, $res['unit_avg']);
    			array_push($amounts, $res['amount_avg']);
    		}

    		$unit_column = array_sum($units);
    		$amount_column = array_sum($amounts);

    		// print_r(json_encode($units));exit;

    		// print_r(json_encode($units_column));exit;

    		// $amount_column = array_column($result, 'amount_avg');
    		// 
    		$units_avg = round($unit_column / count($units), 2);
    		$amount_avg = round($amount_column / count($amounts), 2);

    		$i = 1;
    		foreach ($result as &$result_item) 
    		{
    			$result_item['rank'] = $i++;
    		}

    		foreach ($result as $key => $result_itm) 
    		{
    			if($result_itm['connection_id'] != $connection)
    			{
    				unset($result[$key]);
    			}
    		}
    		
    		// print_r(json_encode($result));exit;
    		$state_data = array();
    		$state_average = array('unit_avg' => $units_avg, 'amount_avg' => $amount_avg);
    		foreach ($result as $itm) {
    			$state_data['rank'] = $itm['rank'];
    			break;
    		}
    		if(!$state_data['rank'])
    		{
    			$state_data['rank'] = 0;
    		}
    		$state_data['state_average'] = $state_average;
    		return $state_data;

    	}

    	$state_data = array();
    	$state_data['rank']  = 0;
    	$state_data['state_average'] = array('unit_avg' => 0, 'amount_avg' => 0);
    	return $state_data;
    }
    
    // Calculates neighbor average
    public function calculate_neighbor_average($state = '', $connection, $load)
    {
    	$distribution = substr($connection, 5,3); 

    	$this->db->select('sc.id as cid, sc.connection_id, sc.serviceno,  avg(sd.amount) as amount_avg, avg(sd.units) unit_avg');    	
    	$this->db->from('server_connection_list sc');
    	$this->db->join('server_usage_data sd', 'sd.fk_connection_id = sc.id', 'LEFT');
    	$this->db->like('sc.serviceno', '-'.$distribution.'-');
    	$this->db->where('sc.load', $load);    
    	$this->db->group_by('sc.id');    
    	$query = $this->db->get();
    	// echo $this->db->last_query();exit;
    	if($query->num_rows() > 0)
    	{
    		$result = $query->result_array();
    		for($i = 0; $i < count($result); $i++)
    		{    			
				$dist = substr($result[$i]['connection_id'], 4, 3);
				if($distribution != $dist)
					unset($result[$i]);    			
    		}
    		$units =array();
    		$amounts = array();
    		foreach($result as $res)
    		{
    			array_push($units, $res['unit_avg']);
    			array_push($amounts, $res['amount_avg']);
    		}
    		$units_column = array_sum($units);
    		$amount_column = array_sum($amounts);
    		$units_avg = round($units_column/ count($units), 2);
    		$amount_avg = round($amount_column/ count($amounts), 2);
    		$neighbor_average = array('unit_avg' => $units_avg, 'amount_avg' => $amount_avg);
    		return $neighbor_average;
    	}
	   	return array('unit_avg' => 0, 'amount_avg' => 0);
    }

    // Calculate average for multiple connections
    public function calculate_average($state, $connections, $load)
    {
    	$response_result = array();
    	if(count($connections) > 0)
    	{
    		$temp_rank 	   = array();
    		$response 	   = array();
    		$temp_response = array();

    		foreach ($connections as $connection) 
    		{
    			$state_average = $this->calculate_state_average('', $connection, $load);
    			$neighbor_average = $this->calculate_neighbor_average('', $connection, $load);
    			// $connection_average = $this->calculate_connection_average('', $connection, $load);

    			$temp_response['connection'] 	   = $connection;
    			$temp_response['state_average']    = $state_average;
    			$temp_response['neighbor_average'] = $neighbor_average;

    			// $temp_response['connection_average'] = $connection_average;

    			$response[][$connection] = $temp_response;
 
    			// array_push($response_result, $temp_response);
  
    			$temp_rank[$connection] = $state_average['amount_avg'];
    		}

    		$response_result['connections'] = $response;
    		asort($temp_rank);
    		$response_result['ranks'] = $temp_rank;
      	}      	
    	return $response_result;
    }

    public function calculate_connection_average($state = '', $connection, $load)
    {
    	$this->db->select('sc.id as cid, sc.connection_id, sc.serviceno, sd.*');    	
    	$this->db->from('server_connection_list sc');
    	$this->db->join('server_usage_data sd', 'sd.fk_connection_id = sc.id', 'LEFT');
    	$this->db->where('sc.connection_id', $connection);
    	$this->db->where('sc.load', $load);    	
    	$query = $this->db->get();

    	// echo $this->db->last_query(); exit;

    	if($query->num_rows() > 0)
    	{
    		$result             = $query->result_array(); 

    		$units = array();
    		$amounts = array();
    		foreach ($result as $res) 
    		{
    		 	array_push($units, $res['units']);
    			array_push($amounts, $res['amount']);
    		} 
    		$units_column       = array_sum($units);
    		$amount_column      = array_sum($amounts);

    		$units_avg          = round($units_column/ count($units), 2);
    		$amount_avg         = round($amount_column/ count($amounts), 2);

    		$connection_average = array('unit_avg' => $units_avg, 'amount_avg' => $amount_avg);
    		
    		return $connection_average;
    	}
    	return array('unit_avg' => 0, 'amount_avg' => 0);
    }

    // Calculate average of each member
    public function calculate_each_average($state, $connection, $load)
    {
		$response 	   = array();
		$state_average = $this->calculate_state_average('', $connection, $load);
		$neighbor_average = $this->calculate_neighbor_average('', $connection, $load);
		$connection_average = $this->calculate_connection_average('', $connection, $load);

		$response['load']				= $load;		
		$response['state'] 				= $state;
		$response['connection'] 		= $connection;
		$response['state_average'] 		= $state_average['state_average'];
		$response['neighbor_average'] 	= $neighbor_average;
		$response['connection_average']	= $connection_average;
		$response['rank'] 				= $state_average['rank'];

		return $response;
    }
    

    // 2016-5-10 for interested in smart meter
    public function set_interested()
    {
    	$mobile = $this->input->post('mobile', TRUE);

    	$query = $this->db->get_where('member_sms_verification', array('mobile' => $mobile));
    	if($query->num_rows() > 0)
    	{
    		$this->db->where('mobile', $mobile);
	    	$this->db->update('member_sms_verification', array('is_interested' => 'Yes'));
	    	return TRUE;
    	}
    	else
    		return FALSE;
    	
    }


    /** For Maharastra Region */

    // Method to check if connection exists in database
    public function is_connection_id_exist_maharashtra($connection_id)
    {
    	$query = $this->db->get_where("server_connection_list_maharashtra",array('connection_id'=>$connection_id));
		if ($query->num_rows() > 0) 
		{
			$data=$query->row_array();
			$query->free_result();
			return $data;	
		}		
		return FALSE;
    }

    // for insertion in database
    public function do_insertion_userdata_maharashtra($connection_id, $bu)
    {
    	$maharashtra_data = $this->get_maharashtra_data($connection_id, $bu);
    	if($maharashtra_data)
		{
			$maharashtra_connection_details = array(
				'connection_id' 	=> $maharashtra_data['connection'],
				'bu' 				=> $maharashtra_data['bu'],
				'name' 				=> $maharashtra_data['name'],
				'connection_type'	=> $maharashtra_data['connection_type'],					
				'address' 			=> $maharashtra_data['address'],					
				'status' 			=> $maharashtra_data['status'],
				'state' 			=> 'Maharashtra',
				'provider' 			=> 'mahadiscom',
			);
			$status = $this->db->insert('server_connection_list_maharashtra', $maharashtra_connection_details);
			if($status)
			{
				$current_insert_id = $this->db->insert_id();
				$this->insert_usage_data_maharashtra($maharashtra_data['usage'], $current_insert_id);
			}
		}

		return $maharashtra_data;
    }

    public function get_maharashtra_data($connection_id, $bu, $last_update = FALSE)
    {
    	if($last_update)
		{
			$temp_d_format = date_create_from_format("Y-m-d",$last_update);
			if($temp_d_format)
			{
				$last_update = date_format($temp_d_format,"Y-m-d");
			}
			else
			{
				$last_update = FALSE;
				$error_result['status'] = "error";
				$error_result['message'] = "Sorry,last update date is invalid.";
				$error_result['data'] = array();
				print_r(json_encode($error_result)); exit;	
			}
		}
		// set post fields for curl request
		$post_fields['uiActionName'] = 'getBillingDetail';
		$post_fields['consumerNumber'] = $connection_id;
		$post_fields['BU'] = $bu;
		$post_fields['hdnConsumerNumber'] = $connection_id;

		// set url to fetch data from
		$url = 'https://wss.mahadiscom.in/wss/wss';

		// execute curl 
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120); // timeout on connect
		// curl_setopt($ch, CURLOPT_TIMEOUT, 120);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
		$data = curl_exec($ch);
		curl_close($ch);		
		// curl end
		$html = $this->dom_parser->str_get_html($data);
		
		// get billing history table
		$tag = $html->getElementById('grdCustBillingDetails');
		// alternative of above code
		// $tag = $html->find('table',3);

		// if tag is null or empty send error response
		if($tag == "" || $tag == null)
		{
			$error_result['status'] = "error";
			$error_result['message'] = "Combination of consumer number ,consumer type and BU do not match";
			$error_result['data'] = array();
			print_r(json_encode($error_result)); 
			exit;	
		}

		$final_usage_data = array();
		$final_response = array();
		$loop_count = -1;
		$account_status = '';
		foreach ($tag->find('tr') as $single_row) 
		{
			if ($loop_count == -1)
			{
				$loop_count++;
				continue;
			}

			$bill_month = trim(strip_tags($single_row->find('td', 0)->innertext));
			$billing_date = date("Y-m-t", strtotime($bill_month));

			$usage_data['billing_date'] = $billing_date;
			$usage_data['units'] 		= trim(strip_tags($single_row->find('td', 1)->innertext));
			$usage_data['bill_amount'] 	= floatval(str_replace(',','',trim(strip_tags($single_row->find('td', 3)->innertext))));
			$usage_data['paid_amount'] 	= floatval(str_replace(',','',trim(strip_tags($single_row->find('td', 4)->innertext))));
			$payment_date 				= trim(strip_tags($single_row->find('td', 5)->innertext));
			$formatted_payment_date 	= date_create_from_format('d M Y', $payment_date);

			if ($formatted_payment_date)
				$usage_data['payment_date'] = date_format($formatted_payment_date, 'Y-m-d');
			else
				$usage_data['payment_date'] = trim(strip_tags($single_row->find('td', 5)->innertext));
			
			if($last_update)
			{
				if( $usage_data['billing_date'] <= $last_update )
				{
					$loop_count++;
					continue;
				}
			}
			if($usage_data['payment_date'])
				$usage_data['status'] = 'Paid';
			else
				$usage_data['status'] = 'Unpaid';

			if($loop_count == 0)
			{
				$account_status  = trim(strip_tags($single_row->find('td', 2)->innertext));
			}
			array_push($final_usage_data, $usage_data);
			$loop_count++;
		}
		// print_r($final_usage_data);exit;
		// get connection details table
		$details_table = $html->find('table',2);
		// echo $details_table; exit;
	
		if($details_table == "" || $details_table == null)
		{
			$error_result['status'] = "error";
			$error_result['message'] = "Combination of consumer number ,consumer type and BU do not match";
			$error_result['data'] = array();
			print_r(json_encode($error_result)); 
			exit;	
		}

		$first_row 			= $details_table->find('tr',1);
		$second_row 		= $details_table->find('tr',2);
		$third_row 			= $details_table->find('tr',3);

		$connection 		= trim(strip_tags($first_row->find('td', 1)->innertext));
		$bu 				= trim(strip_tags($first_row->find('td', 3)->innertext));
		$name 			 	= trim(strip_tags($second_row->find('td', 1)->innertext));
		$connection_type	= trim(strip_tags($second_row->find('td', 3)->innertext));
		$address 			= trim(strip_tags($third_row->find('td', 1)->innertext));
		
		$final_response['connection']		= $connection;
		$final_response['bu']				= $bu;
		$final_response['name']				= $name;
		$final_response['connection_type'] 	= $connection_type;
		$final_response['address']			= $address;
		$final_response['status'] 			= $account_status;
		$final_response['usage'] 			= $final_usage_data;

		return $final_response;
		// print_r(json_encode($final_response));exit;
    }

    // to insert usage data in database table
    public function insert_usage_data_maharashtra($usage_data,$current_insert_id)
	{
		if (count($usage_data) > 0)
		{
			$all_usage_data = array();
			foreach (array_reverse($usage_data) as $single_usage_data) 
			{				
				$temp_array = array(
					'fk_connection_id'	=>	$current_insert_id,
					'billing_date'		=>	$single_usage_data['billing_date'],						
					'units'				=>	$single_usage_data['units'],
					'bill_amount'		=>	$single_usage_data['bill_amount'],
					'paid_amount'		=>	$single_usage_data['paid_amount'],
					'payment_date'		=>	$single_usage_data['payment_date'],
					'status' 			=> 	$single_usage_data['status'],

				);
				array_push($all_usage_data, $temp_array);				
			}
			$this->db->insert_batch('server_usage_history_maharashtra', $all_usage_data);
		}
	}
	// update if update data is available
	public function do_update_userdata_maharashtra($connection_id, $bu, $connection_detail_id, $last_update)
	{
		$maharashtra_data = $this->get_maharashtra_data($connection_id, $bu, $last_update);
		if($maharashtra_data)
		{
			$this->delete_usage_data_maharashtra($connection_detail_id, $last_update);

			$this->insert_usage_data_maharashtra($maharashtra_data['usage'], $connection_detail_id);
		}

		return $maharashtra_data;
	}

	// delete data after last_update date
	public function delete_usage_data_maharashtra($connection_detail_id, $last_update = FALSE)
	{
		$this->db->where('fk_connection_id',$connection_detail_id);
		if ($last_update != FALSE)
		{
			$this->db->where('billing_date >',$last_update);
		}

		$this->db->delete('server_usage_history_maharashtra');

	}

	/** For Maharashtra data Ends */

	/** For Delhi State */
	// fetch data from bses provider for delhi state
	public function get_delhi_data($username, $password, $last_update = FALSE)
	{
		// check if username and password are empty
		if (empty($username) || empty($password)) 
		{
			$result['status'] = "error";
			$result['message'] = "Please Type Username and Password";
			$result['data'] = array();
			print_r(json_encode($result)); 
			exit;	
		} 

		// set post fields
		$post_fields['consUserName'] = $username;
		$post_fields['consPassword'] = $password;

		$url = 'http://www.bsesdelhi.com/bsesdelhi/registerUser.do';
		$billing_url = 'http://www.bsesdelhi.com/bsesdelhi/billDetails.do?caNumber='.$username;

		$this->ckfile = dirname(__FILE__) . '\cookies_delhi.txt';

		if (file_exists($this->ckfile)) {
		    @file_put_contents($this->ckfile, "");
		} 
		// execute curl 
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120); // timeout on connect
		// curl_setopt($ch, CURLOPT_TIMEOUT, 120);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);

		curl_setopt($ch, CURLOPT_COOKIEJAR, $this->ckfile);

		// curl_setopt($ch, CURLOPT_COOKIEFILE, $this->ckfile);
		$data = curl_exec($ch); // connection details data

		curl_setOpt($ch, CURLOPT_HTTPGET, TRUE);
		curl_setOpt($ch, CURLOPT_POST, FALSE);
		curl_setopt($ch, CURLOPT_URL, $billing_url);   

		$data2 = curl_exec($ch); // billing details data
		curl_close($ch);	
		// echo $data; exit;
		$this->load->library('dom_parser');
		$html1 = $this->dom_parser->str_get_html($data);
		
		// if login credentials are invalid
		if (strpos($html1,'You have entered a wrong Username or password, please try again.') != FALSE) 
		{
			$result['status'] = "error";
			$result['message'] = "Sorry, The user credentials are invalid";
			$result['data'] = array();
			print_r(json_encode($result)); 
			exit;	
		}

		$tag1 = $html1->find('table', 0);
		// echo $tag1; exit;

		if($tag1 == '' || $tag1 == null)
		{
			$result['status'] = 'error';
			$result['message'] = 'Sorry, The user credentials are invalid';
			$result['data'] = array();
			print_r(json_encode($result)); 
			exit;
		}

		$connection = trim(strip_tags($html1->find('.gray_txt', 0)->innertext));

		// for name
		$first_row = $tag1->find('tr', 0);
		$name = trim(strip_tags($first_row->find('td',2)->innertext));
		// name
		// echo $first_col; exit;
		
		// $name_table = $first_col->find('table', 0);
		// $name_row = $name_table->find('tr', 0);
		// $name_col = trim(strip_tags($name_row->find('td', 0)->innertext));
		// echo $name_col; exit;
		
		// for address
		$second_row = $tag1->find('tr', 3);		
		$address = trim(strip_tags($second_row->find('td',0)->innertext));

		// address
		// echo $second_col;exit;
		
		$html2 = $this->dom_parser->str_get_html($data2);
		$tag2 = $html2->find('table', 0);

		if($tag2 == '' || $tag2 == null)
		{
			$result['status'] = 'error';
			$result['message'] = 'Sorry, The user credentials are invalid';
			$result['data'] = array();
			print_r(json_encode($result)); 
			exit;
		}
		// echo $tag2; exit;

		$final_response = array();
		$final_usage_data = array();
		$loop_count = -1;
		foreach ($tag2->find('tr') as $single_row) 
		{
			if ($loop_count <= 2)
			{
				$loop_count++;
				continue;
			}

			$bill_month = trim(strip_tags($single_row->find('td', 0)->innertext));
			$billing_date = date("Y-m-t", strtotime($bill_month));
			$usage_data['billing_date'] = $billing_date;
			
			if($last_update)
			{
				if( $usage_data['billing_date'] <= $last_update )
				{
					$loop_count++;
					continue;
				}
			}
			$usage_data['units'] 		= trim(strip_tags($single_row->find('td', 2)->innertext));
			$usage_data['paid_amount'] 	= trim(strip_tags($single_row->find('td', 7)->innertext));
			$payment_date 				= trim(strip_tags($single_row->find('td', 8)->innertext));
			if(empty($payment_date))
			{
				$usage_data['payment_date'] = '0000-00-00';
			}
			else
			{
				$formatted_payment_date 	= date_create_from_format('d-M-Y', $payment_date);

				if ($formatted_payment_date)
					$usage_data['payment_date'] = date_format($formatted_payment_date, 'Y-m-d');
				else
					$usage_data['payment_date'] = trim(strip_tags($single_row->find('td', 8)->innertext));
			}
			
			
			if(empty($usage_data['payment_date']) || $usage_data['payment_date'] == '0000-00-00')
				$usage_data['status'] = 'Unpaid';
			else
				$usage_data['status'] = 'Paid';

			array_push($final_usage_data, $usage_data);
			$loop_count++;
		}

		$final_response['connection']	=	$connection;
		$final_response['name']			=	$name;
		$final_response['address']		=	$address;
		$final_response['status']		=	'Live';
		$final_response['usage']        =	$final_usage_data;

		return $final_response; 
		// print_r(json_encode($final_response)); exit;
	}

	// save scrapped data from bses page and save it in database
	public function save_delhi_data($username, $password, $last_update = FALSE)
	{
		// scraped data from bses page
		$all_fetched_data = $this->get_delhi_data($username, $password, $last_update);

		if($all_fetched_data)
		{
			// check if connection detail exist in our database
			$connection_detail = $this->is_connection_id_exist_delhi($all_fetched_data['connection']);
			
			if ($connection_detail)
			{
				// if exist update the connection list 
				$this->update_connection_data_delhi($all_fetched_data, $connection_detail['id'], $last_update);
			}
			else
			{	
				//if connection detail doesn't exist add connection and billing data in database
				$this->add_connection_data_delhi($all_fetched_data);
			}
		}
		// return fetched data
		return $all_fetched_data;

	}

	// check if connection data is available in database
	public function is_connection_id_exist_delhi($connection_id)
	{
		$query = $this->db->get_where("server_connection_list_delhi",array('connection_id'=>$connection_id));
		if ($query->num_rows() > 0) 
		{
			$data=$query->row_array();	
			return $data;
		}
		return FALSE;
	}

	// ad connection along with usage data in table
	public function add_connection_data_delhi($all_fetched_data)
	{
		if ($all_fetched_data)
		{
			$temp_data = array(
					'connection_id'	=>	$all_fetched_data['connection'],
					'name'         	=>	$all_fetched_data['name'],
					'address'      	=>	$all_fetched_data['address'],
					'status'   		=>	$all_fetched_data['status'],
					'state'        	=>	'delhi',
					'provider'     	=>	'bses',
					
				);
			// insert connection data in table
			$status = $this->db->insert('server_connection_list_delhi', $temp_data);

			if ($status)
			{
				$current_insert_id = $this->db->insert_id();
				$this->insert_usage_history_delhi($all_fetched_data['usage'], $current_insert_id);
			}
		}
	}


	public function insert_usage_history_delhi($usage_history_data, $current_insert_id)
	{
		if (count($usage_history_data) > 0)
		{
			$all_usage_history_array = array();

			// generate array for batch insert usage data in table
			foreach (array_reverse($usage_history_data) as $single_usage_history) 
			{
				$temp_array = array(
					'fk_connection_id'=>$current_insert_id,
					'billing_date'=>$single_usage_history['billing_date'],					
					'units'=>$single_usage_history['units'],
					'paid_amount'=>$single_usage_history['paid_amount'],
					'payment_date'=>$single_usage_history['payment_date'],
					'status'=>$single_usage_history['status'],
				);
				array_push($all_usage_history_array, $temp_array);
			}
			// insert batch usage history of delhi state
			$this->db->insert_batch('server_usage_history_delhi', $all_usage_history_array);
		}
	}

	// update billing detail if connection already exists
	public function update_connection_data_delhi($all_fetched_data, $connection_number, $last_update)
	{
		if($all_fetched_data)
		{
			$this->delete_delhi_data($connection_number, $last_update);
			$this->insert_usage_history_delhi($all_fetched_data['usage'], $connection_number);
		}
	}

	// delete data where biling date greater than last_update date
	public function delete_delhi_data($connection_no, $last_update)
	{
		$this->db->where('fk_connection_id', $connection_no);

		if ($last_update != FALSE)
			$this->db->where('billing_date >', $last_update);
		
		$this->db->delete('server_usage_history_delhi');
	}


	// to fetch billing units
	// 
	public function get_all_billing_units($srch = '')
	{
		if($srch)
		{
			$where = "code like '%$srch%' OR circle like '%$srch%'"; 
			$this->db->where($where);
		}
		$query = $this->db->get('maharashtra_bu');
		if($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		return array();
	}	
}