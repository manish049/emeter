<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/



class Mobile_model extends CI_Model
{
	function __construct()
    {
        // Construct our parent class
        parent::__construct(); 
        $this->load->helper('inflector');

    }
	public function get_region_list($last_update)
	{
		
		$this->db->select_max('last_update');
		
		$new_last_update_query = $this->db->get('emt_region_list');
		if ($new_last_update_query->num_rows() > 0) 
		{
			$res_last_update = $new_last_update_query->row_array();		
			$new_last_update = $res_last_update['last_update'];		
		}
		else
		{
			$new_last_update = 'no';
		}
		
		if($new_last_update != $last_update )
		{	
			if($last_update)
			{
				$query = $this->db->get_where("emt_region_list",array('last_update >'=>$last_update));
			}
			else
			{
				
				$query = $this->db->get("emt_region_list");
				
			}
				
			if ($query->num_rows() > 0) 
			{
				$data_all=$query->result_array();	
				$data= array('status'=>'success','last_update'=>$new_last_update,'data'=>$data_all	);	
			}
			else
			{
				$data = false;
			}
			
			$query->free_result();	
		}
		else
		{
			$data =false;
		}
			
		return $data;

	}

	public function get_section_list($last_update)
	{
		
		$this->db->select_max('last_update');
		$new_last_update_query = $this->db->get('emt_section_list');

		if ($new_last_update_query->num_rows() > 0) 
		{
			$res_last_update = $new_last_update_query->row_array();		
			$new_last_update = $res_last_update['last_update'];		
		}
		else
		{
			$new_last_update = 'no';
		}
		
		if($new_last_update != $last_update )
		{	
			if($last_update)
			{
				$query = $this->db->get_where("emt_section_list",array('last_update >'=>$last_update));
			}
			else
			{
				$query = $this->db->get("emt_section_list");
				
			}
				
			if ($query->num_rows() > 0) 
			{
				$data_all=$query->result_array();	
				$data= array('status'=>'success','last_update'=>$new_last_update,'data'=>$data_all	);	
			}
			else
			{
				$data = false;
			}
			
			$query->free_result();	
		}
		else
		{
			$data =false;
		}
			
		return $data;

	
	}
	
	public function get_section_list_by_region_id($last_update,$region_id)
	{
		if(!$region_id)
		{
			$this->get_section_list($last_update);
			//exit;
		}
		
		$this->db->select_max('last_update');
		$new_last_update_query = $this->db->get('emt_section_list');

		if ($new_last_update_query->num_rows() > 0) 
		{
			$res_last_update = $new_last_update_query->row_array();		
			$new_last_update = $res_last_update['last_update'];		
		}
		else
		{
			$new_last_update = 'no';
		}
		
		if($new_last_update != $last_update )
		{	
			if($last_update)
			{
				$query = $this->db->get_where("emt_section_list",array('last_update >'=>$last_update,'region_id'=>$region_id));
			}
			else
			{
				$query = $this->db->get_where("emt_section_list",array('region_id'=>$region_id));
				
			}
				
			if ($query->num_rows() > 0) 
			{
				$data_all=$query->result_array();	
				$data= array('status'=>'success','last_update'=>$new_last_update,'data'=>$data_all	);	
			}
			else
			{
				$data = false;
			}
			
			$query->free_result();	
		}
		else
		{
			$data =false;
		}
			
		return $data;

	
	}
	
	public function get_distribution_list($last_update)
	{
		
		$this->db->select_max('last_update');
		$new_last_update_query = $this->db->get('emt_distribution_list');

		if ($new_last_update_query->num_rows() > 0) 
		{
			$res_last_update = $new_last_update_query->row_array();		
			$new_last_update = $res_last_update['last_update'];		
		}
		else
		{
			$new_last_update = 'no';
		}
		
		if($new_last_update != $last_update )
		{	
			if($last_update)
			{
				$query = $this->db->get_where("emt_distribution_list",array('last_update >'=>$last_update));
			}
			else
			{
				$query = $this->db->get("emt_distribution_list");
				
			}
				
			if ($query->num_rows() > 0) 
			{
				$data_all=$query->result_array();	
				$data= array('status'=>'success','last_update'=>$new_last_update,'data'=>$data_all	);	
			}
			else
			{
				$data = false;
			}
			
			$query->free_result();	
		}
		else
		{
			$data =false;
		}
			
		return $data;

	
	}
	
	public function get_distribution_list_by_section_id($last_update,$section_id)
	{
		if(!$section_id)
		{
			$this->get_section_list($last_update);
			//exit;
		}
		
		$this->db->select_max('last_update');
		$new_last_update_query = $this->db->get('emt_distribution_list');
		if ($new_last_update_query->num_rows() > 0) 
		{
			$res_last_update = $new_last_update_query->row_array();		
			$new_last_update = $res_last_update['last_update'];		
		}
		else
		{
			$new_last_update = 'no';
		}
		
		if($new_last_update != $last_update )
		{	
			$this->db->distinct();
		    $this->db->select();
   		 	$this->db->from("emt_distribution_list");        
	
			if($last_update)
			{
				$this->db->where('last_update >',$last_update);
				$this->db->where('section_id',$section_id);
				$query = $this->db->get();
			}
			else
			{
				$this->db->where('section_id',$section_id);
				$query = $this->db->get();
				
			}
				
			if ($query->num_rows() > 0) 
			{
				$data_all=$query->result_array();	
				$data= array('status'=>'success','last_update'=>$new_last_update,'data'=>$data_all	);	
			}
			else
			{
				$data = false;
			}
			
			$query->free_result();	
		}
		else
		{
			$data =false;
		}
			
		return $data;

	
	}
	
	public function get_distribution_list_by_reg_and_section_id($last_update,$region_id,$section_id)
	{
		if(!$region_id)
		{
			$this->get_distribution_list_by_section_id($last_update,$section_id);
			exit;
		}
		if(!$section_id)
		{
			$this->get_section_list($last_update);
			//exit;
		}
		
		$this->db->select_max('last_update');
		$new_last_update_query = $this->db->get('emt_distribution_list');
		if ($new_last_update_query->num_rows() > 0) 
		{
			$res_last_update = $new_last_update_query->row_array();		
			$new_last_update = $res_last_update['last_update'];		
		}
		else
		{
			$new_last_update = 'no';
		}
		
		if($new_last_update != $last_update )
		{	
			$this->db->distinct();
		    $this->db->select();
   		 	$this->db->from("emt_distribution_list");        
	
			if($last_update)
			{
				$this->db->where('last_update >',$last_update);
				$this->db->where('section_id',$section_id);
				$this->db->where('region_id',$region_id);
				$query = $this->db->get();
			}
			else
			{
				$this->db->where('section_id',$section_id);
				$this->db->where('region_id',$region_id);
				$query = $this->db->get();
				
			}
				
			if ($query->num_rows() > 0) 
			{
				$data_all=$query->result_array();	
				$data= array('status'=>'success','last_update'=>$new_last_update,'data'=>$data_all	);	
			}
			else
			{
				$data = false;
			}
			
			$query->free_result();	
		}
		else
		{
			$data =false;
		}
			
		return $data;

	
	}
	
	public function get_appliance_list($last_update)
	{
		
		$this->db->select_max('last_update');
		$new_last_update_query = $this->db->get('appliance_list');

		if ($new_last_update_query->num_rows() > 0) 
		{
			$res_last_update = $new_last_update_query->row_array();		
			$new_last_update = $res_last_update['last_update'];		
		}
		else
		{
			$new_last_update = 'no';
		}
		
		if($new_last_update != $last_update )
		{	
			if($last_update)
			{
				$query = $this->db->get_where("appliance_list",array('last_update >'=>$last_update));
			}
			else
			{
				$query = $this->db->get("appliance_list");
				
			}
				
			if ($query->num_rows() > 0) 
			{
				$data_all=$query->result_array();	
				$data= array('status'=>'success','last_update'=>$new_last_update,'image_path'=>base_url().'assets/images/appliance/','data'=>$data_all);	
			}
			else
			{
				$data = false;
			}
			
			$query->free_result();	
		}
		else
		{
			$data =false;
		}
			
		return $data;

	
	}
	
	public function get_appliance_by_id($last_update,$id)
	{
		if(!$id)
		{
			$this->get_section_list($last_update);
			//exit;
		}
		
		$this->db->select_max('last_update');
		$new_last_update_query = $this->db->get('appliance_list');

		if ($new_last_update_query->num_rows() > 0) 
		{
			$res_last_update = $new_last_update_query->row_array();		
			$new_last_update = $res_last_update['last_update'];		
		}
		else
		{
			$new_last_update = 'no';
		}
		
		if($new_last_update != $last_update )
		{	
			if($last_update)
			{
				$query = $this->db->get_where("appliance_list",array('last_update >'=>$last_update,'id'=>$id));
			}
			else
			{
				$query = $this->db->get_where("appliance_list",array('id'=>$id));
				
			}
				
			if ($query->num_rows() > 0) 
			{
				$data_all=$query->result_array();	
				$data= array('status'=>'success','last_update'=>$new_last_update,'data'=>$data_all	);	
			}
			else
			{
				$data = false;
			}
			
			$query->free_result();	
		}
		else
		{
			$data =false;
		}
			
		return $data;

	
	}
	
	public function get_section_id_by_name($name)
	{
		if(!$name)
		{
			return false;
			//exit;
		}
		$this->db->select('emt_region_list.code region_id,emt_section_list.code section_id,emt_section_list.name');
		$this->db->join('emt_region_list','emt_region_list.id = emt_section_list.region_id');
		$query = $this->db->get_where("emt_section_list",array('emt_section_list.name'=>$name));
		if ($query->num_rows() > 0) 
		{
			return $query->row_array();	
		}
		$query->free_result();	
		return false;

	
	}
	public function is_connection_id_exist($connection_id)
	{
			$query = $this->db->get_where("server_connection_list",array('connection_id'=>$connection_id));
			if ($query->num_rows() > 0) 
			{
				$data=$query->row_array();	
			}
			else
			{
				$data = false;
			}
			return $data;
	}
	
	public function do_insertion_userdata($service_no_ip)
	{
		
		$all_data_fetched = $this->get_fetched_data_server($service_no_ip);
		/*
		if(count($all_data_fetched['usage']) >0)
		{
			 $ud_assessment_date = $all_data_fetched['usage'][0]['assessment_date'];
	  		 $ud_reading=$all_data_fetched['usage'][0]['reading'];
	  		 $ud_units = $all_data_fetched['usage'][0]['units'];
	  		 $ud_amount = $all_data_fetched['usage'][0]['amount'];
	  		 $ud_payment_date = $all_data_fetched['usage'][0]['payment_date'];
	   		 $ud_status = $all_data_fetched['usage'][0]['status'];
		}
		else
		{
			 $ud_assessment_date = "Assessment Entry Date";
	  		 $ud_reading= "Reading";
	  		 $ud_units = "Consumed Unit";
	  		 $ud_amount = "Total BillAmount(Rs.)";
	  		 $ud_payment_date = "Payment Date";
	   		 $ud_status = "AssessmentStatus";
		}
		
		
		$data = array(
			'connection_id' => $service_no_ip,
		   'serviceno' => $all_data_fetched['ServiceNo'] ,
		   'name' => $all_data_fetched['ServiceNo'] ,
		   'region' => $all_data_fetched['ServiceNo'],
		   'phase' => $all_data_fetched['Phase'],
		   'circle' => $all_data_fetched['Circle'],
		   'section' => $all_data_fetched['Section'],
		   'load' => $all_data_fetched['Load'],
		   'distribution' => $all_data_fetched['Distribution'],
		   'meterno' => $all_data_fetched['MeterNo'],
		   'connectionnumber' => $all_data_fetched['ConnectionNumber'],
		   'address' => $all_data_fetched['Address'],
		   'servicestatus' => $all_data_fetched['ServiceStatus'],
		   
		   'ud_assessment_date' => $ud_assessment_date,
		   'ud_reading' => $ud_reading,
		   'ud_units' => $ud_units,
		   'ud_amount' => $ud_amount,
		   'ud_payment_date' => $ud_payment_date,
		   'ud_status' => $ud_status
	   
		);
		$status = $this->db->insert('server_connection_list', $data); 
		// $current_insert_id = 1;
		if($status)
		{
			$current_insert_id = $this->db->insert_id();
			//$this->insert_user_data($all_data_fetched,$current_insert_id,true);
		}
		*/
		return $all_data_fetched;
	}
	
	public function insert_user_data($all_data_fetched,$current_insert_id,$is_first_time = false)
	{
	/*
		if((count($all_data_fetched['usage']) > 1 && $is_first_time) ||(count($all_data_fetched['usage']) > 0 && !$is_first_time) )
			{
				$all_data_usage_batch_insert = array();
				$count_loop = 0;
				foreach($all_data_fetched['usage'] as $single_data_usage)
				{
					if($count_loop == 0)
					{
						$count_loop=$count_loop+1;
						continue;
					}
	
					$cur_data = array(
									  'fk_connection_id' => $current_insert_id ,
									  'assessment_date' => $single_data_usage['assessment_date'],
									  'reading' => $single_data_usage['reading'],
									  'units' => $single_data_usage['units'],
									  'amount' => $single_data_usage['amount'],
									  'payment_date' =>   $single_data_usage['payment_date'],
									  'status' => $single_data_usage['status']
									);
					array_push($all_data_usage_batch_insert,$cur_data);
				}
				$this->db->insert_batch('server_usage_data', $all_data_usage_batch_insert); 
			}
			*/
	}
	
	public function do_update_userdata($service_no_ip,$connection_detail,$last_update)
	{
		//NOTE: $last_update is max assessment_date of client;
		$today_date = date("Y-m-d");
		/*
		if($today_date == $last_update)
		{
			$all_data_fetched = $this->get_fetched_data_server($service_no_ip,$connection_detail);
			return $all_data_fetched;
		}
		
		$this->db->select_max('assessment_date');
		$this->db->where('fk_connection_id',$connection_detail['id']);
		$this->db->order_by("assessment_date", "desc");
		$query = $this->db->get("server_usage_data");
		if ($query->num_rows() > 0) 
		{
			$max_date = $query->row_array();	
		}
		else
		{
			$max_date ="";
		}
		
		if($max_date == "")
		{
			$all_data_fetched = $this->get_fetched_data_server($service_no_ip);
		}
		else
		{
			$all_data_fetched = $this->get_fetched_data_server($service_no_ip,$last_update);
		}
		*/
		$all_data_fetched = $this->get_fetched_data_server($service_no_ip,$last_update);
		//print_r($all_data_fetched); exit;
		
		//$this->insert_user_data($all_data_fetched,$connection_detail['id'],false);
		return $all_data_fetched;
	}
	
	public function get_fetched_data_server($service_no_ip,$last_update= false)
	{
		
		error_reporting(0);
		//phpinfo(); exitl
		//	$last_update ="2015-1-1";
		if($last_update)
		{
			$temp_d_format =date_create_from_format("Y-m-d",$last_update);
			if($temp_d_format)
			{
				$last_update = date_format($temp_d_format,"Y-m-d");
			}
			else
			{

				$last_update = false;
				$error_result['status'] = "error";
				$error_result['data'] = "Sorry,last update data is invalid.";
				print_r(json_encode($error_result)); exit;	
			}
		}
		//echo $last_update; exit;
	
		$regno = substr($service_no_ip, 0, 2); // first 2 digits

		if($regno !=  ('01'|| '02'|| '03'|| '04'|| '05'|| '06'|| '07'|| '08'|| '09'))
		{
			$error_result['status'] = "error";
			$error_result['data'] = "Sorry, Your service no. is not valid. Please try with proper input..";
			print_r(json_encode($error_result)); exit;	
		}


		$code = $regno; // same as the reg no
        $sec = substr($service_no_ip,2, 3); // next 3 digits
        $dist = substr($service_no_ip,5, 3); // next 3 digits
        $serno = substr($service_no_ip,8, 99); // remaining digits
        $temp_sn = $serno;
        $disp_service_no = $regno . '-' . $sec. '-' . $dist . '-'. $serno;

		$loop_cur_count =-1;
		$html = "";
		$extra_ordinary = FALSE;
		if($regno == 2 || $regno ==4)
		{
			
			if(strlen($serno) != 4 )
			{
				switch (strlen($serno))
				{
					case 1:
					$serno = "000".$serno;
					break;
					case 2:
					$serno = "00".$serno;
					break;
					case 3:
					$serno = "0".$serno;
					break;
					default:
					break;
					
				}
				$service_no_ip = $regno.$sec.$dist.$serno;
			}
			if (($regno == 4 || $regno == 2)  && strlen($temp_sn)==3) {
				//echo "three<br />";
				$serno = $temp_sn;
				$service_no_ip = $regno.$sec.$dist.$serno;
			}
			//echo $service_no_ip."<br />";
			$url = 'https://wss.tangedco.gov.in/wss/AccSummaryNew.htm?scnumber='.urlencode($service_no_ip);

			$this->load->library('dom_parser');
			//$html = $this->dom_parser->file_get_html("http://tneb.tnebnet.org/newlt/consumerwise_gmc_report.php?encserno=VEFOR0VEQ098fDEyMXx8MDA5fHw4&rsno=MQ%3D%3D");
			$html = $this->dom_parser->file_get_html($url);
			
			if (strpos($html,'Invalid Input') != FALSE) 
			{
				//echo "hello";
				$extra_ordinary = TRUE;
				//echo $serno;

    			$plainserno = 'TANGEDCO||'. $sec .'||'. $dist. '||' . $serno; // introduced as TANGEDCO changed the format to base64 encoded string
    			//$plainserno = 'TANGEDCO||'. $sec .'||'. $dist. '||987'; // introduced as TANGEDCO changed the format to base64 encoded string
            	
            	$encserno = base64_encode($plainserno);
            	//echo $encserno;
            	// exit;
            	//echo urlencode($encserno);
			 	$plainrsno = $regno;
			 
				while(substr($plainrsno,0, 1) == '0')
				{
					$plainrsno = substr($plainrsno,1, strlen($plainrsno));
				}// remove leading zeroes
								 
				$rsno = base64_encode($plainrsno);

				//echo "<br />".urlencode($rsno);
				//exit;
				//echo "http://tneb.tnebnet.org/newlt/consumerwise_gmc_report.php?encserno=VEFOR0VEQ098fDAyOHx8MDA2fHwwNDQx&rsno=NA%3D%3D";
				$url = 'http://tneb.tnebnet.org/newlt/consumerwise_gmc_report.php?encserno='.urlencode($encserno).'&rsno='.urlencode($rsno);
				
				$this->load->library('dom_parser');
			
				//$html = $this->dom_parser->file_get_html("http://tneb.tnebnet.org/newlt/consumerwise_gmc_report.php?encserno=VEFOR0VEQ098fDAyOHx8MDA2fHw0NDE%3D&rsno=NA%3D%3D");
				$html = $this->dom_parser->file_get_html($url);		
				//echo $html;exit;		
			}
		}
		else
		{
			// For breaking security while call url
			$plainserno = 'TANGEDCO||'. $sec .'||'. $dist. '||' . $serno; // introduced as TANGEDCO changed the format to base64 encoded string
            $encserno = base64_encode($plainserno);
			 $plainrsno = $regno;
			 
			 while(substr($plainrsno,0, 1) == '0')
			 {
				 $plainrsno = substr($plainrsno,1, strlen($plainrsno));
			 }// remove leading zeroes
								 
			$rsno = base64_encode($plainrsno);
			
			$url = 'http://tneb.tnebnet.org/newlt/consumerwise_gmc_report.php?encserno='.urlencode($encserno).'&rsno='.urlencode($rsno);
			
			$this->load->library('dom_parser');
		
			//$html = $this->dom_parser->file_get_html("http://tneb.tnebnet.org/newlt/consumerwise_gmc_report.php?encserno=VEFOR0VEQ098fDEyMXx8MDA5fHw4&rsno=MQ%3D%3D");
			$html = $this->dom_parser->file_get_html($url);
					/* 
					NEW POST METHOD (Now it is GET method)
					$url = 'http://tneb.tnebnet.org/newlt/consumerwise_gmc_report.php';//?encserno='.urlencode($encserno).'&rsno='.urlencode($rsno);
					$request_post = array(
					'http' => array(
						'method' => 'POST',
						'content' => http_build_query(array(
							'ctl00$ctl00$MasterPageContentPlaceHolder$ucLogin$txtUserName' => $encserno,
							'ctl00$ctl00$MasterPageContentPlaceHolder$ucLogin$txtPassword' => $rsno
						)),
					)
					);
					$context = stream_context_create($request_post);
					$html = $this->dom_parser->file_get_html($url,false, $context);
					*/

		}
		//echo $html;exit;

		$tag = $html->find('table',7);
		
		
		// $error_result['status'] = "error";
		// $error_result['data'] = $tag;
		// print_r(json_encode($error_result)); exit;	

		//echo urlencode(strip_tags($tag->find("tr",0)->innertext)); exit;
	
		if($tag && urlencode($tag->innertext) == "++%3Ctr+align%3D%22center%22%3E%3Ctd+align%3D%22center%22%3E++%3C%2Ftd%3E%3C%2Ftr%3E" || urlencode($tag->innertext) == "+%3Ctr+align%3D%22center%22%3E%3Ctd+align%3D%22center%22%3E+%3C%2Ftd%3E%3C%2Ftr%3E")
		{
			//for 021000048 && 011210078
			//If Dues To Be Paid  Exist
			$tag = $html->find('table',9);

			
		}
		else if($tag && urlencode(strip_tags($tag->find("tr",0)->innertext)) == "++%09%09%09%09Electricity+Consumption++%09%09%09")
		{
			$tag = $html->find('table',8);
			
		}
		else if($tag && urlencode(strip_tags($tag->find("tr",0)->innertext)) == "++%09%09%09%09Disconnection%2FReconnection+Details++%09%09%09")
		{
			$tag = $html->find('table',9);
			
		}
		
		$count = 0;
		$my_all_result =array();
		$my_all_history = array();
		$my_single_history = array();
		
		//echo $tag;
		if($tag == "" || $tag == null)
		{
			$error_result['status'] = "error";
			$error_result['data'] = "Sorry, Your service no. is not valid. Please try with proper input.";
			print_r(json_encode($error_result)); 
			exit;	
		}
		
		
			
		foreach($tag->find('tr') as $single_row)
		{
			
					//For region 2 and 4 api, there is title at top so skip it
					if(($regno == 2 || $regno ==4) && $loop_cur_count == -1 && $extra_ordinary == FALSE)
					{
						$loop_cur_count = $loop_cur_count+1;
						continue; //Skip top title
					}
					
					$loop_cur_count = $loop_cur_count+1;
					if($last_update)
					{
						if($loop_cur_count == 0)
						{
							continue;
						}
						$my_single_history_temp_ass = str_replace('&nbsp;', '',trim(strip_tags($single_row->find('td',1)->innertext)));
						$my_convert_try = date_create_from_format("d/m/Y",$my_single_history_temp_ass);
						if($my_convert_try)
						{
							$my_single_history['assessment_date']= date_format($my_convert_try,"Y-m-d");
							
						}
						else
						{
								if($my_single_history_temp_ass == "-" && $loop_cur_count > 1)
								{	
									$my_single_history['assessment_date'] = $my_single_history['assessment_date'];
								}
								else
								{
									$my_single_history['assessment_date'] = $my_single_history_temp_ass;
								}
						}
						if($my_single_history['assessment_date'] <= $last_update )
						{
							continue;
						}
					}
					else
					{
						
						
						$my_single_history_temp_ass = str_replace('&nbsp;', '',trim(strip_tags($single_row->find('td',1)->innertext)));
						if($loop_cur_count == 0)
						{
							$my_single_history['assessment_date']=$my_single_history_temp_ass;
						}
						else
						{
							$my_convert_try_else = date_create_from_format("d/m/Y",$my_single_history_temp_ass);
							if($my_convert_try_else)
							{
								$my_single_history['assessment_date']= date_format($my_convert_try_else,"Y-m-d");
							}
							else
							{
							
								if($my_single_history_temp_ass == "-")
								{	
									$my_single_history['assessment_date'] = $my_single_history['assessment_date'];
								}
								else
								{
									$my_single_history['assessment_date']=$my_single_history_temp_ass;
								}
							}
						}
					}
					
					$my_single_history['reading'] =str_replace('&nbsp;', '',trim(strip_tags($single_row->find('td',2)->innertext)));
					$my_single_history['units']=str_replace('&nbsp;', '',trim(strip_tags($single_row->find('td',3)->innertext)));
					if($my_single_history_temp_ass == "-")
					{
						 $my_single_history['amount']=$single_row->find('td',11)->innertext;
					}
					else
					{
						$my_single_history['amount']=str_replace('&nbsp;', '',trim(strip_tags($single_row->find('td',9)->innertext)));
					}
					$my_single_history_temp= str_replace('&nbsp;', '',strip_tags($single_row->find('td',13)->innertext));
					
					
					if($loop_cur_count == 0)
					{
						$my_single_history['payment_date']=$my_single_history_temp;
					}
					else
					{
						$my_convert_try_sec = date_create_from_format("d/m/Y",$my_single_history_temp);
						if($my_convert_try_sec)
						{
							$my_single_history['payment_date']= date_format($my_convert_try_sec,"Y-m-d");
							
						}
						else
						{
							$my_single_history['payment_date'] = $my_single_history_temp;
						}
					}

					$orginal= $single_row->find('td',15)->innertext;
					$my_single_history_status = str_replace('&nbsp;', '', strip_tags($single_row->find('td',15)->innertext));
					
					if($my_single_history['payment_date'] == "" && ($my_single_history_status == "NORMAL" OR $my_single_history_status == "NOT IN USE") )
					{
						if($my_convert_try_sec && $my_single_history_status == "NORMAL")
						{
							$my_single_history['status']="Paid";
						}
						else if($my_single_history_status == "NORMAL")
						{
							$my_single_history['status']="Unpaid";
						}
						else
						{
						$my_single_history['status']=$my_single_history_status;
						}
						$my_single_history['payment_date'] = "0000-00-00";
						
					}
					else if($my_single_history_status == "NORMAL")
					{
						if($my_convert_try_sec)
						{
							$my_single_history['status']="Paid";
						}
						else
						{
							$my_single_history['status']="Unpaid";
						}
						
					}
					else
					{
						if($my_convert_try_sec)
						{
								if($my_single_history_status == null)
								{
									$my_single_history['status']= "Paid";
								}
								else
								{
									$my_single_history['status'] = $my_single_history_status;
								}
						}
						else
						{
							$my_single_history['status']=$my_single_history_status;
						}
					}
					
					array_push($my_all_history,$my_single_history);
					
			//print_r($single_row->innertext);
			
		}
		//var_dump($extra_ordinary);
		if(($regno == 2 || $regno ==4) && $extra_ordinary == FALSE)
		{
			$tag_sub_head = $html->find('table',2);
			
			$first_row = $tag_sub_head->find('tr',0);
			$second_row = $tag_sub_head->find('tr',1);
			$third_row = $tag_sub_head->find('tr',2);
			$fourth_row = $tag_sub_head->find('tr',3);
			$fifth_row = $tag_sub_head->find('tr',4);
			$sixth_row = $tag_sub_head->find('tr',5);
			$seventh_row = $tag_sub_head->find('tr',6);
			$eighth_row = $tag_sub_head->find('tr',7);
			$consumer_name= str_replace('&nbsp;', '',trim(strip_tags($first_row->find('td',1)->innertext)));
			$region_name= str_replace('&nbsp;', '',trim(strip_tags($second_row->find('td',1)->innertext)));
			$circle_name= str_replace('&nbsp;', '',trim(strip_tags($third_row->find('td',1)->innertext)));
			$phase_name= str_replace('&nbsp;', '',trim(strip_tags($third_row->find('td',3)->innertext)));
			$section_name= str_replace('&nbsp;', '',trim(strip_tags($fourth_row->find('td',1)->innertext)));
			$load_name= str_replace('&nbsp;', '',trim(strip_tags($fourth_row->find('td',3)->innertext)));
			$distribution_name= str_replace('&nbsp;', '',trim(strip_tags($fifth_row->find('td',1)->innertext)));
			$service_number = str_replace('&nbsp;', '',trim(strip_tags($sixth_row->find('td',1)->innertext)));
			$meter_number = str_replace('&nbsp;', '',trim(strip_tags($sixth_row->find('td',3)->innertext)));
			$address_name = str_replace('&nbsp;', '',trim(strip_tags($seventh_row->find('td',1)->innertext)));
			$service_status = str_replace('&nbsp;', '',trim(strip_tags($eighth_row->find('td',1)->innertext)));
				
		}
		else
		{
			// print_r($my_all_history);exit;
			$tag_head = $html->find('table',2);
			
			$consumer_head= trim(strip_tags($tag_head->find('tr',0)->find('td',0)->outertext));
			$consumer_name_array = explode("CONSUMER NAME:",$consumer_head);
			
			$tag_sub_head = $html->find('table',3);

			$first_row = $tag_sub_head->find('tr',0);
			$second_row = $tag_sub_head->find('tr',1);
			$region_name= str_replace('&nbsp;', '',trim(strip_tags($first_row->find('td',0)->innertext)));
			$phase_name= str_replace('&nbsp;', '',trim(strip_tags($first_row->find('td',2)->innertext)));
			$circle_name= str_replace('&nbsp;', '',trim(strip_tags($second_row->find('td',0)->innertext)));
			$load_name= str_replace('&nbsp;', '',trim(strip_tags($second_row->find('td',2)->innertext)));
			$load_name_integer = str_replace('&nbsp;', '',trim(str_replace('KW', '',$load_name)));
			//echo  $url; exit;

			 $slabe_rate = $html->find('table',4);
    		 // returns all the <tr> tag inside $table
    		 $all_slabe_rate_trs_count = $slabe_rate->find('tr');
    		 $all_slabe_count = count($all_slabe_rate_trs_count);
    
	 	//	echo $all_slabe_count; exit;

		
			
			if($load_name_integer != "" && $load_name_integer >0)
			{
						
				//	print_r($tag_sub_head->find('tr',9)->innertext); exit;
				$third_row = $tag_sub_head->find('tr',2+$all_slabe_count);
				$fourth_row = $tag_sub_head->find('tr',3+$all_slabe_count);
				$fifth_row =$tag_sub_head->find('tr',4+$all_slabe_count);
				$sixth_row =$tag_sub_head->find('tr',5+$all_slabe_count);
				$seventh_row =$tag_sub_head->find('tr',6+$all_slabe_count);
				//	$eighth_row =$tag_sub_head->find('tr',7+$all_slabe_count);
				
				//print_r($eighth_row->innertext); exit;
				$meter_number= str_replace('&nbsp;', '',trim(strip_tags($fourth_row->find('td',2)->innertext)));
				$service_number=  str_replace('&nbsp;', '',trim(strip_tags($fifth_row->find('td',0)->innertext)));
				$address_name = trim(trim(strip_tags($sixth_row->find('td',0)->innertext)),'&nbsp;');
				$service_status = str_replace('&nbsp;', '',trim(strip_tags($seventh_row->find('td',0)->innertext)));
				$section_name= str_replace('&nbsp;', '',trim(strip_tags($third_row->find('td',0)->innertext)));
				$distribution_name= str_replace('&nbsp;', '',trim(strip_tags($fourth_row->find('td',0)->innertext)));
				
			}
			else 
			{
				$error_result['status'] = "error";
				$error_result['data'] = "Sorry, Your service no. is not valid. Please try with proper input.";
				//print_r(json_encode($error_result)); exit;
				return 	$error_result;
			}
			
		 	   $consumer_name = $consumer_name_array[1];
		}
			   
		
		

		$my_all_result['ServiceNo'] =  $disp_service_no;
		$my_all_result['Name'] = $consumer_name;
		$my_all_result['Region'] = $region_name;
		$my_all_result['Phase'] = $phase_name;
		$my_all_result['Circle'] = $circle_name;
		$my_all_result['Section'] = $section_name;
		$my_all_result['Load'] = $load_name;
		$my_all_result['Distribution'] = $distribution_name;
		$my_all_result['MeterNo'] = $meter_number;
		$my_all_result['ConnectionNumber'] = $service_number;
		$my_all_result['Address'] = $address_name;
		$my_all_result['ServiceStatus'] = $service_status;
		$my_all_result['usage'] = $my_all_history;
		
		
		return $my_all_result;
	}
	
	
	public function get_karnataka($username=FALSE,$password=FALSE)
	{

		if ($username===FALSE OR $password === FALSE) 
		{

			$error_result['status'] = "error";
			$error_result['message'] = "Please Type Username and Password";
			$error_result['data'] = array();
			print_r(json_encode($error_result)); 

			exit;	

		} 
		
		error_reporting(0);

		$client_detail_array = array();		
		$client_detail = array();
		$status = array();
		
		$status['status'] = TRUE;

		$url = "https://www.bescom.co.in/SCP/Myhome.aspx";

		$afterLoginUrl = "https://www.bescom.co.in/SCP/MyAccount/AccountSummary.aspx?Name=IAccountSummaryView";
		
		$this->ckfile = dirname(__FILE__) . '\cookies1.txt';

		if (file_exists($this->ckfile)) {
		    @file_put_contents($this->ckfile, "");
		} 
		
		//$useragent = $_SERVER['HTTP_USER_AGENT'];
		
		// $username = "tprakass";
		// $password = "Muruga78vam$";

		//$username = "srini.bhagyodaya@gmail.com";		
		//$password = "s@1ram";

		//$f = fopen('log.txt', 'w'); // file to write request header for debug purpose

		$regexViewstate = '/__VIEWSTATE\" value=\"(.*)\"/i';
		$regexEventVal  = '/__EVENTVALIDATION\" value=\"(.*)\"/i';
		$regexUip = '/__UipId\" value=\"(.*)\"/i';

		// Initialize CURL
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120); // timeout on connect
		curl_setopt($ch, CURLOPT_TIMEOUT, 120);

		//curl_setOpt($ch, CURLOPT_USERAGENT, $useragent);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $this->ckfile);

		curl_setopt($ch, CURLOPT_COOKIEFILE, $this->ckfile);

		$data=curl_exec($ch);


		//echo $data;exit;
		
		$regs  = array();

		/*
		    Get __VIEWSTATE & __EVENTVALIDATION & __UipId
		 */

		$viewstate = $this->regexExtract_get($data,$regexViewstate,$regs,1);
		//echo $viewstate;exit;
		$eventval = $this->regexExtract_get($data, $regexEventVal,$regs,1);
		//echo $eventval;exit;
		$user_input_id = $this->regexExtract_get($data, $regexUip,$regs,1);
		//print_r($user_input_id);exit;
		$userIP = explode('"', $user_input_id);
		$user_input_id =  $userIP[0];

	    /* Post Data */

		//$postfields['masterPageId'] = "ctl00_ctl00_MasterPageContentPlaceHolder_MasterPageContentPlaceHolder";
		//$postfields['ErrorLabelId'] = 'ctl00$ctl00$MasterPageContentPlaceHolder$lblErrorMessage';
		//$postfields['__LASTFOCUS'] = "";

		$postfields['__EVENTTARGET'] = "";
		$postfields['__EVENTARGUMENT'] = "";
		$postfields['__VIEWSTATE'] = $viewstate;
		//$postfields['__DirtyFlag'] = "N";
		//$postfields['__UipId'] = $user_input_id;

		$postfields['__EVENTVALIDATION'] = $eventval;

		//$postfields['ctl00$ctl00$siteSearch$txtsearch'] = "Search";

		$postfields['ctl00$ctl00$MasterPageContentPlaceHolder$ucLogin$txtUserName'] = $username;
		$postfields['ctl00$ctl00$MasterPageContentPlaceHolder$ucLogin$txtPassword'] = $password;
		$postfields['ctl00$ctl00$MasterPageContentPlaceHolder$ucLogin$btnLogin'] = "Sign In";


		// foreach ( $postfields as $key => $value) {
		//     $post_items[] = rawurlencode($key) . '=' . rawurlencode($value);
		// }

		// $post_string = implode ('&', $post_items);
		// // Start Login Process

		//print_r($post_items);exit;

		curl_setOpt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
		//curl_setopt($ch,CURLOPT_HTTPHEADER,array('Origin: https://www.bescom.co.in', 'Host: www.bescom.co.in', 'Content-Type: application/x-www-form-urlencoded'));

		curl_setopt($ch, CURLOPT_URL, $url);   

		curl_setOpt($ch, CURLOPT_REFERER, 'https://www.bescom.co.in/SCP/Myhome.aspx');		

		$data = curl_exec($ch);

		
		curl_setOpt($ch, CURLOPT_HTTPGET, TRUE);
		curl_setOpt($ch, CURLOPT_POST, FALSE);
		curl_setopt($ch, CURLOPT_URL, $afterLoginUrl);   
		//curl_setopt($ch, CURLOPT_COOKIEFILE, $ckfile);   
		//curl_setopt($ch, CURLOPT_TIMEOUT, 50);   

		$data = curl_exec($ch);


		//curl_close($ch);
		//echo $data;	exit;

		$this->load->library('dom_parser');	
		$html = $this->dom_parser->str_get_html($data);

		$tag = $html->find('table',13);
		
		
		//echo $tag;exit;
		if($tag == "" || $tag == null)
		{
			$error_result['status'] = "error";
			$error_result['message'] = "Sorry, The user credentials are invalid";
			$error_result['data'] = array();

			print_r(json_encode($error_result)); 
			exit;	
		}
		
		$loop_count = -1;
		foreach($tag->find('tr') as $single_row)
		{
			if ($loop_count == -1)
			{
				$loop_count++;
				continue;
			}

			$client_detail['connection'] = trim(strip_tags($single_row->find('td',0)->innertext));

			$connection_no = $client_detail['connection'];
			if ($connection_no == "")
			{
				$status['status'] = "error";
				$status['message'] = "Invalid Username OR Password";
				$error_result['data'] = array();

				print_r(json_encode($error_result)); 
				exit;	
			}
			$client_detail['name'] = trim(strip_tags($single_row->find('td',1)->innertext));
			$client_detail['status']  = trim(strip_tags($single_row->find('td',6)->innertext));
			//print_r(json_encode($client_detail)); 

			$details_url='https://www.bescom.co.in/SCP/MyAccount/AccountDetails.aspx?AccountId='.urlencode($connection_no).'&RowIndex%20='.urlencode($loop_count);
			//$details_url2='https://www.bescom.co.in/SCP/MyAccount/AccountDetails.aspx';
			//$detail_html_data = $this->dom_parser->file_get_html($details_url);
		   // echo $detail_html_data;

			
			// Initialize CURL
			//$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $details_url);
			

			$content=curl_exec($ch);

			//echo $content;exit;
			
			
			//$viewstate = $this->regexExtract_get($content,$regexViewstate,$regs,1);
			
			//$file=file_get_contents($details_url);

			//$eventval = $this->regexExtract_get($content, $regexEventVal,$regs,1);

            
            //$data['__VIEWSTATE']=$viewstate;
            //$data['__EVENTVALIDATION']=$eventval;
            /*$options = array(
				//CURLOPT_RETURNTRANSFER => TRUE, // return web page
				//CURLOPT_HEADER => FALSE, // don't return headers
				//CURLOPT_FOLLOWLOCATION => true, // follow redirects
				CURLOPT_ENCODING => "", // handle all encodings
				//CURLOPT_USERAGENT => "spider", // who am i
				//CURLOPT_AUTOREFERER => true, // set referer on redirect
				CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
				CURLOPT_TIMEOUT => 120, // timeout on response
				CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
				//CURLOPT_POST => true,
				//CURLOPT_POSTFIELDS => $data,
			);
			*/
            //$ch = curl_init( $url );
			//curl_setopt_array( $ch, $options );
			//$content = curl_exec ($ch);
			
            //curl_close($ch);
            $html = $this->dom_parser->str_get_html($content);

			$tag = $html->find('table',0);

			//echo $tag;exit;
			
			$account_summary = array();
			foreach($tag->find('tr') as $single_detail_row)
			{
				$key = underscore(strtolower(trim(strip_tags($single_detail_row->find('td',0)->innertext))));
				$value = trim(strip_tags($single_detail_row->find('td',1)->innertext));
				$account_summary[$key] = $value;

			}
			$client_detail['summary']  =  $account_summary;

			/* Code for Usage History */
			

			$usage_history_url = 'https://www.bescom.co.in/SCP/UsageHistory/UsageHistory.aspx';

			$usage_history_result = $this->get_usage_history($ch, $usage_history_url,$connection_no);
			
			//echo $usage_history_result;
			if (count($usage_history_result) == 0) 
			{
				$error_result['status'] = "error";
				$error_result['message'] = "Username or Password Invalid or No Usage History";
				$error_result['data'] = array();

				print_r(json_encode($error_result)); 
				exit;	
			}

			$client_detail['usage_history']  =  $usage_history_result;
			
			/* code for both billing and payment history */

			$billing_payment_history_url = 'https://www.bescom.co.in/SCP/MyAccount/BillingHistory.aspx';

			$billing_payment_result  = $this->get_billing_payment_history($ch,$billing_payment_history_url, $connection_no);

			$client_detail['billing_history'] = $billing_payment_result['billing_history'];

			$client_detail['payment_history'] = $billing_payment_result['payment_history'];

			//$client_detail_array[$loop_count] = $client_detail;

			array_push($client_detail_array,$client_detail);

			
			$loop_count++;
			

		}
		//echo "<br />".count($client_detail_array);exit;
		
		curl_close($ch);
		if (count($client_detail_array) == 0)
		{

			$status['status'] = "error";
			$status['message'] = "Invalid Username-Password";
			$status['data'] = array();

			//$client_detail_array = $status;
			return $status;
		}
		else 
		{
			$status['status'] = "success";
			$status['message'] = "Login success";
			$status['data'] = $client_detail_array;
			return $status;
		}


		
	}

	function do_post_request($url, $data, $optional_headers = null)
	{
		$params = array('http' => array(
	      	'method' => 'POST',
	      	'content' => $data
	    ));
	  	if ($optional_headers !== null) {
	    	$params['http']['header'] = $optional_headers;
	  	}
	  	$ctx = stream_context_create($params);
	  	$fp = @fopen($url, 'rb', false, $ctx);
	  	if (!$fp) {
	    	throw new Exception("Problem with $url, $php_errormsg");
	  	}
	  	$response = @stream_get_contents($fp);
	  	if ($response === false) {
	    	throw new Exception("Problem reading data from $url, $php_errormsg");
	  	}
	  	return $response;
	}

	function get_results()
	{
		echo "<pre>\n";
			
		// THIS STRING (CASE SENSITIVE) HAS THE LOGIN CREDENTIALS FOR THE SITE
		$post = "txtUserName=tprakass&txtPassword=Muruga78vam%24";

		//$post = "txtUserName=tprakass&txtPassword=Muruga78vam%24";

		// READ THE SITE PAGE WITH THE LOGIN FORM
		$baseurl = 'https://www.bescom.co.in/SCP/Myhome.aspx';

		// FOR FRONIUS USE AN EXPLICIT URL TO PROCESS THE LOGIN
		$posturl = 'https://www.bescom.co.in/SCP/Myhome.aspx';

		// WHERE TO GO AFTER THE LOGIN
		$nexturl = 'https://www.bescom.co.in/SCP/MyAccount/AccountSummary.aspx?Name=IAccountSummaryView';

		// SET UP OUR CURL ENVIRONMENT
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $baseurl);

		curl_setopt($ch, CURLOPT_FAILONERROR, TRUE);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);

		// CALL THE WEB PAGE
		$htm = curl_exec($ch);
		$err = curl_errno($ch);
		$inf = curl_getinfo($ch);
		if ($htm === FALSE)
		{
		    echo "\nCURL GET FAIL: $baseurl CURL_ERRNO=$err ";
		    var_dump($inf);
		    die();
		}

		// NOW POST THE DATA WE HAVE FILLED IN
		curl_setopt($ch, CURLOPT_URL, $posturl);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

		// WAIT A RESPECTABLE PERIOD OF TIME
		sleep(3);

		// CALL THE WEB PAGE TO COMPLETE THE LOGIN
		$xyz = curl_exec($ch);
		$err = curl_errno($ch);
		$inf = curl_getinfo($ch);
		if ($xyz === FALSE)
		{
		    echo "\nCURL POST FAIL: $posturl CURL_ERRNO=$err ";
		    var_dump($inf);
		}

		// NOW ON TO THE NEXT PAGE
		curl_setopt($ch, CURLOPT_URL, $nexturl);
		curl_setopt($ch, CURLOPT_POST, FALSE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, '');

		$xyz = curl_exec($ch);
		$err = curl_errno($ch);
		$inf = curl_getinfo($ch);
		if ($xyz === FALSE)
		{
		    echo "\nCURL 2ND GET FAIL: $posturl CURL_ERRNO=$err ";
		    var_dump($inf);
		}

		print_r($xyz); exit;
		// ACIVATE THIS TO SHOW OFF THE DATA WE RETRIEVED AFTER THE LOGIN
		// echo htmlentities($xyz);

		// REFINE THE DATA SOME
		$xyz = strip_tags($xyz, '<div><td>');

		$arr = explode('<div id="ctl00_MainContent_UpdatePanel1">', $xyz);
		// KEEP THE SECOND HALF
		$xyz = $arr[1];
		// ADD SOME INSURANCE ABOUT SPACING
		$xyz = str_replace('>', '> ', $xyz);
		$xyz = strip_tags($xyz);
		echo htmlentities($xyz);
		exit;
	}
	
	public function get_power_cut()
	{
		error_reporting(0);
		//phpinfo(); exitl
        $url = 'http://tneb.tnebnet.org/cpro/today.html';
		$this->load->library('dom_parser');
		
		$my_final_result_array = array();
		//$html = $this->dom_parser->file_get_html("http://tneb.tnebnet.org/newlt/consumerwise_gmc_report.php?encserno=VEFOR0VEQ098fDEyMXx8MDA5fHw4&rsno=MQ%3D%3D");
		$html = $this->dom_parser->file_get_html($url);
		$tag = $html->find('table',0);
		//print_r(strip_tags($tag->innertext));
		
		$time_a = explode("between",strip_tags($tag->innertext));
		//Get Starting time
		$time_1 = explode("to",$time_a[1]);
		$time_1_ampm =substr(trim($time_1[0]),-4);
		$time_1_num = explode($time_1_ampm,trim($time_1[0]));
		//Get Ending time
		$time_2 = explode("for",$time_1[1]);
		$time_2_ampm = substr(trim($time_2[0]),-4);
		$time_2_num = explode($time_2_ampm,trim($time_2[0]));
		
		$time_array = array(
							'start'=>array('time'=>$time_1_num[0],'ampm'=>$time_1_ampm),
							'end'=>array('time'=>$time_2_num[0],'ampm'=>$time_2_ampm)
							);
		//print_r($time_array);
		
		$tag2 = $html->find('table',1);
		//echo $tag2;
		//$table_row_data = $tag2->find('tr'); 

		//echo count($table_row_data);
		//echo $tag2;
		// $date_array;
		// $power_cut_rows_count = -1;
		// foreach ($tag2->find('tr') as $pc_rows ) 
		// {
		// 	if ($power_cut_rows_count == -1)
		// 	{
		// 		$power_cut_rows_count++;
		// 		continue;
		// 	}
		// 	$date = trim(strip_tags($pc_rows->find('td',0)->innertext));
			
		// 	$area_data = trim(strip_tags($pc_rows->find('td',1)->innertext));

		// 	echo $area_data;

		// 	echo "<br />";
		// 	$temp_areas = explode(":",trim($area_data));
		// 	foreach ($temp_areas[0] as $temp_area) {
		// 		echo $temp_area;
		// 		echo "<br />";
		// 	}
		// 	echo "<br />";
		// 	echo trim($temp_area_data[0]);
		// 	echo "<br />";
		// 	$power_cut_rows_count++;
		// }
		// exit;
		// for($i = 0; $i < count($table_row_data); $i++) 
		// {
		// 	echo $i;
		// 	if ($i== 0) {
		// 		"hello";
		// 		continue;
		// 	}
		// 	//exit;
		// 	$date = trim(strip_tags($table_row_data[$i]->find('td',0)->innertext));
		// 	echo $date."<br/>";exit;
		// 	$area_data = trim(strip_tags($table_row_data[$i]->find('td',1)->innertext));

		// 	echo $area_data."<br/>";
		// 	//exit;
		// 	//$exploded_area_data = explode("AREA",$area_data);
		// 	//print_r($exploded_area_data);
		// 	//unset($exploded_area_data);

		// 	//exit;
		// 	// $foreach ($variable as $key => $value) {
		// 	// 	# code...
		// 	// }
		// 	//print_r($exploded_area_data);exit;
		// 	//var_dump($exploded_area_data);exit;
		// 	echo $exploded_area_data[0]."<br />";
		// 	//echo count($exploded_area_data)."<br />";
		// 	// foreach ($exploded_area_data as $key => $value) {
		// 	// 	# code...
		// 	// }
		// 	// print_r(json_encode($exploded_area_data));
		// 	//echo $i;
		// }

		// exit;
		$my_all_data_without_tag = trim(strip_tags($tag2->innertext));
		
		$date_a = explode("AREA",$my_all_data_without_tag);
		$date = substr(trim($date_a[1]),0,8);
		$all_area_name = array();
		
		//print_r($my_all_data_without_tag);
		$my_area_arr = explode(":",$my_all_data_without_tag);
		$j=0;
					
		// echo count($my_area_arr);exit;
		// print_r(json_encode($my_area_arr));exit;
		for($i =0; $i < count($my_area_arr); $i++ )
		{
			if($i == 0)
			{
				$cur_area_name=substr(trim($date_a[1]),8,strlen(trim($date_a[1])));
				//echo $cur_area_name;exit;
				if($cur_area_name)
				{
					$cur_section_data = $this->get_section_id_by_name($cur_area_name);
					//region_id,code section_id,name
					if($cur_section_data)
					{
						$all_area_name[$j] = array("name"=>$cur_area_name,"section_id"=>$cur_section_data['section_id'],"region_id"=>$cur_section_data['region_id']);
						$j++;
					}
				
				}
				
				continue;
			}
			$temp_arr = explode("AREA",trim($my_area_arr[$i]));
			//print_r(json_encode($temp_arr));exit;
			if($temp_arr[0])
			{
				$area_arr = explode(".",trim($temp_arr[0]));
				//print_r(json_encode($area_arr));
				$cur_area_name = trim($area_arr[count($area_arr)-1]);
				//echo $cur_area_name;exit;
				if($cur_area_name)
				{
					$cur_section_data = $this->get_section_id_by_name($cur_area_name);
					//region_id,code section_id,name
					if($cur_section_data)
					{
						$all_area_name[$j] = array("name"=>$cur_area_name,"section_id"=>$cur_section_data['section_id'],"region_id"=>$cur_section_data['region_id']);
						$j++;
					}
				
				}
			}
		}
		$message_arr = explode($date,$tag2->innertext);
		$my_temp_array = array("time"=>$time_array,"date"=>$date,"area"=>$all_area_name,"message"=>strip_tags($message_arr[1]));
		
		array_push($my_final_result_array,$my_temp_array);
		print_r(json_encode($my_final_result_array));exit;
		return $my_final_result_array;
	
	}


	public function regexExtract_get($text, $regex, $regs, $nthValue)
	{
		if (preg_match($regex, $text, $regs)) {
		 $result = $regs[$nthValue];
		}
		else {
		 $result = "";
		}
		return $result;
	} 

	public function get_usage_history($ch, $url, $connection)
	{
		//$ch = curl_init();

		$data['ctl00$ctl00$MasterPageContentPlaceHolder$MasterPageContentPlaceHolder$ddlcustomeraccounts'] = $connection;

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

		// curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		// curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		// //curl_setopt($ch, CURLOPT_COOKIEJAR, $this->ckfile);
		
		// curl_setopt($ch, CURLOPT_COOKIEFILE, $this->ckfile);

		//$content=curl_exec($ch);

		// echo $content;exit;

		// $viewstate = $this->regexExtract_get($content,$regexViewstate,$regs,1);
		// 	echo $viewstate;exit;
		// //$file=file_get_contents($details_url);

		// $eventval = $this->regexExtract_get($content, $regexEventVal,$regs,1);

        
        //$data['__VIEWSTATE']=$viewstate;
       // $data['__EVENTVALIDATION']=$eventval;

		$content = curl_exec ($ch);

        //curl_close($ch);
		//echo $content;exit;

        
        $html = $this->dom_parser->str_get_html($content);

		$tag = $html->find('table',19);
		$usage_history_array = array();

		//echo $tag;exit;
		if($tag != "" && $tag != null)
		{
			$usage_history_count = -1;

			foreach($tag->find('tr') as $usage_history_row)
			{
				$usage_history = array();
				if ($usage_history_count == -1)
				{
					$usage_history_count++;
					continue;
				}
				$usage_history['usage'] = trim(strip_tags($usage_history_row->find('td',0)->innertext));
				$usage_history['billing_date'] = trim(strip_tags($usage_history_row->find('td',1)->innertext));
				$usage_history['avg_daily_usage'] = trim(strip_tags($usage_history_row->find('td',2)->innertext));
				$usage_history['present_reading'] = trim(strip_tags($usage_history_row->find('td',3)->innertext));
				$usage_history['no_days'] = trim(strip_tags($usage_history_row->find('td',4)->innertext));
				$usage_history['charge'] = trim(strip_tags($usage_history_row->find('td',5)->innertext));

				//$usage_history_array[$usage_history_count] = $usage_history;

				array_push($usage_history_array, $usage_history);

				$usage_history_count++;

			}
		}
		//echo $tag;
		
		//print_r(json_encode($usage_history_array));

		return $usage_history_array;
	}

	public function get_billing_history($billing_url)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $billing_url);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		
		curl_setopt($ch, CURLOPT_COOKIEFILE, $this->ckfile);

		$content=curl_exec($ch);
		
        curl_close($ch);
        //echo $content;
        $html = $this->dom_parser->str_get_html($content);

		$tag = $html->find('table',14);

		//echo $tag;

		$billing_history_array = array();
		$billing_history_count = -1;

		foreach($tag->find('tr') as $billing_history_row)
		{
			$billing_history = array();
			if ($billing_history_count == -1)
			{
				$billing_history_count++;
				continue;
			}
			$billing_history['bill_no'] = trim(strip_tags($billing_history_row->find('td',0)->innertext));
			$billing_history['bill_amt'] = trim(strip_tags($billing_history_row->find('td',1)->innertext));
			$billing_history['bill_date'] = trim(strip_tags($billing_history_row->find('td',2)->innertext));
			
			$billing_history_array[$billing_history_count] = $billing_history;

			$billing_history_count++;
		}
		//echo json_encode($billing_history_array);
		
		return $billing_history_array;
        
	}

	public function get_payment_history($payment_url)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $payment_url);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		
		curl_setopt($ch, CURLOPT_COOKIEFILE, $this->ckfile);

		$content=curl_exec($ch);
		
        curl_close($ch);
        
        $html = $this->dom_parser->str_get_html($content);

		$tag = $html->find('table',16);

		//echo $tag;
		
		$payment_history_array = array();
		$payment_history_count = -1;

		foreach($tag->find('tr') as $payment_history_row)
		{
			$payment_history = array();
			if ($payment_history_count == -1)
			{
				$payment_history_count++;
				continue;
			}
			$payment_history['receipt_id'] = trim(strip_tags($payment_history_row->find('td',0)->innertext));
			$payment_history['payment_amt'] = trim(strip_tags($payment_history_row->find('td',1)->innertext));
			$payment_history['payment_date'] = trim(strip_tags($payment_history_row->find('td',2)->innertext));
			$payment_history['payment_desc'] = trim(strip_tags($payment_history_row->find('td',3)->innertext));
			
			$payment_history_array[$payment_history_count] = $payment_history;

			$payment_history_count++;
		}
		//echo json_encode($payment_history_array);
		//exit;
		return $payment_history_array;
	}

	public function get_billing_payment_history($ch, $billing_payment_url, $connection)
	{
		//$ch = curl_init();

		$data['ctl00$ctl00$MasterPageContentPlaceHolder$MasterPageContentPlaceHolder$BillingHistoryView$ddlCustomerAccountNumber'] = $connection;

		curl_setopt($ch, CURLOPT_URL, $billing_payment_url);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

		// curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		// curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		
		// curl_setopt($ch, CURLOPT_COOKIEFILE, $this->ckfile);

		//$content=curl_exec($ch);
		

        /*$options = array(
			// CURLOPT_RETURNTRANSFER => TRUE, // return web page
			// CURLOPT_HEADER => FASLE, // don't return headers
			// CURLOPT_FOLLOWLOCATION => true, // follow redirects
			// CURLOPT_ENCODING => "", // handle all encodings
			// //CURLOPT_USERAGENT => "spider", // who am i
			// CURLOPT_AUTOREFERER => true, // set referer on redirect

			//CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
			//CURLOPT_TIMEOUT => 120, // timeout on response

			//CURLOPT_MAXREDIRS => 10, // stop after 10 redirects

			CURLOPT_POST => TRUE,
			CURLOPT_POSTFIELDS => $data,
		);
		*/
        //$ch = curl_init( $url );
		//curl_setopt_array($ch, $options);

		$content = curl_exec ($ch);


       // curl_close($ch);

        $html = $this->dom_parser->str_get_html($content);

        $billing_payment_history = array();
		$billing_history_array = array();
        // Code for billing history

        $tag = $html->find('table',14);

        if($tag != "" && $tag != null)
		{
			$billing_history_count = -1;

			foreach($tag->find('tr') as $billing_history_row)
			{
				$billing_history = array();
				if ($billing_history_count == -1)
				{
					$billing_history_count++;
					continue;
				}
				$billing_history['bill_no'] = trim(strip_tags($billing_history_row->find('td',0)->innertext));
				$billing_history['bill_amt'] = trim(strip_tags($billing_history_row->find('td',1)->innertext));
				$billing_history['bill_date'] = trim(strip_tags($billing_history_row->find('td',2)->innertext));
				
				//$billing_history_array[$billing_history_count] = $billing_history;
				array_push($billing_history_array, $billing_history);

				$billing_history_count++;
			}

			$billing_payment_history['billing_history'] = $billing_history_array;
		}

		//echo $tag;
        
		

		$tag = $html->find('table',16);

		$payment_history_array = array();

		if($tag != "" && $tag != null)
		{
			$payment_history_count = -1;

			foreach($tag->find('tr') as $payment_history_row)
			{
				$payment_history = array();
				if ($payment_history_count == -1)
				{
					$payment_history_count++;
					continue;
				}
				$payment_history['receipt_id'] = trim(strip_tags($payment_history_row->find('td',0)->innertext));
				$payment_history['payment_amt'] = trim(strip_tags($payment_history_row->find('td',1)->innertext));
				$payment_history['payment_date'] = trim(strip_tags($payment_history_row->find('td',2)->innertext));
				$payment_history['payment_desc'] = trim(strip_tags($payment_history_row->find('td',3)->innertext));
				
				//$payment_history_array[$payment_history_count] = $payment_history;

				array_push($payment_history_array,$payment_history);

				$payment_history_count++;
			}

			$billing_payment_history['payment_history'] = $payment_history_array;
		}

		//print_r($billing_payment_history);exit;

		return $billing_payment_history;
	}

	public function get_kar()
	{
		$url = "https://www.bescom.co.in/SCP/Myhome.aspx";

		$afterLoginUrl = "https://www.bescom.co.in/SCP/MyAccount/AccountSummary.aspx?Name=IAccountSummaryView";
		
		$this->ckfile = dirname(__FILE__) . '\cookies1.txt';

		if (file_exists($this->ckfile)) {
		    @file_put_contents($this->ckfile, "");
		} 
		
		//$useragent = $_SERVER['HTTP_USER_AGENT'];
		
		// $username = "tprakass";
		// $password = "Muruga78vam$";

		//$username = "srini.bhagyodaya@gmail.com";		
		//$password = "s@1ram";

		//$f = fopen('log.txt', 'w'); // file to write request header for debug purpose

		$regexViewstate = '/__VIEWSTATE\" value=\"(.*)\"/i';
		$regexEventVal  = '/__EVENTVALIDATION\" value=\"(.*)\"/i';
		$regexUip = '/__UipId\" value=\"(.*)\"/i';

		// Initialize CURL
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120); // timeout on connect
		curl_setopt($ch, CURLOPT_TIMEOUT, 120);

		//curl_setOpt($ch, CURLOPT_USERAGENT, $useragent);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $this->ckfile);

		curl_setopt($ch, CURLOPT_COOKIEFILE, $this->ckfile);

		$data=curl_exec($ch);


		//echo $data;exit;
		
		$regs  = array();

		/*
		    Get __VIEWSTATE & __EVENTVALIDATION & __UipId
		 */

		$viewstate = $this->regexExtract_get($data,$regexViewstate,$regs,1);
		//echo $viewstate;exit;
		$eventval = $this->regexExtract_get($data, $regexEventVal,$regs,1);
		//echo $eventval;exit;
		$user_input_id = $this->regexExtract_get($data, $regexUip,$regs,1);
		//print_r($user_input_id);exit;
		$userIP = explode('"', $user_input_id);
		$user_input_id =  $userIP[0];

	    /* Post Data */


		$postfields['__EVENTTARGET'] = "";
		$postfields['__EVENTARGUMENT'] = "";
		$postfields['__VIEWSTATE'] = $viewstate;
		

		$postfields['__EVENTVALIDATION'] = $eventval;

	

		$postfields['ctl00$ctl00$MasterPageContentPlaceHolder$ucLogin$txtUserName'] = $username;
		$postfields['ctl00$ctl00$MasterPageContentPlaceHolder$ucLogin$txtPassword'] = $password;
		$postfields['ctl00$ctl00$MasterPageContentPlaceHolder$ucLogin$btnLogin'] = "Sign In";

		curl_setOpt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
		//curl_setopt($ch,CURLOPT_HTTPHEADER,array('Origin: https://www.bescom.co.in', 'Host: www.bescom.co.in', 'Content-Type: application/x-www-form-urlencoded'));

		curl_setopt($ch, CURLOPT_URL, $url);   

		curl_setOpt($ch, CURLOPT_REFERER, 'https://www.bescom.co.in/SCP/Myhome.aspx');		

		$data = curl_exec($ch);

		print_r($data);
		exit;

	}
}