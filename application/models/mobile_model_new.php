<?php

public function get_fetched_data_server($service_no_ip,$last_update= false)
{
	
	error_reporting(0);
	//phpinfo(); exitl
	//	$last_update ="2015-1-1";
	if($last_update)
	{
		$temp_d_format =date_create_from_format("Y-m-d",$last_update);

		if($temp_d_format)
		{
			$last_update = date_format($temp_d_format,"Y-m-d");
		}
		else
		{

			$last_update = false;
			$error_result['status'] = "error";
			$error_result['data'] = "Sorry,last update data is invalid.";
			print_r(json_encode($error_result)); exit;	

		}
	}
	//echo $last_update; exit;

	$regno = substr($service_no_ip, 0, 2); // first 2 digits

	if($regno !=  ('01'|| '02'|| '03'|| '04'|| '05'|| '06'|| '07'|| '08'|| '09'))
	{
		$error_result['status'] = "error";
		$error_result['data'] = "Sorry, Your service no. is not valid. Please try with proper input..";
		print_r(json_encode($error_result)); exit;	
	}


	$code = $regno; // same as the reg no
    $sec = substr($service_no_ip,2, 3); // next 3 digits
    $dist = substr($service_no_ip,5, 3); // next 3 digits
    $serno = substr($service_no_ip,8, 99); // remaining digits
    $temp_sn = $serno;
    $disp_service_no = $regno . '-' . $sec. '-' . $dist . '-'. $serno;

	$loop_cur_count =-1;
	$html = "";
	$extra_ordinary = FALSE;
	if($regno == 2 || $regno ==4)
	{
		
		if(strlen($serno) != 4 )
		{
			switch (strlen($serno))
			{
				case 1:
				$serno = "000".$serno;
				break;
				case 2:
				$serno = "00".$serno;
				break;
				case 3:
				$serno = "0".$serno;
				break;
				default:
				break;
				
			}
			$service_no_ip = $regno.$sec.$dist.$serno;
		}
		if (($regno == 4 || $regno == 2)  && strlen($temp_sn)==3) {
			//echo "three<br />";
			$serno = $temp_sn;
			$service_no_ip = $regno.$sec.$dist.$serno;
		}
		//echo $service_no_ip."<br />";
		$url = 'https://wss.tangedco.gov.in/wss/AccSummaryNew.htm?scnumber='.urlencode($service_no_ip);

		// $this->load->library('dom_parser');
		//$html = $this->dom_parser->file_get_html("http://tneb.tnebnet.org/newlt/consumerwise_gmc_report.php?encserno=VEFOR0VEQ098fDEyMXx8MDA5fHw4&rsno=MQ%3D%3D");
		$html = $this->dom_parser->file_get_html($url);
		
		if (strpos($html,'Invalid Input') != FALSE) 
		{
			//echo "hello";
			$extra_ordinary = TRUE;
			//echo $serno;

			$plainserno = 'TANGEDCO||'. $sec .'||'. $dist. '||' . $serno; // introduced as TANGEDCO changed the format to base64 encoded string
			//$plainserno = 'TANGEDCO||'. $sec .'||'. $dist. '||987'; // introduced as TANGEDCO changed the format to base64 encoded string
        	
        	$encserno = base64_encode($plainserno);
        	//echo $encserno;
        	// exit;
        	//echo urlencode($encserno);
		 	$plainrsno = $regno;
		 
			while(substr($plainrsno,0, 1) == '0')
			{
				$plainrsno = substr($plainrsno,1, strlen($plainrsno));
			}// remove leading zeroes
							 
			$rsno = base64_encode($plainrsno);

			//echo "<br />".urlencode($rsno);
			//exit;
			//echo "http://tneb.tnebnet.org/newlt/consumerwise_gmc_report.php?encserno=VEFOR0VEQ098fDAyOHx8MDA2fHwwNDQx&rsno=NA%3D%3D";
			$url = 'http://tneb.tnebnet.org/newlt/consumerwise_gmc_report.php?encserno='.urlencode($encserno).'&rsno='.urlencode($rsno);
			
			// $this->load->library('dom_parser');
		
			//$html = $this->dom_parser->file_get_html("http://tneb.tnebnet.org/newlt/consumerwise_gmc_report.php?encserno=VEFOR0VEQ098fDAyOHx8MDA2fHw0NDE%3D&rsno=NA%3D%3D");
			$html = $this->dom_parser->file_get_html($url);		
			//echo $html;exit;		
		}
	}
	else
	{
		// For breaking security while call url
		$plainserno = 'TANGEDCO||'. $sec .'||'. $dist. '||' . $serno; // introduced as TANGEDCO changed the format to base64 encoded string
        $encserno = base64_encode($plainserno);
		 $plainrsno = $regno;
		 
		 while(substr($plainrsno,0, 1) == '0')
		 {
			 $plainrsno = substr($plainrsno,1, strlen($plainrsno));
		 }// remove leading zeroes
							 
		$rsno = base64_encode($plainrsno);
		
		$url = 'http://tneb.tnebnet.org/newlt/consumerwise_gmc_report.php?encserno='.urlencode($encserno).'&rsno='.urlencode($rsno);
		
		$this->load->library('dom_parser');
	
		//$html = $this->dom_parser->file_get_html("http://tneb.tnebnet.org/newlt/consumerwise_gmc_report.php?encserno=VEFOR0VEQ098fDEyMXx8MDA5fHw4&rsno=MQ%3D%3D");
		$html = $this->dom_parser->file_get_html($url);
				
	}
	//echo $html;exit;

	$tag = $html->find('table',7);
	// echo $tag;exit;
	
	// $error_result['status'] = "error";
	// $error_result['data'] = $tag;
	// print_r(json_encode($error_result)); exit;	

	//echo urlencode(strip_tags($tag->find("tr",0)->innertext)); exit;

	if($tag && urlencode($tag->innertext) == "++%3Ctr+align%3D%22center%22%3E%3Ctd+align%3D%22center%22%3E++%3C%2Ftd%3E%3C%2Ftr%3E" || urlencode($tag->innertext) == "+%3Ctr+align%3D%22center%22%3E%3Ctd+align%3D%22center%22%3E+%3C%2Ftd%3E%3C%2Ftr%3E")
	{
		//for 021000048 && 011210078
		//If Dues To Be Paid  Exist
		$tag = $html->find('table',9);

		
	}
	else if($tag && urlencode(strip_tags($tag->find("tr",0)->innertext)) == "++%09%09%09%09Electricity+Consumption++%09%09%09")
	{
		$tag = $html->find('table',8);
		
	}
	else if($tag && urlencode(strip_tags($tag->find("tr",0)->innertext)) == "++%09%09%09%09Disconnection%2FReconnection+Details++%09%09%09")
	{
		$tag = $html->find('table',9);
		
	}
	else
	{
		$url_new = 'https://wss.tangedco.gov.in/wss/AccSummaryNew.htm?scnumber='.urlencode($service_no_ip);
		$html = $this->dom_parser->file_get_html($url);
		$tag = $html->find('table',7);
	}
	
	echo $html;exit;
	$count = 0;
	$my_all_result =array();
	$my_all_history = array();
	$my_single_history = array();
	
	echo $tag;exit;
	if($tag == "" || $tag == null)
	{
		$error_result['status'] = "error";
		$error_result['data'] = "Sorry, Your service no. is not valid. Please try with proper input.";
		print_r(json_encode($error_result)); 
		exit;	
	}
	
	
		
	foreach($tag->find('tr') as $single_row)
	{
		
		
		//For region 2 and 4 api, there is title at top so skip it
		if(($regno == 2 || $regno ==4) && $loop_cur_count == -1 && $extra_ordinary == FALSE)
		{
			//echo "********";
			$loop_cur_count = $loop_cur_count+1;
			continue; //Skip top title
		}
		
		$loop_cur_count = $loop_cur_count+1;
		//echo "<br />-- Start --<br />";
		//echo "<pre>loop count {$loop_cur_count}</pre>";
		//echo "<pre>".$loop_cur_count."</pre>";

		if($last_update)
		{
			if($loop_cur_count == 0)
			{
				continue;
			}
			$my_single_history_temp_ass = str_replace('&nbsp;', '',trim(strip_tags($single_row->find('td',1)->innertext)));
			$my_convert_try = date_create_from_format("d/m/Y",$my_single_history_temp_ass);
			$my_convert_try2 = date_create_from_format("d-m-Y",$my_single_history_temp_ass);
			if($my_convert_try)
			{
				$my_single_history['assessment_date']= date_format($my_convert_try,"Y-m-d");	
			}
			elseif($my_convert_try2)
			{
				$my_single_history['assessment_date']= date_format($my_convert_try2,"Y-m-d");
			}
			else
			{
				if($my_single_history_temp_ass == "-" && $loop_cur_count > 1)
				{	
					$my_single_history['assessment_date'] = $my_single_history['assessment_date'];
				}
				else
				{
					$my_single_history['assessment_date'] = $my_single_history_temp_ass;
				}
			}

			if($my_single_history['assessment_date'] <= $last_update )
			{
				continue;
			}
		}
		else
		{
									
			$my_single_history_temp_ass = str_replace('&nbsp;', '',trim(strip_tags($single_row->find('td',1)->innertext)));
			if($loop_cur_count == 0)
			{
				$my_single_history['assessment_date']=$my_single_history_temp_ass;
			}
			else
			{
				$my_convert_try_else = date_create_from_format("d/m/Y",$my_single_history_temp_ass);
				$my_convert_try_else2 = date_create_from_format("d-m-Y",$my_single_history_temp_ass);
				if($my_convert_try_else)
				{
					$my_single_history['assessment_date']= date_format($my_convert_try_else,"Y-m-d");
				}
				elseif($my_convert_try_else2)
				{
					$my_single_history['assessment_date']= date_format($my_convert_try_else2,"Y-m-d");
				}
				else
				{					
					if($my_single_history_temp_ass == "-")
					{	
						$my_single_history['assessment_date'] = $my_single_history['assessment_date'];
					}
					else
					{
						$my_single_history['assessment_date']=$my_single_history_temp_ass;
					}
				}
			}
		}
		
		
		$my_single_history['reading'] =str_replace('&nbsp;', '',trim(strip_tags($single_row->find('td',2)->innertext)));
		$my_single_history['units']=str_replace('&nbsp;', '',trim(strip_tags($single_row->find('td',3)->innertext)));
		//echo $my_single_history_temp_ass;
		if($my_single_history_temp_ass == "-")
		{
			 $my_single_history['amount']=$single_row->find('td',11)->innertext;
		}
		else
		{
			$my_single_history['amount']=str_replace('&nbsp;', '',trim(strip_tags($single_row->find('td',9)->innertext)));
		}

		$my_single_history_temp= str_replace('&nbsp;', '',trim(strip_tags($single_row->find('td',13)->innertext)));
		
		//echo "<pre>".count($single_row->find('td', 13))."</pre>";
		//echo "<pre>".str_replace('&nbsp;', '',trim(strip_tags($single_row->find('td',13)->innertext)))."</pre>";
		//print_r($my_single_history_temp);
		

		if($loop_cur_count == 0)
		{

			$my_single_history['payment_date']=$my_single_history_temp;

		}
		else
		{

			$my_convert_try_sec = date_create_from_format("d/m/Y",$my_single_history_temp);

			$my_convert_try_sec2 = date_create_from_format("d-m-Y",$my_single_history_temp);
			
			if($my_convert_try_sec)
			{

				$my_single_history['payment_date']= date_format($my_convert_try_sec,"Y-m-d");
				
			}
			elseif($my_convert_try_sec2)
			{
				$my_single_history['payment_date']= date_format($my_convert_try_sec2,"Y-m-d");
			}
			else
			{
				//$my_single_history['payment_date'] = '0000-00-00';

				$my_single_history['payment_date'] = $my_single_history['assessment_date'];

			}

		}

		$orginal= $single_row->find('td',18)->innertext;
		//echo "<pre>".$orginal."</pre>";
		$my_single_history_status = str_replace('&nbsp;', '', strip_tags($single_row->find('td',15)->innertext));
		
		if($my_single_history['payment_date'] == "" && ($my_single_history_status == "NORMAL" || $my_single_history_status == "NOT IN USE"))
		{
			if(($my_convert_try_sec || $my_convert_try_sec2) && $my_single_history_status == "NORMAL")
			{
				$my_single_history['status']="Paid";
			}
			else if($my_single_history_status == "NORMAL")
			{
				$my_single_history['status']="Unpaid";
			}
			else
			{
			$my_single_history['status']=$my_single_history_status;
			}

			$my_single_history['payment_date'] = "0000-00-00";
			
		}
		else if($my_single_history_status == "NORMAL")
		{
			if($my_convert_try_sec)
			{
				$my_single_history['status']="Paid";
			}
			else
			{
				$my_single_history['status']="Unpaid";
			}
			
		}
		else
		{
			if($my_convert_try_sec || $my_convert_try_sec2)
			{
				if($my_single_history_status == null)
				{
					$my_single_history['status']= "Paid";
				}
				else
				{
					$my_single_history['status'] = $my_single_history_status;
				}
			}
			else
			{
				//$my_single_history['status']=$my_single_history_status;
				$my_single_history['status']="Paid";
			}
		}
		
		array_push($my_all_history,$my_single_history);
		//echo "<br />-- End --<br />";
	}
	
	//var_dump($extra_ordinary);
	if(($regno == 2 || $regno ==4))
	{
		$tag_sub_head = $html->find('table',2);
		
		$first_row = $tag_sub_head->find('tr',0);
		$second_row = $tag_sub_head->find('tr',1);
		$third_row = $tag_sub_head->find('tr',2);
		$fourth_row = $tag_sub_head->find('tr',3);
		$fifth_row = $tag_sub_head->find('tr',4);
		$sixth_row = $tag_sub_head->find('tr',5);
		$seventh_row = $tag_sub_head->find('tr',6);
		$eighth_row = $tag_sub_head->find('tr',7);
		$consumer_name= str_replace('&nbsp;', '',trim(strip_tags($first_row->find('td',1)->innertext)));
		$region_name= str_replace('&nbsp;', '',trim(strip_tags($second_row->find('td',1)->innertext)));
		$circle_name= str_replace('&nbsp;', '',trim(strip_tags($third_row->find('td',1)->innertext)));
		$phase_name= str_replace('&nbsp;', '',trim(strip_tags($third_row->find('td',3)->innertext)));
		$section_name= str_replace('&nbsp;', '',trim(strip_tags($fourth_row->find('td',1)->innertext)));
		$load_name= str_replace('&nbsp;', '',trim(strip_tags($fourth_row->find('td',3)->innertext)));
		$distribution_name= str_replace('&nbsp;', '',trim(strip_tags($fifth_row->find('td',1)->innertext)));
		$service_number = str_replace('&nbsp;', '',trim(strip_tags($sixth_row->find('td',1)->innertext)));
		$meter_number = str_replace('&nbsp;', '',trim(strip_tags($sixth_row->find('td',3)->innertext)));
		$address_name = str_replace('&nbsp;', '',trim(strip_tags($seventh_row->find('td',1)->innertext)));
		$service_status = str_replace('&nbsp;', '',trim(strip_tags($eighth_row->find('td',1)->innertext)));
			
	}
	else
	{
		// print_r($my_all_history);exit;
		$tag_head = $html->find('table',2);
		
		$consumer_head= trim(strip_tags($tag_head->find('tr',0)->find('td',0)->outertext));
		$consumer_name_array = explode("CONSUMER NAME:",$consumer_head);
		
		$tag_sub_head = $html->find('table',3);

		$first_row = $tag_sub_head->find('tr',0);
		$second_row = $tag_sub_head->find('tr',1);
		$region_name= str_replace('&nbsp;', '',trim(strip_tags($first_row->find('td',0)->innertext)));
		$phase_name= str_replace('&nbsp;', '',trim(strip_tags($first_row->find('td',2)->innertext)));
		$circle_name= str_replace('&nbsp;', '',trim(strip_tags($second_row->find('td',0)->innertext)));
		$load_name= str_replace('&nbsp;', '',trim(strip_tags($second_row->find('td',2)->innertext)));
		$load_name_integer = str_replace('&nbsp;', '',trim(str_replace('KW', '',$load_name)));
		//echo  $url; exit;

		 $slabe_rate = $html->find('table',4);
		 // returns all the <tr> tag inside $table
		 $all_slabe_rate_trs_count = $slabe_rate->find('tr');
		 $all_slabe_count = count($all_slabe_rate_trs_count);

 	//	echo $all_slabe_count; exit;

	
		
		if($load_name_integer != "" && $load_name_integer >0)
		{
					
			//	print_r($tag_sub_head->find('tr',9)->innertext); exit;
			$third_row = $tag_sub_head->find('tr',2+$all_slabe_count);
			$fourth_row = $tag_sub_head->find('tr',3+$all_slabe_count);
			$fifth_row =$tag_sub_head->find('tr',4+$all_slabe_count);
			$sixth_row =$tag_sub_head->find('tr',5+$all_slabe_count);
			$seventh_row =$tag_sub_head->find('tr',6+$all_slabe_count);
			//	$eighth_row =$tag_sub_head->find('tr',7+$all_slabe_count);
			
			//print_r($eighth_row->innertext); exit;
			$meter_number= str_replace('&nbsp;', '',trim(strip_tags($fourth_row->find('td',2)->innertext)));
			$service_number=  str_replace('&nbsp;', '',trim(strip_tags($fifth_row->find('td',0)->innertext)));
			$address_name = trim(trim(strip_tags($sixth_row->find('td',0)->innertext)),'&nbsp;');
			$service_status = str_replace('&nbsp;', '',trim(strip_tags($seventh_row->find('td',0)->innertext)));
			$section_name= str_replace('&nbsp;', '',trim(strip_tags($third_row->find('td',0)->innertext)));
			$distribution_name= str_replace('&nbsp;', '',trim(strip_tags($fourth_row->find('td',0)->innertext)));
			
		}
		else 
		{
			$error_result['status'] = "error";
			$error_result['data'] = "Sorry, Your service no. is not valid. Please try with proper input.";
			//print_r(json_encode($error_result)); exit;
			return 	$error_result;
		}
		
	 	   $consumer_name = $consumer_name_array[1];
	}

	$my_all_result['ServiceNo'] =  $disp_service_no;
	$my_all_result['Name'] = $consumer_name;
	$my_all_result['Region'] = $region_name;
	$my_all_result['Phase'] = $phase_name;
	$my_all_result['Circle'] = $circle_name;
	$my_all_result['Section'] = $section_name;
	$my_all_result['Load'] = $load_name;
	$my_all_result['Distribution'] = $distribution_name;
	$my_all_result['MeterNo'] = $meter_number;
	$my_all_result['ConnectionNumber'] = $service_number;
	$my_all_result['Address'] = $address_name;
	$my_all_result['ServiceStatus'] = $service_status;
	$my_all_result['usage'] = $my_all_history;
			
	return $my_all_result;
}
