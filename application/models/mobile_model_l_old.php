<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/



class Mobile_model extends CI_Model
{
	function __construct()
    {
        // Construct our parent class
        parent::__construct();

    }
	public function get_region_list($last_update)
	{
		
		$this->db->select_max('last_update');
		
		$new_last_update_query = $this->db->get('emt_region_list');
		if ($new_last_update_query->num_rows() > 0) 
		{
			$res_last_update = $new_last_update_query->row_array();		
			$new_last_update = $res_last_update['last_update'];		
		}
		else
		{
			$new_last_update = 'no';
		}
		
		if($new_last_update != $last_update )
		{	
			if($last_update)
			{
				$query = $this->db->get_where("emt_region_list",array('last_update >'=>$last_update));
			}
			else
			{
				
				$query = $this->db->get("emt_region_list");
				
			}
				
			if ($query->num_rows() > 0) 
			{
				$data_all=$query->result_array();	
				$data= array('status'=>'success','last_update'=>$new_last_update,'data'=>$data_all	);	
			}
			else
			{
				$data = false;
			}
			
			$query->free_result();	
		}
		else
		{
			$data =false;
		}
			
		return $data;

	
	}

	public function get_section_list($last_update)
	{
		
		$this->db->select_max('last_update');
		$new_last_update_query = $this->db->get('emt_section_list');

		if ($new_last_update_query->num_rows() > 0) 
		{
			$res_last_update = $new_last_update_query->row_array();		
			$new_last_update = $res_last_update['last_update'];		
		}
		else
		{
			$new_last_update = 'no';
		}
		
		if($new_last_update != $last_update )
		{	
			if($last_update)
			{
				$query = $this->db->get_where("emt_section_list",array('last_update >'=>$last_update));
			}
			else
			{
				$query = $this->db->get("emt_section_list");
				
			}
				
			if ($query->num_rows() > 0) 
			{
				$data_all=$query->result_array();	
				$data= array('status'=>'success','last_update'=>$new_last_update,'data'=>$data_all	);	
			}
			else
			{
				$data = false;
			}
			
			$query->free_result();	
		}
		else
		{
			$data =false;
		}
			
		return $data;

	
	}
	
	public function get_section_list_by_region_id($last_update,$region_id)
	{
		if(!$region_id)
		{
			$this->get_section_list($last_update);
			//exit;
		}
		
		$this->db->select_max('last_update');
		$new_last_update_query = $this->db->get('emt_section_list');

		if ($new_last_update_query->num_rows() > 0) 
		{
			$res_last_update = $new_last_update_query->row_array();		
			$new_last_update = $res_last_update['last_update'];		
		}
		else
		{
			$new_last_update = 'no';
		}
		
		if($new_last_update != $last_update )
		{	
			if($last_update)
			{
				$query = $this->db->get_where("emt_section_list",array('last_update >'=>$last_update,'region_id'=>$region_id));
			}
			else
			{
				$query = $this->db->get_where("emt_section_list",array('region_id'=>$region_id));
				
			}
				
			if ($query->num_rows() > 0) 
			{
				$data_all=$query->result_array();	
				$data= array('status'=>'success','last_update'=>$new_last_update,'data'=>$data_all	);	
			}
			else
			{
				$data = false;
			}
			
			$query->free_result();	
		}
		else
		{
			$data =false;
		}
			
		return $data;

	
	}
	
	public function get_distribution_list($last_update)
	{
		
		$this->db->select_max('last_update');
		$new_last_update_query = $this->db->get('emt_distribution_list');

		if ($new_last_update_query->num_rows() > 0) 
		{
			$res_last_update = $new_last_update_query->row_array();		
			$new_last_update = $res_last_update['last_update'];		
		}
		else
		{
			$new_last_update = 'no';
		}
		
		if($new_last_update != $last_update )
		{	
			if($last_update)
			{
				$query = $this->db->get_where("emt_distribution_list",array('last_update >'=>$last_update));
			}
			else
			{
				$query = $this->db->get("emt_distribution_list");
				
			}
				
			if ($query->num_rows() > 0) 
			{
				$data_all=$query->result_array();	
				$data= array('status'=>'success','last_update'=>$new_last_update,'data'=>$data_all	);	
			}
			else
			{
				$data = false;
			}
			
			$query->free_result();	
		}
		else
		{
			$data =false;
		}
			
		return $data;

	
	}
	
	public function get_distribution_list_by_section_id($last_update,$section_id)
	{
		if(!$section_id)
		{
			$this->get_section_list($last_update);
			//exit;
		}
		
		$this->db->select_max('last_update');
		$new_last_update_query = $this->db->get('emt_distribution_list');
		if ($new_last_update_query->num_rows() > 0) 
		{
			$res_last_update = $new_last_update_query->row_array();		
			$new_last_update = $res_last_update['last_update'];		
		}
		else
		{
			$new_last_update = 'no';
		}
		
		if($new_last_update != $last_update )
		{	
			$this->db->distinct();
		    $this->db->select();
   		 	$this->db->from("emt_distribution_list");        
	
			if($last_update)
			{
				$this->db->where('last_update >',$last_update);
				$this->db->where('section_id',$section_id);
				$query = $this->db->get();
			}
			else
			{
				$this->db->where('section_id',$section_id);
				$query = $this->db->get();
				
			}
				
			if ($query->num_rows() > 0) 
			{
				$data_all=$query->result_array();	
				$data= array('status'=>'success','last_update'=>$new_last_update,'data'=>$data_all	);	
			}
			else
			{
				$data = false;
			}
			
			$query->free_result();	
		}
		else
		{
			$data =false;
		}
			
		return $data;

	
	}
	
	public function get_distribution_list_by_reg_and_section_id($last_update,$region_id,$section_id)
	{
		if(!$region_id)
		{
			$this->get_distribution_list_by_section_id($last_update,$section_id);
			exit;
		}
		if(!$section_id)
		{
			$this->get_section_list($last_update);
			//exit;
		}
		
		$this->db->select_max('last_update');
		$new_last_update_query = $this->db->get('emt_distribution_list');
		if ($new_last_update_query->num_rows() > 0) 
		{
			$res_last_update = $new_last_update_query->row_array();		
			$new_last_update = $res_last_update['last_update'];		
		}
		else
		{
			$new_last_update = 'no';
		}
		
		if($new_last_update != $last_update )
		{	
			$this->db->distinct();
		    $this->db->select();
   		 	$this->db->from("emt_distribution_list");        
	
			if($last_update)
			{
				$this->db->where('last_update >',$last_update);
				$this->db->where('section_id',$section_id);
				$this->db->where('region_id',$region_id);
				$query = $this->db->get();
			}
			else
			{
				$this->db->where('section_id',$section_id);
				$this->db->where('region_id',$region_id);
				$query = $this->db->get();
				
			}
				
			if ($query->num_rows() > 0) 
			{
				$data_all=$query->result_array();	
				$data= array('status'=>'success','last_update'=>$new_last_update,'data'=>$data_all	);	
			}
			else
			{
				$data = false;
			}
			
			$query->free_result();	
		}
		else
		{
			$data =false;
		}
			
		return $data;

	
	}
	
	public function get_appliance_list($last_update)
	{
		
		$this->db->select_max('last_update');
		$new_last_update_query = $this->db->get('appliance_list');

		if ($new_last_update_query->num_rows() > 0) 
		{
			$res_last_update = $new_last_update_query->row_array();		
			$new_last_update = $res_last_update['last_update'];		
		}
		else
		{
			$new_last_update = 'no';
		}
		
		if($new_last_update != $last_update )
		{	
			if($last_update)
			{
				$query = $this->db->get_where("appliance_list",array('last_update >'=>$last_update));
			}
			else
			{
				$query = $this->db->get("appliance_list");
				
			}
				
			if ($query->num_rows() > 0) 
			{
				$data_all=$query->result_array();	
				$data= array('status'=>'success','last_update'=>$new_last_update,'image_path'=>base_url().'assets/images/appliance/','data'=>$data_all);	
			}
			else
			{
				$data = false;
			}
			
			$query->free_result();	
		}
		else
		{
			$data =false;
		}
			
		return $data;

	
	}
	
	public function get_appliance_by_id($last_update,$id)
	{
		if(!$id)
		{
			$this->get_section_list($last_update);
			//exit;
		}
		
		$this->db->select_max('last_update');
		$new_last_update_query = $this->db->get('appliance_list');

		if ($new_last_update_query->num_rows() > 0) 
		{
			$res_last_update = $new_last_update_query->row_array();		
			$new_last_update = $res_last_update['last_update'];		
		}
		else
		{
			$new_last_update = 'no';
		}
		
		if($new_last_update != $last_update )
		{	
			if($last_update)
			{
				$query = $this->db->get_where("appliance_list",array('last_update >'=>$last_update,'id'=>$id));
			}
			else
			{
				$query = $this->db->get_where("appliance_list",array('id'=>$id));
				
			}
				
			if ($query->num_rows() > 0) 
			{
				$data_all=$query->result_array();	
				$data= array('status'=>'success','last_update'=>$new_last_update,'data'=>$data_all	);	
			}
			else
			{
				$data = false;
			}
			
			$query->free_result();	
		}
		else
		{
			$data =false;
		}
			
		return $data;

	
	}
	
	public function get_section_id_by_name($name)
	{
		if(!$name)
		{
			return false;
			//exit;
		}
			$this->db->select('emt_region_list.code region_id,emt_section_list.code section_id,emt_section_list.name');
			$this->db->join('emt_region_list','emt_region_list.id = emt_section_list.region_id');
			$query = $this->db->get_where("emt_section_list",array('emt_section_list.name'=>$name));
			if ($query->num_rows() > 0) 
			{
				return $query->row_array();	
			}
			$query->free_result();	
		return false;

	
	}
	public function is_connection_id_exist($connection_id)
	{
			$query = $this->db->get_where("server_connection_list",array('connection_id'=>$connection_id));
			if ($query->num_rows() > 0) 
			{
				$data=$query->row_array();	
			}
			else
			{
				$data = false;
			}
			return $data;
	}
	
	public function do_insertion_userdata($service_no_ip)
	{
		
		$all_data_fetched = $this->get_fetched_data_server($service_no_ip);
		/*
		if(count($all_data_fetched['usage']) >0)
		{
			 $ud_assessment_date = $all_data_fetched['usage'][0]['assessment_date'];
	  		 $ud_reading=$all_data_fetched['usage'][0]['reading'];
	  		 $ud_units = $all_data_fetched['usage'][0]['units'];
	  		 $ud_amount = $all_data_fetched['usage'][0]['amount'];
	  		 $ud_payment_date = $all_data_fetched['usage'][0]['payment_date'];
	   		 $ud_status = $all_data_fetched['usage'][0]['status'];
		}
		else
		{
			 $ud_assessment_date = "Assessment Entry Date";
	  		 $ud_reading= "Reading";
	  		 $ud_units = "Consumed Unit";
	  		 $ud_amount = "Total BillAmount(Rs.)";
	  		 $ud_payment_date = "Payment Date";
	   		 $ud_status = "AssessmentStatus";
		}
		
		
		$data = array(
		'connection_id' => $service_no_ip,
	   'serviceno' => $all_data_fetched['ServiceNo'] ,
	   'name' => $all_data_fetched['ServiceNo'] ,
	   'region' => $all_data_fetched['ServiceNo'],
	   'phase' => $all_data_fetched['Phase'],
	   'circle' => $all_data_fetched['Circle'],
	   'section' => $all_data_fetched['Section'],
	   'load' => $all_data_fetched['Load'],
	   'distribution' => $all_data_fetched['Distribution'],
	   'meterno' => $all_data_fetched['MeterNo'],
	   'connectionnumber' => $all_data_fetched['ConnectionNumber'],
	   'address' => $all_data_fetched['Address'],
	   'servicestatus' => $all_data_fetched['ServiceStatus'],
	   
	   'ud_assessment_date' => $ud_assessment_date,
	   'ud_reading' => $ud_reading,
	   'ud_units' => $ud_units,
	   'ud_amount' => $ud_amount,
	   'ud_payment_date' => $ud_payment_date,
	   'ud_status' => $ud_status
	   
	);
		$status = $this->db->insert('server_connection_list', $data); 
		// $current_insert_id = 1;
		if($status)
		{
			$current_insert_id = $this->db->insert_id();
			//$this->insert_user_data($all_data_fetched,$current_insert_id,true);
		}
		*/
		return $all_data_fetched;
	}
	
	public function insert_user_data($all_data_fetched,$current_insert_id,$is_first_time = false)
	{
	/*
		if((count($all_data_fetched['usage']) > 1 && $is_first_time) ||(count($all_data_fetched['usage']) > 0 && !$is_first_time) )
			{
				$all_data_usage_batch_insert = array();
				$count_loop = 0;
				foreach($all_data_fetched['usage'] as $single_data_usage)
				{
					if($count_loop == 0)
					{
						$count_loop=$count_loop+1;
						continue;
					}
	
					$cur_data = array(
									  'fk_connection_id' => $current_insert_id ,
									  'assessment_date' => $single_data_usage['assessment_date'],
									  'reading' => $single_data_usage['reading'],
									  'units' => $single_data_usage['units'],
									  'amount' => $single_data_usage['amount'],
									  'payment_date' =>   $single_data_usage['payment_date'],
									  'status' => $single_data_usage['status']
									);
					array_push($all_data_usage_batch_insert,$cur_data);
				}
				$this->db->insert_batch('server_usage_data', $all_data_usage_batch_insert); 
			}
			*/
	}
	
	public function do_update_userdata($service_no_ip,$connection_detail,$last_update)
	{
		//NOTE: $last_update is max assessment_date of client;
		$today_date = date("Y-m-d");
		/*
		if($today_date == $last_update)
		{
			$all_data_fetched = $this->get_fetched_data_server($service_no_ip,$connection_detail);
			return $all_data_fetched;
		}
		
		$this->db->select_max('assessment_date');
		$this->db->where('fk_connection_id',$connection_detail['id']);
		$this->db->order_by("assessment_date", "desc");
		$query = $this->db->get("server_usage_data");
		if ($query->num_rows() > 0) 
		{
			$max_date = $query->row_array();	
		}
		else
		{
			$max_date ="";
		}
		
		if($max_date == "")
		{
			$all_data_fetched = $this->get_fetched_data_server($service_no_ip);
		}
		else
		{
			$all_data_fetched = $this->get_fetched_data_server($service_no_ip,$last_update);
		}
		*/
		$all_data_fetched = $this->get_fetched_data_server($service_no_ip,$last_update);
		//print_r($all_data_fetched); exit;
		
		//$this->insert_user_data($all_data_fetched,$connection_detail['id'],false);
		return $all_data_fetched;
	}
	
	public function get_fetched_data_server($service_no_ip,$last_update= false)
	{
		
		error_reporting(0);
		//phpinfo(); exitl
	//	$last_update ="2015-1-1";
		if($last_update)
		{
			$temp_d_format =date_create_from_format("Y-m-d",$last_update);
			if($temp_d_format)
			{
				$last_update = date_format($temp_d_format,"Y-m-d");
			}
			else
			{

				$last_update = false;
				$error_result['status'] = "error";
				$error_result['data'] = "Sorry,last update data is invalid.";
				print_r(json_encode($error_result)); exit;	
			}
		}
		//echo $last_update; exit;
	
		$regno = substr($service_no_ip,0, 2); // first 2 digits

		if($regno !=  ('01'|| '02'|| '03'|| '04'|| '05'|| '06'|| '07'|| '07'|| '09'))
		{
			$error_result['status'] = "error";
			$error_result['data'] = "Sorry, Your service no. is not valid. Please try with proper input..";
			print_r(json_encode($error_result)); exit;	
		}


		$code = $regno; // same as the reg no
        $sec = substr($service_no_ip,2, 3); // next 3 digits
        $dist = substr($service_no_ip,5, 3); // next 3 digits
        $serno = substr($service_no_ip,8, 99); // remaining digits

        $disp_service_no = $regno . '-' . $sec. '-' . $dist . '-'. $serno;

		$loop_cur_count =-1;
		$html = "";
		$extra_ordinary = FALSE;
		if($regno == 2 || $regno ==4)
		{

			if(strlen($serno) != 4 )
			{
				switch (strlen($serno))
				{
					case 1:
					$serno = "000".$serno;
					break;
					case 2:
					$serno = "00".$serno;
					break;
					case 3:
					$serno = "0".$serno;
					break;
					default:
					break;
					
				}
				$service_no_ip = $regno.$sec.$dist.$serno;
			}
			$url = 'https://wss.tangedco.gov.in/wss/AccSummaryNew.htm?scnumber='.urlencode($service_no_ip);

			$this->load->library('dom_parser');
			//$html = $this->dom_parser->file_get_html("http://tneb.tnebnet.org/newlt/consumerwise_gmc_report.php?encserno=VEFOR0VEQ098fDEyMXx8MDA5fHw4&rsno=MQ%3D%3D");
			$html = $this->dom_parser->file_get_html($url);
			
			if (strpos($html,'Invalid') != FALSE) 
			{
				//echo "hello";
				$extra_ordinary = TRUE;

    			$plainserno = 'TANGEDCO||'. $sec .'||'. $dist. '||' . $serno; // introduced as TANGEDCO changed the format to base64 encoded string
            	
            	$encserno = base64_encode($plainserno);
            	//echo $encserno;

			 	$plainrsno = $regno;
			 
				while(substr($plainrsno,0, 1) == '0')
				{
					$plainrsno = substr($plainrsno,1, strlen($plainrsno));
				}// remove leading zeroes
								 
				$rsno = base64_encode($plainrsno);
				
				$url = 'http://tneb.tnebnet.org/newlt/consumerwise_gmc_report.php?encserno='.urlencode($encserno).'&rsno='.urlencode($rsno);
				
				$this->load->library('dom_parser');
			
				//$html = $this->dom_parser->file_get_html("http://tneb.tnebnet.org/newlt/consumerwise_gmc_report.php?encserno=VEFOR0VEQ098fDEyMXx8MDA5fHw4&rsno=MQ%3D%3D");
				$html = $this->dom_parser->file_get_html($url);				
			}
		}
		else
		{
			// For breaking security while call url
			$plainserno = 'TANGEDCO||'. $sec .'||'. $dist. '||' . $serno; // introduced as TANGEDCO changed the format to base64 encoded string
            $encserno = base64_encode($plainserno);
			 $plainrsno = $regno;
			 
			 while(substr($plainrsno,0, 1) == '0')
			 {
				 $plainrsno = substr($plainrsno,1, strlen($plainrsno));
			 }// remove leading zeroes
								 
			$rsno = base64_encode($plainrsno);
			
			$url = 'http://tneb.tnebnet.org/newlt/consumerwise_gmc_report.php?encserno='.urlencode($encserno).'&rsno='.urlencode($rsno);
			
			$this->load->library('dom_parser');
		
			//$html = $this->dom_parser->file_get_html("http://tneb.tnebnet.org/newlt/consumerwise_gmc_report.php?encserno=VEFOR0VEQ098fDEyMXx8MDA5fHw4&rsno=MQ%3D%3D");
			$html = $this->dom_parser->file_get_html($url);
					/* 
					NEW POST METHOD (Now it is GET method)
					$url = 'http://tneb.tnebnet.org/newlt/consumerwise_gmc_report.php';//?encserno='.urlencode($encserno).'&rsno='.urlencode($rsno);
					$request_post = array(
					'http' => array(
						'method' => 'POST',
						'content' => http_build_query(array(
							'ctl00$ctl00$MasterPageContentPlaceHolder$ucLogin$txtUserName' => $encserno,
							'ctl00$ctl00$MasterPageContentPlaceHolder$ucLogin$txtPassword' => $rsno
						)),
					)
					);
					$context = stream_context_create($request_post);
					$html = $this->dom_parser->file_get_html($url,false, $context);
					*/

		}
		//echo $html;exit;

		$tag = $html->find('table',7);
		
		
		// echo $tag;exit;
		// $error_result['status'] = "error";
		// $error_result['data'] = $tag;
		// print_r(json_encode($error_result)); exit;	

		//echo urlencode(strip_tags($tag->find("tr",0)->innertext)); exit;
	
		if($tag && urlencode($tag->innertext) == "++%3Ctr+align%3D%22center%22%3E%3Ctd+align%3D%22center%22%3E++%3C%2Ftd%3E%3C%2Ftr%3E" || urlencode($tag->innertext) == "+%3Ctr+align%3D%22center%22%3E%3Ctd+align%3D%22center%22%3E+%3C%2Ftd%3E%3C%2Ftr%3E")
		{
			//for 021000048 && 011210078
			//If Dues To Be Paid  Exist
			$tag = $html->find('table',9);

			
		}
		else if($tag && urlencode(strip_tags($tag->find("tr",0)->innertext)) == "++%09%09%09%09Electricity+Consumption++%09%09%09")
		{
			$tag = $html->find('table',8);
			
		}
		else if($tag && urlencode(strip_tags($tag->find("tr",0)->innertext)) == "++%09%09%09%09Disconnection%2FReconnection+Details++%09%09%09")
		{
			$tag = $html->find('table',9);
			
		}
		
		$count = 0;
		$my_all_result =array();
		$my_all_history = array();
		$my_single_history = array();
		
		//echo $tag;
		if($tag == "" || $tag == null)
		{
			$error_result['status'] = "error";
			$error_result['data'] = "Sorry, Your service no. is not valid. Please try with proper input.";
			print_r(json_encode($error_result)); 
			exit;	
		}
		
		
			
	foreach($tag->find('tr') as $single_row)
	{
		
				//For region 2 and 4 api, there is title at top so skip it
				if(($regno == 2 || $regno ==4) && $loop_cur_count == -1)
				{
					$loop_cur_count = $loop_cur_count+1;
					continue; //Skip top title
				}
				
				$loop_cur_count = $loop_cur_count+1;
				if($last_update)
				{
					if($loop_cur_count == 0)
					{
						continue;
					}
					$my_single_history_temp_ass = str_replace('&nbsp;', '',trim(strip_tags($single_row->find('td',1)->innertext)));
					$my_convert_try = date_create_from_format("d/m/Y",$my_single_history_temp_ass);
					if($my_convert_try)
					{
						$my_single_history['assessment_date']= date_format($my_convert_try,"Y-m-d");
						
					}
					else
					{
							if($my_single_history_temp_ass == "-" && $loop_cur_count > 1)
							{	
								$my_single_history['assessment_date'] = $my_single_history['assessment_date'];
							}
							else
							{
								$my_single_history['assessment_date'] = $my_single_history_temp_ass;
							}
					}
					if($my_single_history['assessment_date'] <= $last_update )
					{
						continue;
					}
				}
				else
				{
					
					
					$my_single_history_temp_ass = str_replace('&nbsp;', '',trim(strip_tags($single_row->find('td',1)->innertext)));
					if($loop_cur_count == 0)
					{
						$my_single_history['assessment_date']=$my_single_history_temp_ass;
					}
					else
					{
						$my_convert_try_else = date_create_from_format("d/m/Y",$my_single_history_temp_ass);
						if($my_convert_try_else)
						{
							$my_single_history['assessment_date']= date_format($my_convert_try_else,"Y-m-d");
						}
						else
						{
						
							if($my_single_history_temp_ass == "-")
							{	
								$my_single_history['assessment_date'] = $my_single_history['assessment_date'];
							}
							else
							{
								$my_single_history['assessment_date']=$my_single_history_temp_ass;
							}
						}
					}
				}
				
				$my_single_history['reading'] =str_replace('&nbsp;', '',trim(strip_tags($single_row->find('td',2)->innertext)));
				$my_single_history['units']=str_replace('&nbsp;', '',trim(strip_tags($single_row->find('td',3)->innertext)));
				if($my_single_history_temp_ass == "-")
				{
					 $my_single_history['amount']=$single_row->find('td',11)->innertext;
				}
				else
				{
					$my_single_history['amount']=str_replace('&nbsp;', '',trim(strip_tags($single_row->find('td',9)->innertext)));
				}
				$my_single_history_temp= str_replace('&nbsp;', '',strip_tags($single_row->find('td',13)->innertext));
				
				
				if($loop_cur_count == 0)
				{
					$my_single_history['payment_date']=$my_single_history_temp;
				}
				else
				{
					$my_convert_try_sec = date_create_from_format("d/m/Y",$my_single_history_temp);
					if($my_convert_try_sec)
					{
						$my_single_history['payment_date']= date_format($my_convert_try_sec,"Y-m-d");
						
					}
					else
					{
						$my_single_history['payment_date'] = $my_single_history_temp;
					}
				}

				$orginal= $single_row->find('td',15)->innertext;
				$my_single_history_status = str_replace('&nbsp;', '', strip_tags($single_row->find('td',15)->innertext));
				
				if($my_single_history['payment_date'] == "" && $my_single_history_status == "NORMAL" )
				{
					if($my_convert_try_sec && $my_single_history_status == "NORMAL")
					{
						$my_single_history['status']="Paid";
					}
					else if($my_single_history_status == "NORMAL")
					{
						$my_single_history['status']="Unpaid";
					}
					else
					{
					$my_single_history['status']=$my_single_history_status;
					}
					$my_single_history['payment_date'] = "0000-00-00";
					
				}
				else if($my_single_history_status == "NORMAL")
				{
					if($my_convert_try_sec)
					{
						$my_single_history['status']="Paid";
					}
					else
					{
						$my_single_history['status']="Unpaid";
					}
					
				}
				else
				{
					if($my_convert_try_sec)
					{
							if($my_single_history_status == null)
							{
								$my_single_history['status']= "Paid";
							}
							else
							{
								$my_single_history['status'] = $my_single_history_status;
							}
					}
					else
					{
						$my_single_history['status']=$my_single_history_status;
					}
				}
				
				array_push($my_all_history,$my_single_history);
				
		//print_r($single_row->innertext);
		
	}
		//var_dump($extra_ordinary);
		if(($regno == 2 || $regno ==4) && $extra_ordinary == FALSE)
		{
				$tag_sub_head = $html->find('table',2);
				
				$first_row = $tag_sub_head->find('tr',0);
				$second_row = $tag_sub_head->find('tr',1);
				$third_row = $tag_sub_head->find('tr',2);
				$fourth_row = $tag_sub_head->find('tr',3);
				$fifth_row = $tag_sub_head->find('tr',4);
				$sixth_row = $tag_sub_head->find('tr',5);
				$seventh_row = $tag_sub_head->find('tr',6);
				$eighth_row = $tag_sub_head->find('tr',7);
				$consumer_name= str_replace('&nbsp;', '',trim(strip_tags($first_row->find('td',1)->innertext)));
				$region_name= str_replace('&nbsp;', '',trim(strip_tags($second_row->find('td',1)->innertext)));
				$circle_name= str_replace('&nbsp;', '',trim(strip_tags($third_row->find('td',1)->innertext)));
				$phase_name= str_replace('&nbsp;', '',trim(strip_tags($third_row->find('td',3)->innertext)));
				$section_name= str_replace('&nbsp;', '',trim(strip_tags($fourth_row->find('td',1)->innertext)));
				$load_name= str_replace('&nbsp;', '',trim(strip_tags($fourth_row->find('td',3)->innertext)));
				$distribution_name= str_replace('&nbsp;', '',trim(strip_tags($fifth_row->find('td',1)->innertext)));
				$service_number = str_replace('&nbsp;', '',trim(strip_tags($sixth_row->find('td',1)->innertext)));
				$meter_number = str_replace('&nbsp;', '',trim(strip_tags($sixth_row->find('td',3)->innertext)));
				$address_name = str_replace('&nbsp;', '',trim(strip_tags($seventh_row->find('td',1)->innertext)));
				$service_status = str_replace('&nbsp;', '',trim(strip_tags($eighth_row->find('td',1)->innertext)));
				
		}
		else
		{
			// print_r($my_all_history);exit;
		$tag_head = $html->find('table',2);
		
		$consumer_head= trim(strip_tags($tag_head->find('tr',0)->find('td',0)->outertext));
		$consumer_name_array = explode("CONSUMER NAME:",$consumer_head);
		
		$tag_sub_head = $html->find('table',3);

			$first_row = $tag_sub_head->find('tr',0);
			$second_row = $tag_sub_head->find('tr',1);
			$region_name= str_replace('&nbsp;', '',trim(strip_tags($first_row->find('td',0)->innertext)));
			$phase_name= str_replace('&nbsp;', '',trim(strip_tags($first_row->find('td',2)->innertext)));
			$circle_name= str_replace('&nbsp;', '',trim(strip_tags($second_row->find('td',0)->innertext)));
			$load_name= str_replace('&nbsp;', '',trim(strip_tags($second_row->find('td',2)->innertext)));
			$load_name_integer = str_replace('&nbsp;', '',trim(str_replace('KW', '',$load_name)));
//echo  $url; exit;

			 $slabe_rate = $html->find('table',4);
    		 // returns all the <tr> tag inside $table
    		 $all_slabe_rate_trs_count = $slabe_rate->find('tr');
    		 $all_slabe_count = count($all_slabe_rate_trs_count);
    
	 	//	echo $all_slabe_count; exit;

		
			
			if($load_name_integer != "" && $load_name_integer >0)
			{
				
				
						
			//	print_r($tag_sub_head->find('tr',9)->innertext); exit;
				$third_row = $tag_sub_head->find('tr',2+$all_slabe_count);
				$fourth_row = $tag_sub_head->find('tr',3+$all_slabe_count);
				$fifth_row =$tag_sub_head->find('tr',4+$all_slabe_count);
				$sixth_row =$tag_sub_head->find('tr',5+$all_slabe_count);
				$seventh_row =$tag_sub_head->find('tr',6+$all_slabe_count);
			//	$eighth_row =$tag_sub_head->find('tr',7+$all_slabe_count);
				
				//print_r($eighth_row->innertext); exit;
				$meter_number= str_replace('&nbsp;', '',trim(strip_tags($fourth_row->find('td',2)->innertext)));
				$service_number=  str_replace('&nbsp;', '',trim(strip_tags($fifth_row->find('td',0)->innertext)));
				$address_name = trim(trim(strip_tags($sixth_row->find('td',0)->innertext)),'&nbsp;');
				$service_status = str_replace('&nbsp;', '',trim(strip_tags($seventh_row->find('td',0)->innertext)));
				$section_name= str_replace('&nbsp;', '',trim(strip_tags($third_row->find('td',0)->innertext)));
				$distribution_name= str_replace('&nbsp;', '',trim(strip_tags($fourth_row->find('td',0)->innertext)));
				
			}
			else 
			{
				$error_result['status'] = "error";
				$error_result['data'] = "Sorry, Your service no. is not valid. Please try with proper input.";
				//print_r(json_encode($error_result)); exit;
				return 	$error_result;
			}
			
		 	   $consumer_name = $consumer_name_array[1];
		}
			   
		
		

		$my_all_result['ServiceNo'] =  $disp_service_no;
		$my_all_result['Name'] = $consumer_name;
		$my_all_result['Region'] = $region_name;
		$my_all_result['Phase'] = $phase_name;
		$my_all_result['Circle'] = $circle_name;
		$my_all_result['Section'] = $section_name;
		$my_all_result['Load'] = $load_name;
		$my_all_result['Distribution'] = $distribution_name;
		$my_all_result['MeterNo'] = $meter_number;
		$my_all_result['ConnectionNumber'] = $service_number;
		$my_all_result['Address'] = $address_name;
		$my_all_result['ServiceStatus'] = $service_status;
		$my_all_result['usage'] = $my_all_history;
		
		
		return $my_all_result;
	}
	
	public function get_karnataka()
	{
/* 
   $url = 'https://www.bescom.co.in/SCP/Myhome.aspx';
   $params = "txtUserName=tprakass&txtPassword=Muruga78vam$"; //you must know what you want to post
   $user_agent = "Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)";
   $ch = curl_init();
   curl_setopt($ch, CURLOPT_POST,1);
   curl_setopt($ch, CURLOPT_POSTFIELDS,$params);
   curl_setopt($ch, CURLOPT_URL,$url);
   curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);

   $result=curl_exec ($ch);
   curl_close ($ch);
print_r($_POST);

   echo "Results: <br>".$result;
   */
   //
  //$this->get_results();

//    $url="https://www.bescom.co.in/SCP/Myhome.aspx";
// //$url="http://www.google.com/accounts/ClientLogin";
// $postdata = http_build_query(
//     array(
//         'txtUserName' => "tprakass",
// 		'txtPassword' => "Muruga78vam$",
//     )
// );
// $opts = array('http' =>
//     array(
//         'header'  => "Content-type: application/x-www-form-urlencoded\r\n".
//         "User-Agent : Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36\r\n",
//                     'Accept : text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
// 					'Connection:'.'keep-alive',
//         'method'  => 'POST',
//         'content' => $postdata,
//         'ignore_errors' => true,
//     )
// );
// $context  = stream_context_create($opts);
// $result = file_get_contents($url, false, $context);
// print_r($result);
//exit;
			/* code By Prabin Sir */
			$url = "https://www.bescom.co.in/SCP/Myhome.aspx";
			//$url = "https://www.bescom.co.in/SCP/MyAccount/AccountSummary.aspx?Name=IAccountSummaryView";
		//$url="http://localhost/post_api/post.php";
		
					$this->load->library('dom_parser');
					$query = http_build_query(array(
							'masterPageId' => "ctl00_ctl00_MasterPageContentPlaceHolder_MasterPageContentPlaceHolder",
							'ErrorLabelId' => "ctl00_ctl00_MasterPageContentPlaceHolder_lblErrorMessage",
							'__LASTFOCUS' => "",
							//'ctl00_ctl00_MasterPageContentPlaceHolder_ToolkitScriptManager2_HiddenField' => ";;AjaxControlToolkit, Version=3.5.40412.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e:en-US:1547e793-5b7e-48fe-8490-03a375b13a33
//:475a4ef5:effe2a26:1d3ed089:5546a2b:497ef277:a43b07eb:751cdd15:dfad98a5:3cf12cf1",
							'ctl00_ctl00_MasterPageContentPlaceHolder_ToolkitScriptManager2_HiddenField' => "",
							'__EVENTTARGET' => "",
							'__EVENTARGUMENT' => "",
							//'__VIEWSTATE' => $view_state,
							'__DirtyFlag' => "N",
							'__UipId' => "bd8bdec1f6184f63ae83a9bb7d36b130",
							'__EVENTVALIDATION' => "/wEWBwLG9ZOlCgL78cLfDgLRucfIBwK73vWLDQL+/NgwAuD/66sHApT4yIAIOkpFam7L2YyQUF5IwnGBrg400w0=",
							'ctl00$ctl00$siteSearch$txtsearch' => "Search",
							'ctl00$ctl00$MasterPageContentPlaceHolder$ucLogin$txtUserName' => "tprakass",
							'ctl00$ctl00$MasterPageContentPlaceHolder$ucLogin$txtPassword' => "Muruga78vam$",
							'ctl00$ctl00$MasterPageContentPlaceHolder$ucLogin$btnLogin' => "Sign In",
							
						));
						
						
					//THIS : echo $this->do_post_request($url,$query);exit;
					//OR   : BELOW ONE

					$request_post = array(
					'http' => array(
					'header' => "Content-Type: application/x-www-form-urlencoded\r\n".
                    //"Content-Length: ".strlen($query)."\r\n".
                    'Cookie :  CC_ppxp0mueij0uba55ruv0s3ey=en-US; ASP.NET_SessionId=iiymcxrf0w4fvw45xursgx55; CC_iiymcxrf0w4fvw45xursgx55=en-US; UipState_b7501ab329894115b555589139bd079d=20150723035939,',
                    //'Cookie : CC_pmkatv55dbnsuzfrr32l3045=en-US; ASP.NET_SessionId=gzszat45zm4p3erbcrzrsrqu; UipState_e257c20da2de43f9b38b1fdd24f46151=20150722091832,; UipState_032c45fad4e640da9291323d356ca31f=20150722091858,; UipState_b7a449555ad142449f7960be184f4dc2=20150722091902,; UipState_d9a08f64bcd54a78b19120d062b9fceb=20150722092351,; UipState_58520fd7bdcc4e18802729e9f2dc50c5=20150722092353,; UipState_f66a6688fc364ace9eade1cec4571574=20150722092821,; CC_gzszat45zm4p3erbcrzrsrqu=en-US; UipState_a61b872a045946d2b11c45cb4a9a3eec=20150722101910,; .ASPXAUTH=AE65CCB7AA45577E74ABAFC8E68CF43D8B3288800EA1E0EB6550C471C90D9AD0D0F6D6EDD394C030FA2E61C66EE1B14676BDB6FD3C8A3C87321B23160B3704364C3E82CA774E9A28ECB846F0708C14CD98AFEC00F2F87C4E1F01ED1BA7902429936926F62C3C6438E4734A1966AA90A6684CC1C65354D51313BE31A2CE5D9D532537537491830A4AECED5A77F9605A207D77C49DE786E4BF9CEBFA7CF3D5527F7AFDB5DA459E29B5343C5DF3999F651184907725B077C78E46B077369AFB45E3B462C986225D7142B39135637E10D0C771B66D55; UipState_731d56fce20d4a6280c6a0db2337d0d7=20150722103446,',
                    //"User-Agent : Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36\r\n",
                    "User-Agent : Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36\r\n",
                    'Accept : text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
					'Connection:'.'keep-alive',
						//'method' => 'GET',
						'method' => 'POST',
						'content' => $query,
					)
					);
					$context = stream_context_create($request_post);
					$html = $this->dom_parser->file_get_html($url,false, $context);
					
					
				//	$url = "http://www.bescom.co.in/SCP/MyAccount/AccountSummary.aspx?Name=IAccountSummaryView";
					//$html = $this->dom_parser->file_get_html($url);
				//print_r($html);

		echo $html->innertext;
		

	}
	function do_post_request($url, $data, $optional_headers = null)
{
  $params = array('http' => array(
              'method' => 'POST',
              'content' => $data
            ));
  if ($optional_headers !== null) {
    $params['http']['header'] = $optional_headers;
  }
  $ctx = stream_context_create($params);
  $fp = @fopen($url, 'rb', false, $ctx);
  if (!$fp) {
    throw new Exception("Problem with $url, $php_errormsg");
  }
  $response = @stream_get_contents($fp);
  if ($response === false) {
    throw new Exception("Problem reading data from $url, $php_errormsg");
  }
  return $response;
}

function get_results()
{
	echo "<pre>\n";
		
// THIS STRING (CASE SENSITIVE) HAS THE LOGIN CREDENTIALS FOR THE SITE
$post = "txtUserName=tprakass&txtPassword=Muruga78vam%24";

//$post = "txtUserName=tprakass&txtPassword=Muruga78vam%24";

// READ THE SITE PAGE WITH THE LOGIN FORM
$baseurl = 'https://www.bescom.co.in/SCP/Myhome.aspx';

// FOR FRONIUS USE AN EXPLICIT URL TO PROCESS THE LOGIN
$posturl = 'https://www.bescom.co.in/SCP/Myhome.aspx';

// WHERE TO GO AFTER THE LOGIN
$nexturl = 'https://www.bescom.co.in/SCP/MyAccount/AccountSummary.aspx?Name=IAccountSummaryView';

// SET UP OUR CURL ENVIRONMENT
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $baseurl);

curl_setopt($ch, CURLOPT_FAILONERROR, TRUE);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
curl_setopt($ch, CURLOPT_TIMEOUT, 5);

// CALL THE WEB PAGE
$htm = curl_exec($ch);
$err = curl_errno($ch);
$inf = curl_getinfo($ch);
if ($htm === FALSE)
{
    echo "\nCURL GET FAIL: $baseurl CURL_ERRNO=$err ";
    var_dump($inf);
    die();
}

// NOW POST THE DATA WE HAVE FILLED IN
curl_setopt($ch, CURLOPT_URL, $posturl);
curl_setopt($ch, CURLOPT_POST, TRUE);
curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

// WAIT A RESPECTABLE PERIOD OF TIME
sleep(3);

// CALL THE WEB PAGE TO COMPLETE THE LOGIN
$xyz = curl_exec($ch);
$err = curl_errno($ch);
$inf = curl_getinfo($ch);
if ($xyz === FALSE)
{
    echo "\nCURL POST FAIL: $posturl CURL_ERRNO=$err ";
    var_dump($inf);
}

// NOW ON TO THE NEXT PAGE
curl_setopt($ch, CURLOPT_URL, $nexturl);
curl_setopt($ch, CURLOPT_POST, FALSE);
curl_setopt($ch, CURLOPT_POSTFIELDS, '');

$xyz = curl_exec($ch);
$err = curl_errno($ch);
$inf = curl_getinfo($ch);
if ($xyz === FALSE)
{
    echo "\nCURL 2ND GET FAIL: $posturl CURL_ERRNO=$err ";
    var_dump($inf);
}

print_r($xyz); exit;
// ACIVATE THIS TO SHOW OFF THE DATA WE RETRIEVED AFTER THE LOGIN
// echo htmlentities($xyz);

// REFINE THE DATA SOME
$xyz = strip_tags($xyz, '<div><td>');

$arr = explode('<div id="ctl00_MainContent_UpdatePanel1">', $xyz);
// KEEP THE SECOND HALF
$xyz = $arr[1];
// ADD SOME INSURANCE ABOUT SPACING
$xyz = str_replace('>', '> ', $xyz);
$xyz = strip_tags($xyz);
echo htmlentities($xyz);
exit;
}
	
	public function get_power_cut()
	{
		error_reporting(0);
		//phpinfo(); exitl
        $url = 'http://tneb.tnebnet.org/cpro/today.html';
		$this->load->library('dom_parser');
		
		$my_final_result_array = array();
		//$html = $this->dom_parser->file_get_html("http://tneb.tnebnet.org/newlt/consumerwise_gmc_report.php?encserno=VEFOR0VEQ098fDEyMXx8MDA5fHw4&rsno=MQ%3D%3D");
		$html = $this->dom_parser->file_get_html($url);
		$tag = $html->find('table',0);
		//print_r(strip_tags($tag->innertext));
		
		$time_a = explode("between",strip_tags($tag->innertext));
		//Get Starting time
		$time_1 = explode("to",$time_a[1]);
		$time_1_ampm =substr(trim($time_1[0]),-4);
		$time_1_num = explode($time_1_ampm,trim($time_1[0]));
		//Get Ending time
		$time_2 = explode("for",$time_1[1]);
		$time_2_ampm = substr(trim($time_2[0]),-4);
		$time_2_num = explode($time_2_ampm,trim($time_2[0]));
		
		$time_array = array(
							'start'=>array('time'=>$time_1_num[0],'ampm'=>$time_1_ampm),
							'end'=>array('time'=>$time_2_num[0],'ampm'=>$time_2_ampm)
							);
		//print_r($time_array);
		
		$tag2 = $html->find('table',1);
		$my_all_data_without_tag = trim(strip_tags($tag2->innertext));
		$date_a = explode("AREA",$my_all_data_without_tag);
		$date = substr(trim($date_a[1]),0,8);
		$all_area_name = array();
		
		//print_r($my_all_data_without_tag);
		$my_area_arr = explode(":",$my_all_data_without_tag);
		$j=0;
					

		//print_r($my_area_arr);exit;
		for($i =0; $i < count($my_area_arr); $i++ )
		{
			if($i == 0)
			{
				$cur_area_name=substr(trim($date_a[1]),8,strlen(trim($date_a[1])));
				if($cur_area_name)
				{
					$cur_section_data = $this->get_section_id_by_name($cur_area_name);
					//region_id,code section_id,name
					if($cur_section_data)
					{
						$all_area_name[$j] = array("name"=>$cur_area_name,"section_id"=>$cur_section_data['section_id'],"region_id"=>$cur_section_data['region_id']);
						$j++;
					}
				
				}
				
				continue;
			}
			$temp_arr = explode("AREA",trim($my_area_arr[$i]));
			if($temp_arr[0])
			{
				$area_arr = explode(".",trim($temp_arr[0]));
				$cur_area_name = trim($area_arr[count($area_arr)-1]);
				if($cur_area_name)
				{
					$cur_section_data = $this->get_section_id_by_name($cur_area_name);
					//region_id,code section_id,name
					if($cur_section_data)
					{
						$all_area_name[$j] = array("name"=>$cur_area_name,"section_id"=>$cur_section_data['section_id'],"region_id"=>$cur_section_data['region_id']);
						$j++;
					}
				
				}
			}
		}
		$message_arr = explode($date,$tag2->innertext);
		$my_temp_array = array("time"=>$time_array,"date"=>$date,"area"=>$all_area_name,"message"=>strip_tags($message_arr[1]));
		
		array_push($my_final_result_array,$my_temp_array);
		return $my_final_result_array;
	
	}
	
	}