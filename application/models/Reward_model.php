<?php  defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */

class Reward_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function get_reward_list()
	{

		$this->db->select('rr.id, rr.name, rr.emailid, rr.reward_type, rr.participantid, rr.total_balance, rr.paid_balance, rr.remaining_balance');
		$this->db->from('reward_referal rr');
		$this->db->join('reward_detail rd', 'rd.participant_id = rr.participantid');
		$this->db->group_by('rr.id');
		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			$result = $query->result_array();

			$query->free_result();

			return $result;
		}

		return FALSE;

	}

	public function pay_reward($participantid)
	{

		$this->db->set('paid_balance', 'paid_balance + '. 50, FALSE);
		$this->db->set('remaining_balance', 'remaining_balance - '. 50, FALSE);
		$this->db->set('last_paid_date', $this->general->get_local_time('now'));

		$this->db->where('id', $participantid);

		$this->db->update('reward_referal');

		return $this->db->affected_rows();

	}

	/**
	 * this method retrieves reward details by participant id
	 * 
	 * @param  integer $participant_id participant id
	 * @return mixed                 
	 */
	public function get_reward_detail($participant_id = 0)
	{
		$this->db->select('rr.reward_type, rd.participant_id, rr.name, rr.emailid, rd.rewarddtlid, rd.rewarded_date , rd.amount, rr.reward_unit');
		$this->db->from('reward_detail rd');
		$this->db->where('participant_id', $participant_id);

		$this->db->join('reward_referal rr', 'rr.participantid = rd.participant_id');

		$query = $this->db->get();
		// echo $this->db->last_query();
		if($query->num_rows() > 0)
		{

			$result = $query->result_array();
			$query->free_result();

			return $result;

		}
		return FALSE;
	}

}