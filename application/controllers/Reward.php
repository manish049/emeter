<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
*/

class Reward extends CI_Controller
{
	function __construct()
    {
    	parent::__construct();

    	$this->load->model('reward_model');
    }

    public function index()
    {
    	
		$data['rewards'] = $this->reward_model->get_reward_list();

		$this->load->view('reward_list', $data);
	
    }

    public function reward_detail($participant_id= 0, $user_email = '')
    {
    	$participant_id = $this->input->post('participant_id');

    	// $data['user_email'] = urldecode($user_email);
    	
    	$data['reward_details'] = $this->reward_model->get_reward_detail($participant_id);
    	$data['participant_id'] = $participant_id;
    	
		$this->load->view('reward_detail_list', $data);
    }

    public function pay_reward($user_id)
    {
    	
    	$reward_pay_status = $this->reward_model->pay_reward($user_id);
    	
    	redirect(base_url().'reward/', 'refresh');
    }
}