<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Mobile extends REST_Controller
{
	function __construct()
    {
        // Construct our parent class
        parent::__construct();
        // Configure limits on our controller methods. Ensure
        // you have created the 'limits' table and enabled 'limits'
        // within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; //500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; //100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; //50 requests per hour per user/key
		$this->load->model('mobile_model');
        $this->load->helper('inflector');

    }
	    
     function all_list_api_get()
    {
        if(!$this->get('last_update'))
        {
        	//$this->response(NULL, 400);
			$last_update = false;
        }
		else
		{
			$last_update = $this->get('last_update');
		}
   			
		$region_list = $this->mobile_model->get_region_list($last_update);
		$section_list = $this->mobile_model->get_section_list($last_update);
		$distribution_list = $this->mobile_model->get_region_list($last_update);
			//	print_r($data); exit;

		if($region_list || $section_list || $distribution_list)
        {
			$result = array('status'=>'success','data'=>array('region_list'=>$region_list,'section_list'=>$section_list,'distribution_list'=>$distribution_list));
			
            $this->response($result, 200); // 200 being the HTTP response code
        }

        else
        {
			$result = array('status'=>'error','data'=>array('message'=>'No new data found'));
            $this->response($result, 200);
        }
    }
    
	 function region_get()
    {
        if(!$this->get('last_update'))
        {
        	//$this->response(NULL, 400);
			$last_update = false;
        }
		else
		{
			$last_update = $this->get('last_update');
		}
   			
		$region_list = $this->mobile_model->get_region_list($last_update);
		
		if($region_list)
        {
			$result = array('status'=>'success','data'=>$region_list);
			
            $this->response($result, 200); // 200 being the HTTP response code
        }
        else
        {
			$result = array('status'=>'error','data'=>array('message'=>'No new data found'));
            $this->response($result, 200);
        }
    }
	
	 function section_get()
    {
        if(!$this->get('last_update'))
        {
        	//$this->response(NULL, 400);
			$last_update = false;
        }
		else
		{
			$last_update = $this->get('last_update');
		}

		if($this->get('q'))
		{
			$region_id =$this->get('q');
			$section_list = $this->mobile_model->get_section_list_by_region_id($last_update,$region_id);
		}
		else
		{
		  $section_list = $this->mobile_model->get_section_list($last_update);
		}
		
		if($section_list)
        {
			$result = array('status'=>'success','data'=>$section_list);
			
            $this->response($result, 200); // 200 being the HTTP response code
        }
        else
        {
			$result = array('status'=>'error','data'=>array('message'=>'No new data found'));
            $this->response($result, 200);
        }
    }
	
	function distribution_get()
    {
        if(!$this->get('last_update'))
        {
        	//$this->response(NULL, 400);
			$last_update = false;
        }
		else
		{
			$last_update = $this->get('last_update');
		}

		if($this->get('q'))
		{
			$section_id =$this->get('q');
			if($this->get('region_id'))
			{
				$region_id =$this->get('region_id');
				$distribution_list = $this->mobile_model->get_distribution_list_by_reg_and_section_id($last_update,$region_id,$section_id);
			}
			else
			{
			$distribution_list = $this->mobile_model->get_distribution_list_by_section_id($last_update,$section_id);
			}
		}
		else
		{
		     $distribution_list = $this->mobile_model->get_distribution_list($last_update);
		}
		
		if($distribution_list)
        {
			$result = array('status'=>'success','data'=>$distribution_list);
			
            $this->response($result, 200); // 200 being the HTTP response code
        }
        else
        {
			$result = array('status'=>'error','data'=>array('message'=>'No new data found'));
            $this->response($result, 200);
        }
    }
	
	function appliance_get()
    {
        if(!$this->get('last_update'))
        {
        	//$this->response(NULL, 400);
			$last_update = false;
        }
		else
		{
			$last_update = $this->get('last_update');
		}

		if($this->get('q'))
		{
			$section_id =$this->get('q');
			$appliance_list = $this->mobile_model->get_appliance_by_id($last_update,$section_id);
		}
		else
		{
		     $appliance_list = $this->mobile_model->get_appliance_list($last_update);
		}
		
		if($appliance_list)
        {
			$result = array('status'=>'success','data'=>$appliance_list);
			
            $this->response($result, 200); // 200 being the HTTP response code
        }
        else
        {
			$result = array('status'=>'error','data'=>array('message'=>'No new data found'));
            $this->response($result, 200);
        }
    }
	
	function user_history_get()
	{
		
			$service_no_ip = $this->input->get('consumer',TRUE);
			if (strlen($service_no_ip) < 7 || !($service_no_ip >= 0 || $service_no_ip < 0)) 
			{ 
				// check 7 coz that is the minimum length needed
				$error_result['status'] = "error";
				$error_result['data'] = "Input Service no. is not valid!";
				print_r(json_encode($error_result)); exit;	
    		} 
			
			if(!$this->get('last_update'))
			{
				//$this->response(NULL, 400);
				$last_update = false;
			}
			else
			{
				$last_update = $this->get('last_update');
			}
			
			//Check if data already exist in database
		/*	$connection_detail =  $this->mobile_model->is_connection_id_exist($service_no_ip);
		
			if($connection_detail)
			{
				print_r(json_encode($this->mobile_model->do_update_userdata($service_no_ip,$connection_detail,$last_update)));
			}
			else
			{
				print_r(json_encode($this->mobile_model->do_insertion_userdata($service_no_ip)));
			}
			*/
			$connection_detail = false;
		print_r(json_encode($this->mobile_model->do_update_userdata($service_no_ip,$connection_detail,$last_update)));

			exit;
	}
	// By Manish for getting data from bescom
	public function user_historyk_get()
	{
		$username = $this->input->get('username');
		$password = $this->input->get('password');
		if (empty($username) OR empty($password))
		{
			$error_result['status'] = "error";
			$error_result['message'] = "Please Type Username AND Password";
			$error_result['data'] = array();
			
			print_r(json_encode($error_result)); 
			exit;	
		}

		print_r(json_encode($this->mobile_model->get_karnataka($username, $password)));
		exit;
		
	}
	public function power_cut_get()
	{
		/*
		echo '[
{
time: {
start: {
time: "9.00 ",
ampm: "A.M."
},
end: {
time: "2.00 ",
ampm: "P.M."
}
},
date: "02-06-15",
area: [
{
name: "KOVOOR",
section_id: "304",
region_id: "9"
}
]
}
]';
exit;
*/		
		// $this->mobile_model->get_power_cut();
		// //print_r(json_encode());
		// exit;

		$data = $this->mobile_model->get_power_cut();
		if($data)
		{
			$status ='success';
		}
		else
		{
			$status ='error';
		}
		print_r(json_encode(array('status'=>$status,'data'=>$data)));
		exit;
	}
	
	public function send_post()
	{
		var_dump($this->request->body);
	}


	public function send_put()
	{
		var_dump($this->put('foo'));
	}

	
	public function multi_get() 
	{
 		//base_url
		$url = "https://www.bescom.co.in/SCP/Myhome.aspx";

		// cookie file
		$this->ckfile = dirname(__FILE__) . '\cookies2.txt';
		if (file_exists($this->ckfile)) {
		    @file_put_contents($this->ckfile, "");
		} 

		// authentication data
 		$username = "tprakass";
		$password = "Muruga78vam$";

		$regexViewstate = '/__VIEWSTATE\" value=\"(.*)\"/i';
		$regexEventVal  = '/__EVENTVALIDATION\" value=\"(.*)\"/i';
		$regexUip = '/__UipId\" value=\"(.*)\"/i';

 		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		
		curl_setopt($ch, CURLOPT_COOKIEJAR, $this->ckfile);

		curl_setopt($ch, CURLOPT_COOKIEFILE, $this->ckfile);

		$html=curl_exec($ch);
		
 		$regs  = array();

		/*
		    Get __VIEWSTATE & __EVENTVALIDATION & __UipId
		 */

		$viewstate = $this->mobile_model->regexExtract_get($html,$regexViewstate,$regs,1);
		//echo $viewstate;exit;
		$eventval = $this->mobile_model->regexExtract_get($html, $regexEventVal,$regs,1);
		//echo $eventval;exit;
		$postfields['__EVENTTARGET'] = "";
		$postfields['__EVENTARGUMENT'] = "";
		$postfields['__VIEWSTATE'] = $viewstate;

		$postfields['__EVENTVALIDATION'] = $eventval;


		$postfields['ctl00$ctl00$MasterPageContentPlaceHolder$ucLogin$txtUserName'] = $username;
		$postfields['ctl00$ctl00$MasterPageContentPlaceHolder$ucLogin$txtPassword'] = $password;
		$postfields['ctl00$ctl00$MasterPageContentPlaceHolder$ucLogin$btnLogin'] = "Sign In";
 		
		$afterLoginUrl = "https://www.bescom.co.in/SCP/MyAccount/AccountSummary.aspx?Name=IAccountSummaryView";

		curl_setOpt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
		curl_setopt($ch, CURLOPT_URL, $url);   
		curl_setOpt($ch, CURLOPT_REFERER, 'https://www.bescom.co.in/SCP/Myhome.aspx');

		$data = curl_exec($ch);


		$client_detail_array = array();		
		$client_detail = array();
		$status = array();
		
		$this->load->library('dom_parser');	
		$html = $this->dom_parser->str_get_html($data);

		$tag = $html->find('table',13);
		
		
		//echo $tag;exit;

		if($tag == "" || $tag == null)
		{
			$error_result['status'] = "error";
			$error_result['data'] = "Sorry, Your service no. is not valid. Please try with proper input.";
			print_r(json_encode($error_result)); 
			exit;	
		}
		
		$loop_count = -1;
		foreach($tag->find('tr') as $single_row)
		{
			if ($loop_count == -1)
			{
				$loop_count++;
				continue;
			}

			$client_detail['connection'] = trim(strip_tags($single_row->find('td',0)->innertext));

			$connection_no = $client_detail['connection'];
			if ($connection_no == "")
			{
				$status['status'] = "error";
				$status['data'] = "Invalid Username OR Password";
				print_r(json_encode($error_result)); 
				exit;	
			}

			$client_detail['name'] = trim(strip_tags($single_row->find('td',1)->innertext));
			$client_detail['status']  = trim(strip_tags($single_row->find('td',6)->innertext));
			

			$details_url='https://www.bescom.co.in/SCP/MyAccount/AccountDetails.aspx?AccountId='.urlencode($connection_no).'&RowIndex%20='.urlencode($loop_count);
			
			curl_setopt($ch, CURLOPT_URL, $details_url);
			

			$content=curl_exec($ch);
            curl_close($ch);

            $html = $this->dom_parser->str_get_html($content);

			$tag = $html->find('table',0);

			//echo $tag;exit;
			
			$account_summary = array();
			foreach($tag->find('tr') as $single_detail_row)
			{
				$key = underscore(strtolower(trim(strip_tags($single_detail_row->find('td',0)->innertext))));
				$value = trim(strip_tags($single_detail_row->find('td',1)->innertext));
				$account_summary[$key] = $value;

			}
			$client_detail['summary']  =  $account_summary;

			array_push($client_detail_array,$client_detail);

			$data = array(array(),array());

		  	$data[0]['url']  = "https://www.bescom.co.in/SCP/UsageHistory/UsageHistory.aspx";
			$data[0]['post'] = array();
			$data[0]['post']['ctl00$ctl00$MasterPageContentPlaceHolder$MasterPageContentPlaceHolder$ddlcustomeraccounts'] = $connection_no;

			$data[1]['url'] = "https://www.bescom.co.in/SCP/MyAccount/BillingHistory.aspx";
			$data[1]['post'] = array();
			$data[0]['post']['ctl00$ctl00$MasterPageContentPlaceHolder$MasterPageContentPlaceHolder$BillingHistoryView$ddlCustomerAccountNumber'] = $connection_no;

			$ch  = curl_init();
			
			curl_setopt($ch, CURLOPT_URL, $details_url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			//curl_setOpt($ch, CURLOPT_USERAGENT, $useragent);
			curl_setopt($ch, CURLOPT_COOKIEJAR, $this->ckfile);
			curl_setopt($ch, CURLOPT_COOKIEFILE, $this->ckfile);

			$content=curl_exec($ch);

			//echo $data;exit;
			
			
			$viewstate = $this->regexExtract_get($content,$regexViewstate,$regs,1);
			
			//$file=file_get_contents($details_url);

			$eventval = $this->regexExtract_get($content, $regexEventVal,$regs,1);

            
            $data['__VIEWSTATE']=$viewstate;
            $data['__EVENTVALIDATION']=$eventval;
            $options = array(
				CURLOPT_RETURNTRANSFER => TRUE, // return web page
				CURLOPT_HEADER => FASLE, // don't return headers
				CURLOPT_FOLLOWLOCATION => true, // follow redirects
				CURLOPT_ENCODING => "", // handle all encodings
				//CURLOPT_USERAGENT => "spider", // who am i
				CURLOPT_AUTOREFERER => true, // set referer on redirect
				CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
				CURLOPT_TIMEOUT => 120, // timeout on response
				CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
				CURLOPT_POST => true,
				CURLOPT_POSTFIELDS => $data,
			);
            //$ch = curl_init( $url );
			curl_setopt_array( $ch, $options );
			$content = curl_exec ($ch);
            curl_close($ch);
			$options = array (

				);
			$r = $this->multiRequest($data);
			print_r($r);exit;
		}

		print_r(json_encode($client_detail_array));exit;

	  	
 
		
		echo $this->multiRequest($data,$options);	
		 
	}


	function multiRequest($data, $options = array()) {
 
	  	// array of curl handles
	  	$curly = array();
	  	// data to be returned
	  	$result = array();
	 
	  	// multi handle
	  	$mh = curl_multi_init();
	 
	  	// loop through $data and create curl handles
	  	// then add them to the multi-handle
	  	foreach ($data as $id => $d) {
	 
		    $curly[$id] = curl_init();
		 
		    $url = (is_array($d) && !empty($d['url'])) ? $d['url'] : $d;
		    curl_setopt($curly[$id], CURLOPT_URL,            $url);
		    curl_setopt($curly[$id], CURLOPT_HEADER,         0);
		    curl_setopt($curly[$id], CURLOPT_RETURNTRANSFER, 1);
		 
		    // post?
		    if (is_array($d)) {
		      if (!empty($d['post'])) {
		        curl_setopt($curly[$id], CURLOPT_POST,       1);
		        curl_setopt($curly[$id], CURLOPT_POSTFIELDS, $d['post']);
		      }
		    }
	 
		    // extra options?
		    if (!empty($options)) {
		      curl_setopt_array($curly[$id], $options);
		    }
	 
	    	curl_multi_add_handle($mh, $curly[$id]);
	  	}
	 
	  	// execute the handles
	  	$running = null;
	  	do {
	    	curl_multi_exec($mh, $running);
	  	} while($running > 0);
	 
	 
	  	// get content and remove handles
	  	foreach($curly as $id => $c) {
	    	$result[$id] = curl_multi_getcontent($c);
	    	curl_multi_remove_handle($mh, $c);
	  	}
	 
	  	// all done
	  	curl_multi_close($mh);
	 
	  	return $result;
	}


	public function usage_history_get()
	{
		$username = $this->input->get('username');
		$password = $this->input->get('password');
		if (empty($username) OR empty($password))
		{
			$error_result['status'] = "error";
			$error_result['data'] = "Please Type Username AND Password";
			
			print_r(json_encode($error_result)); 
			exit;	
		}

		print_r(json_encode($this->mobile_model->get_karnat($username, $password)));
		exit;

	}
}