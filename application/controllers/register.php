<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Register extends CI_Controller {
	
		function __construct() {
			parent::__construct();
					
			//load custom library
			if(SITE_STATUS == 'offline')
			{
				redirect($this->general->lang_uri('offline'));exit;
			}
			//echo "ok"; exit;	
			$this->load->model('api/registration_model');			
		}
		
		public function activation($activation_code = false,$user_id= false)
		{
			
			 if($this->registration_model->activated($activation_code,$user_id)==true)
			 {
				echo "Thank you for activation.";		
			 }
			 else
			 {
				echo "Sorry Invalid code";
			 }
			 exit;
		}
		
			
		
	}
	
	/* End of file welcome.php */
	/* Location: ./application/controllers/welcome.php */