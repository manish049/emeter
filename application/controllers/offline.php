<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Offline extends CI_Controller {

	function __construct() {
		parent::__construct();
		
		if(SITE_STATUS == 'online')
		{
			exit;
			//redirect(site_url(''));
		}
		
		
	}
	
	public function index()
	{
		$this->data = array();
	//	$this->data['offline_msg'] = "";//$this->get_cms(26);
		
		echo SITE_OFFLINE_MSG; exit;
		//set SEO data
	//	$this->page_title = isset($this->data['offline_msg']->page_title)? $this->data['offline_msg']->page_title : DEFAULT_PAGE_TITLE;
		//$this->data['meta_keys'] = isset($this->data['offline_msg']->meta_keys)? $this->data['offline_msg']->meta_keys : DEFAULT_PAGE_TITLE;
		//$this->data['meta_desc'] = isset($this->data['offline_msg']->meta_desc)? $this->data['offline_msg']->meta_desc : DEFAULT_PAGE_TITLE;
		
		//$this->load->view('offline',$this->data);
	}
	
	public function get_cms($parent_id)
	{
		//get language id from configure file
		$lang_id = $this->config->item('current_language_id');
		
		$data = array();
		$query = $this->db->get_where("cms",array("parent_id"=>$parent_id,"lang_id"=>$lang_id,"is_display"=>"Yes"));
		if ($query->num_rows() > 0) 
		{
			$data=$query->row();				
		}	
		else
			{
				$query = $this->db->get_where("cms",array("parent_id"=>$parent_id,"lang_id"=>DEFAULT_LANG_ID));
				if ($query->num_rows() > 0) 
				{
					$data=$query->row();				
				}
			}	
		$query->free_result();
		return $data;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */