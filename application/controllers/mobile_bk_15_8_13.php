<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class Mobile extends REST_Controller
{
	function __construct()
    {
        // Construct our parent class
        parent::__construct();
        // Configure limits on our controller methods. Ensure
        // you have created the 'limits' table and enabled 'limits'
        // within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; //500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; //100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; //50 requests per hour per user/key
		$this->load->model('mobile_model');
    }
	    
     function all_list_api_get()
    {
        if(!$this->get('last_update'))
        {
        	//$this->response(NULL, 400);
			$last_update = false;
        }
		else
		{
			$last_update = $this->get('last_update');
		}
   			
		$region_list = $this->mobile_model->get_region_list($last_update);
		$section_list = $this->mobile_model->get_section_list($last_update);
		$distribution_list = $this->mobile_model->get_region_list($last_update);
			//	print_r($data); exit;

		if($region_list || $section_list || $distribution_list)
        {
			$result = array('status'=>'success','data'=>array('region_list'=>$region_list,'section_list'=>$section_list,'distribution_list'=>$distribution_list));
			
            $this->response($result, 200); // 200 being the HTTP response code
        }

        else
        {
			$result = array('status'=>'error','data'=>array('message'=>'No new data found'));
            $this->response($result, 200);
        }
    }
    
	 function region_get()
    {
        if(!$this->get('last_update'))
        {
        	//$this->response(NULL, 400);
			$last_update = false;
        }
		else
		{
			$last_update = $this->get('last_update');
		}
   			
		$region_list = $this->mobile_model->get_region_list($last_update);
		
		if($region_list)
        {
			$result = array('status'=>'success','data'=>$region_list);
			
            $this->response($result, 200); // 200 being the HTTP response code
        }
        else
        {
			$result = array('status'=>'error','data'=>array('message'=>'No new data found'));
            $this->response($result, 200);
        }
    }
	
	 function section_get()
    {
        if(!$this->get('last_update'))
        {
        	//$this->response(NULL, 400);
			$last_update = false;
        }
		else
		{
			$last_update = $this->get('last_update');
		}

		if($this->get('q'))
		{
			$region_id =$this->get('q');
			$section_list = $this->mobile_model->get_section_list_by_region_id($last_update,$region_id);
		}
		else
		{
		  $section_list = $this->mobile_model->get_section_list($last_update);
		}
		
		if($section_list)
        {
			$result = array('status'=>'success','data'=>$section_list);
			
            $this->response($result, 200); // 200 being the HTTP response code
        }
        else
        {
			$result = array('status'=>'error','data'=>array('message'=>'No new data found'));
            $this->response($result, 200);
        }
    }
	
	function distribution_get()
    {
        if(!$this->get('last_update'))
        {
        	//$this->response(NULL, 400);
			$last_update = false;
        }
		else
		{
			$last_update = $this->get('last_update');
		}

		if($this->get('q'))
		{
			$section_id =$this->get('q');
			if($this->get('region_id'))
			{
				$region_id =$this->get('region_id');
				$distribution_list = $this->mobile_model->get_distribution_list_by_reg_and_section_id($last_update,$region_id,$section_id);
			}
			else
			{
			$distribution_list = $this->mobile_model->get_distribution_list_by_section_id($last_update,$section_id);
			}
		}
		else
		{
		     $distribution_list = $this->mobile_model->get_distribution_list($last_update);
		}
		
		if($distribution_list)
        {
			$result = array('status'=>'success','data'=>$distribution_list);
			
            $this->response($result, 200); // 200 being the HTTP response code
        }
        else
        {
			$result = array('status'=>'error','data'=>array('message'=>'No new data found'));
            $this->response($result, 200);
        }
    }
	
	function appliance_get()
    {
        if(!$this->get('last_update'))
        {
        	//$this->response(NULL, 400);
			$last_update = false;
        }
		else
		{
			$last_update = $this->get('last_update');
		}

		if($this->get('q'))
		{
			$section_id =$this->get('q');
			$appliance_list = $this->mobile_model->get_appliance_by_id($last_update,$section_id);
		}
		else
		{
		     $appliance_list = $this->mobile_model->get_appliance_list($last_update);
		}
		
		if($appliance_list)
        {
			$result = array('status'=>'success','data'=>$appliance_list);
			
            $this->response($result, 200); // 200 being the HTTP response code
        }
        else
        {
			$result = array('status'=>'error','data'=>array('message'=>'No new data found'));
            $this->response($result, 200);
        }
    }
	
	function user_history_get()
	{
		
			$service_no_ip = $this->input->get('consumer',TRUE);
			if (strlen($service_no_ip) < 7 || !($service_no_ip >= 0 || $service_no_ip < 0)) 
			{ 
				// check 7 coz that is the minimum length needed
				$error_result['status'] = "error";
				$error_result['data'] = "Input Service no. is not valid!";
				print_r(json_encode($error_result)); exit;	
    		} 
			
			if(!$this->get('last_update'))
			{
				//$this->response(NULL, 400);
				$last_update = false;
			}
			else
			{
				$last_update = $this->get('last_update');
			}
			
			//Check if data already exist in database
		/*	$connection_detail =  $this->mobile_model->is_connection_id_exist($service_no_ip);
		
			if($connection_detail)
			{
				print_r(json_encode($this->mobile_model->do_update_userdata($service_no_ip,$connection_detail,$last_update)));
			}
			else
			{
				print_r(json_encode($this->mobile_model->do_insertion_userdata($service_no_ip)));
			}
			*/
			$connection_detail = false;
		print_r(json_encode($this->mobile_model->do_update_userdata($service_no_ip,$connection_detail,$last_update)));

			exit;
	}

	// By Manish for getting data from bescom
	public function user_historyk_get()
	{
		print_r(json_encode($this->mobile_model->get_karnataka()));
		exit;
		
	}
	public function power_cut_get()
	{
		/*
		echo '[
{
time: {
start: {
time: "9.00 ",
ampm: "A.M."
},
end: {
time: "2.00 ",
ampm: "P.M."
}
},
date: "02-06-15",
area: [
{
name: "KOVOOR",
section_id: "304",
region_id: "9"
}
]
}
]';
exit;
*/
		$data = $this->mobile_model->get_power_cut();
		if($data)
		{
			$status ='success';
		}
		else
		{
			$status ='error';
		}
		print_r(json_encode(array('status'=>$status,'data'=>$data)));
			exit;
	}
	
	public function send_post()
	{
		var_dump($this->request->body);
	}


	public function send_put()
	{
		var_dump($this->put('foo'));
	}
}