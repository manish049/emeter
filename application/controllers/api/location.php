 <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Location extends CI_Controller {
	
		function __construct() {
			parent::__construct();
					
			//load custom library
			if(SITE_STATUS == 'offline')
			{
				redirect($this->general->lang_uri('offline'));exit;
			}
			$this->load->model('api/location_model');			
		}
		
		
	//Function executes when Add Friend button is clicked
	function update_and_get_location_post()
    {
		//INPUT:  day,month,year,gender,first_name,middle_name,last_name,country,street,city,profession_id
		//OUTPUT:  member_id
		$member_id=$this->input->post('id');
		$updated_info = $this->registration_model->update_and_get_location($member_id);
        if($updated_info['status'] == 'error')
		{
            $this->response($updated_info, 200);
		}
		else if($updated_info['status'] == 'success')
		{
			
			 $this->response($updated_info, 200); // 200 being the HTTP response code
		}
		else
		{
			$this->response(NULL, 400);
		}
    } 
		
	}
	
	/* End of file welcome.php */
	/* Location: ./application/controllers/welcome.php */