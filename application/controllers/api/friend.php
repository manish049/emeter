<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Friend extends CI_Controller {
	
		function __construct() {
			parent::__construct();
					
			//load custom library
			if(SITE_STATUS == 'offline')
			{
				redirect($this->general->lang_uri('offline'));exit;
			}
			//echo "ok"; exit;	
			$this->load->model('api/friend_model');			
		}
		
		
	//Function executes when Add Friend button is clicked
	function send_request($to_friend_id = NULL)
	{
		//START: Permission check NOTE: 9 = Friendship Module [defined in general]
		if(!$this->general->permission_check(9))
		{
			print_r(json_encode('no_permission'));
			//$this->general->redirect_permission();
			die();
		}
		print_r($this->friend_model->start_send_request($to_friend_id));
		
	
	}
	function cancel_request($to_friend_id = NULL)
	{
			//START: Permission check NOTE: 9 = Friendship Module [defined in general]
		if(!$this->general->permission_check(9))
		{
			print_r(json_encode('no_permission'));
			//$this->general->redirect_permission();
			die();
		}
		print_r($this->friend_model->cancel_friend_request($to_friend_id));
		
	
	}
	function accept_request($to_accept_id = NULL)
	{
		//START: Permission check NOTE: 9 = Friendship Module [defined in general]
		if(!$this->general->permission_check(9))
		{
			print_r(json_encode('no_permission'));
			//$this->general->redirect_permission();
			die();
		}
		print_r($this->friend_model->accept_request($to_accept_id));
	}
	function remove_friend($to_remove_friend_id = NULL)
	{
		//START: Permission check NOTE: 9 = Friendship Module [defined in general]
		if(!$this->general->permission_check(9))
		{
			print_r(json_encode('no_permission'));
			//$this->general->redirect_permission();
			die();
		}
		print_r($this->friend_model->remove_friend($to_remove_friend_id));
	}
	
	function friend_list($member_id = NULL)
	{
			$data['friends_list']= $this->friend_model->friends_list_by_id($member_id);
	$data['count_frens_list']= $this->friend_model->count_frens_list($member_id);
	
	$data['own_member_id']= $member_id;
	$this->template
			->set_layout('general')
			->enable_parser(FALSE)	
			->build('friends_list_view',$data);
	}

	
	
	function check_if_follow_or_not_by_id()
	{
		$fren_id = $this->input->post('frens_frens_id');
		print_r(json_encode($friends_list= $this->general->check_if_follow_or_not_by_id($fren_id)));
	}
	/*function other_friend_list($member_id = NULL)
	{
		echo "ok";
	$data['friends_list']= $this->friend_model->others_friends_list_by_id($member_id);
	print_r($data['friends_list']);
	die();
	$this->template
			->set_layout('general')
			->enable_parser(FALSE)	
			->build('other_friends_list_view',$data);
	}
	*/
	function other_friend_list($member_id = NULL)
	{
				
		$data['others_friends_list']= $this->friend_model->friends_list_by_id($member_id);
		$data['friends_member_id']= $member_id;
		$this->template
			->set_layout('general')
			->enable_parser(FALSE)	
			->build('other_friends_list_view',$data);
	}
			
		
}
	
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */