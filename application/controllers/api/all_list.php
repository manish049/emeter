<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Example
 *
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array.
 *
 * @package		CodeIgniter
 * @subpackage	Rest Server
 * @category	Controller
 * @author		Phil Sturgeon
 * @link		http://philsturgeon.co.uk/code/
*/

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH.'/libraries/REST_Controller.php';

class All_list extends REST_Controller
{
	function __construct()
    {
        // Construct our parent class
        parent::__construct();
        
        // Configure limits on our controller methods. Ensure
        // you have created the 'limits' table and enabled 'limits'
        // within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; //500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; //100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; //50 requests per hour per user/key
		$this->load->model('api/all_list_model');
    }
	    
     function all_list_api_get()
    {
        if(!$this->get('last_update'))
        {
        	//$this->response(NULL, 400);
			$last_update = false;
        }
		else
		{
			$last_update = $this->get('last_update');
		}
   			
		$region_list = $this->all_list_model->get_region_list($last_update);
		$section_list = $this->all_list_model->get_section_list($last_update);
		$distribution_list = $this->all_list_model->get_region_list($last_update);
			//	print_r($data); exit;

		if($region_list || $section_list || $distribution_list)
        {
			$result = array('status'=>'success','data'=>array('region_list'=>$region_list,'section_list'=>$section_list,'distribution_list'=>$distribution_list));
			
            $this->response($result, 200); // 200 being the HTTP response code
        }

        else
        {
			$result = array('status'=>'error','data'=>array('message'=>'No new data found'));
            $this->response($result, 200);
        }
    }
    
	
	public function send_post()
	{
		var_dump($this->request->body);
	}


	public function send_put()
	{
		var_dump($this->put('foo'));
	}
}