<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Controller to store the data read from 
 */
class FileReader extends CI_Controller
{
	function __construct()
    {
        // Construct our parent class
        parent::__construct();
       
    }

    public function read_bu()
    {
        header('Content-Type: application/json');
        $file_name = './BU_List.txt';
        // $bu_list = file_get_contents($file_name);
        // $bu_exploded = explode('', $bu_list);
        // print_r(json_encode($bu_exploded));

        $handle = fopen($file_name, "r");
        if ($handle) 
        {
            $final_array = array();
            while (($line = fgets($handle)) !== false) 
            {
                $bu_n_code = explode(',', $line);
                $test_array['code'] = trim($bu_n_code[0]);
                $test_array['circle'] = trim($bu_n_code[1]);
                array_push($final_array, $test_array);
            }

            fclose($handle);
            $this->insert_data('bu', $final_array);

            print_r(json_encode($final_array));
        } 
        else 
        {
            // error opening the file.
        } 
    }
    public function read_circle()
    {
         header('Content-Type: application/json');
        $file_name = './Circle_List.txt';
        // $bu_list = file_get_contents($file_name);
        // $bu_exploded = explode('', $bu_list);
        // print_r(json_encode($bu_exploded));

        $handle = fopen($file_name, "r");
        if ($handle) 
        {
            $final_array = array();
            while (($line = fgets($handle)) !== false) 
            {
                $bu_n_code = explode(',', $line);
                $test_array['code'] = trim($bu_n_code[0]);
                $test_array['circle'] = trim($bu_n_code[1]);
                array_push($final_array, $test_array);
            }

            fclose($handle);
            $this->insert_data('circle', $final_array);
            print_r(json_encode($final_array));
        } 
        else 
        {
            // error opening the file.
        } 
    }

    // insert data managed from file in corresponding table
    public function insert_data($bu_or_circle, $data)
    {
        $table_name = ($bu_or_circle == 'bu')? 'maharashtra_bu' : 'maharashtra_circle';
        $this->db->empty_table($table_name);
        $this->db->insert_batch($table_name, $data);
    }

    public function file_reader()
    {
        $billing_unit = './BU_List.txt';
        $circles = './Circle_List.txt';
        $handler = fopen($billing_unit, 'r');
        if($handler) {
            $final_array = array();
            while(($line = fgets($handler)) != FALSE) {
                $bu_row = explode(',', $line);
                array_push($final_array, array('code' => $bu_row[0], 'circle' => $bu_row[1]));
            }
            $this->insert_data('bu', $final_array);
            // print_r(json_encode($final_array));
        } else {
            echo 'Unable to handle file: BU_List.txt';
        }

        $handler = fopen($circles, 'r');
        if($handler) {
            $final_array = array();
            while(($line = fgets($handler)) != FALSE) {
                $bu_row = explode(',', $line);
                array_push($final_array, array('code' => $bu_row[0], 'circle' => $bu_row[1]));
            }
            $this->insert_data('circle', $final_array);
            // print_r(json_encode($final_array));
        } else {
            echo 'Unable to handle file: Circle_List.txt';
        }

    }
}